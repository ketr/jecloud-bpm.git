/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring;

import com.je.bpm.engine.impl.cfg.TransactionContext;
import com.je.bpm.engine.impl.cfg.TransactionListener;
import com.je.bpm.engine.impl.cfg.TransactionState;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import org.springframework.core.Ordered;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

public class SpringTransactionContext implements TransactionContext {

    protected PlatformTransactionManager transactionManager;
    protected CommandContext commandContext;
    protected Integer transactionSynchronizationAdapterOrder;

    public SpringTransactionContext(PlatformTransactionManager transactionManager, CommandContext commandContext) {
        this(transactionManager, commandContext, null);
    }

    public SpringTransactionContext(PlatformTransactionManager transactionManager, CommandContext commandContext, Integer transactionSynchronizationAdapterOrder) {
        this.transactionManager = transactionManager;
        this.commandContext = commandContext;
        if (transactionSynchronizationAdapterOrder != null) {
            this.transactionSynchronizationAdapterOrder = transactionSynchronizationAdapterOrder;
        } else {
            // Revert to default, which is a high number as the behaviour prior
            // to adding the order would
            // case the TransactionSynchronizationAdapter to be called AFTER all
            // Adapters that implement Ordered
            this.transactionSynchronizationAdapterOrder = Integer.MAX_VALUE;
        }
    }

    @Override
    public void commit() {
        // Do nothing, transaction is managed by spring
    }

    @Override
    public void rollback() {
        // Just in case the rollback isn't triggered by an
        // exception, we mark the current transaction rollBackOnly.
        transactionManager.getTransaction(null).setRollbackOnly();
    }

    @Override
    public void addTransactionListener(final TransactionState transactionState, final TransactionListener transactionListener) {
        if (transactionState.equals(TransactionState.COMMITTING)) {
            TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
                @Override
                public void beforeCommit(boolean readOnly) {
                    transactionListener.execute(commandContext);
                }
            });
        } else if (transactionState.equals(TransactionState.COMMITTED)) {
            TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
                @Override
                public void afterCommit() {
                    transactionListener.execute(commandContext);
                }
            });
        } else if (transactionState.equals(TransactionState.ROLLINGBACK)) {
            TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
                @Override
                public void beforeCompletion() {
                    transactionListener.execute(commandContext);
                }
            });
        } else if (transactionState.equals(TransactionState.ROLLED_BACK)) {
            TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
                @Override
                public void afterCompletion(int status) {
                    if (TransactionSynchronization.STATUS_ROLLED_BACK == status) {
                        transactionListener.execute(commandContext);
                    }
                }
            });
        }
    }

    protected abstract class TransactionSynchronizationAdapter implements TransactionSynchronization, Ordered {

        @Override
        public void suspend() {
        }

        @Override
        public void resume() {
        }

        @Override
        public void flush() {
        }

        @Override
        public void beforeCommit(boolean readOnly) {
        }

        @Override
        public void beforeCompletion() {
        }

        @Override
        public void afterCommit() {
        }

        @Override
        public void afterCompletion(int status) {
        }

        @Override
        public int getOrder() {
            return transactionSynchronizationAdapterOrder;
        }

    }

}
