/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring.application;

import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ApplicationReader {

    private List<ApplicationEntryDiscovery> applicationEntryDiscoveries;

    public ApplicationReader(List<ApplicationEntryDiscovery> applicationEntryDiscoveries) {
        this.applicationEntryDiscoveries = applicationEntryDiscoveries;
    }

    public ApplicationContent read(InputStream inputStream) {
        ApplicationContent application = new ApplicationContent();
        try (ZipInputStream zipInputStream = new ZipInputStream(inputStream)) {
            ZipEntry zipEntry;
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                ZipEntry currentEntry = zipEntry;
                applicationEntryDiscoveries.stream()
                        .filter(applicationEntryDiscovery -> applicationEntryDiscovery.filter(currentEntry).test(currentEntry))
                        .findFirst()
                        .ifPresent(
                                applicationEntryDiscovery ->
                                        application.add(new ApplicationEntry(applicationEntryDiscovery.getEntryType(),
                                                new FileContent(currentEntry.getName(),
                                                        readBytes(zipInputStream
                                                        )))));
            }
        } catch (IOException e) {
            throw new ApplicationLoadException("Unable to read zip file", e);
        }
        return application;
    }

    private byte[] readBytes(ZipInputStream zipInputStream) {
        try {
            return StreamUtils.copyToByteArray(zipInputStream);
        } catch (IOException e) {
            throw new ApplicationLoadException("Unable to read zip file", e);
        }
    }
}
