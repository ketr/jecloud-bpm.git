/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring.process.conf;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.je.bpm.common.util.DateFormatterProvider;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.spring.process.ProcessExtensionResourceReader;
import com.je.bpm.spring.process.ProcessExtensionService;
import com.je.bpm.spring.process.model.ProcessExtensionModel;
import com.je.bpm.spring.process.variable.VariableParsingService;
import com.je.bpm.spring.process.variable.VariableValidationService;
import com.je.bpm.spring.process.variable.types.DateVariableType;
import com.je.bpm.spring.process.variable.types.JavaObjectVariableType;
import com.je.bpm.spring.process.variable.types.JsonObjectVariableType;
import com.je.bpm.spring.process.variable.types.VariableType;
import com.je.bpm.spring.resources.DeploymentResourceLoader;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class ProcessExtensionsAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public DeploymentResourceLoader<ProcessExtensionModel> deploymentResourceLoader() {
        return new DeploymentResourceLoader<>();
    }

    @Bean
    @ConditionalOnMissingBean
    public ProcessExtensionResourceReader processExtensionResourceReader(ObjectMapper objectMapper, Map<String, VariableType> variableTypeMap) {
        return new ProcessExtensionResourceReader(objectMapper, variableTypeMap);
    }

    @Bean
    @ConditionalOnMissingBean
    public ProcessExtensionService processExtensionService(ProcessExtensionResourceReader processExtensionResourceReader, DeploymentResourceLoader<ProcessExtensionModel> deploymentResourceLoader) {
        return new ProcessExtensionService(deploymentResourceLoader, processExtensionResourceReader);
    }

    @Bean
    InitializingBean initRepositoryServiceForProcessExtensionService(RepositoryService repositoryService, ProcessExtensionService processExtensionService) {
        return () -> processExtensionService.setRepositoryService(repositoryService);
    }

    @Bean
    InitializingBean initRepositoryServiceForDeploymentResourceLoader(RepositoryService repositoryService, DeploymentResourceLoader deploymentResourceLoader) {
        return () -> deploymentResourceLoader.setRepositoryService(repositoryService);
    }

    @Bean
    @ConditionalOnMissingBean(name = "variableTypeMap")
    public Map<String, VariableType> variableTypeMap(ObjectMapper objectMapper, DateFormatterProvider dateFormatterProvider) {
        Map<String, VariableType> variableTypeMap = new HashMap<>();
        variableTypeMap.put("boolean", new JavaObjectVariableType(Boolean.class));
        variableTypeMap.put("string", new JavaObjectVariableType(String.class));
        variableTypeMap.put("integer", new JavaObjectVariableType(Integer.class));
        variableTypeMap.put("json", new JsonObjectVariableType(objectMapper));
        variableTypeMap.put("file", new JsonObjectVariableType(objectMapper));
        variableTypeMap.put("folder", new JsonObjectVariableType(objectMapper));
        variableTypeMap.put("date", new DateVariableType(Date.class, dateFormatterProvider));
        variableTypeMap.put("datetime", new DateVariableType(Date.class, dateFormatterProvider));
        variableTypeMap.put("array", new JsonObjectVariableType(objectMapper));
        return variableTypeMap;
    }

    @Bean
    public VariableValidationService variableValidationService(Map<String, VariableType> variableTypeMap) {
        return new VariableValidationService(variableTypeMap);
    }

    @Bean
    public VariableParsingService variableParsingService(Map<String, VariableType> variableTypeMap) {
        return new VariableParsingService(variableTypeMap);
    }
}
