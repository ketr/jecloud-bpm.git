/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring.process;

import com.je.bpm.spring.resources.ResourceFinderDescriptor;
import org.springframework.core.io.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 流程扩展资源查找描述
 */
public class ProcessExtensionResourceFinderDescriptor implements ResourceFinderDescriptor {

    private boolean checkResources;
    private String locationPrefix;
    private List<String> locationSuffixes;

    public ProcessExtensionResourceFinderDescriptor(boolean checkResources, String locationPrefix, String locationSuffix) {
        this.checkResources = checkResources;
        this.locationPrefix = locationPrefix;
        locationSuffixes = new ArrayList<>();
        locationSuffixes.add(locationSuffix);
    }

    @Override
    public List<String> getLocationSuffixes() {
        return locationSuffixes;
    }

    @Override
    public String getLocationPrefix() {
        return locationPrefix;
    }

    @Override
    public boolean shouldLookUpResources() {
        return checkResources;
    }

    @Override
    public String getMsgForEmptyResources() {
        return "No process extensions were found for auto-deployment in the location '" + locationPrefix + "'";
    }

    @Override
    public String getMsgForResourcesFound(List<String> processExtensionFiles) {
        return "The following process extension files will be deployed: " + processExtensionFiles;
    }

    @Override
    public void validate(List<Resource> resources) {

    }

}
