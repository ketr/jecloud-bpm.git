/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring.process;

import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.repository.ProcessDefinition;
import com.je.bpm.spring.process.model.Extension;
import com.je.bpm.spring.process.model.ProcessExtensionModel;
import com.je.bpm.spring.resources.DeploymentResourceLoader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProcessExtensionService {

    private DeploymentResourceLoader<ProcessExtensionModel> processExtensionLoader;
    private ProcessExtensionResourceReader processExtensionReader;
    private RepositoryService repositoryService;
    private static final Extension EMPTY_EXTENSIONS = new Extension();
    private Map<String, Map<String, Extension>> processExtensionModelDeploymentMap = new HashMap<>();

    public ProcessExtensionService(DeploymentResourceLoader<ProcessExtensionModel> processExtensionLoader, ProcessExtensionResourceReader processExtensionReader) {
        this.processExtensionLoader = processExtensionLoader;
        this.processExtensionReader = processExtensionReader;
    }

    private Map<String, Extension> getProcessExtensionsForDeploymentId(String deploymentId) {
        Map<String, Extension> processExtensionModelMap = processExtensionModelDeploymentMap.get(deploymentId);
        if (processExtensionModelMap != null) {
            return processExtensionModelMap;
        }
        List<ProcessExtensionModel> processExtensionModels = processExtensionLoader.loadResourcesForDeployment(deploymentId, processExtensionReader);
        processExtensionModelMap = buildProcessDefinitionAndExtensionMap(processExtensionModels);
        processExtensionModelDeploymentMap.put(deploymentId, processExtensionModelMap);
        return processExtensionModelMap;
    }

    private Map<String, Extension> buildProcessDefinitionAndExtensionMap(List<ProcessExtensionModel> processExtensionModels) {
        Map<String, Extension> buildProcessExtensionMap = new HashMap<>();
        for (ProcessExtensionModel processExtensionModel : processExtensionModels) {
            buildProcessExtensionMap.putAll(processExtensionModel.getAllExtensions());
        }
        return buildProcessExtensionMap;
    }

    public boolean hasExtensionsFor(ProcessDefinition processDefinition) {
        return !EMPTY_EXTENSIONS.equals(getExtensionsFor(processDefinition));
    }

    public boolean hasExtensionsFor(String processDefinitionId) {
        ProcessDefinition processDefinition = repositoryService.getProcessDefinition(processDefinitionId);
        return hasExtensionsFor(processDefinition);
    }

    public Extension getExtensionsFor(ProcessDefinition processDefinition) {
        Map<String, Extension> processExtensionModelMap = getProcessExtensionsForDeploymentId(processDefinition.getDeploymentId());
        Extension extension = processExtensionModelMap.get(processDefinition.getKey());
        return extension != null ? extension : EMPTY_EXTENSIONS;
    }

    public Extension getExtensionsForId(String processDefinitionId) {
        ProcessDefinition processDefinition = repositoryService.getProcessDefinition(processDefinitionId);
        Extension processExtension = getExtensionsFor(processDefinition);
        return processExtension != null ? processExtension : EMPTY_EXTENSIONS;
    }

    public void setRepositoryService(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }
}
