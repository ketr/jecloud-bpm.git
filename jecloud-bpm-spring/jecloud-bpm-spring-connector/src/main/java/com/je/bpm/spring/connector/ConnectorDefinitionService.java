/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring.connector;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.je.bpm.common.connector.model.ConnectorDefinition;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class ConnectorDefinitionService {

    private String connectorRoot;

    private final ObjectMapper objectMapper;

    private ResourcePatternResolver resourceLoader;

    public ConnectorDefinitionService(String connectorRoot, ObjectMapper objectMapper, ResourcePatternResolver resourceLoader) {
        this.connectorRoot = connectorRoot;
        this.objectMapper = objectMapper;
        this.resourceLoader = resourceLoader;
    }

    private Optional<Resource[]> retrieveResources() throws IOException {
        Optional<Resource[]> resources = Optional.empty();
        Resource connectorRootPath = resourceLoader.getResource(connectorRoot);
        if (connectorRootPath.exists()) {
            return Optional.ofNullable(resourceLoader.getResources(connectorRoot + "**.json"));
        }
        return resources;
    }

    private ConnectorDefinition read(InputStream inputStream) throws IOException {
        return objectMapper.readValue(inputStream, ConnectorDefinition.class);
    }

    public List<ConnectorDefinition> get() throws IOException {
        List<ConnectorDefinition> connectorDefinitions = new ArrayList<>();
        Optional<Resource[]> resourcesOptional = retrieveResources();
        if (resourcesOptional.isPresent()) {
            for (Resource resource : resourcesOptional.get()) {
                connectorDefinitions.add(read(resource.getInputStream()));
            }
            validate(connectorDefinitions);
        }
        return connectorDefinitions;
    }

    protected void validate(List<ConnectorDefinition> connectorDefinitions) {
        if (!connectorDefinitions.isEmpty()) {
            Set<String> processedNames = new HashSet<>();
            for (ConnectorDefinition connectorDefinition : connectorDefinitions) {
                String name = connectorDefinition.getName();
                if (name == null || name.isEmpty()) {
                    throw new IllegalStateException("connectorDefinition name cannot be null or empty");
                }

                if (name.contains(".")) {
                    throw new IllegalStateException("connectorDefinition name cannot have '.' character");
                }

                if (!processedNames.add(name)) {
                    throw new IllegalStateException("More than one connectorDefinition with name '" + name + "' was found. Names must be unique.");
                }
            }
        }
    }
}
