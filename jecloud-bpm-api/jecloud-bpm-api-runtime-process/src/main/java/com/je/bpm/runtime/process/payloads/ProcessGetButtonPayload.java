/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.runtime.process.payloads;

import com.je.bpm.model.shared.Payload;

import java.util.Map;
import java.util.UUID;

/**
 * 获取流程初始按钮
 */
public class ProcessGetButtonPayload implements Payload {

    private static final long serialVersionUID = 1L;

    /**
     * 唯一标识
     */
    private String id;
    /**
     * 流程绑定的funcCode
     */
    private String funcCode;
    /**
     * 数据创建人userId
     */
    private String userId;
    /**
     * beanId
     */
    private String beanId;
    /**
     * bean
     */
    private Map<String, Object> bean;

    private String prod;

    public ProcessGetButtonPayload() {
        this.id = UUID.randomUUID().toString();
    }

    public ProcessGetButtonPayload(String funcCode) {
        this();
        this.funcCode = funcCode;
    }

    public ProcessGetButtonPayload(String funcCode, String userId, String beanId, Map<String, Object> bean) {
        this();
        this.funcCode = funcCode;
        this.userId = userId;
        this.beanId = beanId;
        this.bean = bean;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public ProcessGetButtonPayload setFuncCode(String funcCode) {
        this.funcCode = funcCode;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public ProcessGetButtonPayload setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public static ProcessGetButtonPayload build() {
        return new ProcessGetButtonPayload();
    }

    public String getBeanId() {
        return beanId;
    }

    public ProcessGetButtonPayload setBeanId(String beanId) {
        this.beanId = beanId;
        return this;
    }

    public Map<String, Object> getBean() {
        return bean;
    }

    public ProcessGetButtonPayload setBean(Map<String, Object> bean) {
        this.bean = bean;
        return this;
    }

    public String getProd() {
        return prod;
    }

    public ProcessGetButtonPayload setProd(String prod) {
        this.prod = prod;
        return this;
    }
}
