/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.shared.model;

public interface WorkFlowConfig extends ApplicationElement {

    public enum WorkFlowAudFlag {
        NOSTATUS("未启动"),
        WAIT("审批中"),
        HANDUPED("已挂起"),
        SUSPEND("已作废"),
        ENDED("审批结束");

        private String name;

        WorkFlowAudFlag(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

    }

    public String getExigency();

    public void setExigency(String exigency);

    public String getEnableSimpleComments();

    public void setEnableSimpleComments(String enableSimpleComments);

    public String getSimpleApproval();

    public void setSimpleApproval(String simpleApproval);

    public String getSelectAll();

    public void setSelectAll(String selectAll);

    public String getAsynTree();

    public void setAsynTree(String asynTree);

    public String getCurrentNodeId();

    public void setCurrentNodeId(String currentNodeId);

    public String getCurrentNodeName();

    public void setCurrentNodeName(String currentNodeName);

    public String getCurrentTarget();

    public void setCurrentTarget(String currentTarget);

    public WorkFlowAudFlag getAudFlag();

    public void setAudFlag(WorkFlowAudFlag audFlag);

    public String getSequential();

    public void setSequential(String sequential);

    public String getType();

    public void setType(String type);

    public String getMultiple();

    public void setMultiple(String multiple);

    public String getShowSequentialConfig();

    public void setShowSequentialConfig(String showSequentialConfig);

    public String getListSynchronization();

    public void setListSynchronization(String listSynchronization);

    public String getPersonnelAdjustments();

    public void setPersonnelAdjustments(String personnelAdjustments);

    public String getExigencyValue();

    public void setExigencyValue(String exigencyValue);

}
