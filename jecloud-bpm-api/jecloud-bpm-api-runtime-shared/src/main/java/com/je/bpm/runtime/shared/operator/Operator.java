/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.runtime.shared.operator;

import com.je.bpm.model.shared.Payload;
import com.je.bpm.model.shared.Result;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.PayloadValidErrorException;

import java.util.Map;

/**
 * 操作定义
 */
public interface Operator<BUTTON_PARAMS_PAYLOAD extends Payload, BUTTON_RESULT_ENTITY extends Result> {

    /**
     * 获取操作标识
     *
     * @return
     */
    String getId();

    /**
     * 获取操作名称
     *
     * @return
     */
    String getName();

    /**
     * 获取操作参数
     *
     * @return
     */
    OperationParamDesc getParamDesc();

    /**
     * 操作动作
     *
     * @param params 消息载体
     * @return 返回实体
     */
    BUTTON_RESULT_ENTITY operate(Map<String, Object> params) throws PayloadValidErrorException;

    /**
     * 操作动作
     *
     * @param payload 消息载体
     * @return 返回实体
     */
    BUTTON_RESULT_ENTITY operate(BUTTON_PARAMS_PAYLOAD payload) throws PayloadValidErrorException;

    /**
     * 转换参数
     *
     * @param params
     * @return payload
     */
    BUTTON_PARAMS_PAYLOAD paramsParse(Map<String, Object> params);

    default String formatString(Object value) {
        return value == null ? null : value.toString();
    }

    default Map<String, Object> formatMap(Object value) {
        return value == null ? null : (Map<String, Object>) value;
    }

    default Integer formatInteger(Object value) {
        return value == null ? null : Integer.valueOf(value.toString());
    }

    default Boolean formatBoolean(Object value) {
        return value == null ? null : Boolean.valueOf(value.toString());
    }
}
