/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.runtime.shared.identity;

import com.je.bpm.common.identity.ActivitiUser;
import com.je.bpm.common.identity.AllUserInfo;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 用户管理服务
 */
public interface UserManager  extends BaseUserManager{

    /**
     * 根据用户ID查询用户
     * @param userId 用户ID
     * @return
     */
    ActivitiUser findUser(String userId);

    /**
     * 根据用户ID集合获取所有用户
     * @param userIds
     * @return
     */
    List<ActivitiUser> findUsers(Collection<String> userIds);

    /**
     * 获取部门用户
     * @param departmentId
     * @return
     */
    List<ActivitiUser> findUsersByDepartmentId(String departmentId);

    /**
     * 获取角色用户
     * @param roleId
     * @return
     */
    List<ActivitiUser> findUsersByRoleId(String roleId);

    /**
     * 获取岗位用户
     * @param positionId
     * @return
     */
    List<ActivitiUser> findUsersByPositionId(String positionId);

    /**
     * 获取工作组用户
     * @param workGroupId
     * @param type
     * @return
     */
    List<ActivitiUser> findUsersByWorkGroupId(String workGroupId,String type);

    /**
     * 获取所有类型的用户
     * @param departmentId 部门ID
     * @param roleId
     * @param positionId
     * @param userIdList
     * @return
     */
    AllUserInfo findUsersByTyped(String departmentId, String roleId, String positionId, List<String> userIdList);

    /**
     * 获取当前登录人
     * @return
     */
    ActivitiUser findCurrentUser();

    /**
     * 获取直属领导
     * @param userId
     * @return
     */
    ActivitiUser findDirectLeader(String userId);

    /**
     * 获取直属领导
     * @param userId
     * @return
     */
    ActivitiUser findDeptLeader(String userId);

    /**
     * 获取用户所在部门监管领导
     * @param userId
     * @return
     */
    List<ActivitiUser> findDeptMonitorLeader(String userId);

    /**
     * 获取用户所在部门负责人
     * @param userId
     * @return
     */
    ActivitiUser findDeptHeader(String userId);

    /**
     * 获取用户所在部门领导和监管领导
     * @param userId
     * @return
     */
    List<ActivitiUser> findDeptLeaderAndMonitorLeader(String userId);

    /**
     * 获取用户所在部门领导（含监管+直属）
     * @param userId
     * @return
     */
    List<ActivitiUser> findDeptLeaderAndDirectLeaderAndMonitorLeader(String userId);

    /**
     * 获取流程启动人
     * @param processInstanceId
     * @return
     */
    ActivitiUser findStartUser(String processInstanceId);

    /**
     * 获取前置任务处理人
     * @param processInstance
     * @param taskId
     * @return
     */
    ActivitiUser findPreTaskAssignee(String processInstance,String taskId);

    /**
     * 湖区前置任务处理人领导
     * @param processInstance
     * @param taskId
     * @return
     */
    ActivitiUser findPreTaskAssigneeLeader(String processInstance,String taskId);

    /**
     * 查询部门人员
     * @param userId
     * @param isContainQueryUserId
     * @return
     */
    List<ActivitiUser> findDeptUsers(String userId,boolean isContainQueryUserId);

    /**
     * 查询部门（包括子部门）人员
     * @param userId
     * @param isContainQueryUserId
     * @return
     */
    List<ActivitiUser> findDeptAllUsers(String userId,boolean isContainQueryUserId);

    /**
     * 查询部门（包括子部门+监管部门）人员
     * @param userId
     * @param isContainQueryUserId
     * @return
     */
    List<ActivitiUser> findDeptAllAndMonitorDeptUsers(String userId,boolean isContainQueryUserId);

    Object findUsers(List<String> userIds, Boolean showCompany,Boolean multiple,Boolean addOwn);
}
