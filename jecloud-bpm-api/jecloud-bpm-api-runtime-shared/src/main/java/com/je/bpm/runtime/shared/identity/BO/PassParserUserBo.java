/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.runtime.shared.identity.BO;

import com.je.bpm.core.model.config.task.PassRoundResource;

import java.util.List;
import java.util.Map;

public class PassParserUserBo {
    String nodeId;
    String nodeName;
    List<PassRoundResource> list;
    String userId;
    Map<String, Object> bean;
    String prod;
    String startUserId;

    private PassParserUserBo(Builder builder) {
        this.nodeId = builder.nodeId;
        this.nodeName = builder.nodeName;
        this.list = builder.list;
        this.userId = builder.userId;
        this.bean = builder.bean;
        this.prod = builder.prod;
        this.startUserId = builder.startUserId;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        String startUserId;
        String nodeId;
        String nodeName;
        List<PassRoundResource> list;
        String userId;
        Map<String, Object> bean;
        String prod;

        public Builder setNodeId(String nodeId) {
            this.nodeId = nodeId;
            return this;
        }

        public Builder setNodeName(String nodeName) {
            this.nodeName = nodeName;
            return this;
        }

        public Builder setList(List<PassRoundResource> list) {
            this.list = list;
            return this;
        }

        public Builder setUserId(String userId) {
            this.userId = userId;
            return this;
        }

        public Builder setBean(Map<String, Object> bean) {
            this.bean = bean;
            return this;
        }

        public Builder setProd(String prod) {
            this.prod = prod;
            return this;
        }

        public Builder setStartUserId(String startUserId) {
            this.startUserId = startUserId;
            return this;
        }


        public PassParserUserBo build() {
            return new PassParserUserBo(this);
        }
    }


    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public List<PassRoundResource> getList() {
        return list;
    }

    public void setList(List<PassRoundResource> list) {
        this.list = list;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Map<String, Object> getBean() {
        return bean;
    }

    public void setBean(Map<String, Object> bean) {
        this.bean = bean;
    }

    public String getProd() {
        return prod;
    }

    public void setProd(String prod) {
        this.prod = prod;
    }

    public String getStartUserId() {
        return startUserId;
    }

    public void setStartUserId(String startUserId) {
        this.startUserId = startUserId;
    }
}

