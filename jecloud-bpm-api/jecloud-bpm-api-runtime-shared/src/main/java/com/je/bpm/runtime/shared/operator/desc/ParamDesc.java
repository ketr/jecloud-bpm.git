/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.runtime.shared.operator.desc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 按钮参数
 */
public class ParamDesc implements Serializable {

    /**
     * 参数名称
     */
    private String name;
    /**
     * 参数类型
     */
    private ParamType type;
    /**
     * 参数描述
     */
    private String description;
    /**
     * 是否必填
     */
    private boolean required;
    /**
     * 子参数
     */
    private List<ParamDesc> childParams;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ParamType getType() {
        return type;
    }

    public void setType(ParamType type) {
        this.type = type;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ParamDesc> getChildParams() {
        return childParams;
    }

    public void setChildParams(List<ParamDesc> childParams) {
        this.childParams = childParams;
    }

    public void addChildParam(ParamDesc child){
        if(this.childParams == null || this.childParams.isEmpty()){
            this.childParams = new ArrayList<>();
        }
        this.childParams.add(child);
    }

    public void removeChildParam(ParamDesc child){
        if(this.childParams == null){
            return;
        }
        this.childParams.remove(child);
    }

    @Override
    public String toString() {
        return "ParamDesc{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", description='" + description + '\'' +
                ", required=" + required +
                ", childParams=" + childParams +
                '}';
    }
}
