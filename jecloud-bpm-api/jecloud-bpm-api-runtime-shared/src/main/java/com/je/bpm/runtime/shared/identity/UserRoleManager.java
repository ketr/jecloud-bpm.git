/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.runtime.shared.identity;

import com.je.bpm.common.identity.ActivitiRole;
import com.je.bpm.runtime.shared.identity.BO.AssignmentPermissionBo;

import java.util.Collection;
import java.util.List;

/**
 * 用户角色管理
 */
public interface UserRoleManager extends BaseUserManager {

    /**
     * 根据用户编码获取用户角色列表
     *
     * @param userId
     * @return
     */
    List<ActivitiRole> findUserRoles(String userId);

    Boolean logUserRoleIsInRoleIds(String userId, String roleIds);

    /**
     * 获取角色树
     *
     * @param userId                 用户id
     * @param assignmentPermissionBo 权限
     * @param roleIds                角色ids
     * @param showCompany            是否展示公司
     * @return
     */
    Object findUserRoles(String userId, String roleIds, AssignmentPermissionBo assignmentPermissionBo, Boolean showCompany, Boolean multiple,Boolean addOwn);

    /**
     * 根据角色ID获取角色
     *
     * @param roleId
     * @return
     */
    ActivitiRole findRole(String roleId);

    /**
     * 根据角色ID集合获取所有角色
     *
     * @param roleIds
     * @return
     */
    List<ActivitiRole> findRoles(Collection<String> roleIds);

    /**
     *
     * @param userId                    用户id
     * @param roleIds                   角色ids
     * @param assignmentPermissionBo    权限
     * @return
     */
    Boolean checkContainsCurrentUser(String userId, String roleIds, AssignmentPermissionBo assignmentPermissionBo);
}
