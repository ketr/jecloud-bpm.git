/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.payloads;

import com.je.bpm.model.shared.Payload;
import java.util.List;
import java.util.UUID;

/**
 * 发起消息
 */
public class ProcessMessageSponsorPayload implements Payload {

    private static final long serialVersionUID = 1L;

    /**
     * 唯一标识
     */
    private String id;
    /**
     * 产品标识
     */
    private String prod;
    /**
     * 流程定义ID
     */
    private String messageRef;
    private String beanId;
    /**
     * 下一个节点处理人
     */
    private String assignee;

    public ProcessMessageSponsorPayload() {
        this.id = UUID.randomUUID().toString();
    }

    public ProcessMessageSponsorPayload(String prod, String messageRef, String beanId, String assignee) {
        this();
        this.prod = prod;
        this.messageRef = messageRef;
        this.beanId = beanId;
        this.assignee = assignee;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getProd() {
        return prod;
    }

    public ProcessMessageSponsorPayload setProd(String prod) {
        this.prod = prod;
        return this;
    }

    public String getMessageRef() {
        return messageRef;
    }

    public ProcessMessageSponsorPayload setMessageRef(String messageRef) {
        this.messageRef = messageRef;
        return this;
    }

    public String getBeanId() {
        return beanId;
    }

    public ProcessMessageSponsorPayload setBeanId(String beanId) {
        this.beanId = beanId;
        return this;
    }

    public String getAssignee() {
        return assignee;
    }

    public ProcessMessageSponsorPayload setAssignee(String assignee) {
        this.assignee = assignee;
        return this;
    }

    public static ProcessMessageSponsorPayload build(){
        return new ProcessMessageSponsorPayload();
    }

}
