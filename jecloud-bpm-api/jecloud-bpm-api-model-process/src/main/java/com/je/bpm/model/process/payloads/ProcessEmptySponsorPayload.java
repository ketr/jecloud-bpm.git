/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.payloads;

import com.je.bpm.model.shared.Payload;

import java.util.Map;
import java.util.UUID;

/**
 * 发起流程
 */
public class ProcessEmptySponsorPayload implements Payload {

    /**
     * 唯一标识
     */
    private String id;
    /**
     * 产品标识
     */
    private String prod;
    /**
     * 流程定义标识
     */
    private String processDefinitionId;
    private String beanId;
    /**
     * 下一个节点处理人
     */
    private String assignee;
    /**
     * bean信息
     */
    Map<String, Object> bean;

    String secondTaskRef;

    String tableCode;
    String comment;
    /**
     * 手动指定顺序或者并须执行
     */
    private String sequentials;
    /**
     * 是否是跳跃
     */
    private String isJump;
    private String files;

    public ProcessEmptySponsorPayload() {
        this.id = UUID.randomUUID().toString();
    }

    public ProcessEmptySponsorPayload(String prod, String processDefinitionId, String beanId, Map<String, Object> bean,
                                      String assignee, String secondTaskRef, String isJump, String files) {
        this();
        this.prod = prod;
        this.processDefinitionId = processDefinitionId;
        this.beanId = beanId;
        this.assignee = assignee;
        this.secondTaskRef = secondTaskRef;
        this.bean = bean;
        this.isJump = isJump;
        this.files = files;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getProd() {
        return prod;
    }

    public ProcessEmptySponsorPayload setProd(String prod) {
        this.prod = prod;
        return this;
    }

    public String getProcessDefinitionKey() {
        return processDefinitionId;
    }

    public ProcessEmptySponsorPayload setProcessDefinitionKey(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
        return this;
    }

    public String getBeanId() {
        return beanId;
    }

    public ProcessEmptySponsorPayload setBeanId(String beanId) {
        this.beanId = beanId;
        return this;
    }

    public String getAssignee() {
        return assignee;
    }

    public ProcessEmptySponsorPayload setAssignee(String assignee) {
        this.assignee = assignee;
        return this;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public ProcessEmptySponsorPayload setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
        return this;
    }

    public Map<String, Object> getBean() {
        return bean;
    }

    public ProcessEmptySponsorPayload setBean(Map<String, Object> bean) {
        this.bean = bean;
        return this;
    }

    public String getSecondTaskRef() {
        return secondTaskRef;
    }

    public void setSecondTaskRef(String secondTaskRef) {
        this.secondTaskRef = secondTaskRef;
    }

    public static ProcessEmptySponsorPayload build() {
        return new ProcessEmptySponsorPayload();
    }

    public String getTableCode() {
        return tableCode;
    }

    public void setTableCode(String tableCode) {
        this.tableCode = tableCode;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSequentials() {
        return sequentials;
    }

    public void setSequentials(String sequentials) {
        this.sequentials = sequentials;
    }

    public String getIsJump() {
        return isJump;
    }

    public void setIsJump(String isJump) {
        this.isJump = isJump;
    }

    public String getFiles() {
        return files;
    }

    public void setFiles(String files) {
        this.files = files;
    }
}
