/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.payloads;

import com.google.common.base.Strings;
import com.je.bpm.model.shared.Payload;

import java.util.UUID;

/**
 * 保存模型
 */
public class SaveOrUpdateProcessModelPayload implements Payload {


    /**
     * 唯一标识
     */
    private String id;
    /**
     * 模型ID
     */
    private String modelId;
    /**
     * 运行模式编码，开发模式/生产模式
     */
    private String runModeCode;
    /**
     * 运行模式名称，开发模式/生产模式
     */
    private String runModeName;
    /**
     * 流程分类
     */
    private String category;
    /**
     * 功能编码
     */
    private String funcCode;
    /**
     * 功能名称
     */
    private String funcName;
    /**
     * 流程定义json信息
     */
    private String metaInfo;

    /**
     * 是否部署
     */
    protected int deployStatus;
    /**
     * 是否禁用
     */
    protected int disabled;

    public SaveOrUpdateProcessModelPayload() {
        this.id = UUID.randomUUID().toString();
    }

    @Override
    public String getId() {
        return id;
    }

    public String getModelId() {
        return modelId;
    }

    public SaveOrUpdateProcessModelPayload setModelId(String modelId) {
        if (Strings.isNullOrEmpty(modelId)) {
            return this;
        }
        this.modelId = modelId;
        return this;
    }

    public String getRunModeCode() {
        return runModeCode;
    }

    public SaveOrUpdateProcessModelPayload setRunModeCode(String runModeCode) {
        this.runModeCode = runModeCode;
        return this;
    }

    public String getRunModeName() {
        return runModeName;
    }

    public SaveOrUpdateProcessModelPayload setRunModeName(String runModeName) {
        this.runModeName = runModeName;
        return this;
    }

    public String getCategory() {
        return category;
    }

    public SaveOrUpdateProcessModelPayload setCategory(String category) {
        this.category = category;
        return this;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public SaveOrUpdateProcessModelPayload setFuncCode(String funcCode) {
        this.funcCode = funcCode;
        return this;
    }

    public String getFuncName() {
        return funcName;
    }

    public SaveOrUpdateProcessModelPayload setFuncName(String funcName) {
        this.funcName = funcName;
        return this;
    }

    public String getMetaInfo() {
        return metaInfo;
    }

    public SaveOrUpdateProcessModelPayload setMetaInfo(String metaInfo) {
        this.metaInfo = metaInfo;
        return this;
    }

    public int getDeployStatus() {
        return deployStatus;
    }

    public void setDeployStatus(int deployStatus) {
        this.deployStatus = deployStatus;
    }

    public int getDisabled() {
        return disabled;
    }

    public void setDisabled(int disabled) {
        this.disabled = disabled;
    }

    public static SaveOrUpdateProcessModelPayload build() {
        return new SaveOrUpdateProcessModelPayload();
    }


}
