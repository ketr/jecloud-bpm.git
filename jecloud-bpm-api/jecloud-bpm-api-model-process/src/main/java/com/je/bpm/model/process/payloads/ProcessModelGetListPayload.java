/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.payloads;

import com.je.bpm.model.shared.Payload;

import java.util.UUID;

/**
 * 获取流程定义model
 */
public class ProcessModelGetListPayload implements Payload {

    /**
     * payloadId
     */
    private String id;
    /**
     * 分页参数start
     */
    private Integer start;
    /**
     * 分页参数limit
     */
    private Integer limit;
    /**
     * 名称
     */
    private String name;
    /**
     * 流程key
     */
    private String key;
    /**
     * 流程分类
     */
    private String category;
    /**
     * 部署id
     */
    private String deploymentId;
    /**
     * 租户id
     */
    private String tenantId;
    /**
     * 功能code
     */
    private String funcCode;
    /**
     * 功能名称
     */
    private String funcName;
    /**
     * 部署模式CODE
     */
    private String runModeCode;
    /**
     * 是否部署
     */
    private String deployStatus;
    /**
     * 是否禁用
     */
    private String disabled;
    /**
     * 是否启用一值多or条件模糊查询，输入一个参数，模糊查询name,key等值符合的model数据
     */
    private Boolean multipleConditionsOrQuery = false;

    public ProcessModelGetListPayload() {
        this.id = UUID.randomUUID().toString();
    }

    public ProcessModelGetListPayload(Integer start, Integer limit) {
        this();
        this.start = start;
        this.limit = limit;
    }

    @Override
    public String getId() {
        return id;
    }

    public Integer getStart() {
        return start;
    }

    public ProcessModelGetListPayload setStart(Integer start) {
        this.start = start;
        return this;
    }

    public Integer getLimit() {
        return limit;
    }

    public ProcessModelGetListPayload setLimit(Integer limit) {
        this.limit = limit;
        return this;
    }

    public static ProcessModelGetListPayload build() {
        return new ProcessModelGetListPayload();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDeploymentId() {
        return deploymentId;
    }

    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public void setFuncCode(String funcCode) {
        this.funcCode = funcCode;
    }

    public String getFuncName() {
        return funcName;
    }

    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

    public String getRunModeCode() {
        return runModeCode;
    }

    public void setRunModeCode(String runModeCode) {
        this.runModeCode = runModeCode;
    }

    public Boolean getMultipleConditionsOrQuery() {
        return multipleConditionsOrQuery;
    }

    public void setMultipleConditionsOrQuery(Boolean multipleConditionsOrQuery) {
        this.multipleConditionsOrQuery = multipleConditionsOrQuery;
    }

    public String getDeployStatus() {
        return deployStatus;
    }

    public void setDeployStatus(String deployStatus) {
        this.deployStatus = deployStatus;
    }

    public String getDisabled() {
        return disabled;
    }

    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }
}
