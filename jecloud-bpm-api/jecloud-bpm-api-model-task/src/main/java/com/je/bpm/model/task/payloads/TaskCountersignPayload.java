/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.task.payloads;

import java.util.Map;
import java.util.UUID;

/**
 * 加签payload
 */
public class TaskCountersignPayload extends AbstractTaskPayload {

    private static final long serialVersionUID = 1L;
    private String id;
    private String taskId;
    private String prod;
    private Map<String, Object> bean;
    private String beanId;
    private String assignee;
    private String signBackId;
    private String nodeId;
    private String target;
    /**
     * 是否是跳跃
     */
    private String isJump = "0";

    private String sequentials;

    public TaskCountersignPayload() {
        this.id = UUID.randomUUID().toString();
    }

    public static TaskCountersignPayload builder(String taskId, String prod, Map<String, Object> bean, String comment, String beanId) {
        TaskCountersignPayload personBeingUrgedList = new TaskCountersignPayload();
        personBeingUrgedList.setTaskId(taskId).setProd(prod).setBean(bean).setComment(comment).setBeanId(beanId);
        return personBeingUrgedList;
    }

    @Override
    public TaskCountersignPayload setComment(String comment) {
        this.comment = comment;
        return this;
    }

    @Override
    public String getId() {
        return id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    private void setId(String id) {
        this.id = id;
    }

    public static TaskCountersignPayload build() {
        return new TaskCountersignPayload();
    }

    public String getTaskId() {
        return taskId;
    }

    public TaskCountersignPayload setTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getProd() {
        return prod;
    }

    public TaskCountersignPayload setProd(String prod) {
        this.prod = prod;
        return this;
    }

    public Map<String, Object> getBean() {
        return bean;
    }

    public TaskCountersignPayload setBean(Map<String, Object> bean) {
        this.bean = bean;
        return this;
    }

    public String getBeanId() {
        return beanId;
    }

    public TaskCountersignPayload setBeanId(String beanId) {
        this.beanId = beanId;
        return this;
    }

    public String getAssignee() {
        return assignee;
    }

    public TaskCountersignPayload setAssignee(String assignee) {
        this.assignee = assignee;
        return this;
    }


    public String getSignBackId() {
        return signBackId;
    }

    public TaskCountersignPayload setSignBackId(String signBackId) {
        this.signBackId = signBackId;
        return this;
    }

    public String getNodeId() {
        return nodeId;
    }

    public TaskCountersignPayload setNodeId(String nodeId) {
        this.nodeId = nodeId;
        return this;
    }

    public String getTarget() {
        return target;
    }

    public TaskCountersignPayload setTarget(String target) {
        this.target = target;
        return this;
    }

    public String getIsJump() {
        return isJump;
    }

    public TaskCountersignPayload setIsJump(String isJump) {
        this.isJump = isJump;
        return this;
    }

    public String getSequentials() {
        return sequentials;
    }

    public TaskCountersignPayload setSequentials(String sequentials) {
        this.sequentials = sequentials;
        return this;
    }
}
