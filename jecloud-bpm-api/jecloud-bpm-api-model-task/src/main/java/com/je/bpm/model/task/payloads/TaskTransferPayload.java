/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.task.payloads;

import java.util.Map;
import java.util.UUID;

/**
 * 任务转办payload
 */
public class TaskTransferPayload extends AbstractTaskPayload {

    private static final long serialVersionUID = 1L;
    private String id;
    /**
     * 产品
     */
    private String prod;
    /**
     * 流程实例ID
     */
    private String piid;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 业务Bean主键
     */
    private String beanId;
    /**
     * 指派人
     */
    private String assignee;

    private Map<String, Object> bean;

    public TaskTransferPayload() {
        this.id = UUID.randomUUID().toString();
    }

    public TaskTransferPayload(String prod, String piid, String taskId, String beanId, String assignee) {
        this();
        this.prod = prod;
        this.piid = piid;
        this.taskId = taskId;
        this.beanId = beanId;
        this.assignee = assignee;
    }

    @Override
    public String getId() {
        return id;
    }

    public String getProd() {
        return prod;
    }

    public TaskTransferPayload setProd(String prod) {
        this.prod = prod;
        return this;
    }

    public String getPiid() {
        return piid;
    }

    public TaskTransferPayload setPiid(String piid) {
        this.piid = piid;
        return this;
    }

    public String getTaskId() {
        return taskId;
    }

    public TaskTransferPayload setTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getBeanId() {
        return beanId;
    }

    public TaskTransferPayload setBeanId(String beanId) {
        this.beanId = beanId;
        return this;
    }

    public String getAssignee() {
        return assignee;
    }

    public TaskTransferPayload setAssignee(String assignee) {
        this.assignee = assignee;
        return this;
    }

    public String getComment() {
        return comment;
    }

    public TaskTransferPayload setComment(String comment) {
        this.comment = comment;
        return this;
    }

    public static TaskTransferPayload build(){
        return new TaskTransferPayload();
    }

    public Map<String, Object> getBean() {
        return bean;
    }

    public void setBean(Map<String, Object> bean) {
        this.bean = bean;
    }
}
