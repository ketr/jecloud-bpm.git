/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.task.result;

import com.je.bpm.model.process.model.ProcessConfig;
import com.je.bpm.model.shared.Payload;
import com.je.bpm.model.shared.Result;
import com.je.bpm.model.shared.model.Button;
import com.je.bpm.model.task.model.Task;
import java.util.List;

/**
 * 任务结果，包含：
 * <p>
 *     业务bean
 *     流程基础配置
 *     流程按钮
 *     任务按钮
 * </p>
 */
public class TaskResult extends Result<Task> {

    private static final String TYPE = "taskResult";

    private ProcessConfig processConfig;
    private List<Button> processButtons;
    private List<Button> taskButtons;

    public TaskResult() {
        super(TYPE);
    }

    public TaskResult(Payload payload, Task entity) {
        super(TYPE,payload,entity);
    }

    public TaskResult(Payload payload, Task entity, ProcessConfig processConfig) {
        super(TYPE,payload,entity);
        this.processConfig = processConfig;
    }

    public TaskResult(Payload payload, Task entity,ProcessConfig processConfig,List<Button> processButtons, List<Button> taskButtons) {
        super(TYPE,payload,entity);
        this.processConfig = processConfig;
        this.processButtons = processButtons;
        this.taskButtons = taskButtons;
    }

    public ProcessConfig getProcessConfig() {
        return processConfig;
    }

    public void setProcessConfig(ProcessConfig processConfig) {
        this.processConfig = processConfig;
    }

    public List<Button> getProcessButtons() {
        return processButtons;
    }

    public void setProcessButtons(List<Button> processButtons) {
        this.processButtons = processButtons;
    }

    public List<Button> getTaskButtons() {
        return taskButtons;
    }

    public void setTaskButtons(List<Button> taskButtons) {
        this.taskButtons = taskButtons;
    }

}
