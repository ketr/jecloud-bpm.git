/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.task.payloads;

import java.util.*;

/**
 * 催办payload
 */
public class TaskUrgePayload extends AbstractTaskPayload {

    private static final long serialVersionUID = 1L;
    private String id;
    /**
     * 提醒方式
     */
    List<String> reminderMethod;
    /**
     * 被催办人
     */
    List<Map<String, String>> personBeingUrged;
    /**
     * 催办内容
     */
    String urgentContent;
    /**
     * 抄送人
     */
    List<Map<String, String>> ccUser;
    /**
     * 抄送内容
     */
    String ccContent;
    /**
     * 任务id
     */
    String taskId;
    /**
     * 当前节点key
     */
    String currentNodeId;
    /**
     * 功能编码
     */
    String funcCode;


    public TaskUrgePayload() {
        this.id = UUID.randomUUID().toString();
    }

    public TaskUrgePayload(List<String> reminderMethod, List<Map<String, String>> personBeingUrged, String urgentContent,
                           List<Map<String, String>> ccUser, String ccContent, String taskId, String currentNodeId, String funcCode) {
        this();
        this.taskId = taskId;
        this.reminderMethod = reminderMethod;
        this.personBeingUrged = personBeingUrged;
        this.urgentContent = urgentContent;
        this.ccUser = ccUser;
        this.ccContent = ccContent;
        this.currentNodeId = currentNodeId;
        this.funcCode = funcCode;
    }

    public static TaskUrgePayload builder(String reminderMethod, String personBeingUrgedIds, String personBeingUrgedNames,
                                          String urgentContent, String ccUserIds, String ccUserNames, String ccContent,
                                          String taskId, String currentNodeId, String funcCode) {
        List<String> reminderMethodList = Arrays.asList(reminderMethod.split(","));
        List<Map<String, String>> personBeingUrgedList = twoIdsToMap(personBeingUrgedIds, personBeingUrgedNames);
        List<Map<String, String>> ccUserList = twoIdsToMap(ccUserIds, ccUserNames);
        return new TaskUrgePayload(reminderMethodList, personBeingUrgedList, urgentContent, ccUserList, ccContent, taskId, currentNodeId, funcCode);
    }

    private static List<Map<String, String>> twoIdsToMap(String ids, String names) {
        List<Map<String, String>> personBeingUrgeds = new ArrayList<>();
        String[] idsArray = ids.split(",");
        String[] namesArray = names.split(",");
        for (int i = 0; i < idsArray.length; i++) {
            Map<String, String> personBeingUrgedMap = new HashMap<>();
            personBeingUrgedMap.put("id", idsArray[i]);
            personBeingUrgedMap.put("userName", namesArray[i]);
            personBeingUrgeds.add(personBeingUrgedMap);
        }
        return personBeingUrgeds;
    }

    @Override
    public String getId() {
        return id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getUrgentContent() {
        return urgentContent;
    }

    public void setUrgentContent(String urgentContent) {
        this.urgentContent = urgentContent;
    }

    public String getCcContent() {
        return ccContent;
    }

    public void setCcContent(String ccContent) {
        this.ccContent = ccContent;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getCurrentNodeId() {
        return currentNodeId;
    }

    public void setCurrentNodeId(String currentNodeId) {
        this.currentNodeId = currentNodeId;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public void setFuncCode(String funcCode) {
        this.funcCode = funcCode;
    }

    public List<String> getReminderMethod() {
        return reminderMethod;
    }

    public void setReminderMethod(List<String> reminderMethod) {
        this.reminderMethod = reminderMethod;
    }

    public List<Map<String, String>> getPersonBeingUrged() {
        return personBeingUrged;
    }

    public void setPersonBeingUrged(List<Map<String, String>> personBeingUrged) {
        this.personBeingUrged = personBeingUrged;
    }

    public List<Map<String, String>> getCcUser() {
        return ccUser;
    }

    public void setCcUser(List<Map<String, String>> ccUser) {
        this.ccUser = ccUser;
    }

    @Override
    public TaskUrgePayload setComment(String comment) {
        this.comment = comment;
        return this;
    }


    public static TaskUrgePayload build() {
        return new TaskUrgePayload();
    }

}
