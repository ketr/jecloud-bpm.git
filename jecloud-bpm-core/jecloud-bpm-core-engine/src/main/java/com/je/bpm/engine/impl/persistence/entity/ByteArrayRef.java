/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.impl.context.Context;

import java.io.Serializable;

/**
 * <p>
 * Encapsulates the logic for transparently working with {@link ByteArrayEntity} .
 * </p>
 */
public class ByteArrayRef implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private ByteArrayEntity entity;
    private CsByteArrayEntity csEntity;
    protected boolean deleted;

    public ByteArrayRef() {
    }

    // Only intended to be used by ByteArrayRefTypeHandler
    public ByteArrayRef(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public byte[] getBytes(boolean isCs) {
        ensureInitialized(isCs);
        return (entity != null ? entity.getBytes() : null);
    }

    public void setValue(String name, byte[] bytes, boolean isCs) {
        this.name = name;
        setBytes(bytes, isCs);
    }

    private void setBytes(byte[] bytes, boolean isCs) {
        if (id == null) {
            if (bytes != null) {
                if (isCs) {
                    CsByteArrayEntityManager byteArrayEntityManager = Context.getCommandContext().getCsByteArrayEntityManager();
                    csEntity = byteArrayEntityManager.create();
                    csEntity.setName(name);
                    csEntity.setBytes(bytes);
                    byteArrayEntityManager.insert(csEntity);
                    id = csEntity.getId();
                } else {
                    ByteArrayEntityManager byteArrayEntityManager = Context.getCommandContext().getByteArrayEntityManager();
                    entity = byteArrayEntityManager.create();
                    entity.setName(name);
                    entity.setBytes(bytes);
                    byteArrayEntityManager.insert(entity);
                    id = entity.getId();
                }
            }
        } else {
            ensureInitialized(isCs);
            entity.setBytes(bytes);
        }
    }

    public ByteArrayEntity getEntity(boolean isCs) {
        ensureInitialized(isCs);
        return entity;
    }

    public void delete(boolean isCs) {
        if (!deleted && id != null) {
            if (entity != null) {
                // if the entity has been loaded already,
                // we might as well use the safer optimistic locking delete.
                if (isCs) {
                    Context.getCommandContext().getCsByteArrayEntityManager().delete(csEntity);
                } else {
                    Context.getCommandContext().getByteArrayEntityManager().delete(entity);
                }
            } else {
                if (isCs) {
                    Context.getCommandContext().getCsByteArrayEntityManager().deleteByteArrayById(id);
                } else {
                    Context.getCommandContext().getByteArrayEntityManager().deleteByteArrayById(id);
                }
            }
            entity = null;
            id = null;
            deleted = true;
        }
    }

    private void ensureInitialized(boolean isCs) {
        if (id != null && entity == null) {
            if (isCs) {
                csEntity = Context.getCommandContext().getCsByteArrayEntityManager().findById(id);
                name = csEntity.getName();
            } else {
                entity = Context.getCommandContext().getByteArrayEntityManager().findById(id);
                name = entity.getName();
            }
        }
    }

    public boolean isDeleted() {
        return deleted;
    }

    @Override
    public String toString() {
        return "ByteArrayRef[id=" + id + ", name=" + name + ", entity=" + entity + (deleted ? ", deleted]" : "]");
    }
}
