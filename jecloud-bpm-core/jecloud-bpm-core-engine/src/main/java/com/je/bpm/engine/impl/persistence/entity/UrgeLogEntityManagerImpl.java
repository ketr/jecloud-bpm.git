/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.persistence.entity.data.DataManager;
import com.je.bpm.engine.impl.persistence.entity.data.UrgeLogDataManager;

import java.util.List;

public class UrgeLogEntityManagerImpl extends AbstractEntityManager<UrgeLogEntity> implements UrgeLogEntityManager {

    private UrgeLogDataManager urgeLogDataManager;

    public UrgeLogEntityManagerImpl(ProcessEngineConfigurationImpl processEngineConfiguration, UrgeLogDataManager urgeLogDataManager) {
        super(processEngineConfiguration);
        this.urgeLogDataManager = urgeLogDataManager;
    }

    @Override
    protected DataManager<UrgeLogEntity> getDataManager() {
        return urgeLogDataManager;
    }

    @Override
    public void read(String taskId, String userId) {
        urgeLogDataManager.read(taskId, userId);
    }

    @Override
    public List<UrgeLogEntity> findByTaskId(String taskId) {
        return urgeLogDataManager.findByTaskId(taskId);
    }

    @Override
    public List<UrgeLogEntity> findByProcessInstanceId(String processInstanceId) {
        return urgeLogDataManager.findByProcessInstanceId(processInstanceId);
    }

    @Override
    public List<UrgeLogEntity> findByProcessDefinitionId(String processDefinitionId) {
        return urgeLogDataManager.findByProcessDefinitionId(processDefinitionId);
    }

    @Override
    public List<UrgeLogEntity> findByTaskIdAndTo(String taskId, String to) {
        return urgeLogDataManager.findByTaskIdAndTo(taskId, to);
    }

    @Override
    public List<UrgeLogEntity> findByTo(String to) {
        return urgeLogDataManager.findByTo(to);
    }

    @Override
    public List<UrgeLogEntity> findByTaskIdAndFrom(String taskId, String from) {
        return urgeLogDataManager.findByTaskIdAndFrom(taskId, from);
    }

    @Override
    public List<UrgeLogEntity> findByFrom(String from) {
        return urgeLogDataManager.findByFrom(from);
    }

}
