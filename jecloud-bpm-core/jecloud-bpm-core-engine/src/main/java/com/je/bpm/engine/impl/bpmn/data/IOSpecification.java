/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.data;

import com.je.bpm.engine.delegate.DelegateExecution;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.unmodifiableList;

/**
 * Implementation of the BPMN 2.0 'ioSpecification'
 */
public class IOSpecification {

    protected List<Data> dataInputs;

    protected List<Data> dataOutputs;

    protected List<DataRef> dataInputRefs;

    protected List<DataRef> dataOutputRefs;

    public IOSpecification() {
        this.dataInputs = new ArrayList<Data>();
        this.dataOutputs = new ArrayList<Data>();
        this.dataInputRefs = new ArrayList<DataRef>();
        this.dataOutputRefs = new ArrayList<DataRef>();
    }

    public void initialize(DelegateExecution execution) {
        for (Data data : this.dataInputs) {
            execution.setVariable(data.getName(), data.getDefinition().createInstance());
        }

        for (Data data : this.dataOutputs) {
            execution.setVariable(data.getName(), data.getDefinition().createInstance());
        }
    }

    public List<Data> getDataInputs() {
        return unmodifiableList(this.dataInputs);
    }

    public List<Data> getDataOutputs() {
        return unmodifiableList(this.dataOutputs);
    }

    public void addInput(Data data) {
        this.dataInputs.add(data);
    }

    public void addOutput(Data data) {
        this.dataOutputs.add(data);
    }

    public void addInputRef(DataRef dataRef) {
        this.dataInputRefs.add(dataRef);
    }

    public void addOutputRef(DataRef dataRef) {
        this.dataOutputRefs.add(dataRef);
    }

    public String getFirstDataInputName() {
        return this.dataInputs.get(0).getName();
    }

    public String getFirstDataOutputName() {
        if (this.dataOutputs != null && !this.dataOutputs.isEmpty()) {
            return this.dataOutputs.get(0).getName();
        } else {
            return null;
        }
    }
}
