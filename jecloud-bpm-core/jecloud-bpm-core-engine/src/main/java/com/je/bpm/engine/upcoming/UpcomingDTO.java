/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.upcoming;

import com.google.common.base.Strings;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.config.process.ProcessRemindTemplate;
import com.je.bpm.engine.task.GetTakeNodeNameUtil;
import com.je.bpm.engine.task.Task;

public class UpcomingDTO {
    private String modelName;
    private String modelKey;
    private ProcessRemindTemplate processRemindTemplate;
    private UpcomingCommentInfoDTO upcomingInfo;
    private String funcCode;
    private String funcName;
    private String tableCode;
    private String upcomingUserId;
    private String taskId;
    private String nodeId;
    private String nodeName;
    private String pdid;
    private String piid;
    private String startUserId;
    private String executionId;
    private Boolean isSponsor;
    private String jeCloudDingTalkId;


    public static UpcomingDTO build(BpmnModel bpmnModel, Task task, Object upcomingInfo, String upcomingUserId, String startUserId) {
        UpcomingDTO upcomingDTO = new UpcomingDTO();
        upcomingDTO.setModelKey(bpmnModel.getMainProcess().getId());
        upcomingDTO.setModelName(bpmnModel.getMainProcess().getName());
        upcomingDTO.setFuncCode(bpmnModel.getMainProcess().getProcessConfig().getFuncCode());
        upcomingDTO.setTableCode(bpmnModel.getMainProcess().getProcessConfig().getTableCode());
        upcomingDTO.setProcessRemindTemplate(bpmnModel.getMainProcess().getMessageSetting().getMessageUpcomingDefinitions());
        if (upcomingInfo != null && upcomingInfo instanceof UpcomingCommentInfoDTO) {
            upcomingDTO.setUpcomingInfo((UpcomingCommentInfoDTO) upcomingInfo);
        }
        upcomingDTO.setUpcomingUserId(upcomingUserId);
        upcomingDTO.setTaskId(task.getId());
        upcomingDTO.setNodeId(task.getTaskDefinitionKey());
        if (Strings.isNullOrEmpty(task.getName())) {
            FlowElement flowElement = bpmnModel.getMainProcess().getFlowElementMap().get(task.getTaskDefinitionKey());
            upcomingDTO.setNodeName(GetTakeNodeNameUtil.builder().getTakeNodeName(flowElement));
        } else {
            upcomingDTO.setNodeName(task.getName());
        }
        upcomingDTO.setPdid(task.getProcessDefinitionId());
        upcomingDTO.setPiid(task.getProcessInstanceId());
        upcomingDTO.setStartUserId(startUserId);
        upcomingDTO.setExecutionId(task.getExecutionId());
        upcomingDTO.setFuncName(bpmnModel.getMainProcess().getProcessConfig().getFuncName());
        upcomingDTO.setJeCloudDingTalkId(bpmnModel.getMainProcess().getMessageSetting().getJeCloudDingTalkId());
        return upcomingDTO;
    }


    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelKey() {
        return modelKey;
    }

    public void setModelKey(String modelKey) {
        this.modelKey = modelKey;
    }

    public ProcessRemindTemplate getProcessRemindTemplate() {
        return processRemindTemplate;
    }

    public void setProcessRemindTemplate(ProcessRemindTemplate processRemindTemplate) {
        this.processRemindTemplate = processRemindTemplate;
    }

    public UpcomingCommentInfoDTO getUpcomingInfo() {
        return upcomingInfo;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public void setFuncCode(String funcCode) {
        this.funcCode = funcCode;
    }

    public String getTableCode() {
        return tableCode;
    }

    public void setTableCode(String tableCode) {
        this.tableCode = tableCode;
    }

    public String getUpcomingUserId() {
        return upcomingUserId;
    }

    public void setUpcomingUserId(String upcomingUserId) {
        this.upcomingUserId = upcomingUserId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getPdid() {
        return pdid;
    }

    public void setPdid(String pdid) {
        this.pdid = pdid;
    }

    public void setUpcomingInfo(UpcomingCommentInfoDTO upcomingInfo) {
        this.upcomingInfo = upcomingInfo;
    }

    public String getPiid() {
        return piid;
    }

    public void setPiid(String piid) {
        this.piid = piid;
    }

    public String getStartUserId() {
        return startUserId;
    }

    public void setStartUserId(String startUserId) {
        this.startUserId = startUserId;
    }

    public String getExecutionId() {
        return executionId;
    }

    public void setExecutionId(String executionId) {
        this.executionId = executionId;
    }

    public Boolean getIsSponsor() {
        return isSponsor;
    }

    public UpcomingDTO setIsSponsor(Boolean isSponsor) {
        this.isSponsor = isSponsor;
        return this;
    }

    public String getFuncName() {
        return funcName;
    }

    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

    public String getJeCloudDingTalkId() {
        return jeCloudDingTalkId;
    }

    public void setJeCloudDingTalkId(String jeCloudDingTalkId) {
        this.jeCloudDingTalkId = jeCloudDingTalkId;
    }
}
