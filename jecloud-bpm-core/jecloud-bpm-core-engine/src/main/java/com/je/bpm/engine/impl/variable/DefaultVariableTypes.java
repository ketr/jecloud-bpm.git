/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.variable;

import com.je.bpm.engine.ActivitiException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultVariableTypes implements VariableTypes, Serializable {

    private static final long serialVersionUID = 1L;

    private final List<VariableType> typesList = new ArrayList<VariableType>();
    private final Map<String, VariableType> typesMap = new HashMap<String, VariableType>();

    @Override
    public DefaultVariableTypes addType(VariableType type) {
        return addType(type, typesList.size());
    }

    @Override
    public DefaultVariableTypes addType(VariableType type, int index) {
        typesList.add(index, type);
        typesMap.put(type.getTypeName(), type);
        return this;
    }

    public void setTypesList(List<VariableType> typesList) {
        this.typesList.clear();
        this.typesList.addAll(typesList);
        this.typesMap.clear();
        for (VariableType type : typesList) {
            typesMap.put(type.getTypeName(), type);
        }
    }

    @Override
    public VariableType getVariableType(String typeName) {
        return typesMap.get(typeName);
    }

    @Override
    public VariableType findVariableType(Object value) {
        for (VariableType type : typesList) {
            if (type.isAbleToStore(value)) {
                return type;
            }
        }
        throw new ActivitiException("couldn't find a variable type that is able to serialize " + value);
    }

    @Override
    public int getTypeIndex(VariableType type) {
        return typesList.indexOf(type);
    }

    @Override
    public int getTypeIndex(String typeName) {
        VariableType type = typesMap.get(typeName);
        if (type != null) {
            return getTypeIndex(type);
        } else {
            return -1;
        }
    }

    @Override
    public VariableTypes removeType(VariableType type) {
        typesList.remove(type);
        typesMap.remove(type.getTypeName());
        return this;
    }
}
