/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl;

import com.je.bpm.engine.ActivitiIllegalArgumentException;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.persistence.entity.VariableInstanceEntity;
import com.je.bpm.engine.impl.variable.*;
import java.io.Serializable;

/**
 * Represents a variable value used in queries.
 */
public class QueryVariableValue implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private Object value;
    private QueryOperator operator;

    private VariableInstanceEntity variableInstanceEntity;
    private boolean local;

    public QueryVariableValue(String name, Object value, QueryOperator operator, boolean local) {
        this.name = name;
        this.value = value;
        this.operator = operator;
        this.local = local;
    }

    public void initialize(VariableTypes types) {
        if (variableInstanceEntity == null) {
            VariableType type = types.findVariableType(value);
            if (type instanceof ByteArrayType) {
                throw new ActivitiIllegalArgumentException("Variables of type ByteArray cannot be used to query");
            } else {
                // Type implementation determines which fields are set on the entity
                variableInstanceEntity = Context.getCommandContext().getVariableInstanceEntityManager().create(name, type, value);
            }
        }
    }

    public String getName() {
        return name;
    }

    public String getOperator() {
        if (operator != null) {
            return operator.toString();
        }
        return QueryOperator.EQUALS.toString();
    }

    public String getTextValue() {
        if (variableInstanceEntity != null) {
            return variableInstanceEntity.getTextValue();
        }
        return null;
    }

    public Long getLongValue() {
        if (variableInstanceEntity != null) {
            return variableInstanceEntity.getLongValue();
        }
        return null;
    }

    public Double getDoubleValue() {
        if (variableInstanceEntity != null) {
            return variableInstanceEntity.getDoubleValue();
        }
        return null;
    }

    public String getTextValue2() {
        if (variableInstanceEntity != null) {
            return variableInstanceEntity.getTextValue2();
        }
        return null;
    }

    public String getType() {
        if (variableInstanceEntity != null) {
            return variableInstanceEntity.getType().getTypeName();
        }
        return null;
    }

    public boolean isLocal() {
        return local;
    }
}
