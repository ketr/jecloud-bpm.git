/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.je.bpm.engine.delegate.event.ActivitiCountersignedOpinionType;
import com.je.bpm.engine.impl.bpmn.behavior.MultiInstanceActivityBehavior;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.task.Comment;

import java.util.HashMap;
import java.util.Map;

/**
 * 弃权任务命令
 */
public class AbstainTaskCmd extends AbstractCompleteTaskCmd {

    private String taskId;

    private String comment;

    protected Map<String, Object> variables = new HashMap<>();

    private String files;

    /**
     * 是否本任务作用域
     */
    protected boolean localScope;

    public AbstainTaskCmd(String taskId, String prod, Map<String, Object> bean, String comment, String files) {
        super(taskId, prod, bean);
        this.taskId = taskId;
        this.comment = comment;
        this.files = files;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    @Override
    protected Void execute(CommandContext commandContext, TaskEntity task) {
        addTaskComment(taskId, task.getProcessInstanceId(), Comment.USER_COMMENT, comment);
        addTaskComment(taskId, task.getProcessInstanceId(), Comment.FILES_COMMENT, files);
        addTaskComment(taskId, task.getProcessInstanceId(), Comment.COUNTERSIGNED, ActivitiCountersignedOpinionType.ABSTAIN.toString());
        commandContext.addAttribute(MultiInstanceActivityBehavior.COUNTERSIGN_PASS_USER, Authentication.getAuthenticatedUser().getDeptId());
        commandContext.addAttribute(MultiInstanceActivityBehavior.CUSTOMER_MESSAGE, ActivitiCountersignedOpinionType.ABSTAIN.toString());
        executeTaskComplete(commandContext, task, variables, localScope);
        return null;
    }

}
