/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.google.common.base.Strings;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.config.CustomEvent;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.ActivitiIllegalArgumentException;
import com.je.bpm.engine.ActivitiObjectNotFoundException;
import com.je.bpm.engine.delegate.DelegateHelper;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.runtime.ProcessInstance;
import com.je.bpm.engine.task.Comment;
import com.je.bpm.engine.task.Task;
import com.je.bpm.engine.upcoming.UpcomingCommentInfoDTO;
import com.je.bpm.runtime.shared.operator.AudflagEnum;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class DeleteProcessInstanceCmd extends NeedsActiveTaskCmd<Void> implements Serializable {

    private static final long serialVersionUID = 1L;
    protected String processInstanceId;
    protected String deleteReason;
    protected String businessKey;
    protected String prod;
    protected Map<String, Object> bean;
    protected Boolean isVoid = false;

    public DeleteProcessInstanceCmd(String processInstanceId, String deleteReason, String businessKey, String prod, Map<String, Object> bean) {
        super(false, prod, bean);
        this.processInstanceId = processInstanceId;
        this.deleteReason = deleteReason;
        this.prod = prod;
        this.bean = bean;
        this.businessKey = businessKey;
        this.isVoid = true;
    }

    public DeleteProcessInstanceCmd(String processInstanceId, String deleteReason, String prod, Map<String, Object> bean) {
        super(false, prod, bean);
        this.processInstanceId = processInstanceId;
        this.deleteReason = deleteReason;
        this.isVoid = false;
    }

    @Override
    public Void execute(CommandContext commandContext, TaskEntity taskEntity) {
        if (processInstanceId == null) {
            throw new ActivitiIllegalArgumentException("processInstanceId is null");
        }
        ExecutionEntity processInstanceEntity = commandContext.getExecutionEntityManager().findById(processInstanceId);
        String pdid = processInstanceEntity.getProcessDefinitionId();
        boolean isMe = false;
        List<Task> list = commandContext.getProcessEngineConfiguration().getTaskService().createTaskQuery().processInstanceId(processInstanceId).list();
        for (Task task : list) {
            if (!Strings.isNullOrEmpty(task.getAssignee()) && task.getAssignee().equals(Authentication.getAuthenticatedUser().getDeptId())) {
                isMe = true;
                break;
            }
        }

        //如果不等于
        if (!isMe) {
            if (!Authentication.getAuthenticatedUser().getDeptId().equals(processInstanceEntity.getStartUserId())) {
                throw new ActivitiException("没有操作权限！");
            }
            deleteReason = String.format("(由流程发起人%s进行作废)" + deleteReason, Authentication.getAuthenticatedUser().getName());
        }


        String beanId = processInstanceEntity.getBusinessKey();
        if (!isVoid) {
            commandContext.getExecutionEntityManager().deleteProcessInstance(processInstanceEntity.getProcessInstanceId(), deleteReason, false);
            return null;
        }


        StringBuffer nodeIds = new StringBuffer();
        StringBuffer nodeNames = new StringBuffer();
        String busk = "";
        for (Task task : list) {
            pdid = task.getProcessDefinitionId();
            busk = task.getBusinessKey();
            if (nodeIds.length() > 0) {
                nodeIds.append(",");
                nodeNames.append(",");
            }
            nodeIds.append(task.getId());
            nodeNames.append(task.getName());
            commandContext.getProcessEngineConfiguration().getTaskService().addComment(task.getId(), processInstanceId,
                    Comment.USER_COMMENT, deleteReason);
            commandContext.getProcessEngineConfiguration().getTaskService().addComment(task.getId(), processInstanceId,
                    Comment.NODE_TYPE, "审批作废");
        }

        BpmnModel bpmnModel = commandContext.getProcessEngineConfiguration().getRepositoryService().getBpmnModel(pdid, processInstanceId, busk);
        UpcomingCommentInfoDTO upcomingInfo = DelegateHelper.buildUpcomingInfo(null, deleteReason, SubmitTypeEnum.INVALID, beanId
                , "", null);
        commandContext.getProcessEngineConfiguration().getActivitiUpcomingRun().completeUpcoming(upcomingInfo);
        if (processInstanceEntity == null) {
            throw new ActivitiObjectNotFoundException("No process instance found for id '" + processInstanceId + "'", ProcessInstance.class);
        }

        commandContext.addAttribute("processType", "void");
        //执行自定义流程、节点策略
        List<Map<String, String>> assignees = new ArrayList<>();
        commandContext.getProcessEngineConfiguration().getProcessInstanceHelper().invokeEvent
                (bpmnModel.getMainProcess().getCustomEventListeners(), nodeNames.toString(),
                        nodeIds.toString(), "",
                        CustomEvent.CustomEventEnum.PROCESS_VOID, commandContext, deleteReason, "", OperatorEnum.PROCESS_INVALID_OPERATOR, businessKey, bpmnModel, assignees);
        commandContext.getProcessEngineConfiguration().getProcessInstanceHelper().updateBeanInfo
                (bpmnModel.getMainProcess().getProcessConfig(), businessKey,
                        AudflagEnum.SUSPEND.toString(), commandContext);

        commandContext.getExecutionEntityManager().deleteProcessInstance(processInstanceEntity.getProcessInstanceId(), deleteReason, false);
        return null;
    }

}
