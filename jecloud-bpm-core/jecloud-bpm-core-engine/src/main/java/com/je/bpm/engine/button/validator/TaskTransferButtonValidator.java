/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.button.validator;

import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.button.Button;
import com.je.bpm.core.model.button.TaskTransferButton;
import com.je.bpm.core.model.gateway.kaite.KaiteGateway;
import com.je.bpm.core.model.task.*;
import com.je.bpm.engine.impl.persistence.entity.TaskEntityImpl;

/**
 * 任务转办按钮
 */
public class TaskTransferButtonValidator extends ButtonValidator<TaskTransferButton> {
    @Override
    public Button exec(ButtonValidateParam param, Button button, BpmnModel bpmnModel, TaskEntityImpl taskEntity) {
        String nodeCode = "";
        if (taskEntity != null) {
            nodeCode = taskEntity.getTaskDefinitionKey();
        }
//        if (!Strings.isNullOrEmpty(taskEntity.getOwner())) {
//            return null;
//        }
        if (!bpmnModel.getMainProcess().getExtendedConfiguration().isCanTransfer()) {
            return null;
        }
        FlowElement flowElement = bpmnModel.getMainProcess().getFlowElement(taskEntity.getTaskDefinitionKey());
        //只有（当前）节点是用户节点，和判断节点 可支持转办操作
        if (!(flowElement instanceof KaiteUserTask || flowElement instanceof KaiteDecideUserTask)) {
            return null;
        }

        //上个节点是 多人、分支聚合、会签 不可以转办
        if (flowElement instanceof KaiteBaseUserTask) {
            KaiteBaseUserTask kaiteBaseUserTask = (KaiteBaseUserTask) flowElement;
            if (kaiteBaseUserTask.getIncomingFlows().size() == 1) {
                FlowElement targetFlowElement = kaiteBaseUserTask.getIncomingFlows().get(0).getSourceFlowElement();
                if (targetFlowElement instanceof KaiteMultiUserTask || targetFlowElement instanceof KaiteCounterSignUserTask
                        || targetFlowElement instanceof KaiteGateway) {
                    return null;
                }
            }
        }

        return getModelTaskButtonByCode(button, bpmnModel, nodeCode);
    }
}
