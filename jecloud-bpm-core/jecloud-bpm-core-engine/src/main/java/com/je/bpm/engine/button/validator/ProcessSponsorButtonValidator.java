/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.button.validator;

import com.google.common.base.Strings;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.button.Button;
import com.je.bpm.core.model.button.ProcessSponsorButton;
import com.je.bpm.core.model.config.process.StartupSettingsConfigImpl;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.engine.impl.persistence.entity.TaskEntityImpl;

/**
 * 流程发起按钮
 */
public class ProcessSponsorButtonValidator extends ButtonValidator<ProcessSponsorButton> {

    @Override
    public ProcessSponsorButton exec(ButtonValidateParam param, Button button, BpmnModel bpmnModel, TaskEntityImpl taskEntity) {

        StartupSettingsConfigImpl startupSettings = bpmnModel.getMainProcess().getStartupSettings();
        //流程已启动
        if (param.getStarting()) {
            return null;
        }

        //不可发起
        if (!bpmnModel.getMainProcess().getExtendedConfiguration().isCanSponsor()) {
            return null;
        }

        //任何人可启动
        if (startupSettings.isCanEveryoneStart()) {
            return buildButton(button, bpmnModel);
        }

        //可启动角色
        if (!Strings.isNullOrEmpty(startupSettings.getCanEveryoneRolesId())) {
            if (!param.getLogUserRoleInStartRoleIds()) {
                return null;
            } else {
                return buildButton(button, bpmnModel);
            }
        }

        //创建人可以启动
        if (param.getBean().get("SY_CREATEUSERID") != null) {
            String createUser = (String) param.getBean().get("SY_CREATEUSERID");
            String logUserId = Authentication.getAuthenticatedUser().getRealUser().getId();
            if (!createUser.equals(logUserId)) {
                return null;
            }
        }
        return buildButton(button, bpmnModel);
    }

    private ProcessSponsorButton buildButton(Button button, BpmnModel bpmnModel) {
        ProcessSponsorButton sponsorButton = (ProcessSponsorButton) getModelProcessButtonByCode(button, bpmnModel);
        sponsorButton.setDisplayExpressionWhenStarted(bpmnModel.getMainProcess().getStartupSettings().getStartExpression());
        sponsorButton.setDisplayExpressionWhenStartedFn(bpmnModel.getMainProcess().getStartupSettings().getStartExpressionFn());
        return sponsorButton;
    }

}
