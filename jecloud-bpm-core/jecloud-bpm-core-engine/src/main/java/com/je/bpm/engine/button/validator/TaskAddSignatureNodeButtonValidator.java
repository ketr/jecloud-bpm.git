/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.button.validator;

import com.google.common.base.Strings;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.button.Button;
import com.je.bpm.core.model.button.TaskAddSignatureNodeButton;
import com.je.bpm.core.model.config.process.StartupSettingsConfigImpl;
import com.je.bpm.core.model.event.StartEvent;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.engine.impl.persistence.entity.TaskEntityImpl;

/**
 * 加签按钮
 */
public class TaskAddSignatureNodeButtonValidator extends ButtonValidator<TaskAddSignatureNodeButton> {

    @Override
    public Button exec(ButtonValidateParam param, Button button, BpmnModel bpmnModel, TaskEntityImpl taskEntity) {
        StartupSettingsConfigImpl startupSettings = bpmnModel.getMainProcess().getStartupSettings();

        if (!bpmnModel.getMainProcess().getExtendedConfiguration().isCountersignNode()) {
            return null;
        }

        //未启动
        if (taskEntity == null) {
            //不可发起
            if (!bpmnModel.getMainProcess().getExtendedConfiguration().isCanSponsor()) {
                return null;
            }

            Process mainProcess = bpmnModel.getMainProcess();
            StartEvent startEvent = (StartEvent) mainProcess.getInitialFlowElement();
            String firstTaskRef = startEvent.getOutgoingFlows().get(0).getTargetRef();
            //找到第一个任务节点
            KaiteBaseUserTask firstBaseUserTask = (KaiteBaseUserTask) bpmnModel.getFlowElement(firstTaskRef);
            if (!firstBaseUserTask.getTaskBasicConfig().isCountersignNode()) {
                return null;
            }

            //任何人可启动
            if (startupSettings.isCanEveryoneStart()) {
                return super.exec(param, button, bpmnModel, taskEntity);
            }

            //可启动角色
            if (!Strings.isNullOrEmpty(startupSettings.getCanEveryoneRolesId())) {
                if (!param.getLogUserRoleInStartRoleIds()) {
                    return null;
                } else {
                    return super.exec(param, button, bpmnModel, taskEntity);
                }
            }

            //创建人可以启动
            if (param.getBean().get("SY_CREATEUSERID") != null) {
                String createUser = (String) param.getBean().get("SY_CREATEUSERID");
                String logUserId = Authentication.getAuthenticatedUser().getRealUser().getId();
                if (!createUser.equals(logUserId)) {
                    return null;
                }
            }
            return getNoModelTaskButtonByCode(button, bpmnModel);
        }
        //加签只支持单人节点
        FlowElement flowElement = bpmnModel.getMainProcess().getFlowElement(taskEntity.getTaskDefinitionKey());
        if (flowElement instanceof KaiteBaseUserTask) {
            KaiteBaseUserTask kaiteBaseUserTask = (KaiteBaseUserTask) flowElement;
            if (kaiteBaseUserTask.getLoopCharacteristics() != null) {
                return null;
            }
            if (!kaiteBaseUserTask.getTaskBasicConfig().isCountersignNode()) {
                return null;
            }
        }

        String logUserId = Authentication.getAuthenticatedUser().getDeptId();
        if (!taskEntity.getAssignee().equals(logUserId)) {
            return null;
        }

        return super.exec(param, button, bpmnModel, taskEntity);
    }

    public Button getNoModelTaskButtonByCode(Button button, BpmnModel bpmnModel) {
        copy(button, new TaskAddSignatureNodeButton());
        button.setName("加签->" + bpmnModel.getMainProcess().getName());
        button.setDisplayExpressionWhenStarted(bpmnModel.getMainProcess().getStartupSettings().getStartExpression());
        button.setDisplayExpressionWhenStartedFn(bpmnModel.getMainProcess().getStartupSettings().getStartExpressionFn());
        return button;
    }

}
