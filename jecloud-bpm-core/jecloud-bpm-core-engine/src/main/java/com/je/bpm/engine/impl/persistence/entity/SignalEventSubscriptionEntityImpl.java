/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.core.model.signal.Signal;

import java.text.MessageFormat;


public class SignalEventSubscriptionEntityImpl extends EventSubscriptionEntityImpl implements SignalEventSubscriptionEntity {

    private static final long serialVersionUID = 1L;

    // Using json here, but not worth of adding json dependency lib for this
    private static final String CONFIGURATION_TEMPLATE = "'{'\"scope\":\"{0}\"'}'";

    public SignalEventSubscriptionEntityImpl() {
        eventType = EVENT_TYPE;
    }

    @Override
    public void setConfiguration(String configuration) {
        if (configuration != null && configuration.contains("{\"scope\":")) {
            this.configuration = configuration;
        } else {
            this.configuration = MessageFormat.format(CONFIGURATION_TEMPLATE, configuration);
        }
    }

    @Override
    public boolean isProcessInstanceScoped() {
        String scope = extractScopeFormConfiguration();
        return (scope != null) && (Signal.SCOPE_PROCESS_INSTANCE.equals(scope));
    }

    @Override
    public boolean isGlobalScoped() {
        String scope = extractScopeFormConfiguration();
        return (scope == null) || (Signal.SCOPE_GLOBAL.equals(scope));
    }

    protected String extractScopeFormConfiguration() {
        if (this.configuration == null) {
            return null;
        } else {
            return this.configuration.substring(10, this.configuration.length() - 2); // 10 --> length of {"scope": and -2 for removing"}
        }
    }

}
