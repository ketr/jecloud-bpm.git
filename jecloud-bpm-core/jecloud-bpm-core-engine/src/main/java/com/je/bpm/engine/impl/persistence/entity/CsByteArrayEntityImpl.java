/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Arrays;


public class CsByteArrayEntityImpl extends AbstractEntity implements CsByteArrayEntity, Serializable {

    private static final long serialVersionUID = 1L;

    protected String name;
    protected byte[] bytes;
    protected String deploymentId;

    public CsByteArrayEntityImpl() {

    }

    public static CsByteArrayEntity buildByteArrayToCsByteArray(CsByteArrayEntity csByteArrayEntity, ByteArrayEntity byteArrayEntity) {
        csByteArrayEntity.setBytes(byteArrayEntity.getBytes());
        csByteArrayEntity.setDeploymentId(byteArrayEntity.getDeploymentId());
        csByteArrayEntity.setId(byteArrayEntity.getId());
        csByteArrayEntity.setName(byteArrayEntity.getName());
        csByteArrayEntity.setDeleted(byteArrayEntity.isDeleted());
        csByteArrayEntity.setRevision(byteArrayEntity.getRevision());
        return csByteArrayEntity;
    }

    @Override
    public byte[] getBytes() {
        return bytes;
    }

    @Override
    public Object getPersistentState() {
        return new PersistentState(name, bytes);
    }

    // getters and setters ////////////////////////////////////////////////////////
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDeploymentId() {
        return deploymentId;
    }

    @Override
    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }

    @Override
    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    @Override
    public String toString() {
        return "ByteArrayEntity[id=" + id + ", name=" + name + ", size=" + (bytes != null ? bytes.length : 0) + "]";
    }

    // Wrapper for a byte array, needed to do byte array comparisons
    // See https://activiti.atlassian.net/browse/ACT-1524
    private static class PersistentState {

        private final String name;
        private final byte[] bytes;

        public PersistentState(String name, byte[] bytes) {
            this.name = name;
            this.bytes = bytes;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof PersistentState) {
                PersistentState other = (PersistentState) obj;
                return StringUtils.equals(this.name, other.name) && Arrays.equals(this.bytes, other.bytes);
            }
            return false;
        }

        @Override
        public int hashCode() {
            throw new UnsupportedOperationException();
        }

    }

}
