/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.je.bpm.engine.impl.interceptor.Command;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.IdentityLinkEntity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.task.IdentityLink;
import com.je.bpm.engine.task.IdentityLinkType;

import java.io.Serializable;
import java.util.List;


public class GetIdentityLinksForTaskCmd implements Command<List<IdentityLink>>, Serializable {

    private static final long serialVersionUID = 1L;
    protected String taskId;

    public GetIdentityLinksForTaskCmd(String taskId) {
        this.taskId = taskId;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public List<IdentityLink> execute(CommandContext commandContext) {
        TaskEntity task = commandContext.getTaskEntityManager().findById(taskId);
        if (task == null) {
            return null;
        }
        List<IdentityLink> identityLinks = (List) task.getIdentityLinks();

        // assignee is not part of identity links in the db.
        // so if there is one, we add it here.
        // @Tom: we discussed this long on skype and you agreed ;-)
        // an assignee *is* an identityLink, and so must it be reflected in the
        // API
        //
        // Note: we cant move this code to the TaskEntity (which would be
        // cleaner),
        // since the task.delete cascaded to all associated identityLinks
        // and of course this leads to exception while trying to delete a
        // non-existing identityLink
        if (task.getAssignee() != null) {
            IdentityLinkEntity identityLink = commandContext.getIdentityLinkEntityManager().create();
            identityLink.setUserId(task.getAssignee());
            identityLink.setType(IdentityLinkType.ASSIGNEE);
            identityLink.setTaskId(task.getId());
            identityLinks.add(identityLink);
        }
        if (task.getOwner() != null) {
            IdentityLinkEntity identityLink = commandContext.getIdentityLinkEntityManager().create();
            identityLink.setUserId(task.getOwner());
            identityLink.setTaskId(task.getId());
            identityLink.setType(IdentityLinkType.OWNER);
            identityLinks.add(identityLink);
        }

        return (List) task.getIdentityLinks();
    }

}
