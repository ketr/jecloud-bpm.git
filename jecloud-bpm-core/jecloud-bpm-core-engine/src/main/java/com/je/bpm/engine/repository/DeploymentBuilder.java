/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.repository;

import com.je.bpm.common.project.model.ProjectManifest;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.engine.internal.Internal;
import org.springframework.core.io.Resource;
import java.io.InputStream;
import java.util.Date;
import java.util.zip.ZipInputStream;

/**
 * Builder for creating new deployments.
 *
 * A builder instance can be obtained through {@link com.je.bpm.engine.RepositoryService#createDeployment()}.
 *
 * Multiple resources can be added to one deployment before calling the {@link #deploy()} operation.
 *
 * After deploying, no more changes can be made to the returned deployment and the builder instance can be disposed.
 *
 */
@Internal
public interface DeploymentBuilder {

  DeploymentBuilder addInputStream(String resourceName, InputStream inputStream);

  DeploymentBuilder addInputStream(String resourceName,Resource resource);

  DeploymentBuilder addClasspathResource(String resource);

  DeploymentBuilder addString(String resourceName, String text);

  DeploymentBuilder addBytes(String resourceName, byte[] bytes);

  DeploymentBuilder addZipInputStream(ZipInputStream zipInputStream);

  DeploymentBuilder addBpmnModel(String resourceName, BpmnModel bpmnModel);

  DeploymentBuilder setProjectManifest(ProjectManifest projectManifest);

  DeploymentBuilder setEnforcedAppVersion(Integer enforcedAppVersion);

  /**
   * If called, no XML schema validation against the BPMN 2.0 XSD.
   *
   * Not recommended in general.
   */
  DeploymentBuilder disableSchemaValidation();

  /**
   * If called, no validation that the process definition is executable on the engine will be done against the process definition.
   *
   * Not recommended in general.
   */
  DeploymentBuilder disableBpmnValidation();

  /**
   * Gives the deployment the given name.
   */
  DeploymentBuilder name(String name);

  /**
   * Gives the deployment the given category.
   */
  DeploymentBuilder category(String category);

  /**
   * Gives the deployment the given key.
   */
  DeploymentBuilder key(String key);

  /**
   * Gives the deployment the given tenant id.
   */
  DeploymentBuilder tenantId(String tenantId);

  /**
   * If set, this deployment will be compared to any previous deployment. This means that every (non-generated) resource will be compared with the provided resources of this deployment.
   */
  DeploymentBuilder enableDuplicateFiltering();

  /**
   * Sets the date on which the process definitions contained in this deployment will be activated. This means that all process definitions will be deployed as usual, but they will be suspended from
   * the start until the given activation date.
   */
  DeploymentBuilder activateProcessDefinitionsOn(Date date);

  /**
   * Allows to add a property to this {@link DeploymentBuilder} that influences the deployment.
   */
  DeploymentBuilder deploymentProperty(String propertyKey, Object propertyValue);

  /**
   * Deploys all provided sources to the Activiti engine.
   */
  Deployment deploy();

}
