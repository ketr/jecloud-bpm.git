/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.alibaba.fastjson2.JSONObject;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.MultiInstanceLoopCharacteristics;
import com.je.bpm.core.model.config.CounterSignPassTypeEnum;
import com.je.bpm.core.model.task.KaiteCounterSignUserTask;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.delegate.DelegateExecution;
import com.je.bpm.engine.delegate.event.ActivitiCountersignedOpinionType;
import com.je.bpm.engine.history.HistoricTaskInstance;
import com.je.bpm.engine.impl.HistoricTaskInstanceQueryImpl;
import com.je.bpm.engine.impl.bpmn.behavior.MultiInstanceActivityBehavior;
import com.je.bpm.engine.impl.bpmn.behavior.ParallelMultiInstanceBehavior;
import com.je.bpm.engine.impl.bpmn.behavior.SequentialMultiInstanceBehavior;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.CommentEntity;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.impl.util.ProcessDefinitionUtil;
import com.je.bpm.engine.task.Comment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 改签任务命令
 */
public class CountersignedRebookCmd extends AbstractCompleteTaskCmd {

    private String taskId;

    private String assignee;

    private ActivitiCountersignedOpinionType opinionType;

    private String comment;

    public CountersignedRebookCmd(String assignee, String taskId, String comment, Map<String, Object> bean, String prod, ActivitiCountersignedOpinionType opinionType,String files) {
        super(taskId, prod, bean);
        this.taskId = taskId;
        this.comment = comment;
        this.assignee = assignee;
        this.opinionType = opinionType;
    }

    @Override
    protected Void execute(CommandContext commandContext, TaskEntity task) {
        // 根据执行实例ID获取当前执行实例
        String executionId = task.getExecutionId();

        String processDefinitionId = task.getProcessDefinitionId();
        BpmnModel bpmnModel = ProcessDefinitionUtil.getBpmnModel(processDefinitionId, task.getProcessInstanceId(), task.getBusinessKey());
        String taskDefinitionKey = task.getTaskDefinitionKey();
        KaiteCounterSignUserTask miActivityElement = (KaiteCounterSignUserTask) bpmnModel.getFlowElement(taskDefinitionKey);
        MultiInstanceLoopCharacteristics multiInstanceLoopCharacteristics = miActivityElement.getLoopCharacteristics();
        ExecutionEntity currentExecutionEntity = task.getExecution();
        ExecutionEntity miExecution = (ExecutionEntity) getMultiInstanceRootExecution(task.getExecution());
        List<String> users = getListVariable(miExecution, MultiInstanceActivityBehavior.PROCESSING_USERS_INFO);
        if (!users.contains(assignee)) {
            throw new ActivitiException(String.format("%s:不存在，无法改签！", assignee));
        }

        if (miActivityElement.getLoopCharacteristics() == null) {
            throw new ActivitiException("不是多实例节点 " + executionId);
        }
        String oldOpinion = "";
        JSONObject processingInfo = getJsonObject(miExecution, MultiInstanceActivityBehavior.PROCESSING_INFO);
        if (processingInfo == null) {
            throw new ActivitiException(String.format("没有审批，无法改签！", assignee));
        }
        HashMap<String, String> opinions = (HashMap<String, String>) processingInfo.get("opinions");
        Object opinion = opinions.get(assignee);
        oldOpinion = (String) opinion;
        if (opinion == null) {
            throw new ActivitiException(String.format("没有审批，无法改签！", assignee));
        }

        if (opinionType.toString().equals((String) opinion)) {
            throw new ActivitiException(String.format("与上次审批意见相同，无法改签！", assignee));
        }
        updateComment(multiInstanceLoopCharacteristics, task, commandContext);
        //通过数量
        int nrOfPassInstances = getLoopVariable(miExecution, MultiInstanceActivityBehavior.NUMBER_OF_PASS_INSTANCES);
        //否决数量
        int nrOfVetoInstances = getLoopVariable(miExecution, MultiInstanceActivityBehavior.NUMBER_OF_VETO_INSTANCES);
        //弃权数量
        int nrOfAbstainInstances = getLoopVariable(miExecution, MultiInstanceActivityBehavior.NUMBER_OF_ABSTAIN_INSTANCES);

        if (oldOpinion.equals(ActivitiCountersignedOpinionType.PASS.toString())) {
            nrOfPassInstances -= 1;
        }

        if (oldOpinion.equals(ActivitiCountersignedOpinionType.VETO.toString())) {
            nrOfVetoInstances -= 1;
        }

        if (oldOpinion.equals(ActivitiCountersignedOpinionType.ABSTAIN.toString())) {
            nrOfAbstainInstances -= 1;
        }

        if (opinionType.toString().equals(ActivitiCountersignedOpinionType.PASS.toString())) {
            nrOfPassInstances += 1;
        }

        if (opinionType.toString().equals(ActivitiCountersignedOpinionType.VETO.toString())) {
            nrOfVetoInstances += 1;
        }

        if (opinionType.toString().equals(ActivitiCountersignedOpinionType.ABSTAIN.toString())) {
            nrOfAbstainInstances += 1;
        }

        miExecution.setVariableLocal(MultiInstanceActivityBehavior.NUMBER_OF_PASS_INSTANCES, nrOfPassInstances);
        miExecution.setVariableLocal(MultiInstanceActivityBehavior.NUMBER_OF_VETO_INSTANCES, nrOfVetoInstances);
        miExecution.setVariableLocal(MultiInstanceActivityBehavior.NUMBER_OF_ABSTAIN_INSTANCES, nrOfAbstainInstances);

        opinions.put(assignee, opinionType.toString());
        miExecution.setVariableLocal(MultiInstanceActivityBehavior.PROCESSING_INFO, processingInfo);
        //负责人决定制度,能更改负责人意见，说明肯定是全部审批，处理完直接return，不用leave
        if (miActivityElement.getCounterSignConfig().getCounterSignPassType().equals(CounterSignPassTypeEnum.PASS_PRINCIPAL.toString())) {
            //通过负责人
            String oneBallotUserId = getStringVariable(miExecution, MultiInstanceActivityBehavior.PASS_PRINCIPAL);
            if (oneBallotUserId.equals(assignee)) {
                miExecution.setVariableLocal(MultiInstanceActivityBehavior.PRINCIPAL_OPINION, opinionType.toString());
            }
            return null;
        }

        if (miActivityElement.getCounterSignConfig().isVoteAll()) {
            return null;
        }

        if (!multiInstanceLoopCharacteristics.isSequential()) {
            //prallel 并行多实例行为
            ParallelMultiInstanceBehavior prallelMultiInstanceBehavior = (ParallelMultiInstanceBehavior) miActivityElement
                    .getBehavior();
            int nrOfInstances = getLoopVariable(miExecution, MultiInstanceActivityBehavior.NUMBER_OF_INSTANCES);
            task.getExecution().inactivate();
            prallelMultiInstanceBehavior.lockFirstParentScope(task.getExecution());
            if (prallelMultiInstanceBehavior.completionConditionSatisfied(task.getExecution().getParent())) {
                prallelMultiInstanceBehavior.countersigned(nrOfInstances, task.getExecution(), task.getExecution());
            }
        } else {
            //sequential 顺序多实例行为
            SequentialMultiInstanceBehavior prallelMultiInstanceBehavior = (SequentialMultiInstanceBehavior) miActivityElement
                    .getBehavior();
            if (prallelMultiInstanceBehavior.completionConditionSatisfied(miExecution)) {
                prallelMultiInstanceBehavior.countersigned(currentExecutionEntity, miExecution);
            }
        }
        return null;
    }

    public void updateComment(MultiInstanceLoopCharacteristics multiInstanceLoopCharacteristics, TaskEntity task, CommandContext commandContext) {
        HistoricTaskInstanceQueryImpl historicTaskInstanceQuery = new HistoricTaskInstanceQueryImpl();
        List<HistoricTaskInstance> histTasks = new ArrayList<>();
        if (!multiInstanceLoopCharacteristics.isSequential()) {//并行审批，executionId是不一致的
            historicTaskInstanceQuery.processInstanceId(task.getProcessInstanceId());
            historicTaskInstanceQuery.taskDefinitionKey(task.getTaskDefinitionKey());
            historicTaskInstanceQuery.taskAssignee(Authentication.getAuthenticatedUser().getDeptId());
            histTasks = historicTaskInstanceQuery.orderByTaskCreateTime().desc().list();
        } else {
            historicTaskInstanceQuery.executionId(task.getExecutionId());
            historicTaskInstanceQuery.taskAssignee(Authentication.getAuthenticatedUser().getDeptId());
            histTasks = historicTaskInstanceQuery.list();
        }
        if (histTasks.size() > 0) {
            String histTaskId = histTasks.get(0).getId();
            List<Comment> list = commandContext.getProcessEngineConfiguration().getTaskService().getTaskComments(histTaskId);
            if (list.size() > 0) {
                for (Comment comment : list) {
                    if (comment.getType().equals(Comment.COUNTERSIGNED)) {
                        CommentEntity commentEntity = (CommentEntity) comment;
                        commentEntity.setMessage(opinionType.toString());
                        commentEntity.setFullMessage(opinionType.toString());
                        commandContext.getProcessEngineConfiguration().getCommentDataManager().updateComment(commentEntity);
                    }
                }
            }
        }
    }

    protected DelegateExecution getMultiInstanceRootExecution(DelegateExecution executionEntity) {
        DelegateExecution multiInstanceRootExecution = null;
        DelegateExecution currentExecution = executionEntity;
        while (currentExecution != null && multiInstanceRootExecution == null && currentExecution.getParent() != null) {
            if (currentExecution.isMultiInstanceRoot()) {
                multiInstanceRootExecution = currentExecution;
            } else {
                currentExecution = currentExecution.getParent();
            }
        }
        return multiInstanceRootExecution;
    }

    protected Integer getLoopVariable(DelegateExecution execution, String variableName) {
        Object value = execution.getVariableLocal(variableName);
        DelegateExecution parent = execution.getParent();
        while (value == null && parent != null) {
            value = parent.getVariableLocal(variableName);
            parent = parent.getParent();
        }
        return (Integer) (value != null ? value : 0);
    }

    protected String getStringVariable(DelegateExecution execution, String variableName) {
        Object value = execution.getVariableLocal(variableName);
        DelegateExecution parent = execution.getParent();
        while (value == null && parent != null) {
            value = parent.getVariableLocal(variableName);
            parent = parent.getParent();
        }
        return (String) (value != null ? value : "");
    }


    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public ActivitiCountersignedOpinionType getOpinionType() {
        return opinionType;
    }

    public void setOpinionType(ActivitiCountersignedOpinionType opinionType) {
        this.opinionType = opinionType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


}
