/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.config.CustomEvent;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.task.Comment;
import com.je.bpm.engine.task.DelegationState;

import java.util.Map;

/**
 * 委托任务命令
 */
public class DelegateTaskCmd extends NeedsActiveTaskCmd<Object> {

    private static final long serialVersionUID = 1L;
    protected String userId;
    private String comment;
    private String assignee;
    private String files;

    public DelegateTaskCmd(String taskId, String prod, Map<String, Object> bean, String userId, String comment, String assignee, String files) {
        super(taskId, prod, bean);
        this.userId = userId;
        this.comment = comment;
        this.assignee = assignee;
        this.files = files;
    }

    @Override
    protected Object execute(CommandContext commandContext, TaskEntity task) {
        task.setVariable(task.getId() + "Delegate", task.getAssignee());
        BpmnModel bpmnModel = commandContext.getProcessEngineConfiguration().getRepositoryService().getBpmnModel(task.getProcessDefinitionId(), task.getProcessInstanceId(), task.getBusinessKey());

        executeBeforeHandler(bpmnModel, task, CustomEvent.CustomEventEnum.TASK_ENTRUST_BEFORE, comment, OperatorEnum.TASK_DELEGATE_OPERATOR, assignee);

        commandContext.getProcessEngineConfiguration().getTaskService().addComment(taskId, task.getProcessInstanceId(),
                Comment.CANDIDATE_USER_ID, comment);
        commandContext.getProcessEngineConfiguration().getTaskService().addComment(taskId, task.getProcessInstanceId(),
                Comment.FILES_COMMENT, files);
        commandContext.getProcessEngineConfiguration().getTaskService().addComment(taskId, task.getProcessInstanceId(),
                Comment.DELEGATE_USER_ID, userId);

        task.setDelegationState(DelegationState.PENDING);

        if (task.getOwner() == null) {
            task.setOwner(task.getAssignee());
        }
        commandContext.getTaskEntityManager().changeTaskAssignee(task, userId);
        executeAfterHandler(bpmnModel, task, CustomEvent.CustomEventEnum.TASK_ENTRUST_AFTER, comment, OperatorEnum.TASK_DELEGATE_OPERATOR, assignee);
        return null;
    }

    @Override
    protected String getSuspendedTaskException() {
        return "Cannot delegate a suspended task";
    }

}
