/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.task;

import com.je.bpm.engine.internal.Internal;
import java.util.Date;

/**
 * Represents one task for a human user.
 */
@Internal
public interface Task extends TaskInfo {

    /**
     * Default value used for priority when a new {@link Task} is created.
     */
    int DEFAULT_PRIORITY = 0;

    /**
     * Name or title of the task.
     */
    void setName(String name);

    /**
     * Sets an optional localized name for the task.
     */
    void setLocalizedName(String name);

    /**
     * Change the description of the task
     */
    void setDescription(String description);

    /**
     * Sets an optional localized description for the task.
     */
    void setLocalizedDescription(String description);

    /**
     * Sets the indication of how important/urgent this task is
     */
    void setPriority(int priority);

    /**
     * The {@link User.getId() userId} of the person that is responsible for this task.
     */
    void setOwner(String owner);

    /**
     * The {@link User.getId() userId} of the person to which this task is delegated.
     */
    void setAssignee(String assignee);

    /**
     * The current {@link DelegationState} for this task.
     */
    DelegationState getDelegationState();

    /**
     * The current {@link DelegationState} for this task.
     */
    void setDelegationState(DelegationState delegationState);

    /**
     * Change due date of the task.
     */
    void setDueDate(Date dueDate);

    /**
     * Change the category of the task. This is an optional field and allows to 'tag' tasks as belonging to a certain category.
     */
    void setCategory(String category);

    /**
     * the parent task for which this task is a subtask
     */
    void setParentTaskId(String parentTaskId);

    /**
     * Change the tenantId of the task
     */
    void setTenantId(String tenantId);

    /**
     * Change the form key of the task
     */
    void setFormKey(String formKey);

    /**
     * Indicates whether this task is suspended or not.
     */
    boolean isSuspended();

    Integer getAppVersion();

    void setAppVersion(Integer appVersion);

}
