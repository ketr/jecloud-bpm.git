/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.el;

import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.delegate.Expression;
import com.je.bpm.engine.delegate.VariableScope;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.delegate.invocation.ExpressionGetInvocation;
import com.je.bpm.engine.impl.delegate.invocation.ExpressionSetInvocation;
import com.je.bpm.engine.impl.interceptor.DelegateInterceptor;

import javax.el.ELContext;
import javax.el.MethodNotFoundException;
import javax.el.PropertyNotFoundException;
import javax.el.ValueExpression;
import java.util.Map;

/**
 * Expression implementation backed by a JUEL {@link ValueExpression}.
 * 由JUEL支持的表达式实现
 */
public class JuelExpression implements Expression {

    private String expressionText;
    private ValueExpression valueExpression;

    public JuelExpression(ValueExpression valueExpression, String expressionText) {
        this.valueExpression = valueExpression;
        this.expressionText = expressionText;
    }

    @Override
    public Object getValue(VariableScope variableScope) {
        ELContext elContext = Context.getProcessEngineConfiguration().getExpressionManager().getElContext(variableScope);
        return getValueFromContext(elContext, Context.getProcessEngineConfiguration().getDelegateInterceptor());
    }

    @Override
    public void setValue(Object value, VariableScope variableScope) {
        ELContext elContext = Context.getProcessEngineConfiguration().getExpressionManager().getElContext(variableScope);
        try {
            ExpressionSetInvocation invocation = new ExpressionSetInvocation(valueExpression, elContext, value);
            Context.getProcessEngineConfiguration().getDelegateInterceptor().handleInvocation(invocation);
        } catch (Exception e) {
            throw new ActivitiException("Error while evaluating expression: " + expressionText, e);
        }
    }

    @Override
    public String toString() {
        if (valueExpression != null) {
            return valueExpression.getExpressionString();
        }
        return super.toString();
    }

    @Override
    public String getExpressionText() {
        return expressionText;
    }

    @Override
    public Object getValue(ExpressionManager expressionManager, DelegateInterceptor delegateInterceptor, Map<String, Object> availableVariables) {
        ELContext elContext = expressionManager.getElContext(availableVariables);
        return getValueFromContext(elContext, delegateInterceptor);
    }

    private Object getValueFromContext(ELContext elContext, DelegateInterceptor delegateInterceptor) {
        try {
            ExpressionGetInvocation invocation = new ExpressionGetInvocation(valueExpression, elContext);
            delegateInterceptor.handleInvocation(invocation);
            return invocation.getInvocationResult();
        } catch (PropertyNotFoundException pnfe) {
            throw new ActivitiException("Unknown property used in expression: " + expressionText, pnfe);
        } catch (MethodNotFoundException mnfe) {
            throw new ActivitiException("Unknown method used in expression: " + expressionText, mnfe);
        } catch (Exception ele) {
            throw new ActivitiException("Error while evaluating expression: " + expressionText, ele);
        }
    }
}
