/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

public enum SubmitTypeEnum {
    START("启动"),
    SPONSOR("发起"),
    SUBMIT("送交"),
    DIRECTSEND("直送"),
    TRANSFER("转办"),
    CANCELDELEGATE("取消委托"),
    DELEGATE("委托"),
    RETRIEVE("取回"),
    WITHDRAW("撤回"),
    GOBACK("退回"),
    DISMISS("驳回"),
    INVALID("作废"),
    PASSROUND("传阅"),
    PASSROUNDREAD("已阅"),
    RECEIVE("领取"),
    PASS("通过"),
    VETO("否决"),
    REBOOK("改签"),
    ABSTAIN("弃权"),
    CHANGEASSIGNEE("更换负责人"),
    CANCEL("撤销"),
    ADD("加签"),
    REDUCE("减签"),
    END("结束"),
    ADJUST("调整"),
    OUTGOING("节点流出事件");

    String name;

    SubmitTypeEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static String getNameByType(String type) {
        if (type.equals(START.toString())) {
            return START.getName();
        } else if (type.equals(SUBMIT.toString())) {
            return SUBMIT.getName();
        } else if (type.equals(SPONSOR.toString())) {
            return SPONSOR.getName();
        } else if (type.equals(DIRECTSEND.toString())) {
            return DIRECTSEND.getName();
        } else if (type.equals(TRANSFER.toString())) {
            return TRANSFER.getName();
        } else if (type.equals(CANCELDELEGATE.toString())) {
            return CANCELDELEGATE.getName();
        } else if (type.equals(DELEGATE.toString())) {
            return DELEGATE.getName();
        } else if (type.equals(RETRIEVE.toString())) {
            return RETRIEVE.getName();
        } else if (type.equals(GOBACK.toString())) {
            return GOBACK.getName();
        } else if (type.equals(DISMISS.toString())) {
            return DISMISS.getName();
        } else if (type.equals(INVALID.toString())) {
            return INVALID.getName();
        } else if (type.equals(PASSROUND.toString())) {
            return PASSROUND.getName();
        } else if (type.equals(PASSROUNDREAD.toString())) {
            return PASSROUNDREAD.getName();
        } else if (type.equals(RECEIVE.toString())) {
            return RECEIVE.getName();
        } else if (type.equals(PASS.toString())) {
            return PASS.getName();
        } else if (type.equals(VETO.toString())) {
            return VETO.getName();
        } else if (type.equals(ABSTAIN.toString())) {
            return ABSTAIN.getName();
        } else if (type.equals(CHANGEASSIGNEE.toString())) {
            return CHANGEASSIGNEE.getName();
        } else if (type.equals(CANCEL.toString())) {
            return CANCEL.getName();
        } else if (type.equals(ADD.toString())) {
            return ADD.getName();
        } else if (type.equals(REDUCE.toString())) {
            return REDUCE.getName();
        } else if (type.equals(END.toString())) {
            return END.getName();
        } else if (type.equals(WITHDRAW.toString())) {
            return WITHDRAW.getName();
        }
        return "";
    }


}
