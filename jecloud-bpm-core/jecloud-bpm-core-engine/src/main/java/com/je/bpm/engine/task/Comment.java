/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.task;

import com.je.bpm.engine.history.HistoricData;
import com.je.bpm.engine.internal.Internal;

import java.util.Date;

/**
 * User comments that form discussions around tasks.
 * 围绕任务形成的用户评论，评论不属于流程/任务运行时
 *
 * @see {@link com.je.bpm.engine.TaskService#getTaskComments(String)
 * @deprecated this interface and its implementations are going to be removed in future iterations
 * Comments doesn't belong to the Process/Task Runtime
 */
@Internal
public interface Comment extends HistoricData {

    public String USER_COMMENT = "user";
    public String FILES_COMMENT = "files";
    public String USER_STAGING_COMMENT = "userStaging";
    public String NODE_TYPE = "node";
    public String COUNTERSIGNED = "countersigned";
    public String CANDIDATE = "candidateInfo";
    //委托人
    public String CANDIDATE_USER_ID = "candidateUserId";
    //被委托人
    public String DELEGATE_USER_ID = "delegateUserId";
    //取消委托人
    public String CANCEL_CANDIDATE_USER_ID = "cancelCandidateUserId";
    //转办人
    public String TRANSFER_USER_ID = "transferUserId";
    //被转办人
    public String DELEGATE_TRANSFER_USER_ID = "delegateTransferUserId";
    //回签人员
    public String SIGN_BACK_USER_ID = "signBackUserId";
    //加签操作人
    public String COUNTERSIGN_USER_ID = "countersignuUserId";

    /**
     * unique identifier for this comment
     */
    String getId();

    /**
     * reference to the user that made the comment
     */
    String getUserId();

    /**
     * time and date when the user made the comment
     */
    @Override
    Date getTime();

    /**
     * reference to the task on which this comment was made
     */
    String getTaskId();

    /**
     * reference to the process instance on which this comment was made
     */
    String getProcessInstanceId();

    /**
     * reference to the type given to the comment
     */
    String getType();

    /**
     * the full comment message the user had related to the task and/or process instance
     *
     * @see com.je.bpm.engine.TaskService#getTaskComments(String)
     */
    String getFullMessage();
}
