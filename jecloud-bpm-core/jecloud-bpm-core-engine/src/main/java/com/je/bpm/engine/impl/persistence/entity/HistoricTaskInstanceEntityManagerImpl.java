/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.history.HistoricTaskInstance;
import com.je.bpm.engine.impl.HistoricTaskInstanceQueryImpl;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.history.HistoryLevel;
import com.je.bpm.engine.impl.persistence.entity.data.DataManager;
import com.je.bpm.engine.impl.persistence.entity.data.HistoricTaskInstanceDataManager;

import java.util.List;
import java.util.Map;

import static java.util.Collections.emptyList;

/**
 */
public class HistoricTaskInstanceEntityManagerImpl extends AbstractEntityManager<HistoricTaskInstanceEntity> implements HistoricTaskInstanceEntityManager {

  protected HistoricTaskInstanceDataManager historicTaskInstanceDataManager;

  public HistoricTaskInstanceEntityManagerImpl(ProcessEngineConfigurationImpl processEngineConfiguration, HistoricTaskInstanceDataManager historicTaskInstanceDataManager) {
    super(processEngineConfiguration);
    this.historicTaskInstanceDataManager = historicTaskInstanceDataManager;
  }

  @Override
  protected DataManager<HistoricTaskInstanceEntity> getDataManager() {
      return historicTaskInstanceDataManager;
  }

  @Override
  public HistoricTaskInstanceEntity create(TaskEntity task, ExecutionEntity execution) {
    return historicTaskInstanceDataManager.create(task, execution);
  }

  @Override
  public void deleteHistoricTaskInstancesByProcessInstanceId(String processInstanceId) {
    if (getHistoryManager().isHistoryLevelAtLeast(HistoryLevel.AUDIT)) {
      List<HistoricTaskInstanceEntity> taskInstances = historicTaskInstanceDataManager.findHistoricTaskInstanceByProcessInstanceId(processInstanceId);
      for (HistoricTaskInstanceEntity historicTaskInstanceEntity : taskInstances) {
        delete(historicTaskInstanceEntity.getId()); // Needs to be by id (since that method is overridden, see below !)
      }
    }
  }

  @Override
  public long findHistoricTaskInstanceCountByQueryCriteria(HistoricTaskInstanceQueryImpl historicTaskInstanceQuery) {
    if (getHistoryManager().isHistoryEnabled()) {
      return historicTaskInstanceDataManager.findHistoricTaskInstanceCountByQueryCriteria(historicTaskInstanceQuery);
    }
    return 0;
  }

  @Override
  @SuppressWarnings("unchecked")
  public List<HistoricTaskInstance> findHistoricTaskInstancesByQueryCriteria(HistoricTaskInstanceQueryImpl historicTaskInstanceQuery) {
    if (getHistoryManager().isHistoryEnabled()) {
      return historicTaskInstanceDataManager.findHistoricTaskInstancesByQueryCriteria(historicTaskInstanceQuery);
    }
    return emptyList();
  }

  @Override
  @SuppressWarnings("unchecked")
  public List<HistoricTaskInstance> findHistoricTaskInstancesAndVariablesByQueryCriteria(HistoricTaskInstanceQueryImpl historicTaskInstanceQuery) {
    if (getHistoryManager().isHistoryEnabled()) {
     return historicTaskInstanceDataManager.findHistoricTaskInstancesAndVariablesByQueryCriteria(historicTaskInstanceQuery);
    }
    return emptyList();
  }

  @Override
  public void delete(String id) {
    if (getHistoryManager().isHistoryEnabled()) {
      HistoricTaskInstanceEntity historicTaskInstance = findById(id);
      if (historicTaskInstance != null) {

        List<HistoricTaskInstanceEntity> subTasks = historicTaskInstanceDataManager.findHistoricTasksByParentTaskId(historicTaskInstance.getId());
        for (HistoricTaskInstance subTask: subTasks) {
          delete(subTask.getId());
        }

        getHistoricDetailEntityManager().deleteHistoricDetailsByTaskId(id);
        getHistoricVariableInstanceEntityManager().deleteHistoricVariableInstancesByTaskId(id);
        getCommentEntityManager().deleteCommentsByTaskId(id);
        getAttachmentEntityManager().deleteAttachmentsByTaskId(id);
        getHistoricIdentityLinkEntityManager().deleteHistoricIdentityLinksByTaskId(id);

        delete(historicTaskInstance);
      }
    }
  }

  @Override
  public List<HistoricTaskInstance> findHistoricTaskInstancesByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults) {
    return historicTaskInstanceDataManager.findHistoricTaskInstancesByNativeQuery(parameterMap, firstResult, maxResults);
  }

  @Override
  public long findHistoricTaskInstanceCountByNativeQuery(Map<String, Object> parameterMap) {
    return historicTaskInstanceDataManager.findHistoricTaskInstanceCountByNativeQuery(parameterMap);
  }

  public HistoricTaskInstanceDataManager getHistoricTaskInstanceDataManager() {
    return historicTaskInstanceDataManager;
  }

  public void setHistoricTaskInstanceDataManager(HistoricTaskInstanceDataManager historicTaskInstanceDataManager) {
    this.historicTaskInstanceDataManager = historicTaskInstanceDataManager;
  }

}
