/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl;

import com.je.bpm.engine.ActivitiIllegalArgumentException;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.interceptor.CommandExecutor;
import com.je.bpm.engine.impl.variable.VariableTypes;
import com.je.bpm.engine.query.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract query class that adds methods to query for variable values.
 * 需要查询变量的查询需要继承此抽象实现
 */
public abstract class AbstractVariableQueryImpl<T extends Query<?, ?>, U> extends AbstractQuery<T, U> {

  private static final long serialVersionUID = 1L;

  protected List<QueryVariableValue> queryVariableValues = new ArrayList<QueryVariableValue>();

  public AbstractVariableQueryImpl() {
  }

  public AbstractVariableQueryImpl(CommandContext commandContext) {
    super(commandContext);
  }

  public AbstractVariableQueryImpl(CommandExecutor commandExecutor) {
    super(commandExecutor);
  }

  @Override
  public abstract long executeCount(CommandContext commandContext);

  @Override
  public abstract List<U> executeList(CommandContext commandContext, Page page);

  public T variableValueEquals(String name, Object value) {
    return variableValueEquals(name, value, true);
  }

  @SuppressWarnings("unchecked")
  protected T variableValueEquals(String name, Object value, boolean localScope) {
    addVariable(name, value, QueryOperator.EQUALS, localScope);
    return (T) this;
  }

  public T variableValueEquals(Object value) {
    return variableValueEquals(value, true);
  }

  @SuppressWarnings("unchecked")
  protected T variableValueEquals(Object value, boolean localScope) {
    queryVariableValues.add(new QueryVariableValue(null, value, QueryOperator.EQUALS, localScope));
    return (T) this;
  }

  public T variableValueEqualsIgnoreCase(String name, String value) {
    return variableValueEqualsIgnoreCase(name, value, true);
  }

  @SuppressWarnings("unchecked")
  protected T variableValueEqualsIgnoreCase(String name, String value, boolean localScope) {
    if(value == null) {
      throw new ActivitiIllegalArgumentException("value is null");
    }
    addVariable(name, value.toLowerCase(), QueryOperator.EQUALS_IGNORE_CASE, localScope);
    return (T) this;
  }

  public T variableValueNotEqualsIgnoreCase(String name, String value) {
    return variableValueNotEqualsIgnoreCase(name, value, true);
  }

  @SuppressWarnings("unchecked")
  protected T variableValueNotEqualsIgnoreCase(String name, String value, boolean localScope) {
    if(value == null) {
      throw new ActivitiIllegalArgumentException("value is null");
    }
    addVariable(name, value.toLowerCase(), QueryOperator.NOT_EQUALS_IGNORE_CASE, localScope);
    return (T) this;
  }

  public T variableValueNotEquals(String name, Object value) {
    return variableValueNotEquals(name, value, true);
  }

  @SuppressWarnings("unchecked")
  protected T variableValueNotEquals(String name, Object value, boolean localScope) {
    addVariable(name, value, QueryOperator.NOT_EQUALS, localScope);
    return (T) this;
  }

  public T variableValueGreaterThan(String name, Object value) {
    return variableValueGreaterThan(name, value, true);
  }

  @SuppressWarnings("unchecked")
  protected T variableValueGreaterThan(String name, Object value, boolean localScope) {
    addVariable(name, value, QueryOperator.GREATER_THAN, localScope);
    return (T) this;
  }

  public T variableValueGreaterThanOrEqual(String name, Object value) {
    return variableValueGreaterThanOrEqual(name, value, true);
  }

  @SuppressWarnings("unchecked")
  protected T variableValueGreaterThanOrEqual(String name, Object value, boolean localScope) {
    addVariable(name, value, QueryOperator.GREATER_THAN_OR_EQUAL, localScope);
    return (T) this;
  }

  public T variableValueLessThan(String name, Object value) {
    return variableValueLessThan(name, value, true);
  }

  @SuppressWarnings("unchecked")
  protected T variableValueLessThan(String name, Object value, boolean localScope) {
    addVariable(name, value, QueryOperator.LESS_THAN, localScope);
    return (T) this;
  }

  public T variableValueLessThanOrEqual(String name, Object value) {
    return variableValueLessThanOrEqual(name, value, true);
  }

  @SuppressWarnings("unchecked")
  protected T variableValueLessThanOrEqual(String name, Object value, boolean localScope) {
    addVariable(name, value, QueryOperator.LESS_THAN_OR_EQUAL, localScope);
    return (T) this;
  }

  public T variableValueLike(String name, String value) {
    return variableValueLike(name, value, true);
  }

  public T variableValueLikeIgnoreCase(String name, String value) {
    return variableValueLikeIgnoreCase(name, value, true);
  }

  @SuppressWarnings("unchecked")
  protected T variableValueLike(String name, String value, boolean localScope) {
    addVariable(name, value, QueryOperator.LIKE, localScope);
    return (T) this;
  }

  @SuppressWarnings("unchecked")
  protected T variableValueLikeIgnoreCase(String name, String value, boolean localScope) {
    addVariable(name, value.toLowerCase(), QueryOperator.LIKE_IGNORE_CASE, localScope);
    return (T) this;
  }

  protected void addVariable(String name, Object value, QueryOperator operator, boolean localScope) {
    if(name == null) {
      throw new ActivitiIllegalArgumentException("name is null");
    }
    if (value == null || isBoolean(value)) {
      // Null-values and booleans can only be used in EQUALS and
      // NOT_EQUALS
      switch (operator) {
      case GREATER_THAN:
        throw new ActivitiIllegalArgumentException("Booleans and null cannot be used in 'greater than' condition");
      case LESS_THAN:
        throw new ActivitiIllegalArgumentException("Booleans and null cannot be used in 'less than' condition");
      case GREATER_THAN_OR_EQUAL:
        throw new ActivitiIllegalArgumentException("Booleans and null cannot be used in 'greater than or equal' condition");
      case LESS_THAN_OR_EQUAL:
        throw new ActivitiIllegalArgumentException("Booleans and null cannot be used in 'less than or equal' condition");
      }

      if (operator == QueryOperator.EQUALS_IGNORE_CASE && !(value instanceof String)) {
        throw new ActivitiIllegalArgumentException("Only string values can be used with 'equals ignore case' condition");
      }

      if (operator == QueryOperator.NOT_EQUALS_IGNORE_CASE && !(value instanceof String)) {
        throw new ActivitiIllegalArgumentException("Only string values can be used with 'not equals ignore case' condition");
      }

      if((operator == QueryOperator.LIKE || operator == QueryOperator.LIKE_IGNORE_CASE) && !(value instanceof String))
      {
        throw new ActivitiIllegalArgumentException("Only string values can be used with 'like' condition");
      }
    }
    queryVariableValues.add(new QueryVariableValue(name, value, operator, localScope));
  }

  protected boolean isBoolean(Object value) {
    if (value == null) {
      return false;
    }
    return Boolean.class.isAssignableFrom(value.getClass()) || boolean.class.isAssignableFrom(value.getClass());
  }

  protected void ensureVariablesInitialized() {
    if (!queryVariableValues.isEmpty()) {
      VariableTypes variableTypes = Context.getProcessEngineConfiguration().getVariableTypes();
      for (QueryVariableValue queryVariableValue : queryVariableValues) {
        queryVariableValue.initialize(variableTypes);
      }
    }
  }

  public List<QueryVariableValue> getQueryVariableValues() {
    return queryVariableValues;
  }

  public boolean hasLocalQueryVariableValue() {
    for (QueryVariableValue qvv : queryVariableValues) {
        if (qvv.isLocal()) {
            return true;
        }
    }
    return false;
  }

  public boolean hasNonLocalQueryVariableValue() {
    for (QueryVariableValue qvv : queryVariableValues) {
        if (!qvv.isLocal()) {
            return true;
        }
    }
    return false;
  }

}
