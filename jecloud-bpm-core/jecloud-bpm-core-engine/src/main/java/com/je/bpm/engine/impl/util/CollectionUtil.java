/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.util;

import com.je.bpm.engine.ActivitiIllegalArgumentException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * helper/convenience methods for working with collections.
 */
public class CollectionUtil {

    // No need to instantiate
    private CollectionUtil() {
    }

    /**
     * Helper method that creates a singleton map.
     * <p>
     * Alternative for singletonMap()), since that method returns a generic typed map <K,T> depending on the input type, but we often need a <String, Object> map.
     */
    public static Map<String, Object> singletonMap(String key, Object value) {
        Map<String, Object> map = new HashMap<>();
        map.put(key, value);
        return map;
    }

    /**
     * Helper method to easily create a map with keys of type String and values of type Object. Null values are allowed.
     *
     * @param objects varargs containing the key1, value1, key2, value2, etc. Note: although an Object, we will cast the key to String internally
     * @throws com.je.bpm.engine.ActivitiIllegalArgumentException when objects are not even or key/value are not expected types
     */
    public static Map<String, Object> map(Object... objects) {
        return mapOfClass(Object.class, objects);
    }

    /**
     * Helper method to easily create a map with keys of type String and values of a given Class. Null values are allowed.
     *
     * @param clazz   the target Value class
     * @param objects varargs containing the key1, value1, key2, value2, etc. Note: although an Object, we will cast the key to String internally
     * @throws com.je.bpm.engine.ActivitiIllegalArgumentException when objects are not even or key/value are not expected types
     */
    public static <T> Map<String, T> mapOfClass(Class<T> clazz, Object... objects) {
        if (objects.length % 2 != 0) {
            throw new ActivitiIllegalArgumentException("The input should always be even since we expect a list of key-value pairs!");
        }

        Map<String, T> map = new HashMap();
        for (int i = 0; i < objects.length; i += 2) {
            int keyIndex = i;
            int valueIndex = i + 1;
            Object key = objects[keyIndex];
            Object value = objects[valueIndex];
            if (!String.class.isInstance(key)) {
                throw new ActivitiIllegalArgumentException("key at index " + keyIndex + " should be a String but is a " + key.getClass());
            }
            if (value != null && !clazz.isInstance(value)) {
                throw new ActivitiIllegalArgumentException("value at index " + valueIndex + " should be a " + clazz + " but is a " + value.getClass());
            }
            map.put((String) key, (T) value);
        }
        return map;
    }

    public static boolean isEmpty(Collection<?> collection) {
        return (collection == null || collection.isEmpty());
    }

    public static boolean isNotEmpty(Collection<?> collection) {
        return !isEmpty(collection);
    }

}
