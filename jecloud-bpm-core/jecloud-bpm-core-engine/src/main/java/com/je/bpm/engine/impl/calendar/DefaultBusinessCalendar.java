/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.calendar;

import com.je.bpm.engine.ActivitiIllegalArgumentException;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.internal.Internal;
import java.util.*;

@Internal
public class DefaultBusinessCalendar implements BusinessCalendar {

    private static Map<String, Integer> units = new HashMap<String, Integer>();

    static {
        units.put("millis", Calendar.MILLISECOND);
        units.put("seconds", Calendar.SECOND);
        units.put("second", Calendar.SECOND);
        units.put("minute", Calendar.MINUTE);
        units.put("minutes", Calendar.MINUTE);
        units.put("hour", Calendar.HOUR);
        units.put("hours", Calendar.HOUR);
        units.put("day", Calendar.DAY_OF_YEAR);
        units.put("days", Calendar.DAY_OF_YEAR);
        units.put("week", Calendar.WEEK_OF_YEAR);
        units.put("weeks", Calendar.WEEK_OF_YEAR);
        units.put("month", Calendar.MONTH);
        units.put("months", Calendar.MONTH);
        units.put("year", Calendar.YEAR);
        units.put("years", Calendar.YEAR);
    }

    @Override
    public Date resolveDuedate(String duedateDescription, int maxIterations) {
        return resolveDuedate(duedateDescription);
    }

    @Override
    public Date resolveDuedate(String duedate) {
        Date resolvedDuedate = Context.getProcessEngineConfiguration().getClock().getCurrentTime();

        String[] tokens = duedate.split(" and ");
        for (String token : tokens) {
            resolvedDuedate = addSingleUnitQuantity(resolvedDuedate, token);
        }

        return resolvedDuedate;
    }

    @Override
    public Boolean validateDuedate(String duedateDescription, int maxIterations, Date endDate, Date newTimer) {
        return true;
    }

    @Override
    public Date resolveEndDate(String endDate) {
        return null;
    }

    protected Date addSingleUnitQuantity(Date startDate, String singleUnitQuantity) {
        int spaceIndex = singleUnitQuantity.indexOf(" ");
        if (spaceIndex == -1 || singleUnitQuantity.length() < spaceIndex + 1) {
            throw new ActivitiIllegalArgumentException("invalid duedate format: " + singleUnitQuantity);
        }

        String quantityText = singleUnitQuantity.substring(0, spaceIndex);
        Integer quantity = new Integer(quantityText);

        String unitText = singleUnitQuantity.substring(spaceIndex + 1).trim().toLowerCase();

        int unit = units.get(unitText);

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(startDate);
        calendar.add(unit, quantity);

        return calendar.getTime();
    }
}
