/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.impl.ExecutionQueryImpl;
import com.je.bpm.engine.impl.Page;
import com.je.bpm.engine.impl.ProcessInstanceQueryImpl;
import com.je.bpm.engine.internal.Internal;
import com.je.bpm.engine.repository.ProcessDefinition;
import com.je.bpm.engine.runtime.Execution;
import com.je.bpm.engine.runtime.ProcessInstance;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Internal
public interface ExecutionEntityManager extends EntityManager<ExecutionEntity> {

    ExecutionEntity createProcessInstanceExecution(ProcessDefinition processDefinition, String businessKey, String tenantId, String initiatorVariableName);

    ExecutionEntity createChildExecution(ExecutionEntity parentExecutionEntity);

    ExecutionEntity createSubprocessInstance(ProcessDefinition processDefinition, ExecutionEntity superExecutionEntity, String businessKey);

    /**
     * Finds the {@link ExecutionEntity} for the given root process instance id.
     * All children will have been fetched and initialized.
     */
    ExecutionEntity findByRootProcessInstanceId(String rootProcessInstanceId);

    ExecutionEntity findSubProcessInstanceBySuperExecutionId(String superExecutionId);

    List<ExecutionEntity> findChildExecutionsByParentExecutionId(String parentExecutionId);

    List<ExecutionEntity> findChildExecutionsByProcessInstanceId(String processInstanceId);

    List<ExecutionEntity> findExecutionsByParentExecutionAndActivityIds(String parentExecutionId, Collection<String> activityIds);

    long findExecutionCountByQueryCriteria(ExecutionQueryImpl executionQuery);

    List<ExecutionEntity> findExecutionsByQueryCriteria(ExecutionQueryImpl executionQuery, Page page);

    long findProcessInstanceCountByQueryCriteria(ProcessInstanceQueryImpl executionQuery);

    List<ProcessInstance> findProcessInstanceByQueryCriteria(ProcessInstanceQueryImpl executionQuery);

    List<ProcessInstance> findProcessInstanceAndVariablesByQueryCriteria(ProcessInstanceQueryImpl executionQuery);

    Collection<ExecutionEntity> findInactiveExecutionsByProcessInstanceId(String processInstanceId);

    Collection<ExecutionEntity> findInactiveExecutionsByActivityIdAndProcessInstanceId(String activityId, String processInstanceId);

    List<Execution> findExecutionsByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults);

    List<ProcessInstance> findProcessInstanceByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults);

    long findExecutionCountByNativeQuery(Map<String, Object> parameterMap);


    /**
     * Returns all child executions of a given {@link ExecutionEntity}.
     * In the list, child executions will be behind parent executions.
     */
    List<ExecutionEntity> collectChildren(ExecutionEntity executionEntity);

    ExecutionEntity findFirstScope(ExecutionEntity executionEntity);

    ExecutionEntity findFirstMultiInstanceRoot(ExecutionEntity executionEntity);


    void updateExecutionTenantIdForDeployment(String deploymentId, String newTenantId);

    String updateProcessInstanceBusinessKey(ExecutionEntity executionEntity, String businessKey);

    ExecutionEntity updateProcessInstanceStartDate(ExecutionEntity processInstanceExecution);

    void deleteProcessInstancesByProcessDefinition(String processDefinitionId, String deleteReason, boolean cascade);

    void deleteProcessInstance(String processInstanceId, String deleteReason, boolean cascade);

    void deleteProcessInstanceExecutionEntity(String processInstanceId, String currentFlowElementId,
                                              String deleteReason, boolean cascade, boolean cancel);

    void deleteChildExecutions(ExecutionEntity executionEntity, String deleteReason);

    void cancelChildExecutions(ExecutionEntity executionEntity, String deleteReason);

    void deleteExecutionAndRelatedData(ExecutionEntity executionEntity, String deleteReason);

    void cancelExecutionAndRelatedData(ExecutionEntity executionEntity, String deleteReason);

    void updateProcessInstanceLockTime(String processInstanceId);

    void clearProcessInstanceLockTime(String processInstanceId);

}
