/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.impl.db.BulkDeleteable;
import com.je.bpm.engine.impl.variable.ValueFields;
import com.je.bpm.engine.impl.variable.VariableType;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class VariableInstanceEntityImpl extends AbstractEntity implements VariableInstanceEntity, ValueFields, BulkDeleteable, Serializable {

    private static final long serialVersionUID = 1L;

    protected String name;
    protected VariableType type;
    protected String typeName;

    protected String processInstanceId;
    protected String executionId;
    protected String taskId;

    protected Long longValue;
    protected Double doubleValue;
    protected String textValue;
    protected String textValue2;
    protected ByteArrayRef byteArrayRef;

    protected Object cachedValue;
    protected boolean forcedUpdate;
    protected boolean deleted;

    public VariableInstanceEntityImpl() {

    }

    @Override
    public Object getPersistentState() {
        Map<String, Object> persistentState = new HashMap<String, Object>();
        if (longValue != null) {
            persistentState.put("longValue", longValue);
        }
        if (doubleValue != null) {
            persistentState.put("doubleValue", doubleValue);
        }
        if (textValue != null) {
            persistentState.put("textValue", textValue);
        }
        if (textValue2 != null) {
            persistentState.put("textValue2", textValue2);
        }
        if (byteArrayRef != null && byteArrayRef.getId() != null) {
            persistentState.put("byteArrayValueId", byteArrayRef.getId());
        }
        if (forcedUpdate) {
            persistentState.put("forcedUpdate", Boolean.TRUE);
        }
        return persistentState;
    }

    @Override
    public void setExecution(ExecutionEntity execution) {
        this.executionId = execution.getId();
        this.processInstanceId = execution.getProcessInstanceId();
        forceUpdate();
    }

    @Override
    public void forceUpdate() {
        forcedUpdate = true;
    }

    @Override
    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    @Override
    public void setExecutionId(String executionId) {
        this.executionId = executionId;
    }

    // byte array value ///////////////////////////////////////////////////////////

    @Override
    public byte[] getBytes() {
        ensureByteArrayRefInitialized();
        return byteArrayRef.getBytes(false);
    }

    @Override
    public void setBytes(byte[] bytes) {
        ensureByteArrayRefInitialized();
        byteArrayRef.setValue("var-" + name, bytes,false);
    }

    @Override
    public ByteArrayRef getByteArrayRef() {
        return byteArrayRef;
    }

    protected void ensureByteArrayRefInitialized() {
        if (byteArrayRef == null) {
            byteArrayRef = new ByteArrayRef();
        }
    }

    // value //////////////////////////////////////////////////////////////////////
    @Override
    public Object getValue() {
        if (!type.isCachable() || cachedValue == null) {
            cachedValue = type.getValue(this);
        }
        return cachedValue;
    }

    @Override
    public void setValue(Object value) {
        type.setValue(value, this);
        typeName = type.getTypeName();
        cachedValue = value;
    }

    // getters and setters ////////////////////////////////////////////////////////
    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getTypeName() {
        if (typeName != null) {
            return typeName;
        } else if (type != null) {
            return type.getTypeName();
        } else {
            return typeName;
        }
    }

    @Override
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public VariableType getType() {
        return type;
    }

    @Override
    public void setType(VariableType type) {
        this.type = type;
    }

    @Override
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    @Override
    public String getTaskId() {
        return taskId;
    }

    @Override
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public String getExecutionId() {
        return executionId;
    }

    @Override
    public Long getLongValue() {
        return longValue;
    }

    @Override
    public void setLongValue(Long longValue) {
        this.longValue = longValue;
    }

    @Override
    public Double getDoubleValue() {
        return doubleValue;
    }

    @Override
    public void setDoubleValue(Double doubleValue) {
        this.doubleValue = doubleValue;
    }

    @Override
    public String getTextValue() {
        return textValue;
    }

    @Override
    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }

    @Override
    public String getTextValue2() {
        return textValue2;
    }

    @Override
    public void setTextValue2(String textValue2) {
        this.textValue2 = textValue2;
    }

    @Override
    public Object getCachedValue() {
        return cachedValue;
    }

    @Override
    public void setCachedValue(Object cachedValue) {
        this.cachedValue = cachedValue;
    }

    // misc methods ///////////////////////////////////////////////////////////////

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("VariableInstanceEntity[");
        sb.append("id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", type=").append(type != null ? type.getTypeName() : "null");
        if (longValue != null) {
            sb.append(", longValue=").append(longValue);
        }
        if (doubleValue != null) {
            sb.append(", doubleValue=").append(doubleValue);
        }
        if (textValue != null) {
            sb.append(", textValue=").append(StringUtils.abbreviate(textValue, 40));
        }
        if (textValue2 != null) {
            sb.append(", textValue2=").append(StringUtils.abbreviate(textValue2, 40));
        }
        if (byteArrayRef != null && byteArrayRef.getId() != null) {
            sb.append(", byteArrayValueId=").append(byteArrayRef.getId());
        }
        sb.append("]");
        return sb.toString();
    }

}
