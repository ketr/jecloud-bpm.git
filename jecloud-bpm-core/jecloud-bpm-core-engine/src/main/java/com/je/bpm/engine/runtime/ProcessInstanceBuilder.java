/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.runtime;

import com.je.bpm.engine.internal.Internal;

import java.util.List;
import java.util.Map;

/**
 * Helper for starting new ProcessInstance.
 * <p>
 * An instance can be obtained through {@link com.je.bpm.engine.RuntimeService#createProcessInstanceBuilder()}.
 * <p>
 * processDefinitionId or processDefinitionKey should be set before calling {@link #start()} to start a process instance.
 */
@Internal
public interface ProcessInstanceBuilder {

    /**
     * Set the id of the process definition
     **/
    ProcessInstanceBuilder processDefinitionId(String processDefinitionId);

    /**
     * Set the key of the process definition, latest version of the process definition with the given key.
     * If processDefinitionId was set this will be ignored
     **/
    ProcessInstanceBuilder processDefinitionKey(String processDefinitionKey);

    /**
     * Set the message name that needs to be used to look up the process definition that needs to be used to start the process instance.
     */
    ProcessInstanceBuilder messageName(String messageName);

    /**
     * Set the name of process instance
     **/
    ProcessInstanceBuilder name(String processInstanceName);

    /**
     * Set the businessKey of process instance
     **/
    ProcessInstanceBuilder businessKey(String businessKey);

    /**
     * Set the firstUser of process instance
     **/
    ProcessInstanceBuilder firstUser(String firstUser);

    ProcessInstanceBuilder assigneeList(String assignee);

    /**
     * Set the tenantId of process instance
     **/
    ProcessInstanceBuilder tenantId(String tenantId);

    /**
     * Sets the process variables
     */
    ProcessInstanceBuilder variables(Map<String, Object> variables);

    /**
     * Adds a variable to the process instance
     **/
    ProcessInstanceBuilder variable(String variableName, Object value);

    /**
     * Sets the transient variables
     */
    ProcessInstanceBuilder transientVariables(Map<String, Object> transientVariables);

    /**
     * Adds a transient variable to the process instance
     */
    ProcessInstanceBuilder transientVariable(String variableName, Object value);

    /**
     * Start the process instance
     *
     * @throws com.je.bpm.engine.ActivitiIllegalArgumentException if processDefinitionKey and processDefinitionId are null
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException  when no process definition is deployed with the given processDefinitionKey or processDefinitionId
     **/
    ProcessInstance start();

    /**
     * Create the process instance
     *
     * @throws com.je.bpm.engine.ActivitiIllegalArgumentException if processDefinitionKey and processDefinitionId are null
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException  when no process definition is deployed with the given processDefinitionKey or processDefinitionId
     **/
    ProcessInstance create();

}
