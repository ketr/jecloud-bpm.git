/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.event.logger.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.je.bpm.engine.delegate.event.ActivitiEntityEvent;
import com.je.bpm.engine.delegate.event.ActivitiEvent;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.engine.impl.persistence.entity.EventLogEntryEntity;
import com.je.bpm.engine.impl.util.ProcessDefinitionUtil;
import com.je.bpm.engine.repository.ProcessDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Date;
import java.util.Map;

/**
 *
 */
public abstract class AbstractDatabaseEventLoggerEventHandler implements EventLoggerEventHandler {

    private static final Logger logger = LoggerFactory.getLogger(AbstractDatabaseEventLoggerEventHandler.class);

    protected ActivitiEvent event;
    protected Date timeStamp;
    protected ObjectMapper objectMapper;

    public AbstractDatabaseEventLoggerEventHandler() {
    }

    protected EventLogEntryEntity createEventLogEntry(Map<String, Object> data) {
        return createEventLogEntry(null, null, null, null, data);
    }

    protected EventLogEntryEntity createEventLogEntry(String processDefinitionId, String processInstanceId, String executionId, String taskId, Map<String, Object> data) {
        return createEventLogEntry(event.getType().name(), processDefinitionId, processInstanceId, executionId, taskId, data);
    }

    protected EventLogEntryEntity createEventLogEntry(String type, String processDefinitionId, String processInstanceId, String executionId, String taskId, Map<String, Object> data) {
        EventLogEntryEntity eventLogEntry = Context.getCommandContext().getEventLogEntryEntityManager().create();
        eventLogEntry.setProcessDefinitionId(processDefinitionId);
        eventLogEntry.setProcessInstanceId(processInstanceId);
        eventLogEntry.setExecutionId(executionId);
        eventLogEntry.setTaskId(taskId);
        eventLogEntry.setType(type);
        eventLogEntry.setTimeStamp(timeStamp);
        putInMapIfNotNull(data, Fields.TIMESTAMP, timeStamp);

        // Current user
        String userId = Authentication.getAuthenticatedUser().getDeptId();
        if (userId != null) {
            eventLogEntry.setUserId(userId);
            putInMapIfNotNull(data, "userId", userId);
        }

        // Current tenant
        if (!data.containsKey(Fields.TENANT_ID) && processDefinitionId != null) {
            ProcessDefinition processDefinition = ProcessDefinitionUtil.getProcessDefinition(processDefinitionId);
            if (processDefinition != null && !ProcessEngineConfigurationImpl.NO_TENANT_ID.equals(processDefinition.getTenantId())) {
                putInMapIfNotNull(data, Fields.TENANT_ID, processDefinition.getTenantId());
            }
        }

        try {
            eventLogEntry.setData(objectMapper.writeValueAsBytes(data));
        } catch (Exception e) {
            logger.warn("Could not serialize event data. Data will not be written to the database", e);
        }

        return eventLogEntry;

    }

    @Override
    public void setEvent(ActivitiEvent event) {
        this.event = event;
    }

    @Override
    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    // Helper methods //////////////////////////////////////////////////////

    @SuppressWarnings("unchecked")
    public <T> T getEntityFromEvent() {
        return (T) ((ActivitiEntityEvent) event).getEntity();
    }

    public void putInMapIfNotNull(Map<String, Object> map, String key, Object value) {
        if (value != null) {
            map.put(key, value);
        }
    }

}
