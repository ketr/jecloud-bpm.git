/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.delegate.event.ActivitiEventType;
import com.je.bpm.engine.delegate.event.impl.ActivitiEventBuilder;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.persistence.CountingExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.data.DataManager;
import com.je.bpm.engine.impl.persistence.entity.data.IdentityLinkDataManager;
import com.je.bpm.engine.task.IdentityLinkType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 */
public class IdentityLinkEntityManagerImpl extends AbstractEntityManager<IdentityLinkEntity> implements IdentityLinkEntityManager {

    protected IdentityLinkDataManager identityLinkDataManager;

    public IdentityLinkEntityManagerImpl(ProcessEngineConfigurationImpl processEngineConfiguration, IdentityLinkDataManager identityLinkDataManager) {
        super(processEngineConfiguration);
        this.identityLinkDataManager = identityLinkDataManager;
    }

    @Override
    protected DataManager<IdentityLinkEntity> getDataManager() {
        return identityLinkDataManager;
    }

    @Override
    public void insert(IdentityLinkEntity entity, boolean fireCreateEvent) {
        super.insert(entity, fireCreateEvent);
        getHistoryManager().recordIdentityLinkCreated(entity);

        if (entity.getProcessInstanceId() != null && isExecutionRelatedEntityCountEnabledGlobally()) {
            CountingExecutionEntity executionEntity = (CountingExecutionEntity) getExecutionEntityManager().findById(entity.getProcessInstanceId());
            if (isExecutionRelatedEntityCountEnabled(executionEntity)) {
                executionEntity.setIdentityLinkCount(executionEntity.getIdentityLinkCount() + 1);
            }
        }
    }

    @Override
    public void deleteIdentityLink(IdentityLinkEntity identityLink, boolean cascadeHistory) {
        delete(identityLink, false);
        if (cascadeHistory) {
            getHistoryManager().deleteHistoricIdentityLink(identityLink.getId());
        }

        if (identityLink.getProcessInstanceId() != null && isExecutionRelatedEntityCountEnabledGlobally()) {
            CountingExecutionEntity executionEntity = (CountingExecutionEntity) getExecutionEntityManager().findById(identityLink.getProcessInstanceId());
            if (isExecutionRelatedEntityCountEnabled(executionEntity)) {
                executionEntity.setIdentityLinkCount(executionEntity.getIdentityLinkCount() - 1);
            }
        }

        if (getEventDispatcher().isEnabled()) {
            getEventDispatcher().dispatchEvent(ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_DELETED, identityLink));
        }
    }

    @Override
    public List<IdentityLinkEntity> findIdentityLinksByTaskId(String taskId) {
        return identityLinkDataManager.findIdentityLinksByTaskId(taskId);
    }

    @Override
    public List<IdentityLinkEntity> findIdentityLinksByProcessInstanceId(String processInstanceId) {
        return identityLinkDataManager.findIdentityLinksByProcessInstanceId(processInstanceId);
    }

    @Override
    public List<IdentityLinkEntity> findIdentityLinksByProcessDefinitionId(String processDefinitionId) {
        return identityLinkDataManager.findIdentityLinksByProcessDefinitionId(processDefinitionId);
    }

    @Override
    public List<IdentityLinkEntity> findIdentityLinkByTaskUserGroupAndType(String taskId, String userId, String groupId, String type) {
        return identityLinkDataManager.findIdentityLinkByTaskUserGroupAndType(taskId, userId, groupId, type);
    }

    @Override
    public List<IdentityLinkEntity> findIdentityLinkByProcessInstanceUserGroupAndType(String processInstanceId, String userId, String groupId, String type) {
        return identityLinkDataManager.findIdentityLinkByProcessInstanceUserGroupAndType(processInstanceId, userId, groupId, type);
    }

    @Override
    public IdentityLinkEntity findStartUserIdentityLinkByProcessInstanceUser(String processInstanceId) {
        return identityLinkDataManager.findStartUserIdentityLinkByProcessInstanceUser(processInstanceId);
    }

    @Override
    public List<IdentityLinkEntity> findIdentityLinkByProcessDefinitionUserAndGroup(String processDefinitionId, String userId, String groupId) {
        return identityLinkDataManager.findIdentityLinkByProcessDefinitionUserAndGroup(processDefinitionId, userId, groupId);
    }

    @Override
    public IdentityLinkEntity addIdentityLink(ExecutionEntity executionEntity, String userId, String groupId, String type) {
        IdentityLinkEntity identityLinkEntity = identityLinkDataManager.create();
        executionEntity.getIdentityLinks().add(identityLinkEntity);
        identityLinkEntity.setProcessInstance(executionEntity.getProcessInstance() != null ? executionEntity.getProcessInstance() : executionEntity);
        identityLinkEntity.setUserId(userId);
        identityLinkEntity.setGroupId(groupId);
        identityLinkEntity.setType(type);
        insert(identityLinkEntity);
        return identityLinkEntity;
    }

    @Override
    public IdentityLinkEntity addIdentityLink(TaskEntity taskEntity, String userId, String groupId, String type) {
        IdentityLinkEntity identityLinkEntity = identityLinkDataManager.create();
        taskEntity.getIdentityLinks().add(identityLinkEntity);
        identityLinkEntity.setTask(taskEntity);
        identityLinkEntity.setUserId(userId);
        identityLinkEntity.setGroupId(groupId);
        identityLinkEntity.setType(type);
        insert(identityLinkEntity);
        if (userId != null && taskEntity.getProcessInstanceId() != null) {
            involveUser(taskEntity.getProcessInstance(), userId, IdentityLinkType.PARTICIPANT);
        }
        return identityLinkEntity;
    }

    @Override
    public IdentityLinkEntity addIdentityLink(ProcessDefinitionEntity processDefinitionEntity, String userId, String groupId) {
        IdentityLinkEntity identityLinkEntity = identityLinkDataManager.create();
        processDefinitionEntity.getIdentityLinks().add(identityLinkEntity);
        identityLinkEntity.setProcessDef(processDefinitionEntity);
        identityLinkEntity.setUserId(userId);
        identityLinkEntity.setGroupId(groupId);
        identityLinkEntity.setType(IdentityLinkType.CANDIDATE);
        insert(identityLinkEntity);
        return identityLinkEntity;
    }

    /**
     * Adds an IdentityLink for the given user id with the specified type,
     * but only if the user is not associated with the execution entity yet.
     **/
    @Override
    public IdentityLinkEntity involveUser(ExecutionEntity executionEntity, String userId, String type) {
        for (IdentityLinkEntity identityLink : executionEntity.getIdentityLinks()) {
            if (identityLink.isUser() && identityLink.getUserId().equals(userId)) {
                return identityLink;
            }
        }
        return addIdentityLink(executionEntity, userId, null, type);
    }

    @Override
    public void addCandidateUser(TaskEntity taskEntity, String userId) {
        addIdentityLink(taskEntity, userId, null, IdentityLinkType.CANDIDATE);
    }

    @Override
    public void addCandidateUsers(TaskEntity taskEntity, Collection<String> candidateUsers) {
        for (String candidateUser : candidateUsers) {
            addCandidateUser(taskEntity, candidateUser);
        }
    }

    @Override
    public void addCandidateGroup(TaskEntity taskEntity, String groupId) {
        addIdentityLink(taskEntity, null, groupId, IdentityLinkType.CANDIDATE);
    }

    @Override
    public void addCandidateGroups(TaskEntity taskEntity, Collection<String> candidateGroups) {
        for (String candidateGroup : candidateGroups) {
            addCandidateGroup(taskEntity, candidateGroup);
        }
    }

    @Override
    public void addGroupIdentityLink(TaskEntity taskEntity, String groupId, String identityLinkType) {
        addIdentityLink(taskEntity, null, groupId, identityLinkType);
    }

    @Override
    public void addUserIdentityLink(TaskEntity taskEntity, String userId, String identityLinkType) {
        addIdentityLink(taskEntity, userId, null, identityLinkType);
    }

    @Override
    public void deleteIdentityLink(ExecutionEntity executionEntity, String userId, String groupId, String type) {
        String id = executionEntity.getProcessInstanceId() != null ? executionEntity.getProcessInstanceId() : executionEntity.getId();
        List<IdentityLinkEntity> identityLinks = findIdentityLinkByProcessInstanceUserGroupAndType(id, userId, groupId, type);

        for (IdentityLinkEntity identityLink : identityLinks) {
            deleteIdentityLink(identityLink, true);
        }

        executionEntity.getIdentityLinks().removeAll(identityLinks);
    }

    @Override
    public void deleteIdentityLink(TaskEntity taskEntity, String userId, String groupId, String type) {
        List<IdentityLinkEntity> identityLinks = findIdentityLinkByTaskUserGroupAndType(taskEntity.getId(), userId, groupId, type);

        List<String> identityLinkIds = new ArrayList<String>();
        for (IdentityLinkEntity identityLink : identityLinks) {
            deleteIdentityLink(identityLink, true);
            identityLinkIds.add(identityLink.getId());
        }

        // fix deleteCandidate() in create TaskListener
        List<IdentityLinkEntity> removedIdentityLinkEntities = new ArrayList<IdentityLinkEntity>();
        for (IdentityLinkEntity identityLinkEntity : taskEntity.getIdentityLinks()) {
            if (IdentityLinkType.CANDIDATE.equals(identityLinkEntity.getType()) &&
                    identityLinkIds.contains(identityLinkEntity.getId()) == false) {

                if ((userId != null && userId.equals(identityLinkEntity.getUserId()))
                        || (groupId != null && groupId.equals(identityLinkEntity.getGroupId()))) {

                    deleteIdentityLink(identityLinkEntity, true);
                    removedIdentityLinkEntities.add(identityLinkEntity);

                }
            }
        }

        taskEntity.getIdentityLinks().removeAll(removedIdentityLinkEntities);
    }

    @Override
    public void deleteIdentityLink(ProcessDefinitionEntity processDefinitionEntity, String userId, String groupId) {
        List<IdentityLinkEntity> identityLinks = findIdentityLinkByProcessDefinitionUserAndGroup(processDefinitionEntity.getId(), userId, groupId);
        for (IdentityLinkEntity identityLink : identityLinks) {
            deleteIdentityLink(identityLink, false);
        }
    }

    @Override
    public void deleteIdentityLinksByTaskId(String taskId) {
        List<IdentityLinkEntity> identityLinks = findIdentityLinksByTaskId(taskId);
        for (IdentityLinkEntity identityLink : identityLinks) {
            deleteIdentityLink(identityLink, false);
        }
    }

    @Override
    public void deleteIdentityLinksByProcDef(String processDefId) {
        identityLinkDataManager.deleteIdentityLinksByProcDef(processDefId);
    }

    public IdentityLinkDataManager getIdentityLinkDataManager() {
        return identityLinkDataManager;
    }

    public void setIdentityLinkDataManager(IdentityLinkDataManager identityLinkDataManager) {
        this.identityLinkDataManager = identityLinkDataManager;
    }

}
