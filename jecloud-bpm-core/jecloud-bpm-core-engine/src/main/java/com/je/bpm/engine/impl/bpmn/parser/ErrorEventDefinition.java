/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.parser;

import java.io.Serializable;
import java.util.Comparator;


public class ErrorEventDefinition implements Serializable {

    public static Comparator<ErrorEventDefinition> comparator = new Comparator<ErrorEventDefinition>() {
        @Override
        public int compare(ErrorEventDefinition o1, ErrorEventDefinition o2) {
            return o2.getPrecedence().compareTo(o1.getPrecedence());
        }
    };

    private static final long serialVersionUID = 1L;

    protected final String handlerActivityId;
    protected String errorCode;
    protected Integer precedence = 0;

    public ErrorEventDefinition(String handlerActivityId) {
        this.handlerActivityId = handlerActivityId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getHandlerActivityId() {
        return handlerActivityId;
    }

    public Integer getPrecedence() {
        // handlers with error code take precedence over catchall-handlers
        return precedence + (errorCode != null ? 1 : 0);
    }

    public void setPrecedence(Integer precedence) {
        this.precedence = precedence;
    }

    public boolean catches(String errorCode) {
        return errorCode == null || this.errorCode == null || this.errorCode.equals(errorCode);
    }

}
