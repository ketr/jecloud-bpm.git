/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.jobexecutor;

import com.je.bpm.engine.impl.util.json.JSONException;
import com.je.bpm.engine.impl.util.json.JSONObject;

public class TimerEventHandler {

    public static final String PROPERTYNAME_TIMER_ACTIVITY_ID = "activityId";
    public static final String PROPERTYNAME_END_DATE_EXPRESSION = "timerEndDate";
    public static final String PROPERTYNAME_CALENDAR_NAME_EXPRESSION = "calendarName";

    public static String createConfiguration(String id, String endDate, String calendarName) {
        JSONObject cfgJson = new JSONObject();
        cfgJson.put(PROPERTYNAME_TIMER_ACTIVITY_ID, id);
        if (endDate != null) {
            cfgJson.put(PROPERTYNAME_END_DATE_EXPRESSION, endDate);
        }
        if (calendarName != null) {
            cfgJson.put(PROPERTYNAME_CALENDAR_NAME_EXPRESSION, calendarName);
        }
        return cfgJson.toString();
    }

    public static String setActivityIdToConfiguration(String jobHandlerConfiguration, String activityId) {
        try {
            JSONObject cfgJson = new JSONObject(jobHandlerConfiguration);
            cfgJson.put(PROPERTYNAME_TIMER_ACTIVITY_ID, activityId);
            return cfgJson.toString();
        } catch (JSONException ex) {
            return jobHandlerConfiguration;
        }
    }

    public static String getActivityIdFromConfiguration(String jobHandlerConfiguration) {
        try {
            JSONObject cfgJson = new JSONObject(jobHandlerConfiguration);
            return cfgJson.get(PROPERTYNAME_TIMER_ACTIVITY_ID).toString();
        } catch (JSONException ex) {
            return jobHandlerConfiguration;
        }
    }

    public static String geCalendarNameFromConfiguration(String jobHandlerConfiguration) {
        try {
            JSONObject cfgJson = new JSONObject(jobHandlerConfiguration);
            return cfgJson.get(PROPERTYNAME_CALENDAR_NAME_EXPRESSION).toString();
        } catch (JSONException ex) {
            // calendar name is not specified
            return "";
        }
    }

    public static String setEndDateToConfiguration(String jobHandlerConfiguration, String endDate) {
        JSONObject cfgJson = null;
        try {
            cfgJson = new JSONObject(jobHandlerConfiguration);
        } catch (JSONException ex) {
            // create the json config
            cfgJson = new JSONObject();
            cfgJson.put(PROPERTYNAME_TIMER_ACTIVITY_ID, jobHandlerConfiguration);
        }
        if (endDate != null) {
            cfgJson.put(PROPERTYNAME_END_DATE_EXPRESSION, endDate);
        }

        return cfgJson.toString();
    }

    public static String getEndDateFromConfiguration(String jobHandlerConfiguration) {
        try {
            JSONObject cfgJson = new JSONObject(jobHandlerConfiguration);
            return cfgJson.get(PROPERTYNAME_END_DATE_EXPRESSION).toString();
        } catch (JSONException ex) {
            return null;
        }
    }

}
