/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity.data.impl;

import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.persistence.CachedEntityMatcher;
import com.je.bpm.engine.impl.persistence.entity.PassRoundEntity;
import com.je.bpm.engine.impl.persistence.entity.PassRoundEntityImpl;
import com.je.bpm.engine.impl.persistence.entity.data.AbstractDataManager;
import com.je.bpm.engine.impl.persistence.entity.data.PassRoundDataManager;
import com.je.bpm.engine.impl.persistence.entity.data.impl.cachematcher.PassRoundsByTaskIdMatcher;
import com.je.bpm.engine.passround.PassRoundQuery;
import com.je.bpm.engine.passround.PassRoundQueryImpl;

import java.util.List;

public class MybatisPassRoundDataManager extends AbstractDataManager<PassRoundEntity> implements PassRoundDataManager {

    protected CachedEntityMatcher<PassRoundEntity> passRoundEntityCachedEntityMatcher = new PassRoundsByTaskIdMatcher();

    public MybatisPassRoundDataManager(ProcessEngineConfigurationImpl processEngineConfiguration) {
        super(processEngineConfiguration);
    }

    @Override
    public Class<? extends PassRoundEntity> getManagedEntityClass() {
        return PassRoundEntityImpl.class;
    }

    @Override
    public PassRoundEntity create() {
        return new PassRoundEntityImpl();
    }

    @Override
    public List<PassRoundEntity> findByTaskId(String taskId) {
        return getList("com.je.bpm.engine.impl.persistence.entity.PassRoundEntityImpl.findByTaskId",
                taskId, passRoundEntityCachedEntityMatcher, false);
    }

    @Override
    public List<PassRoundEntity> findByTaskIdAndUserId(String taskId, String userId) {
        PassRoundQuery passRoundQuery = new PassRoundQueryImpl();
        passRoundQuery.taskId(taskId);
        passRoundQuery.from(userId);
        return getDbSqlSession().selectListWithRawParameter("com.je.bpm.engine.impl.persistence.entity.PassRoundEntityImpl.findByTaskIdAndUserId",
                passRoundQuery, 0, Integer.MAX_VALUE);
    }

    @Override
    public List<PassRoundEntity> findByProcessInstanceId(String processInstanceId) {
        return getList("com.je.bpm.engine.impl.persistence.entity.PassRoundEntityImpl.findByProcessInstanceId",
                processInstanceId, passRoundEntityCachedEntityMatcher, false);
    }

    @Override
    public List<PassRoundEntity> findByProcessDefinitionId(String processDefinitionId) {
        return getList("com.je.bpm.engine.impl.persistence.entity.PassRoundEntityImpl.findByProcessDefinitionId",
                processDefinitionId, passRoundEntityCachedEntityMatcher, false);
    }

    @Override
    public List<PassRoundEntity> findByTo(String taskId, String to, int start, int limit) {
        PassRoundQuery passRoundQuery = new PassRoundQueryImpl();
        passRoundQuery.taskId(taskId);
        passRoundQuery.to(to);
        return getDbSqlSession().selectListWithRawParameter("com.je.bpm.engine.impl.persistence.entity.PassRoundEntityImpl.findByTo", passRoundQuery, start, limit);
    }

    @Override
    public List<PassRoundEntity> findByToAndNoReview(String piid, String to) {
        PassRoundQuery passRoundQuery = new PassRoundQueryImpl();
        passRoundQuery.processInstanceId(piid);
        passRoundQuery.to(to);
        return getDbSqlSession().selectListWithRawParameter("com.je.bpm.engine.impl.persistence.entity.PassRoundEntityImpl.findByToAndNoReview", passRoundQuery, 0, 1);
    }

    @Override
    public List<PassRoundEntity> findByToAndNoReviewAndTaskId(String piid, String to, String taskId) {
        PassRoundQuery passRoundQuery = new PassRoundQueryImpl();
        passRoundQuery.processInstanceId(piid);
        passRoundQuery.to(to);
        passRoundQuery.taskId(taskId);
        return getDbSqlSession().selectListWithRawParameter("com.je.bpm.engine.impl.persistence.entity.PassRoundEntityImpl.findByToAndNoReviewAndTaskId", passRoundQuery, 0, 1);
    }

    @Override
    public List<PassRoundEntity> findByFrom(String taskId, String from, int start, int limit) {
        PassRoundQuery passRoundQuery = new PassRoundQueryImpl();
        passRoundQuery.taskId(taskId);
        passRoundQuery.from(from);
        return getDbSqlSession().selectListWithRawParameter("findByFrom", passRoundQuery, start, limit);
    }

    @Override
    public void read(String piid, String userId, String comment) {
        PassRoundQuery passRoundQuery = new PassRoundQueryImpl();
        passRoundQuery.processInstanceId(piid);
        passRoundQuery.to(userId);
        passRoundQuery.content(comment);
        getDbSqlSession().update("updatePassRoundState", passRoundQuery);
    }

}
