/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.config.CustomEvent;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.impl.bpmn.behavior.KaiteBaseUserTaskActivityBehavior;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.task.Comment;
import com.je.bpm.engine.upcoming.UpcomingCommentInfoDTO;

import java.util.HashMap;
import java.util.Map;

import static com.je.bpm.engine.impl.bpmn.behavior.MultiInstanceActivityBehavior.NUMBER_OF_COMPLETED_INSTANCES;

/**
 * 直送
 */
public class DirectCompleteCmd extends AbstractJumpTask {

    private String comment;
    private String assignee;
    private String files;

    public DirectCompleteCmd(String taskId, String comment, Map<String, Object> bean, String prod, String assignee, String files) {
        super(taskId, prod, bean);
        this.taskId = taskId;
        this.comment = comment;
        this.assignee = assignee;
        this.files = files;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    protected Void execute(CommandContext commandContext, TaskEntity taskEntity) {
        BpmnModel bpmnModel = commandContext.getProcessEngineConfiguration().getRepositoryService().getBpmnModel(taskEntity.getProcessDefinitionId(), taskEntity.getProcessInstanceId(), taskEntity.getBusinessKey());

        executeBeforeHandler(bpmnModel, taskEntity, CustomEvent.CustomEventEnum.TASK_STRAIGHT_BEFORE, comment, OperatorEnum.TASK_DIRECT_SEND_OPERATOR, assignee);

        commandContext.getProcessEngineConfiguration().getTaskService().addComment(taskId, taskEntity.getProcessInstanceId(),
                Comment.USER_COMMENT, comment);
        commandContext.getProcessEngineConfiguration().getTaskService().addComment(taskId, taskEntity.getProcessInstanceId(),
                Comment.FILES_COMMENT, files);

        ExecutionEntity execution = taskEntity.getExecution();

        FlowElement flowElement = bpmnModel.getFlowElement(taskEntity.getTaskDefinitionKey());
        if (flowElement == null) {
            throw new ActivitiException("Can't find the flow element in the process definition.");
        }

        if (!(flowElement instanceof KaiteBaseUserTask)) {
            throw new ActivitiException("Only kaite user task can be jumped.");
        }
        KaiteBaseUserTask kaiteBaseUserTask = (KaiteBaseUserTask) flowElement;

        //查找目标节点
        Object dismissInfoObjs = execution.getVariable(DismissTaskCmd.DISMISS_INFO_KEY);
        if (dismissInfoObjs == null || !(dismissInfoObjs instanceof JSONObject)) {
            throw new ActivitiException("无法找到直送节点!");
        }

        JSONObject dismissInfos = (JSONObject) dismissInfoObjs;
        Map<String, Object> dismissInfo = (Map<String, Object>) dismissInfos.get(taskEntity.getTaskDefinitionKey());
        String nodeId = (String) dismissInfo.get(DismissTaskCmd.DISMISS_INFO_NODE_ID_KEY);

        //上一个处理节点
        FlowElement targetFlowElement = bpmnModel.getFlowElement(nodeId);
        if (!(targetFlowElement instanceof KaiteBaseUserTask)) {
            throw new ActivitiException("Only kaite user task can jump.");
        }

        KaiteBaseUserTask targetBaseUserTask = (KaiteBaseUserTask) targetFlowElement;
        ExecutionEntity finalExecutionEntity;
        //删除任务之前调用审批告知变量保存
        String assigneeUsers = getHistoryAssigneeByTask(commandContext, taskEntity.getProcessInstanceId(), flowElement);
        Context.getCommandContext().getProcessEngineConfiguration().getProcessInstanceHelper()
                .addApprovalNotificationVariable(bpmnModel, kaiteBaseUserTask.getId(), comment, SubmitTypeEnum.DIRECTSEND.getName());
        //如果属于多实例节点，在没有开始审批前可以跳转，但是一旦有人审批，则不可以跳转
        if (kaiteBaseUserTask.hasMultiInstanceLoopCharacteristics()) {
            Integer completed = getLoopVariable(execution, NUMBER_OF_COMPLETED_INSTANCES);
            if (completed != null && completed > 0) {
                throw new ActivitiException("The multi instance task already have approve record.");
            }
            //删除所有任务
            finalExecutionEntity = deleteAllMultiTask(commandContext, execution);
        } else {
            //删除当前任务
            commandContext.getTaskEntityManager().deleteTask(taskEntity, comment, false, false);
            finalExecutionEntity = execution;
        }

        finalExecutionEntity.setCurrentFlowElement(targetFlowElement);
        Map<String, Object> transientVariables = new HashMap<>();
        //多人节点，获取多个人
        String assigneeUser = getHistoryAssigneeByTask(commandContext, taskEntity.getProcessInstanceId(), targetFlowElement);
        transientVariables.put(nodeId, assigneeUser);

        JSONArray jsonArray = new JSONArray();
        JSONObject assigneeUserJsonObject = new JSONObject();
        assigneeUserJsonObject.put("nodeId", nodeId);
        assigneeUserJsonObject.put("nodeName", targetFlowElement.getName());
        assigneeUserJsonObject.put("assignee", assigneeUser);
        assigneeUserJsonObject.put("assigneeName", "");
        jsonArray.add(assigneeUserJsonObject);
        UpcomingCommentInfoDTO upcomingInfo = UpcomingCommentInfoDTO.build(SubmitTypeEnum.DIRECTSEND, Context.getCommandContext().getBean(), taskEntity.getBusinessKey(),
                comment, taskId, null, jsonArray.toJSONString());
        Context.getCommandContext().addAttribute(KaiteBaseUserTaskActivityBehavior.UPCOMINGINFO, upcomingInfo);
        finalExecutionEntity.setTransientVariables(transientVariables);

        //如果目标任务属于多实例节点，则要根据多实例要求创建多个任务（串行创建一个，并行创建多个）
        if (targetBaseUserTask.hasMultiInstanceLoopCharacteristics()) {
            finalExecutionEntity.setActive(true);
            Context.getAgenda().planContinueMultiInstanceOperation(finalExecutionEntity);
        } else {
            Context.getAgenda().planContinueProcessOperation(finalExecutionEntity);
        }
        executeAfterHandler(bpmnModel, taskEntity, CustomEvent.CustomEventEnum.TASK_STRAIGHT_AFTER, comment, OperatorEnum.TASK_DIRECT_SEND_OPERATOR, assignee);
        return null;
    }


}
