/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.util;

/**
 * Util class for manipulating bit-flag in ints.
 *
 * Currently, only 8-bits are supported, but can be extended to use all 31 bits in the integer (1st of 32 bits is used for sign).
 *

 */
public class BitMaskUtil {

  // First 8 masks as constant to prevent having to math.pow() every time a
  // bit needs flipping.

  private static final int FLAG_BIT_1 = 1; // 000...00000001
  private static final int FLAG_BIT_2 = 2; // 000...00000010
  private static final int FLAG_BIT_3 = 4; // 000...00000100
  private static final int FLAG_BIT_4 = 8; // 000...00001000
  private static final int FLAG_BIT_5 = 16; // 000...00010000
  private static final int FLAG_BIT_6 = 32; // 000...00100000
  private static final int FLAG_BIT_7 = 64; // 000...01000000
  private static final int FLAG_BIT_8 = 128; // 000...10000000

  private static int[] MASKS = { FLAG_BIT_1, FLAG_BIT_2, FLAG_BIT_3, FLAG_BIT_4, FLAG_BIT_5, FLAG_BIT_6, FLAG_BIT_7, FLAG_BIT_8 };

  /**
   * Set bit to '1' in the given int.
   *
   * @param value
   *          integer value
   * @param bitNumber
   *          number of the bit to set to '1' (right first bit starting at 1).
   */
  public static int setBitOn(int value, int bitNumber) {
    if (bitNumber <= 0 || bitNumber > 8) {
      throw new IllegalArgumentException("Only bits 1 through 8 are supported");
    }

    // To turn on, OR with the correct mask
    return value | MASKS[bitNumber - 1];
  }

  /**
   * Set bit to '0' in the given int.
   *
   * @param value
   *          integer value
   * @param bitNumber
   *          number of the bit to set to '0' (right first bit starting at 1).
   */
  public static int setBitOff(int value, int bitNumber) {
    if (bitNumber <= 0 || bitNumber > 8) {
      throw new IllegalArgumentException("Only bits 1 through 8 are supported");
    }

    // To turn on, OR with the correct mask
    return value & ~MASKS[bitNumber - 1];
  }

  /**
   * Check if the bit is set to '1'
   *
   * @param value
   *          integer to check bit
   * @param value
   *          of bit to check (right first bit starting at 1)
   */
  public static boolean isBitOn(int value, int bitNumber) {
    if (bitNumber <= 0 || bitNumber > 8) {
      throw new IllegalArgumentException("Only bits 1 through 8 are supported");
    }

    return ((value & MASKS[bitNumber - 1]) == MASKS[bitNumber - 1]);
  }

  /**
   * Set bit to '0' or '1' in the given int.
   *
   * @param value
   *          integer value
   * @param bitNumber
   *          number of the bit to set to '0' or '1' (right first bit starting at 1).
   * @param bitValue
   *          if true, bit set to '1'. If false, '0'.
   */
  public static int setBit(int value, int bitNumber, boolean bitValue) {
    if (bitValue) {
      return setBitOn(value, bitNumber);
    } else {
      return setBitOff(value, bitNumber);
    }
  }
}
