/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.agenda;

import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.*;

import java.util.Collection;

/**
 * Destroys a scope (for example a subprocess): this means that all child executions,
 * tasks, jobs, variables, etc within that scope are deleted.
 * <p>
 * The typical example is an interrupting boundary event that is on the boundary
 * of a subprocess and is triggered. At that point, everything within the subprocess would
 * need to be destroyed.
 */
public class DestroyScopeOperation extends AbstractOperation {

    public DestroyScopeOperation(CommandContext commandContext, ExecutionEntity execution) {
        super(commandContext, execution);
    }

    @Override
    public void run() {
        // Find the actual scope that needs to be destroyed.
        // This could be the incoming execution, or the first parent execution where isScope = true

        // Find parent scope execution
        ExecutionEntity scopeExecution = execution.isScope() ? execution : findFirstParentScopeExecution(execution);

        if (scopeExecution == null) {
            throw new ActivitiException("Programmatic error: no parent scope execution found for boundary event");
        }

        ExecutionEntityManager executionEntityManager = commandContext.getExecutionEntityManager();
        deleteAllChildExecutions(executionEntityManager,scopeExecution);

        // Delete all scope tasks
        TaskEntityManager taskEntityManager = commandContext.getTaskEntityManager();
        deleteAllScopeTasks(scopeExecution, taskEntityManager);

        // Delete all scope jobs
        TimerJobEntityManager timerJobEntityManager = commandContext.getTimerJobEntityManager();
        deleteAllScopeJobs(scopeExecution, timerJobEntityManager);

        // Remove variables associated with this scope
        VariableInstanceEntityManager variableInstanceEntityManager = commandContext.getVariableInstanceEntityManager();
        removeAllVariablesFromScope(scopeExecution, variableInstanceEntityManager);

        commandContext.getHistoryManager().recordActivityEnd(scopeExecution, scopeExecution.getDeleteReason());
        executionEntityManager.delete(scopeExecution);
    }

    private void removeAllVariablesFromScope(ExecutionEntity scopeExecution, VariableInstanceEntityManager variableInstanceEntityManager) {
        Collection<VariableInstanceEntity> variablesForExecution = variableInstanceEntityManager.findVariableInstancesByExecutionId(scopeExecution.getId());
        for (VariableInstanceEntity variable : variablesForExecution) {
            variableInstanceEntityManager.delete(variable);
        }
    }

    private void deleteAllScopeJobs(ExecutionEntity scopeExecution, TimerJobEntityManager timerJobEntityManager) {
        Collection<TimerJobEntity> timerJobsForExecution = timerJobEntityManager.findJobsByExecutionId(scopeExecution.getId());
        for (TimerJobEntity job : timerJobsForExecution) {
            timerJobEntityManager.delete(job);
        }

        JobEntityManager jobEntityManager = commandContext.getJobEntityManager();
        Collection<JobEntity> jobsForExecution = jobEntityManager.findJobsByExecutionId(scopeExecution.getId());
        for (JobEntity job : jobsForExecution) {
            jobEntityManager.delete(job);
        }

        SuspendedJobEntityManager suspendedJobEntityManager = commandContext.getSuspendedJobEntityManager();
        Collection<SuspendedJobEntity> suspendedJobsForExecution = suspendedJobEntityManager.findJobsByExecutionId(scopeExecution.getId());
        for (SuspendedJobEntity job : suspendedJobsForExecution) {
            suspendedJobEntityManager.delete(job);
        }

        DeadLetterJobEntityManager deadLetterJobEntityManager = commandContext.getDeadLetterJobEntityManager();
        Collection<DeadLetterJobEntity> deadLetterJobsForExecution = deadLetterJobEntityManager.findJobsByExecutionId(scopeExecution.getId());
        for (DeadLetterJobEntity job : deadLetterJobsForExecution) {
            deadLetterJobEntityManager.delete(job);
        }
    }

    private void deleteAllScopeTasks(ExecutionEntity scopeExecution, TaskEntityManager taskEntityManager) {
        Collection<TaskEntity> tasksForExecution = taskEntityManager.findTasksByExecutionId(scopeExecution.getId());
        for (TaskEntity taskEntity : tasksForExecution) {
            taskEntityManager.deleteTask(taskEntity,
                    execution.getDeleteReason(),
                    false,
                    false);
        }
    }

    private ExecutionEntityManager deleteAllChildExecutions(ExecutionEntityManager executionEntityManager, ExecutionEntity scopeExecution) {
        // Delete all child executions
        Collection<ExecutionEntity> childExecutions = executionEntityManager.findChildExecutionsByParentExecutionId(scopeExecution.getId());
        for (ExecutionEntity childExecution : childExecutions) {
            executionEntityManager.deleteExecutionAndRelatedData(childExecution, execution.getDeleteReason());
        }
        return executionEntityManager;
    }
}
