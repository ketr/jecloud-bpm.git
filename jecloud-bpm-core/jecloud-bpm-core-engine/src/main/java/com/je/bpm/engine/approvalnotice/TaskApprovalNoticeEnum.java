/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.approvalnotice;

import java.util.ArrayList;
import java.util.List;

public enum TaskApprovalNoticeEnum {
    /**
     * 本流程节点发起人收到通知
     */
    STARTUSER("启动人"),
    /**
     * 流程已审批过的人员收到通知
     */
    APPROVEDPERSON("已审批人"),
    /**
     * 本节点审批人的直属领导收到通知
     */
    APPROVERDIRECTLEADER("审批人直属领导"),
    /**
     * 本节点审批人的部门领导收到通知
     */
    APPROVERDEPTLEADER("审批人部门领导"),
    ALL("所有");

    private String name;

    public String getName() {
        return this.name;
    }

    private TaskApprovalNoticeEnum (String name){
        this.name = name;
    }

    public static List<TaskApprovalNoticeEnum> getDefaultRemindTypes() {
        List<TaskApprovalNoticeEnum> taskApprovalNoticeEnums = new ArrayList();
        return taskApprovalNoticeEnums;
    }

    public static TaskApprovalNoticeEnum getType(String type) {
        if (STARTUSER.toString().equalsIgnoreCase(type)) {
            return STARTUSER;
        }else if (APPROVEDPERSON.toString().equalsIgnoreCase(type)) {
            return APPROVEDPERSON;
        }else if (APPROVERDIRECTLEADER.toString().equalsIgnoreCase(type)) {
            return APPROVERDIRECTLEADER;
        }else if (APPROVERDEPTLEADER.toString().equalsIgnoreCase(type)) {
            return APPROVERDEPTLEADER;
        }else {
            return ALL.toString().equalsIgnoreCase(type) ? ALL : null;
        }
    }
}
