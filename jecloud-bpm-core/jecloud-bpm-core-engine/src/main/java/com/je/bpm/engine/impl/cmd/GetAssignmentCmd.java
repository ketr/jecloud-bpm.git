/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.google.common.base.Strings;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.FlowNode;
import com.je.bpm.core.model.SequenceFlow;
import com.je.bpm.core.model.config.task.assignment.TaskAssigneeConfigImpl;
import com.je.bpm.core.model.event.StartEvent;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.delegate.DelegateExecution;
import com.je.bpm.engine.history.HistoricActivityInstance;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.engine.impl.interceptor.Command;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.runtime.shared.identity.BO.ParserUserBo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 获取人员详细信息
 */
public class GetAssignmentCmd implements Command<Object>, Serializable {
    /**
     * 人员配置
     */
    private TaskAssigneeConfigImpl taskAssigneeConfig;
    private KaiteBaseUserTask kaiteBaseUserTask;
    /**
     * 节点id
     */
    private String taskId;
    private Map<String, Object> bean;
    private String prod;
    private String pdid;
    private Boolean multiple;
    /**
     * 默认添加自己
     */
    private Boolean addOwn = true;
    /**
     * 提交过来的节点 如果是可跳跃 flowNode是目标节点的上个节点
     */
    private FlowNode flowNode;

    private String operationId;

    private String businessKey;

    public GetAssignmentCmd(Boolean addOwn, Boolean multiple, String pdid,
                            KaiteBaseUserTask kaiteBaseUserTask, String taskId, String prod, Map<String, Object> bean, FlowNode flowNode, String operationId, String businessKey) {
        this.taskAssigneeConfig = kaiteBaseUserTask.getTaskAssigneeConfig();
        this.kaiteBaseUserTask = kaiteBaseUserTask;
        this.taskId = taskId;
        this.bean = bean;
        this.prod = prod;
        this.pdid = pdid;
        this.multiple = multiple;
        this.addOwn = addOwn;
        this.flowNode = flowNode;
        this.operationId = operationId;
        this.businessKey = businessKey;
    }

    public Object execute(CommandContext commandContext) {
        TaskEntity task = commandContext.getTaskEntityManager().findById(taskId);
        String directTask = "";
        String userId = "";
        String starter = "";
        String piid = "";
        //人员参照
        TaskAssigneeConfigImpl.ReferToEnum referToEnum = taskAssigneeConfig.getReferTo();
        if (task == null) {
            starter = Authentication.getAuthenticatedUser().getDeptId();
            userId = Authentication.getAuthenticatedUser().getDeptId();
            directTask = findStartElementId(commandContext);
        } else {
            piid = task.getProcessInstanceId();
            directTask = task.getTaskDefinitionKey();
            ExecutionEntity execution = task.getExecution();
            if (referToEnum != null && referToEnum.toString().equals(TaskAssigneeConfigImpl.ReferToEnum.STARTUSER.toString())) {
                userId = commandContext.getIdentityLinkEntityManager().findStartUserIdentityLinkByProcessInstanceUser(execution.getProcessInstanceId()).getUserId();
            } else {
                userId = Authentication.getAuthenticatedUser().getDeptId();
            }
            starter = commandContext.getIdentityLinkEntityManager().findStartUserIdentityLinkByProcessInstanceUser(execution.getProcessInstanceId()).getUserId();
        }
        if (flowNode != null && flowNode instanceof KaiteBaseUserTask) {
            directTask = flowNode.getId();
        }
        RepositoryService repositoryService = commandContext.getProcessEngineConfiguration().getRepositoryService();
        BpmnModel bpmnModel = repositoryService.getBpmnModel(pdid, piid, businessKey);
        Map<String, String> assigner = getAssigner(bpmnModel, task);
        return commandContext.getProcessEngineConfiguration().getResultUserParser().parserResultUserForNextAssignee(
                ParserUserBo.build(addOwn, kaiteBaseUserTask, multiple, taskAssigneeConfig, userId, directTask, bean, prod,
                        assigner.get("assigner"), assigner.get("frontAssigner"), starter, operationId));
    }

    /**
     * 获取任务指派人
     *
     * @return
     */
    private Map<String, String> getAssigner(BpmnModel bpmnModel, TaskEntity task) {
        Map<String, String> assigners = new HashMap<>();
        if (task == null) {
            return assigners;
        }
        CommandContext commandContext = Context.getCommandContext();
        List<Map<String, String>> list =
                commandContext.getProcessEngineConfiguration().getTaskService().getGobackAndRetrieveNodeInfo(taskId, "goBack");
        if (list.size() == 0) {
            return assigners;
        }
        String taskKey = list.get(0).get("directTaskId");
        String frontAssigner = "";
        FlowElement flowElement = bpmnModel.getFlowElement(taskKey);
        if (flowElement instanceof KaiteBaseUserTask) {
            KaiteBaseUserTask kaiteBaseUserTask = (KaiteBaseUserTask) flowElement;
            List<SequenceFlow> sequenceFlowList = kaiteBaseUserTask.getIncomingFlows();

            List<HistoricActivityInstance> historicActivityInstanceList = commandContext.getProcessEngineConfiguration()
                    .getHistoryService().createHistoricActivityInstanceQuery().processInstanceId(task.getProcessInstanceId())
                    .orderByHistoricActivityInstanceStartTime().desc().list();

            for (SequenceFlow sequenceFlow : sequenceFlowList) {
                if (!Strings.isNullOrEmpty(frontAssigner)) {
                    break;
                }
                FlowElement flowElement1 = sequenceFlow.getSourceFlowElement();
                if (flowElement1 instanceof KaiteBaseUserTask) {
                    KaiteBaseUserTask kaiteBaseUserTask1 = (KaiteBaseUserTask) flowElement1;
                    for (HistoricActivityInstance historicActivityInstance : historicActivityInstanceList) {
                        if (kaiteBaseUserTask1.getId().equals(historicActivityInstance.getActivityId())) {
                            frontAssigner = historicActivityInstance.getAssignee();
                            break;
                        }
                    }
                }
            }
        }
        assigners.put("frontAssigner", frontAssigner);
        assigners.put("assigner", list.get(0).get("prevAssignee"));
        return assigners;
    }

    private String findStartElementId(CommandContext commandContext) {
        RepositoryService repositoryService = commandContext.getProcessEngineConfiguration().getRepositoryService();
        BpmnModel bpmnModel = repositoryService.getBpmnModel(pdid, "", businessKey);
        List<FlowElement> list = bpmnModel.getMainProcess().getFlowElementList();
        for (FlowElement flowElement : list) {
            if (flowElement instanceof StartEvent) {
                StartEvent startEvent = (StartEvent) flowElement;
                FlowElement firstNode = startEvent.getOutgoingFlows().get(0);
                String targetRef = ((SequenceFlow) firstNode).getTargetRef();
                for (FlowElement kaiteUserTask : list) {
                    if (kaiteUserTask.getId().equals(targetRef)) {
                        return kaiteUserTask.getId();
                    }
                }
            }
        }
        return "";
    }

    protected String getStringVariable(DelegateExecution execution, String variableName) {
        Object value = execution.getVariable(variableName);
        if (value == null) {
            value = execution.getParent().getVariable(variableName);
        }
        return (String) (value != null ? value : null);
    }


}
