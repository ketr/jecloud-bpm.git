/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine;

import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.button.Button;
import com.je.bpm.core.model.button.ProcessButton;
import com.je.bpm.core.process.validation.ValidationError;
import com.je.bpm.engine.button.validator.ButtonValidateParam;
import com.je.bpm.engine.impl.persistence.entity.Entity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntityImpl;
import com.je.bpm.engine.impl.persistence.entity.VariableInstanceEntity;
import com.je.bpm.engine.internal.Internal;
import com.je.bpm.engine.repository.*;
import com.je.bpm.engine.task.IdentityLink;
import com.je.bpm.engine.task.Task;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 * Service providing access to the repository of process definitions and deployments.
 * 流程仓库Service，用于管理流程仓库，例如：部署，删除， 读取流程资源
 */
@Internal
public interface RepositoryService {

    /**
     * Starts creating a new deployment
     */
    DeploymentBuilder createDeployment();

    /**
     * Deletes the given deployment.
     *
     * @param deploymentId id of the deployment, cannot be null.
     * @throwns RuntimeException if there are still runtime or history process instances or jobs.
     */
    void deleteDeployment(String deploymentId);

    /**
     * Deletes the given deployment and cascade deletion to process instances, history process instances and jobs.
     *
     * @param deploymentId id of the deployment, cannot be null.
     */
    void deleteDeployment(String deploymentId, boolean cascade);

    /**
     * Sets the category of the deployment. Deployments can be queried by category: see {@link DeploymentQuery#deploymentCategory(String)}.
     *
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException if no deployment with the provided id can be found.
     */
    void setDeploymentCategory(String deploymentId, String category);

    /**
     * Sets the key of the deployment. Deployments can be queried by key: see {@link DeploymentQuery#deploymentKey(String)}.
     *
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException if no deployment with the provided id can be found.
     */
    void setDeploymentKey(String deploymentId, String key);

    /**
     * Retrieves a list of deployment resources for the given deployment, ordered alphabetically.
     *
     * @param deploymentId id of the deployment, cannot be null.
     */
    List<String> getDeploymentResourceNames(String deploymentId);

    /**
     * Gives access to a deployment resource through a stream of bytes.
     *
     * @param deploymentId id of the deployment, cannot be null.
     * @param resourceName name of the resource, cannot be null.
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException when the resource doesn't exist in the given deployment or when no deployment exists for the given deploymentId.
     */
    InputStream getResourceAsStream(String deploymentId, String resourceName);

    /**
     * EXPERIMENTAL FEATURE!
     * <p>
     * Changes the tenant identifier of a deployment to match the given tenant identifier. This change will cascade to any related entity: - process definitions related to the deployment - process
     * instances related to those process definitions - executions related to those process instances - tasks related to those process instances - jobs related to the process definitions and process
     * instances
     * <p>
     * This method can be used in the case that there was no tenant identifier set on the deployment or those entities before.
     * <p>
     * This method can be used to remove a tenant identifier from the deployment and related entities (simply pass null).
     * <p>
     * Important: no optimistic locking will be done while executing the tenant identifier change!
     * <p>
     * This is an experimental feature, mainly because it WILL NOT work properly in a clustered environment without special care: suppose some process instance is in flight. The process definition is in
     * the process definition cache. When a task or job is created when continuing the process instance, the process definition cache will be consulted to get the process definition and from it the
     * tenant identifier. Since it's cached, it will not be the new tenant identifier. This method does clear the cache for this engineinstance , but it will not be cleared on other nodes in a cluster
     * (unless using a shared process definition cache).
     *
     * @param deploymentId The id of the deployment of which the tenant identifier will be changed.
     * @param newTenantId  The new tenant identifier.
     */
    void changeDeploymentTenantId(String deploymentId, String newTenantId);

    /**
     * Query process definitions.
     */
    ProcessDefinitionQuery createProcessDefinitionQuery();

    /**
     * Returns a new {@link com.je.bpm.engine.query.NativeQuery} for process definitions.
     */
    NativeProcessDefinitionQuery createNativeProcessDefinitionQuery();

    /**
     * Query deployment.
     */
    DeploymentQuery createDeploymentQuery();

    /**
     * Returns a new {@link com.je.bpm.engine.query.NativeQuery} for deployment.
     */
    NativeDeploymentQuery createNativeDeploymentQuery();

    /**
     * Suspends the process definition with the given id.
     * <p>
     * If a process definition is in state suspended, it will not be possible to start new process instances based on the process definition.
     *
     * <strong>Note: all the process instances of the process definition will still be active (ie. not suspended)!</strong>
     *
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException if no such processDefinition can be found
     * @throws com.je.bpm.engine.ActivitiException               if the process definition is already in state suspended.
     */
    void suspendProcessDefinitionById(String processDefinitionId);

    /**
     * Suspends the process definition with the given id.
     * <p>
     * If a process definition is in state suspended, it will not be possible to start new process instances based on the process definition.
     *
     * @param suspendProcessInstances If true, all the process instances of the provided process definition will be suspended too.
     * @param suspensionDate          The date on which the process definition will be suspended. If null, the process definition is suspended immediately. Note: The job executor needs to be active to use this!
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException if no such processDefinition can be found.
     * @throws com.je.bpm.engine.ActivitiException               if the process definition is already in state suspended.
     */
    void suspendProcessDefinitionById(String processDefinitionId, boolean suspendProcessInstances, Date suspensionDate);

    /**
     * Suspends the <strong>all</strong> process definitions with the given key (= id in the bpmn20.xml file).
     * <p>
     * If a process definition is in state suspended, it will not be possible to start new process instances based on the process definition.
     *
     * <strong>Note: all the process instances of the process definition will still be active (ie. not suspended)!</strong>
     *
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException if no such processDefinition can be found
     * @throws com.je.bpm.engine.ActivitiException               if the process definition is already in state suspended.
     */
    void suspendProcessDefinitionByKey(String processDefinitionKey);

    /**
     * Suspends the <strong>all</strong> process definitions with the given key (= id in the bpmn20.xml file).
     * <p>
     * If a process definition is in state suspended, it will not be possible to start new process instances based on the process definition.
     *
     * @param suspendProcessInstances If true, all the process instances of the provided process definition will be suspended too.
     * @param suspensionDate          The date on which the process definition will be suspended. If null, the process definition is suspended immediately. Note: The job executor needs to be active to use this!
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException if no such processDefinition can be found
     * @throws com.je.bpm.engine.ActivitiException               if the process definition is already in state suspended.
     */
    void suspendProcessDefinitionByKey(String processDefinitionKey, boolean suspendProcessInstances, Date suspensionDate);

    /**
     * Similar to {@link #suspendProcessDefinitionByKey(String)}, but only applicable for the given tenant identifier.
     */
    void suspendProcessDefinitionByKey(String processDefinitionKey, String tenantId);

    /**
     * Similar to {@link #suspendProcessDefinitionByKey(String, boolean, Date)}, but only applicable for the given tenant identifier.
     */
    void suspendProcessDefinitionByKey(String processDefinitionKey, boolean suspendProcessInstances, Date suspensionDate, String tenantId);

    /**
     * Activates the process definition with the given id.
     *
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException if no such processDefinition can be found or if the process definition is already in state active.
     */
    void activateProcessDefinitionById(String processDefinitionId);

    /**
     * Activates the process definition with the given id.
     *
     * @param activationDate The date on which the process definition will be activated. If null, the process definition is activated immediately. Note: The job executor needs to be active to use this!
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException if no such processDefinition can be found.
     * @throws com.je.bpm.engine.ActivitiException               if the process definition is already in state active.
     */
    void activateProcessDefinitionById(String processDefinitionId, boolean activateProcessInstances, Date activationDate);

    /**
     * Activates the process definition with the given key (=id in the bpmn20.xml file).
     *
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException if no such processDefinition can be found.
     * @throws com.je.bpm.engine.ActivitiException               if the process definition is already in state active.
     */
    void activateProcessDefinitionByKey(String processDefinitionKey);

    /**
     * Activates the process definition with the given key (=id in the bpmn20.xml file).
     *
     * @param activationDate The date on which the process definition will be activated. If null, the process definition is activated immediately. Note: The job executor needs to be active to use this!
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException if no such processDefinition can be found.
     * @throws com.je.bpm.engine.ActivitiException               if the process definition is already in state active.
     */
    void activateProcessDefinitionByKey(String processDefinitionKey, boolean activateProcessInstances, Date activationDate);

    /**
     * Similar to {@link #activateProcessDefinitionByKey(String)}, but only applicable for the given tenant identifier.
     */
    void activateProcessDefinitionByKey(String processDefinitionKey, String tenantId);

    /**
     * Similar to {@link #activateProcessDefinitionByKey(String, boolean, Date)} , but only applicable for the given tenant identifier.
     */
    void activateProcessDefinitionByKey(String processDefinitionKey, boolean activateProcessInstances, Date activationDate, String tenantId);

    /**
     * Sets the category of the process definition. Process definitions can be queried by category: see {@link ProcessDefinitionQuery#processDefinitionCategory(String)}.
     *
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException if no process definition with the provided id can be found.
     */
    void setProcessDefinitionCategory(String processDefinitionId, String category);

    /**
     * Gives access to a deployed process model, e.g., a BPMN 2.0 XML file, through a stream of bytes.
     *
     * @param processDefinitionId id of a {@link ProcessDefinition}, cannot be null.
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException when the process model doesn't exist.
     */
    InputStream getProcessModel(String processDefinitionId);

    void checkVersionEvent();

    /**
     * Returns the {@link ProcessDefinition} including all BPMN information like additional Properties (e.g. documentation).
     */
    ProcessDefinition getProcessDefinition(String processDefinitionId);


    /**
     * Checks if the process definition is suspended.
     */
    boolean isProcessDefinitionSuspended(String processDefinitionId);

    /**
     * Returns the {@link BpmnModel} corresponding with the process definition with the provided process definition id. The {@link BpmnModel} is a pojo versions of the BPMN 2.0 xml and can be used to
     * introspect the process definition using regular Java.
     */
    BpmnModel getBpmnModel(String processDefinitionId,String processInstanceId,String businessKey);

    BpmnModel getBpmnModel(String processDefinitionId);

    /**
     * 是不是节点按钮的处理人
     */
    boolean isTaskButtonHandler(Task task, boolean isMulti, boolean getModel, boolean isOwner, String assignee, String owner, List<String> candidateIds);


    /**
     * 根据流程的key获取最新的部署model
     *
     * @param key
     * @return
     */
    public BpmnModel getLatestDeployBpmnModelByKey(String key);

//    /**
//     * 根据流程的key获取最新的部署model
//     *
//     * @param key
//     * @return
//     */
//    public BpmnModel getLatestBpmnModelByKey(String key);

    /**
     * Creates a new model. The model is transient and must be saved using {@link #saveModel(Model)}.
     */
    public Model newModel();

    /**
     * Saves the model. If the model already existed, the model is updated otherwise a new model is created.
     *
     * @param model model to save, cannot be null.
     */
    public void saveModel(Model model);

    /**
     * @param modelId id of model to delete, cannot be null. When an id is passed for an unexisting model, this operation is ignored.
     */
    public void deleteModel(String modelId);

    /**
     * 加签的model
     * @param modelId
     * @param isCsModel
     */
    public void deleteModel(String modelId,boolean isCsModel);

    /**
     * Saves the model editor source for a model
     *
     * @param modelId id of model to delete, cannot be null. When an id is passed for an unexisting model, this operation is ignored.
     */
    public void addModelEditorSource(String modelId, byte[] bytes);

    /**
     * Saves the model editor source extra for a model
     *
     * @param modelId id of model to delete, cannot be null. When an id is passed for an unexisting model, this operation is ignored.
     */
    public void addModelEditorSourceExtra(String modelId, byte[] bytes);

    /**
     * Query models.
     */
    public ModelQuery createModelQuery();

    /**
     * Returns a new {@link com.je.bpm.engine.query.NativeQuery} for process definitions.
     */
    NativeModelQuery createNativeModelQuery();

    /**
     * Returns the {@link Model}
     *
     * @param modelId id of model
     */
    public Model getModel(String modelId);

    /**
     * Returns the model editor source as a byte array
     *
     * @param modelId id of model
     */
    public byte[] getModelEditorSource(String modelId);

    /**
     * Returns the model editor source extra as a byte array
     *
     * @param modelId id of model
     */
    public byte[] getModelEditorSourceExtra(String modelId);

    /**
     * Authorizes a candidate user for a process definition.
     *
     * @param processDefinitionId id of the process definition, cannot be null.
     * @param userId              id of the user involve, cannot be null.
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException when the process definition or user doesn't exist.
     */
    void addCandidateStarterUser(String processDefinitionId, String userId);

    /**
     * Authorizes a candidate group for a process definition.
     *
     * @param processDefinitionId id of the process definition, cannot be null.
     * @param groupId             id of the group involve, cannot be null.
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException when the process definition or group doesn't exist.
     */
    void addCandidateStarterGroup(String processDefinitionId, String groupId);

    /**
     * Removes the authorization of a candidate user for a process definition.
     *
     * @param processDefinitionId id of the process definition, cannot be null.
     * @param userId              id of the user involve, cannot be null.
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException when the process definition or user doesn't exist.
     */
    void deleteCandidateStarterUser(String processDefinitionId, String userId);

    /**
     * Removes the authorization of a candidate group for a process definition.
     *
     * @param processDefinitionId id of the process definition, cannot be null.
     * @param groupId             id of the group involve, cannot be null.
     * @throws com.je.bpm.engine.ActivitiObjectNotFoundException when the process definition or group doesn't exist.
     */
    void deleteCandidateStarterGroup(String processDefinitionId, String groupId);

    /**
     * Retrieves the {@link IdentityLink}s associated with the given process definition. Such an {@link IdentityLink} informs how a certain identity (eg. group or user) is authorized for a certain
     * process definition
     */
    List<IdentityLink> getIdentityLinksForProcessDefinition(String processDefinitionId);

    /**
     * Validates the given process definition against the rules for executing a process definition on the Activiti engine.
     * <p>
     * To create such a {@link BpmnModel} from a String, following code may be used:
     * <p>
     * XMLInputFactory xif = XMLInputFactory.newInstance(); InputStreamReader in = new InputStreamReader(new ByteArrayInputStream(myProcess.getBytes()), "UTF-8"); // Change to other streams for eg from
     * classpath XMLStreamReader xtr = xif.createXMLStreamReader(in); bpmnModel = new BpmnXMLConverter().convertToBpmnModel(xtr);
     */
    List<ValidationError> validateProcess(BpmnModel bpmnModel);

    Entity getByteArrayById(String id);

    Entity getCsByteArrayById(String id);

    /**
     * 获取流程级别按钮
     *
     * @param buttonValidateParam
     * @return
     */
    List<ProcessButton> getProcessButtonsByFuncCode(ButtonValidateParam buttonValidateParam);

    /**
     * 获取runTask按钮
     *
     * @param buttonVariable 按钮变量
     * @return
     */
    List<Button> getRunTaskButtons(VariableInstanceEntity buttonVariable, TaskEntityImpl taskEntity, ButtonValidateParam buttonValidateParam,String prod);

    BpmnModel getLastBpmnModelByKey(String key);
}
