/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.util;

import com.je.bpm.engine.runtime.Clock;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class DefaultClockImpl implements Clock {

    private static volatile Calendar CURRENT_TIME;

    @Override
    public void setCurrentTime(Date currentTime) {
        Calendar time = null;
        if (currentTime != null) {
            time = new GregorianCalendar();
            time.setTime(currentTime);
        }
        setCurrentCalendar(time);
    }

    @Override
    public void setCurrentCalendar(Calendar currentTime) {
        CURRENT_TIME = currentTime;
    }

    @Override
    public void reset() {
        CURRENT_TIME = null;
    }

    @Override
    public Date getCurrentTime() {
        return CURRENT_TIME == null ? new Date() : CURRENT_TIME.getTime();
    }

    @Override
    public Calendar getCurrentCalendar() {
        return CURRENT_TIME == null ? new GregorianCalendar() : (Calendar) CURRENT_TIME.clone();
    }

    @Override
    public Calendar getCurrentCalendar(TimeZone timeZone) {
        return TimeZoneUtil.convertToTimeZone(getCurrentCalendar(), timeZone);
    }

    @Override
    public TimeZone getCurrentTimeZone() {
        return getCurrentCalendar().getTimeZone();
    }

}
