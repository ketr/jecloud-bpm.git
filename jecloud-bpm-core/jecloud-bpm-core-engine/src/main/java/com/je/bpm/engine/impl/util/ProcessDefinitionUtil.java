/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.util;


import com.google.common.base.Strings;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.persistence.deploy.DeploymentManager;
import com.je.bpm.engine.impl.persistence.deploy.ProcessDefinitionCacheEntry;
import com.je.bpm.engine.impl.persistence.entity.ProcessDefinitionEntity;
import com.je.bpm.engine.impl.persistence.entity.ProcessDefinitionEntityManager;
import com.je.bpm.engine.repository.ProcessDefinition;

/**
 * A utility class that hides the complexity of {@link ProcessDefinitionEntity} and {@link Process} lookup. Use this class rather than accessing the process definition cache or
 * {@link DeploymentManager} directly.
 */
public class ProcessDefinitionUtil {

    public static ProcessDefinition getProcessDefinition(String processDefinitionId) {
        return getProcessDefinition(processDefinitionId, false);
    }

    public static ProcessDefinition getProcessDefinition(String processDefinitionId, boolean checkCacheOnly) {
        ProcessEngineConfigurationImpl processEngineConfiguration = Context.getProcessEngineConfiguration();
        if (checkCacheOnly) {
            ProcessDefinitionCacheEntry cacheEntry = processEngineConfiguration.getProcessDefinitionCache().get(processDefinitionId);
            if (cacheEntry != null) {
                return cacheEntry.getProcessDefinition();
            }
            return null;

        } else {
            // This will check the cache in the findDeployedProcessDefinitionById method
            return processEngineConfiguration.getDeploymentManager().findDeployedProcessDefinitionById(processDefinitionId);
        }
    }

    public static Process getProcess(String processDefinitionId, String processInstanceId, String businessKey) {
        DeploymentManager deploymentManager = Context.getProcessEngineConfiguration().getDeploymentManager();
        // This will check the cache in the findDeployedProcessDefinitionById and resolveProcessDefinition method
        String new_processDefinitionId = "";
        if (!Strings.isNullOrEmpty(processInstanceId) || !Strings.isNullOrEmpty(businessKey)) {
            new_processDefinitionId = Context.getProcessEngineConfiguration().getRnPlanningManager().getProcessDefinitionIdByProcessInstanceId(processInstanceId, businessKey);
        }
        String key = "";
        if (!Strings.isNullOrEmpty(new_processDefinitionId)) {
            key = processDefinitionId.split(":")[0];
            processDefinitionId = new_processDefinitionId;
        }
        ProcessDefinition processDefinitionEntity = deploymentManager.findDeployedProcessDefinitionById(processDefinitionId);
        Process process = deploymentManager.resolveProcessDefinition(processDefinitionEntity).getProcess();
        if (!Strings.isNullOrEmpty(key)) {
            process.setId(key);
        }
        return process;
    }

    public static BpmnModel getBpmnModel(String processDefinitionId) {
        return getBpmnModel(processDefinitionId, "", "");
    }

    //TODO 改完把方法加上

    public static BpmnModel getBpmnModel(String processDefinitionId, String processInstanceId, String beanId) {
        DeploymentManager deploymentManager = Context.getProcessEngineConfiguration().getDeploymentManager();
        String new_processDefinitionId = Context.getProcessEngineConfiguration().getRnPlanningManager().getProcessDefinitionIdByProcessInstanceId(processInstanceId, beanId);
        String key = "";
        if (!Strings.isNullOrEmpty(new_processDefinitionId)) {
            key = processDefinitionId.split(":")[0];
            processDefinitionId = new_processDefinitionId;
        }
        // This will check the cache in the findDeployedProcessDefinitionById and resolveProcessDefinition method
        ProcessDefinition processDefinitionEntity = deploymentManager.findDeployedProcessDefinitionById(processDefinitionId);
        if (processDefinitionEntity == null) {
            return null;
        }
        BpmnModel bpmnModel = deploymentManager.resolveProcessDefinition(processDefinitionEntity).getBpmnModel();
        if (!Strings.isNullOrEmpty(key)) {
            bpmnModel.getMainProcess().setId(key);
        }
        return bpmnModel;
    }

    public static BpmnModel getBpmnModel(DeploymentManager deploymentManager, String processDefinitionId) {
        // This will check the cache in the findDeployedProcessDefinitionById and resolveProcessDefinition method
        ProcessDefinition processDefinitionEntity = deploymentManager.findDeployedProcessDefinitionById(processDefinitionId);
        return deploymentManager.resolveProcessDefinition(processDefinitionEntity).getBpmnModel();
    }

    public static BpmnModel getLastDeployBpmnModelByKey(String key) {
        DeploymentManager deploymentManager = Context.getProcessEngineConfiguration().getDeploymentManager();

        // This will check the cache in the findDeployedProcessDefinitionById and resolveProcessDefinition method
        ProcessDefinition processDefinitionEntity = deploymentManager.findDeployedLatestProcessDefinitionByKey(key);
        return deploymentManager.resolveProcessDefinition(processDefinitionEntity).getBpmnModel();
    }

    public static BpmnModel getLastBpmnModelByKey(String key) {
        DeploymentManager deploymentManager = Context.getProcessEngineConfiguration().getDeploymentManager();

        // This will check the cache in the findDeployedProcessDefinitionById and resolveProcessDefinition method
        ProcessDefinition processDefinitionEntity = deploymentManager.findDeployedLatestProcessDefinitionByKey(key);
        return deploymentManager.resolveProcessDefinition(processDefinitionEntity).getBpmnModel();
    }

    public static BpmnModel getBpmnModelFromCache(String processDefinitionId) {
        ProcessDefinitionCacheEntry cacheEntry = Context.getProcessEngineConfiguration().getProcessDefinitionCache().get(processDefinitionId);
        if (cacheEntry != null) {
            return cacheEntry.getBpmnModel();
        }
        return null;
    }

    public static boolean isProcessDefinitionSuspended(String processDefinitionId) {
        ProcessDefinitionEntity processDefinition = getProcessDefinitionFromDatabase(processDefinitionId);
        return processDefinition.isSuspended();
    }

    public static ProcessDefinitionEntity getProcessDefinitionFromDatabase(String processDefinitionId) {
        ProcessDefinitionEntityManager processDefinitionEntityManager = Context.getProcessEngineConfiguration().getProcessDefinitionEntityManager();
        ProcessDefinitionEntity processDefinition = processDefinitionEntityManager.findById(processDefinitionId);
        if (processDefinition == null) {
            throw new ActivitiException("No process definition found with id " + processDefinitionId);
        }

        return processDefinition;
    }
}
