/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.SequenceFlow;
import com.je.bpm.core.model.config.CustomEvent;
import com.je.bpm.core.model.event.StartEvent;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.ActivitiIllegalArgumentException;
import com.je.bpm.engine.ActivitiObjectNotFoundException;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.runtime.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 撤销命令
 */
public class CancelProcessInstanceCmd extends NeedsActiveTaskCmd<Void> implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(CancelProcessInstanceCmd.class);

    private static final long serialVersionUID = 1L;
    protected String processInstanceId;
    protected String cancelReason;
    protected String businessKey;

    public CancelProcessInstanceCmd(String processInstanceId, String cancelReason, String businessKey, Map<String, Object> bean,
                                    String prod) {
        super(false, prod, bean);
        this.processInstanceId = processInstanceId;
        this.cancelReason = cancelReason;
        this.businessKey = businessKey;
    }

    @Override
    public Void execute(CommandContext commandContext, TaskEntity taskEntity) {
        if (processInstanceId == null) {
            throw new ActivitiIllegalArgumentException("processInstanceId is null");
        }

        ExecutionEntity processInstanceEntity = commandContext.getExecutionEntityManager().findById(processInstanceId);
        if (processInstanceEntity == null) {
            throw new ActivitiObjectNotFoundException("No process instance found for id '" + processInstanceId + "'", ProcessInstance.class);
        }

        BpmnModel bpmnModel = commandContext.getProcessEngineConfiguration().getRepositoryService().getBpmnModel(processInstanceEntity.getProcessDefinitionId(), processInstanceId, businessKey);

        FlowElement firstNode = findFirstFlowElement(commandContext, processInstanceEntity.getProcessDefinitionId());

        List<TaskEntity> tasks = commandContext.getProcessEngineConfiguration().getTaskEntityManager().findTasksByProcessInstanceId(processInstanceId);
        if (tasks == null || tasks.size() > 1) {
            throw new ActivitiException("Can't cancel this process instance because of this process instance has multi tasks!");
        }
        logger.info("===========================执行撤销！");
        commandContext.getHistoricProcessInstanceEntityManager().deleteHistoricProcessInstanceByBusinessKey(businessKey);
        if (tasks == null || tasks.size() < 1) {
            throw new ActivitiException("Can't cancel this process instance because of this process instance has no tasks!");
        }

        if (!tasks.get(0).getTaskDefinitionKey().equals(firstNode.getId())) {
            throw new ActivitiException("Only at the first task can cancel the process instance!");
        }
        commandContext.getExecutionEntityManager().deleteProcessInstance(processInstanceEntity.getProcessInstanceId(), cancelReason, false);
        //执行自定义流程节点策略
        List<Map<String, String>> assignees = new ArrayList<>();
        commandContext.getProcessEngineConfiguration().getProcessInstanceHelper().invokeEvent
                (bpmnModel.getMainProcess().getCustomEventListeners(), tasks.get(0).getName(),
                        tasks.get(0).getBusinessKey(), "",
                        CustomEvent.CustomEventEnum.PROCESS_REVOKE, commandContext, "", taskId, OperatorEnum.PROCESS_CANCEL_OPERATOR, tasks.get(0).getBusinessKey(),
                        bpmnModel, assignees);

        Map<String, Object> bean = (Map<String, Object>) commandContext.getAttribute(CommandContext.BEAN);
        bean.put("SY_PIID", "");
        bean.put("SY_PDID", "");
        bean.put("SY_STARTEDUSER", "");
        bean.put("SY_STARTEDUSERNAME", "");
        bean.put("SY_APPROVEDUSERS", "");
        bean.put("SY_APPROVEDUSERNAMES", "");
        bean.put("SY_PREAPPROVUSERS", "");
        bean.put("SY_LASTFLOWINFO", "");
        bean.put("SY_PREAPPROVUSERNAMES", "");
        bean.put("SY_LASTFLOWUSER", "");
        bean.put("SY_LASTFLOWUSERID", "");
        bean.put("SY_WFWARN", "");
        bean.put("SY_CURRENTTASK", "");
        bean.put("SY_WARNFLAG", "");
        commandContext.addAttribute(CommandContext.BEAN, bean);
        commandContext.getProcessEngineConfiguration().getProcessInstanceHelper().updateBeanInfo
                (bpmnModel.getMainProcess().getProcessConfig(), tasks.get(0).getBusinessKey(),
                        "NOSTATUS", commandContext);
        logger.info("===========================已撤销！");
        return null;
    }

    private FlowElement findFirstFlowElement(CommandContext commandContext, String processDefinitionId) {
        BpmnModel bpmnModel = commandContext.getProcessEngineConfiguration().getRepositoryService().getBpmnModel(processDefinitionId, processInstanceId, businessKey);
        FlowElement startElement = bpmnModel.getMainProcess().getInitialFlowElement();
        if (startElement == null) {
            throw new ActivitiException("Can't find the start element!");
        }
        StartEvent startEvent = (StartEvent) startElement;
        if (startEvent.getOutgoingFlows() == null || startEvent.getOutgoingFlows().size() < 1) {
            throw new ActivitiException("Can't find the outgoing from the start element!");
        }

        SequenceFlow sequenceFlow = startEvent.getOutgoingFlows().get(0);
        if (sequenceFlow == null) {
            throw new ActivitiException("Can't find the outgoing from the start element!");
        }
        String targetRef = sequenceFlow.getTargetRef();
        FlowElement firstNode = bpmnModel.getFlowElement(targetRef);
        if (firstNode == null) {
            throw new ActivitiException("Can't find the first element from the definition!");
        }
        return firstNode;
    }

}
