/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.delegate.event;

/**
 * Base event listener that can be used when implementing an {@link ActivitiEventListener} to get notified when an entity is created, updated, deleted or if another entity-related event occurs.
 * <p>
 * Override the <code>onXX(..)</code> methods to respond to entity changes accordingly.
 */
public class BaseEntityEventListener implements ActivitiEventListener {

    protected boolean failOnException;
    protected Class<?> entityClass;

    /**
     * Create a new BaseEntityEventListener, notified when an event that targets any type of entity is received. Returning true when {@link #isFailOnException()} is called.
     */
    public BaseEntityEventListener() {
        this(true, null);
    }

    /**
     * Create a new BaseEntityEventListener.
     *
     * @param failOnException return value for {@link #isFailOnException()}.
     */
    public BaseEntityEventListener(boolean failOnException) {
        this(failOnException, null);
    }

    public BaseEntityEventListener(boolean failOnException, Class<?> entityClass) {
        this.failOnException = failOnException;
        this.entityClass = entityClass;
    }

    @Override
    public final void onEvent(ActivitiEvent event) {
        if (isValidEvent(event)) {
            // Check if this event
            if (event.getType() == ActivitiEventType.ENTITY_CREATED) {
                onCreate(event);
            } else if (event.getType() == ActivitiEventType.ENTITY_INITIALIZED) {
                onInitialized(event);
            } else if (event.getType() == ActivitiEventType.ENTITY_DELETED) {
                onDelete(event);
            } else if (event.getType() == ActivitiEventType.ENTITY_UPDATED) {
                onUpdate(event);
            } else {
                // Entity-specific event
                onEntityEvent(event);
            }
        }
    }

    @Override
    public boolean isFailOnException() {
        return failOnException;
    }

    /**
     * @return true, if the event is an {@link ActivitiEntityEvent} and (if needed) the entityClass set in this instance, is assignable from the entity class in the event.
     */
    protected boolean isValidEvent(ActivitiEvent event) {
        boolean valid = false;
        if (event instanceof ActivitiEntityEvent) {
            if (entityClass == null) {
                valid = true;
            } else {
                valid = entityClass.isAssignableFrom(((ActivitiEntityEvent) event).getEntity().getClass());
            }
        }
        return valid;
    }

    /**
     * Called when an entity create event is received.
     */
    protected void onCreate(ActivitiEvent event) {
        // Default implementation is a NO-OP
    }

    /**
     * Called when an entity initialized event is received.
     */
    protected void onInitialized(ActivitiEvent event) {
        // Default implementation is a NO-OP
    }

    /**
     * Called when an entity delete event is received.
     */
    protected void onDelete(ActivitiEvent event) {
        // Default implementation is a NO-OP
    }

    /**
     * Called when an entity update event is received.
     */
    protected void onUpdate(ActivitiEvent event) {
        // Default implementation is a NO-OP
    }

    /**
     * Called when an event is received, which is not a create, an update or delete.
     */
    protected void onEntityEvent(ActivitiEvent event) {
        // Default implementation is a NO-OP
    }
}
