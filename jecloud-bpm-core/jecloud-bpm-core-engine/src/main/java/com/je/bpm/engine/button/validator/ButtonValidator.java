/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.button.validator;

import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.button.Button;
import com.je.bpm.core.model.button.ProcessButton;
import com.je.bpm.core.model.button.TaskButton;
import com.je.bpm.core.model.button.factory.ButtonEnum;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.engine.impl.persistence.entity.TaskEntityImpl;

import java.util.List;

public class ButtonValidator<B> implements ValidatorFunction<Button> {

    @Override
    public Button exec(ButtonValidateParam param, Button button, BpmnModel bpmnModel, TaskEntityImpl taskEntity) {
        String nodeCode = "";
        if (taskEntity != null) {
            nodeCode = taskEntity.getTaskDefinitionKey();
        }
        return getModelTaskButtonByCode(button, bpmnModel, nodeCode);
    }

    public Button getModelTaskButtonByCode(Button button, BpmnModel bpmnModel, String nodeCode) {
        FlowElement flowElement = bpmnModel.getFlowElement(nodeCode);
        if (flowElement == null) {
            return null;
        }
        if (flowElement instanceof KaiteBaseUserTask) {
            KaiteBaseUserTask kaiteBaseUserTask = (KaiteBaseUserTask) flowElement;
            List<TaskButton> list = kaiteBaseUserTask.getButtons().getButtons();
            for (Button taskButton : list) {
                if (taskButton.getCode().equals(button.getCode())) {
                    copy(button, taskButton);
                    return button;
                }
                if (button.getCode().equals(ButtonEnum.CUSTOM_SUBMIT_BTN.getCode()) &&
                        taskButton.getCode().equals(ButtonEnum.SUBMIT_BTN.getCode())) {
                    copy(button, taskButton);
                    return button;
                }
            }
        }
        return null;
    }

    public Button getModelProcessButtonByCode(Button button, BpmnModel bpmnModel) {
        List<ProcessButton> processButtonList = bpmnModel.getMainProcess().getButtons().getButtons();
        for (Button processButton : processButtonList) {
            if (processButton.getCode().equals(button.getCode())) {
                copy(button, processButton);
                return button;
            }
        }
        return null;
    }

    protected void copy(Button button, Button bpmModelButton) {
        button.setPcListeners(bpmModelButton.getPcListeners());
        button.setAppListeners(bpmModelButton.getAppListeners());
        button.setCustomizeComments(bpmModelButton.getCustomizeComments());
        button.setCustomizeName(bpmModelButton.getCustomizeName());
    }

}
