/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.config.CustomEvent;
import com.je.bpm.core.model.event.StartEvent;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.core.model.task.KaiteUserTask;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.delegate.DelegateHelper;
import com.je.bpm.engine.impl.bpmn.behavior.KaiteBaseUserTaskActivityBehavior;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.engine.impl.interceptor.Command;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.deploy.DeploymentManager;
import com.je.bpm.engine.impl.runtime.ProcessInstanceBuilderImpl;
import com.je.bpm.engine.impl.util.ProcessDefinitionRetriever;
import com.je.bpm.engine.impl.util.ProcessInstanceHelper;
import com.je.bpm.engine.repository.ProcessDefinition;
import com.je.bpm.engine.runtime.ProcessInstance;
import com.je.bpm.runtime.shared.operator.AudflagEnum;
import com.je.common.auth.AuthAccount;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.*;

/**
 * 启动流程实例命令
 */
public class StartProcessInstanceCmd<T> implements Command<ProcessInstance>, Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 流程定义key
     */
    protected String processDefinitionKey;
    /**
     * 流程定义id
     */
    protected String processDefinitionId;
    /**
     * 流程变量
     */
    protected Map<String, Object> variables;
    /**
     * 瞬息变量
     */
    protected Map<String, Object> transientVariables;
    /**
     * 业务key
     */
    protected String businessKey;
    protected String assigneeUser;
    /**
     * 租户id
     */
    protected String tenantId;
    protected String processInstanceName;
    protected ProcessInstanceHelper processInstanceHelper;
    protected Map<String, Object> bean;
    protected String prod;
    protected String funcCode;

    public StartProcessInstanceCmd(String processDefinitionKey, String processDefinitionId, String businessKey,
                                   Map<String, Object> variables) {
        this.processDefinitionKey = processDefinitionKey;
        this.processDefinitionId = processDefinitionId;
        this.businessKey = businessKey;
        this.variables = variables;
    }

    public StartProcessInstanceCmd(String processDefinitionKey, String processDefinitionId, String businessKey,
                                   Map<String, Object> variables, Map<String, Object> bean, String prod) {
        this.processDefinitionKey = processDefinitionKey;
        this.processDefinitionId = processDefinitionId;
        this.businessKey = businessKey;
        this.variables = variables;
        this.bean = bean;
        this.prod = prod;
    }


    public StartProcessInstanceCmd(ProcessInstanceBuilderImpl processInstanceBuilder) {
        this(processInstanceBuilder.getProcessDefinitionKey(),
                processInstanceBuilder.getProcessDefinitionId(),
                processInstanceBuilder.getBusinessKey(),
                processInstanceBuilder.getVariables(),
                processInstanceBuilder.getBean(), processInstanceBuilder.getProd());
        this.processInstanceName = processInstanceBuilder.getProcessInstanceName();
        this.transientVariables = processInstanceBuilder.getTransientVariables();
    }

    @Override
    public ProcessInstance execute(CommandContext commandContext) {
        commandContext.addAttribute(CommandContext.BEAN, bean);
        commandContext.addAttribute(CommandContext.PROD, prod);
        DeploymentManager deploymentCache = commandContext.getProcessEngineConfiguration().getDeploymentManager();
        processInstanceHelper = commandContext.getProcessEngineConfiguration().getProcessInstanceHelper();
        //获取流程定义
        ProcessDefinitionRetriever processRetriever = new ProcessDefinitionRetriever(this.tenantId, deploymentCache);
        ProcessDefinition processDefinition = processRetriever.getProcessDefinition(this.processDefinitionId, this.processDefinitionKey);

        //找到第一个和第二个节点的id
        BpmnModel bpmnModel = commandContext.getProcessEngineConfiguration().getRepositoryService().getBpmnModel(processDefinition.getId(), "", businessKey);
        Process mainProcess = bpmnModel.getMainProcess();
        StartEvent startEvent = findStartEvent(mainProcess.getFlowElements());
        if (startEvent == null) {
            throw new ActivitiException("Can't find the start event from this process.");
        }
        String firstTaskRef = startEvent.getOutgoingFlows().get(0).getTargetRef();
        //找到第一个任务节点
        KaiteBaseUserTask firstBaseUserTask = (KaiteBaseUserTask) bpmnModel.getFlowElement(firstTaskRef);
        if (!(firstBaseUserTask instanceof KaiteUserTask)) {
            throw new ActivitiException("Start process only effective on the first task is KaiteUserTask.");
        }
        String firstTaskId = firstBaseUserTask.getId();
        if (variables == null) {
            variables = new HashMap<>();
        }

        if (StringUtils.isEmpty(assigneeUser)) {
            AuthAccount activitiUser = Authentication.getAuthenticatedUser();
            if (activitiUser == null) {
                throw new ActivitiException("Can not get logined user from the context.");
            }
            assigneeUser = activitiUser.getDeptId();
        }

        //设置用户
        if (transientVariables == null) {
            transientVariables = new HashMap<>();
        }
        transientVariables.put(KaiteBaseUserTaskActivityBehavior.DIRECT_TASK_ID, firstBaseUserTask.getId());
        transientVariables.put(KaiteBaseUserTaskActivityBehavior.DIRECT_TASK_NAME, firstBaseUserTask.getName());
        transientVariables.put(KaiteBaseUserTaskActivityBehavior.DIRECT_TASK_TARGET, firstBaseUserTask.getIncomingFlows().get(0).getId());


        JSONArray jsonArray = new JSONArray();
        JSONObject assigneeUserJsonObject = new JSONObject();
        assigneeUserJsonObject.put("nodeId", firstBaseUserTask.getId());
        assigneeUserJsonObject.put("nodeName", firstBaseUserTask.getName());
        assigneeUserJsonObject.put("assignee", assigneeUser);
        assigneeUserJsonObject.put("assigneeName", Authentication.getAuthenticatedUser().getName());
        jsonArray.add(assigneeUserJsonObject);
        Context.getCommandContext().addAttribute(KaiteBaseUserTaskActivityBehavior.UPCOMINGINFO, DelegateHelper.buildUpcomingInfo
                (bean, "启动", SubmitTypeEnum.START, businessKey, "", null, jsonArray.toJSONString()));
        variables.put(firstTaskId, assigneeUser);
        ProcessInstance processInstance = createAndStartProcessInstance(processDefinition, businessKey, processInstanceName, variables, transientVariables);

        processInstanceHelper.buildStartBeanInfo(bean);
        //执行自定义事件
        List<Map<String, String>> assignees = new ArrayList<>();
        commandContext.getProcessEngineConfiguration().getProcessInstanceHelper().invokeEvent
                (mainProcess.getCustomEventListeners(), firstBaseUserTask.getName(),
                        firstBaseUserTask.getId(), firstBaseUserTask.getIncomingFlows().get(0).getId(),
                        CustomEvent.CustomEventEnum.PROCESS_START, commandContext, "启动", "", OperatorEnum.PROCESS_EMPTY_START_OPERATOR,
                        businessKey, bpmnModel, assignees);
        bean = commandContext.getBean();
        bean.put("SY_PDID", processInstance.getProcessDefinitionId());
        bean.put("SY_PIID", processInstance.getProcessInstanceId());
        bean.put("SY_AUDFLAG", AudflagEnum.WAIT.toString());
        commandContext.updateBean(bean, businessKey, bpmnModel.getMainProcess().getProcessConfig());
        return processInstance;
    }


    protected ProcessInstance createAndStartProcessInstance(ProcessDefinition processDefinition, String businessKey, String processInstanceName,
                                                            Map<String, Object> variables, Map<String, Object> transientVariables) {
        return processInstanceHelper.createAndStartProcessInstance(processDefinition, businessKey, processInstanceName, variables, transientVariables);
    }

    private StartEvent findStartEvent(Collection<FlowElement> elements) {
        StartEvent startEvent = null;
        for (FlowElement eachElement : elements) {
            if (eachElement instanceof StartEvent) {
                startEvent = (StartEvent) eachElement;
                break;
            }
        }
        return startEvent;
    }

}
