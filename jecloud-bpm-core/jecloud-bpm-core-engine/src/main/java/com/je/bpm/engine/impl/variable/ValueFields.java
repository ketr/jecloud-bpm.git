/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.variable;

import com.je.bpm.engine.internal.Internal;

/**
 * Common interface for regular and historic variable entities.
 */
@Internal
public interface ValueFields {

    /**
     * @return the name of the variable
     */
    String getName();

    /**
     * @return the process instance id of the variable
     */
    String getProcessInstanceId();

    /**
     * @return the execution id of the variable
     */
    String getExecutionId();

    /**
     * @return the task id of the variable
     */
    String getTaskId();

    /**
     * @return the first text value, if any, or null.
     */
    String getTextValue();

    /**
     * Sets the first text value. A value of null is allowed.
     */
    void setTextValue(String textValue);

    /**
     * @return the second text value, if any, or null.
     */
    String getTextValue2();

    /**
     * Sets second text value. A value of null is allowed.
     */
    void setTextValue2(String textValue2);

    /**
     * @return the long value, if any, or null.
     */
    Long getLongValue();

    /**
     * Sets the long value. A value of null is allowed.
     */
    void setLongValue(Long longValue);

    /**
     * @return the double value, if any, or null.
     */
    Double getDoubleValue();

    /**
     * Sets the double value. A value of null is allowed.
     */
    void setDoubleValue(Double doubleValue);

    /**
     * @return the byte array value, if any, or null.
     */
    byte[] getBytes();

    /**
     * Sets the byte array value. A value of null is allowed.
     */
    void setBytes(byte[] bytes);

    Object getCachedValue();

    void setCachedValue(Object cachedValue);

}
