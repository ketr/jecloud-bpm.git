/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.delegate.event.ActivitiEventType;
import com.je.bpm.engine.delegate.event.impl.ActivitiEventBuilder;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.persistence.entity.data.AttachmentDataManager;
import com.je.bpm.engine.impl.persistence.entity.data.DataManager;
import com.je.bpm.engine.internal.Internal;
import com.je.bpm.engine.task.Attachment;
import com.je.bpm.engine.task.Task;

import java.util.List;


@Internal
public class AttachmentEntityManagerImpl extends AbstractEntityManager<AttachmentEntity> implements AttachmentEntityManager {

  protected AttachmentDataManager attachmentDataManager;

  public AttachmentEntityManagerImpl(ProcessEngineConfigurationImpl processEngineConfiguration, AttachmentDataManager attachmentDataManager) {
    super(processEngineConfiguration);
    this.attachmentDataManager = attachmentDataManager;
  }

  @Override
  protected DataManager<AttachmentEntity> getDataManager() {
    return attachmentDataManager;
  }

  @Override
  public List<AttachmentEntity> findAttachmentsByProcessInstanceId(String processInstanceId) {
    checkHistoryEnabled();
    return attachmentDataManager.findAttachmentsByProcessInstanceId(processInstanceId);
  }

  @Override
  public List<AttachmentEntity> findAttachmentsByTaskId(String taskId) {
    checkHistoryEnabled();
    return attachmentDataManager.findAttachmentsByTaskId(taskId);
  }

  @Override
  public void deleteAttachmentsByTaskId(String taskId) {
    checkHistoryEnabled();
    List<AttachmentEntity> attachments = findAttachmentsByTaskId(taskId);
    boolean dispatchEvents = getEventDispatcher().isEnabled();

    String processInstanceId = null;
    String processDefinitionId = null;
    String executionId = null;

    if (dispatchEvents && attachments != null && !attachments.isEmpty()) {
      // Forced to fetch the task to get hold of the process definition
      // for event-dispatching, if available
      Task task = getTaskEntityManager().findById(taskId);
      if (task != null) {
        processDefinitionId = task.getProcessDefinitionId();
        processInstanceId = task.getProcessInstanceId();
        executionId = task.getExecutionId();
      }
    }

    for (Attachment attachment : attachments) {
      String contentId = attachment.getContentId();
      if (contentId != null) {
        getByteArrayEntityManager().deleteByteArrayById(contentId);
      }

      attachmentDataManager.delete((AttachmentEntity) attachment);

      if (dispatchEvents) {
        getEventDispatcher().dispatchEvent(
            ActivitiEventBuilder.createEntityEvent(ActivitiEventType.ENTITY_DELETED, attachment, executionId, processInstanceId, processDefinitionId));
      }
    }
  }

  protected void checkHistoryEnabled() {
    if (!getHistoryManager().isHistoryEnabled()) {
      throw new ActivitiException("In order to use attachments, history should be enabled");
    }
  }

  public AttachmentDataManager getAttachmentDataManager() {
    return attachmentDataManager;
  }

  public void setAttachmentDataManager(AttachmentDataManager attachmentDataManager) {
    this.attachmentDataManager = attachmentDataManager;
  }

}
