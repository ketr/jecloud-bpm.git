/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.history;

import com.je.bpm.engine.internal.Internal;

import java.util.Date;

/**
 * Represents one execution of an activity and it's stored permanent for statistics, audit and other business intelligence purposes.
 */
@Internal
public interface HistoricActivityInstance extends HistoricData {

    /**
     * The unique identifier of this historic activity instance.
     */
    String getId();

    /**
     * The unique identifier of the activity in the process
     */
    String getActivityId();

    /**
     * The display name for the activity
     */
    String getActivityName();

    /**
     * The XML tag of the activity as in the process file
     */
    String getActivityType();

    /**
     * Process definition reference
     */
    String getProcessDefinitionId();

    /**
     * Process instance reference
     */
    String getProcessInstanceId();

    /**
     * Execution reference
     */
    String getExecutionId();

    /**
     * The corresponding task in case of task activity
     */
    String getTaskId();

    /**
     * The called process instance in case of call activity
     */
    String getCalledProcessInstanceId();

    /**
     * Assignee in case of user task activity
     */
    String getAssignee();

    /**
     * Time when the activity instance started
     */
    Date getStartTime();

    /**
     * Time when the activity instance ended
     */
    Date getEndTime();

    /**
     * Difference between {@link #getEndTime()} and {@link #getStartTime()}.
     */
    Long getDurationInMillis();

    /**
     * Returns the delete reason for this activity, if any was set (if completed normally, no delete reason is set)
     */
    String getDeleteReason();

    /**
     * Returns the tenant identifier for the historic activity
     */
    String getTenantId();
}
