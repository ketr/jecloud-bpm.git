/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.internal.Internal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

@Internal
public class CommentEntityImpl extends AbstractEntityNoRevision implements CommentEntity, Serializable {

  private static final long serialVersionUID = 1L;

  public static String MESSAGE_PARTS_MARKER = "_|_";
  public static Pattern MESSAGE_PARTS_MARKER_REGEX = Pattern.compile("_\\|_");

  // If comments would be removable, revision needs to be added!

  protected String type;
  protected String userId;
  protected Date time;
  protected String taskId;
  protected String processInstanceId;
  protected String action;
  protected String message;
  protected String fullMessage;

  public CommentEntityImpl() {

  }

  @Override
  public Object getPersistentState() {
    return CommentEntityImpl.class;
  }

  @Override
  public byte[] getFullMessageBytes() {
    return (fullMessage != null ? fullMessage.getBytes() : null);
  }

  @Override
  public void setFullMessageBytes(byte[] fullMessageBytes) {
    fullMessage = (fullMessageBytes != null ? new String(fullMessageBytes) : null);
  }

  @Override
  public void setMessage(String[] messageParts) {
    StringBuilder stringBuilder = new StringBuilder();
    for (String part : messageParts) {
      if (part != null) {
        stringBuilder.append(part.replace(MESSAGE_PARTS_MARKER, " | "));
        stringBuilder.append(MESSAGE_PARTS_MARKER);
      } else {
        stringBuilder.append("null");
        stringBuilder.append(MESSAGE_PARTS_MARKER);
      }
    }
    for (int i = 0; i < MESSAGE_PARTS_MARKER.length(); i++) {
      stringBuilder.deleteCharAt(stringBuilder.length() - 1);
    }
    message = stringBuilder.toString();
  }

  @Override
  public List<String> getMessageParts() {
    if (message == null) {
      return null;
    }
    List<String> messageParts = new ArrayList<String>();

    String[] parts = MESSAGE_PARTS_MARKER_REGEX.split(message);
    for (String part : parts) {
      if ("null".equals(part)) {
        messageParts.add(null);
      } else {
        messageParts.add(part);
      }
    }
    return messageParts;
  }

  // getters and setters
  // //////////////////////////////////////////////////////

  @Override
  public String getUserId() {
    return userId;
  }

  @Override
  public void setUserId(String userId) {
    this.userId = userId;
  }

  @Override
  public String getTaskId() {
    return taskId;
  }

  @Override
  public void setTaskId(String taskId) {
    this.taskId = taskId;
  }

  @Override
  public String getMessage() {
    return message;
  }

  @Override
  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public Date getTime() {
    return time;
  }

  @Override
  public void setTime(Date time) {
    this.time = time;
  }

  @Override
  public String getProcessInstanceId() {
    return processInstanceId;
  }

  @Override
  public void setProcessInstanceId(String processInstanceId) {
    this.processInstanceId = processInstanceId;
  }

  @Override
  public String getType() {
    return type;
  }

  @Override
  public void setType(String type) {
    this.type = type;
  }

  @Override
  public String getFullMessage() {
    return fullMessage;
  }

  @Override
  public void setFullMessage(String fullMessage) {
    this.fullMessage = fullMessage;
  }

  @Override
  public String getAction() {
    return action;
  }

  @Override
  public void setAction(String action) {
    this.action = action;
  }

}
