/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.asyncexecutor;

import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.internal.Internal;
import com.je.bpm.engine.runtime.Job;

@Internal
public interface AsyncExecutor {

  /**
   * Starts the Async Executor: jobs will be acquired and executed.
   */
  void start();

  /**
   * Stops executing jobs.
   */
  void shutdown();

  /**
   * Offers the provided {@link com.je.bpm.engine.impl.persistence.entity.JobEntity} to this {@link AsyncExecutor} instance
   * to execute. If the offering does not work for some reason, false
   * will be returned (For example when the job queue is full in the {@link DefaultAsyncJobExecutor}).
   */
  boolean executeAsyncJob(Job job);

  /* Getters and Setters */

  void setProcessEngineConfiguration(ProcessEngineConfigurationImpl processEngineConfiguration);

  ProcessEngineConfigurationImpl getProcessEngineConfiguration();

  boolean isAutoActivate();

  void setAutoActivate(boolean isAutoActivate);

  boolean isActive();

  String getLockOwner();

  int getTimerLockTimeInMillis();

  void setTimerLockTimeInMillis(int lockTimeInMillis);

  int getAsyncJobLockTimeInMillis();

  void setAsyncJobLockTimeInMillis(int lockTimeInMillis);

  int getDefaultTimerJobAcquireWaitTimeInMillis();

  void setDefaultTimerJobAcquireWaitTimeInMillis(int waitTimeInMillis);

  int getDefaultAsyncJobAcquireWaitTimeInMillis();

  void setDefaultAsyncJobAcquireWaitTimeInMillis(int waitTimeInMillis);

  public int getDefaultQueueSizeFullWaitTimeInMillis();

  public void setDefaultQueueSizeFullWaitTimeInMillis(int defaultQueueSizeFullWaitTimeInMillis);

  int getMaxAsyncJobsDuePerAcquisition();

  void setMaxAsyncJobsDuePerAcquisition(int maxJobs);

  int getMaxTimerJobsPerAcquisition();

  void setMaxTimerJobsPerAcquisition(int maxJobs);

  int getRetryWaitTimeInMillis();

  void setRetryWaitTimeInMillis(int retryWaitTimeInMillis);

  int getResetExpiredJobsInterval();

  void setResetExpiredJobsInterval(int resetExpiredJobsInterval);

  int getResetExpiredJobsPageSize();

  void setResetExpiredJobsPageSize(int resetExpiredJobsPageSize);

}
