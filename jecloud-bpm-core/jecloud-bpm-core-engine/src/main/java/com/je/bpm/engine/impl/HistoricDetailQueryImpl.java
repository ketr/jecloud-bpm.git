/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl;

import com.je.bpm.engine.history.HistoricDetail;
import com.je.bpm.engine.history.HistoricDetailQuery;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.interceptor.CommandExecutor;
import com.je.bpm.engine.impl.persistence.entity.HistoricDetailVariableInstanceUpdateEntity;

import java.util.List;


public class HistoricDetailQueryImpl extends AbstractQuery<HistoricDetailQuery, HistoricDetail> implements HistoricDetailQuery {

    private static final long serialVersionUID = 1L;
    protected String id;
    protected String taskId;
    protected String processInstanceId;
    protected String executionId;
    protected String activityId;
    protected String activityInstanceId;
    protected String type;
    protected boolean excludeTaskRelated;

    public HistoricDetailQueryImpl() {
    }

    public HistoricDetailQueryImpl(CommandContext commandContext) {
        super(commandContext);
    }

    public HistoricDetailQueryImpl(CommandExecutor commandExecutor) {
        super(commandExecutor);
    }

    @Override
    public HistoricDetailQueryImpl id(String id) {
        this.id = id;
        return this;
    }

    @Override
    public HistoricDetailQueryImpl processInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
        return this;
    }

    @Override
    public HistoricDetailQueryImpl executionId(String executionId) {
        this.executionId = executionId;
        return this;
    }

    public HistoricDetailQueryImpl activityId(String activityId) {
        this.activityId = activityId;
        return this;
    }

    @Override
    public HistoricDetailQueryImpl activityInstanceId(String activityInstanceId) {
        this.activityInstanceId = activityInstanceId;
        return this;
    }

    @Override
    public HistoricDetailQueryImpl taskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public HistoricDetailQueryImpl formProperties() {
        this.type = "FormProperty";
        return this;
    }

    @Override
    public HistoricDetailQueryImpl variableUpdates() {
        this.type = "VariableUpdate";
        return this;
    }

    @Override
    public HistoricDetailQueryImpl excludeTaskDetails() {
        this.excludeTaskRelated = true;
        return this;
    }

    @Override
    public long executeCount(CommandContext commandContext) {
        checkQueryOk();
        return commandContext.getHistoricDetailEntityManager().findHistoricDetailCountByQueryCriteria(this);
    }

    @Override
    public List<HistoricDetail> executeList(CommandContext commandContext, Page page) {
        checkQueryOk();
        List<HistoricDetail> historicDetails = commandContext.getHistoricDetailEntityManager().findHistoricDetailsByQueryCriteria(this, page);

        HistoricDetailVariableInstanceUpdateEntity varUpdate = null;
        if (historicDetails != null) {
            for (HistoricDetail historicDetail : historicDetails) {
                if (historicDetail instanceof HistoricDetailVariableInstanceUpdateEntity) {
                    varUpdate = (HistoricDetailVariableInstanceUpdateEntity) historicDetail;

                    // Touch byte-array to ensure initialized inside context
                    // TODO there should be a generic way to initialize variable
                    // values
                    varUpdate.getBytes();
                    //todo 此处去除了jpa的处理
                }
            }
        }
        return historicDetails;
    }

    // order by
    // /////////////////////////////////////////////////////////////////
    @Override
    public HistoricDetailQueryImpl orderByProcessInstanceId() {
        orderBy(HistoricDetailQueryProperty.PROCESS_INSTANCE_ID);
        return this;
    }

    @Override
    public HistoricDetailQueryImpl orderByTime() {
        orderBy(HistoricDetailQueryProperty.TIME);
        return this;
    }

    @Override
    public HistoricDetailQueryImpl orderByVariableName() {
        orderBy(HistoricDetailQueryProperty.VARIABLE_NAME);
        return this;
    }

    @Override
    public HistoricDetailQueryImpl orderByFormPropertyId() {
        orderBy(HistoricDetailQueryProperty.VARIABLE_NAME);
        return this;
    }

    @Override
    public HistoricDetailQueryImpl orderByVariableRevision() {
        orderBy(HistoricDetailQueryProperty.VARIABLE_REVISION);
        return this;
    }

    @Override
    public HistoricDetailQueryImpl orderByVariableType() {
        orderBy(HistoricDetailQueryProperty.VARIABLE_TYPE);
        return this;
    }

    // getters and setters
    // //////////////////////////////////////////////////////

    public String getId() {
        return id;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getActivityId() {
        return activityId;
    }

    public String getType() {
        return type;
    }

    public boolean getExcludeTaskRelated() {
        return excludeTaskRelated;
    }

    public String getExecutionId() {
        return executionId;
    }

    public String getActivityInstanceId() {
        return activityInstanceId;
    }

}
