/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;


import com.je.bpm.engine.impl.JobQueryImpl;
import com.je.bpm.engine.impl.Page;
import com.je.bpm.engine.runtime.Job;

import java.util.List;

/**
 * {@link EntityManager} responsible for the {@link JobEntity} class.
 *

 */
public interface JobEntityManager extends EntityManager<JobEntity> {

  /**
   * Insert the {@link JobEntity}, similar to {@link #insert(JobEntity)},
   * but returns a boolean in case the insert did not go through.
   * This could happen if the execution related to the {@link JobEntity} has been removed.
   */
  boolean insertJobEntity(JobEntity timerJobEntity);

  /**
   * Returns {@link JobEntity} that are eligble to be executed.
   *
   * For example used by the default {@link com.je.bpm.engine.impl.cmd.AcquireJobsCmd} command used by
   * the default {@link com.je.bpm.engine.impl.asyncexecutor.AcquireTimerJobsRunnable} implementation to get async jobs
   * that can be executed.
   */
  List<JobEntity> findJobsToExecute(Page page);

  /**
   * Returns all {@link JobEntity} instances related to on {@link ExecutionEntity}.
   */
  List<JobEntity> findJobsByExecutionId(String executionId);

  /**
   * Returns all {@link JobEntity} instances related to on {@link ProcessDefinitionEntity}.
   */
  List<JobEntity> findJobsByProcessDefinitionId(String processDefinitionId);

  /**
   * Returns all {@link JobEntity} instances related to on {@link ProcessDefinitionEntity}.
   */
  List<JobEntity> findJobsByTypeAndProcessDefinitionId(String jobTypeTimer, String id);

  /**
   * Returns all {@link JobEntity} instances related to one process instance {@link ExecutionEntity}.
   */
  List<JobEntity> findJobsByProcessInstanceId(String processInstanceId);

  /**
   * Returns all {@link JobEntity} instance which are expired, which means
   * that the lock time of the {@link JobEntity} is past a certain configurable
   * date and is deemed to be in error.
   */
  List<JobEntity> findExpiredJobs(Page page);

  /**
   * Executes a {@link JobQueryImpl} and returns the matching {@link JobEntity} instances.
   */
  List<Job> findJobsByQueryCriteria(JobQueryImpl jobQuery, Page page);

  /**
   * Same as {@link #findJobsByQueryCriteria(JobQueryImpl, Page)}, but only returns a count
   * and not the instances itself.
   */
  long findJobCountByQueryCriteria(JobQueryImpl jobQuery);

  /**
   * Resets an expired job. These are jobs that were locked, but not completed.
   * Resetting these will make them available for being picked up by other executors.
   */
  void resetExpiredJob(String jobId);

  /**
   * Changes the tenantId for all jobs related to a given {@link DeploymentEntity}.
   */
  void updateJobTenantIdForDeployment(String deploymentId, String newTenantId);

}
