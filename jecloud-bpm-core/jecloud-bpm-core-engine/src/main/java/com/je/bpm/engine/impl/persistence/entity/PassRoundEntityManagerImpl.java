/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.persistence.entity.data.DataManager;
import com.je.bpm.engine.impl.persistence.entity.data.PassRoundDataManager;

import java.util.List;

/**
 * 传阅实体管理实现
 */
public class PassRoundEntityManagerImpl extends AbstractEntityManager<PassRoundEntity> implements PassRoundEntityManager {

    protected PassRoundDataManager passRoundDataManager;

    public PassRoundEntityManagerImpl(ProcessEngineConfigurationImpl processEngineConfiguration, PassRoundDataManager passRoundDataManager) {
        super(processEngineConfiguration);
        this.passRoundDataManager = passRoundDataManager;
    }

    @Override
    protected DataManager<PassRoundEntity> getDataManager() {
        return passRoundDataManager;
    }

    @Override
    public void read(String piid, String userId, String comment) {
        passRoundDataManager.read(piid, userId, comment);
    }

    @Override
    public List<PassRoundEntity> findByTaskId(String taskId) {
        return passRoundDataManager.findByTaskId(taskId);
    }

    @Override
    public List<PassRoundEntity> findByTaskIdAndUserId(String taskId, String userId) {
        return passRoundDataManager.findByTaskIdAndUserId(taskId, userId);
    }

    @Override
    public List<PassRoundEntity> findByProcessInstanceId(String processInstanceId) {
        return passRoundDataManager.findByProcessInstanceId(processInstanceId);
    }

    @Override
    public List<PassRoundEntity> findByProcessDefinitionId(String processDefinitionId) {
        return passRoundDataManager.findByProcessDefinitionId(processDefinitionId);
    }

    @Override
    public List<PassRoundEntity> findByTo(String taskId, String to, int start, int limit) {
        return passRoundDataManager.findByTo(taskId, to, start, limit);
    }

    @Override
    public List<PassRoundEntity> findByToAndNoReview(String piid, String to) {
        return passRoundDataManager.findByToAndNoReview(piid, to);
    }

    @Override
    public List<PassRoundEntity> findByToAndNoReviewAndTaskId(String piid, String to, String taskId) {
        return passRoundDataManager.findByToAndNoReviewAndTaskId(piid, to, taskId);
    }

    @Override
    public List<PassRoundEntity> findByFrom(String taskId, String from, int start, int limit) {
        return passRoundDataManager.findByFrom(taskId, from, start, limit);
    }

}
