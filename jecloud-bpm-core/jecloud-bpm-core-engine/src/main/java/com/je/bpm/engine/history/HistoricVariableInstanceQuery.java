/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.history;

import com.je.bpm.engine.internal.Internal;
import com.je.bpm.engine.query.Query;

import java.util.Set;

/**
 * Programmatic querying for {@link HistoricVariableInstance}s.
 */
@Internal
public interface HistoricVariableInstanceQuery extends Query<HistoricVariableInstanceQuery, HistoricVariableInstance> {

    /**
     * Only select a historic variable with the given id.
     */
    HistoricVariableInstanceQuery id(String id);

    /**
     * Only select historic process variables with the given process instance.
     */
    HistoricVariableInstanceQuery processInstanceId(String processInstanceId);

    /**
     * Only select historic process variables with the given id.
     **/
    HistoricVariableInstanceQuery executionId(String executionId);

    /**
     * Only select historic process variables whose id is in the given set of ids.
     */
    HistoricVariableInstanceQuery executionIds(Set<String> executionIds);

    /**
     * Only select historic process variables with the given task.
     */
    HistoricVariableInstanceQuery taskId(String taskId);

    /**
     * Only select historic process variables whose id is in the given set of ids.
     */
    HistoricVariableInstanceQuery taskIds(Set<String> taskIds);

    /**
     * Only select historic process variables with the given variable name.
     */
    HistoricVariableInstanceQuery variableName(String variableName);

    /**
     * Only select historic process variables where the given variable name is like.
     */
    HistoricVariableInstanceQuery variableNameLike(String variableNameLike);

    /**
     * Only select historic process variables which were not set task-local.
     */
    HistoricVariableInstanceQuery excludeTaskVariables();

    /**
     * Don't initialize variable values. This is foremost a way to deal with variable delete queries
     */
    HistoricVariableInstanceQuery excludeVariableInitialization();

    /**
     * only select historic process variables with the given name and value
     */
    HistoricVariableInstanceQuery variableValueEquals(String variableName, Object variableValue);

    /**
     * only select historic process variables that don't have the given name and value
     */
    HistoricVariableInstanceQuery variableValueNotEquals(String variableName, Object variableValue);

    /**
     * only select historic process variables like the given name and value
     */
    HistoricVariableInstanceQuery variableValueLike(String variableName, String variableValue);

    /**
     * only select historic process variables like the given name and value (case insensitive)
     */
    HistoricVariableInstanceQuery variableValueLikeIgnoreCase(String variableName, String variableValue);

    HistoricVariableInstanceQuery orderByProcessInstanceId();

    HistoricVariableInstanceQuery orderByVariableName();

}
