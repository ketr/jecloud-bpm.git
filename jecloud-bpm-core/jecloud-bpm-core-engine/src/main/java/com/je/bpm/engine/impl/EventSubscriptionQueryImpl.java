/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl;

import com.je.bpm.engine.ActivitiIllegalArgumentException;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.interceptor.CommandExecutor;
import com.je.bpm.engine.impl.persistence.entity.EventSubscriptionEntity;

import java.io.Serializable;
import java.util.List;

/**

 */
public class EventSubscriptionQueryImpl extends AbstractQuery<EventSubscriptionQueryImpl, EventSubscriptionEntity> implements Serializable {

  private static final long serialVersionUID = 1L;

  protected String eventSubscriptionId;
  protected String eventName;
  protected String eventType;
  protected String executionId;
  protected String processInstanceId;
  protected String activityId;
  protected String tenantId;
  protected String configuration;

  public EventSubscriptionQueryImpl(CommandContext commandContext) {
    super(commandContext);
  }

  public EventSubscriptionQueryImpl(CommandExecutor commandExecutor) {
    super(commandExecutor);
  }

  public EventSubscriptionQueryImpl eventSubscriptionId(String eventSubscriptionId) {
    if (eventSubscriptionId == null) {
      throw new ActivitiIllegalArgumentException("Provided event subscription id is null");
    }
    this.eventSubscriptionId = eventSubscriptionId;
    return this;
  }

  public EventSubscriptionQueryImpl eventName(String eventName) {
    if (eventName == null) {
      throw new ActivitiIllegalArgumentException("Provided event name is null");
    }
    this.eventName = eventName;
    return this;
  }

  public EventSubscriptionQueryImpl executionId(String executionId) {
    if (executionId == null) {
      throw new ActivitiIllegalArgumentException("Provided execution id is null");
    }
    this.executionId = executionId;
    return this;
  }

  public EventSubscriptionQueryImpl processInstanceId(String processInstanceId) {
    if (processInstanceId == null) {
      throw new ActivitiIllegalArgumentException("Provided process instance id is null");
    }
    this.processInstanceId = processInstanceId;
    return this;
  }

  public EventSubscriptionQueryImpl activityId(String activityId) {
    if (activityId == null) {
      throw new ActivitiIllegalArgumentException("Provided activity id is null");
    }
    this.activityId = activityId;
    return this;
  }

  public EventSubscriptionQueryImpl eventType(String eventType) {
    if (eventType == null) {
      throw new ActivitiIllegalArgumentException("Provided event type is null");
    }
    this.eventType = eventType;
    return this;
  }

  public String getTenantId() {
    return tenantId;
  }

  public EventSubscriptionQueryImpl tenantId(String tenantId) {
    this.tenantId = tenantId;
    return this;
  }

  public EventSubscriptionQueryImpl configuration(String configuration) {
      this.configuration = configuration;
      return this;
    }


  public EventSubscriptionQueryImpl orderByCreated() {
    return orderBy(EventSubscriptionQueryProperty.CREATED);
  }

  // results //////////////////////////////////////////

  @Override
  public long executeCount(CommandContext commandContext) {
    checkQueryOk();
    return commandContext.getEventSubscriptionEntityManager().findEventSubscriptionCountByQueryCriteria(this);
  }

  @Override
  public List<EventSubscriptionEntity> executeList(CommandContext commandContext, Page page) {
    checkQueryOk();
    return commandContext.getEventSubscriptionEntityManager().findEventSubscriptionsByQueryCriteria(this, page);
  }

  // getters //////////////////////////////////////////

  public String getEventSubscriptionId() {
    return eventSubscriptionId;
  }

  public String getEventName() {
    return eventName;
  }

  public String getEventType() {
    return eventType;
  }

  public String getExecutionId() {
    return executionId;
  }

  public String getProcessInstanceId() {
    return processInstanceId;
  }

  public String getActivityId() {
    return activityId;
  }

  public String getConfiguration() {
      return configuration;
    }


}
