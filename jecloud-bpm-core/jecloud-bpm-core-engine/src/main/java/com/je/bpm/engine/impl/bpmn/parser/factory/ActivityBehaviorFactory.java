/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.parser.factory;

import com.je.bpm.core.model.*;
import com.je.bpm.core.model.event.*;
import com.je.bpm.core.model.gateway.EventGateway;
import com.je.bpm.core.model.gateway.ExclusiveGateway;
import com.je.bpm.core.model.gateway.InclusiveGateway;
import com.je.bpm.core.model.gateway.ParallelGateway;
import com.je.bpm.core.model.message.Message;
import com.je.bpm.core.model.process.SubProcess;
import com.je.bpm.core.model.process.Transaction;
import com.je.bpm.core.model.signal.Signal;
import com.je.bpm.core.model.task.*;
import com.je.bpm.engine.impl.bpmn.behavior.*;
import com.je.bpm.engine.impl.bpmn.helper.ClassDelegate;
import com.je.bpm.engine.impl.delegate.ActivityBehavior;
import com.je.bpm.engine.internal.Internal;

/**
 * Factory class used by the {@link com.je.bpm.engine.impl.bpmn.parser.BpmnParser} and {@link com.je.bpm.engine.impl.bpmn.parser.BpmnParse} to instantiate the behaviour classes. For example when parsing an exclusive gateway, this factory will be requested to create a
 * new {@link ActivityBehavior} that will be set on the {@link ActivityImpl} of that step of the process and will implement the spec-compliant behavior of the exclusive gateway.
 *
 * You can provide your own implementation of this class. This way, you can give different execution semantics to a standard bpmn xml construct. Eg. you could tweak the exclusive gateway to do
 * something completely different if you would want that. Creating your own {@link ActivityBehaviorFactory} is only advisable if you want to change the default behavior of any BPMN default construct.
 * And even then, think twice, because it won't be spec compliant bpmn anymore.
 *
 * Note that you can always express any custom step as a service task with a class delegation.
 *
 * The easiest and advisable way to implement your own {@link ActivityBehaviorFactory} is to extend the {@link DefaultActivityBehaviorFactory} class and override the method specific to the
 * {@link ActivityBehavior} you want to change.
 *
 * An instance of this interface can be injected in the {@link com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl} and its subclasses.
 */
@Internal
public interface ActivityBehaviorFactory {

    NoneStartEventActivityBehavior createNoneStartEventActivityBehavior(StartEvent startEvent);

    TaskActivityBehavior createTaskActivityBehavior(Task task);

    ManualTaskActivityBehavior createManualTaskActivityBehavior(ManualTask manualTask);

    ReceiveTaskActivityBehavior createReceiveTaskActivityBehavior(ReceiveTask receiveTask);

    UserTaskActivityBehavior createUserTaskActivityBehavior(UserTask userTask);

    ClassDelegate createClassDelegateServiceTask(ServiceTask serviceTask);

    ServiceTaskDelegateExpressionActivityBehavior createServiceTaskDelegateExpressionActivityBehavior(ServiceTask serviceTask);

    ActivityBehavior createDefaultServiceTaskBehavior(ServiceTask serviceTask);

    ServiceTaskExpressionActivityBehavior createServiceTaskExpressionActivityBehavior(ServiceTask serviceTask);

    // We do not want a hard dependency on the Mule module, hence we return
    // ActivityBehavior and instantiate the delegate instance using a string instead of the Class itself.
    ActivityBehavior createMuleActivityBehavior(ServiceTask serviceTask);

    ActivityBehavior createMuleActivityBehavior(SendTask sendTask);

    ActivityBehavior createCamelActivityBehavior(ServiceTask serviceTask);

    ActivityBehavior createCamelActivityBehavior(SendTask sendTask);

    ShellActivityBehavior createShellActivityBehavior(ServiceTask serviceTask);

    ActivityBehavior createBusinessRuleTaskActivityBehavior(BusinessRuleTask businessRuleTask);

    ScriptTaskActivityBehavior createScriptTaskActivityBehavior(ScriptTask scriptTask);

    ExclusiveGatewayActivityBehavior createExclusiveGatewayActivityBehavior(ExclusiveGateway exclusiveGateway);

    ParallelGatewayActivityBehavior createParallelGatewayActivityBehavior(ParallelGateway parallelGateway);

    InclusiveGatewayActivityBehavior createInclusiveGatewayActivityBehavior(InclusiveGateway inclusiveGateway);

    EventBasedGatewayActivityBehavior createEventBasedGatewayActivityBehavior(EventGateway eventGateway);

    SequentialMultiInstanceBehavior createSequentialMultiInstanceBehavior(Activity activity, AbstractBpmnActivityBehavior innerActivityBehavior);

    ParallelMultiInstanceBehavior createParallelMultiInstanceBehavior(Activity activity, AbstractBpmnActivityBehavior innerActivityBehavior);

    SubProcessActivityBehavior createSubprocessActivityBehavior(SubProcess subProcess);

    EventSubProcessErrorStartEventActivityBehavior createEventSubProcessErrorStartEventActivityBehavior(StartEvent startEvent);

    EventSubProcessMessageStartEventActivityBehavior createEventSubProcessMessageStartEventActivityBehavior(StartEvent startEvent, MessageEventDefinition messageEventDefinition);

    AdhocSubProcessActivityBehavior createAdhocSubprocessActivityBehavior(SubProcess subProcess);

    CallActivityBehavior createCallActivityBehavior(CallActivity callActivity);

    TransactionActivityBehavior createTransactionActivityBehavior(Transaction transaction);

    IntermediateCatchEventActivityBehavior createIntermediateCatchEventActivityBehavior(IntermediateCatchEvent intermediateCatchEvent);

    IntermediateCatchMessageEventActivityBehavior createIntermediateCatchMessageEventActivityBehavior(IntermediateCatchEvent intermediateCatchEvent,MessageEventDefinition messageEventDefinition);

    IntermediateCatchTimerEventActivityBehavior createIntermediateCatchTimerEventActivityBehavior(IntermediateCatchEvent intermediateCatchEvent, TimerEventDefinition timerEventDefinition);

    IntermediateCatchSignalEventActivityBehavior createIntermediateCatchSignalEventActivityBehavior(IntermediateCatchEvent intermediateCatchEvent,SignalEventDefinition signalEventDefinition, Signal signal);

    IntermediateThrowNoneEventActivityBehavior createIntermediateThrowNoneEventActivityBehavior(ThrowEvent throwEvent);

    IntermediateThrowSignalEventActivityBehavior createIntermediateThrowSignalEventActivityBehavior(ThrowEvent throwEvent, SignalEventDefinition signalEventDefinition, Signal signal);

    IntermediateThrowCompensationEventActivityBehavior createIntermediateThrowCompensationEventActivityBehavior(ThrowEvent throwEvent, CompensateEventDefinition compensateEventDefinition);

    IntermediateThrowMessageEventActivityBehavior createThrowMessageEventActivityBehavior(ThrowEvent throwEvent,MessageEventDefinition messageEventDefinition,Message message);

    NoneEndEventActivityBehavior createNoneEndEventActivityBehavior(EndEvent endEvent);

    ErrorEndEventActivityBehavior createErrorEndEventActivityBehavior(EndEvent endEvent, ErrorEventDefinition errorEventDefinition);

    CancelEndEventActivityBehavior createCancelEndEventActivityBehavior(EndEvent endEvent);

    TerminateEndEventActivityBehavior createTerminateEndEventActivityBehavior(EndEvent endEvent);

    BoundaryEventActivityBehavior createBoundaryEventActivityBehavior(BoundaryEvent boundaryEvent, boolean interrupting);

    BoundaryCancelEventActivityBehavior createBoundaryCancelEventActivityBehavior(CancelEventDefinition cancelEventDefinition);

    BoundaryTimerEventActivityBehavior createBoundaryTimerEventActivityBehavior(BoundaryEvent boundaryEvent, TimerEventDefinition timerEventDefinition, boolean interrupting);

    BoundarySignalEventActivityBehavior createBoundarySignalEventActivityBehavior(BoundaryEvent boundaryEvent, SignalEventDefinition signalEventDefinition, Signal signal, boolean interrupting);

    BoundaryMessageEventActivityBehavior createBoundaryMessageEventActivityBehavior(BoundaryEvent boundaryEvent, MessageEventDefinition messageEventDefinition, boolean interrupting);

    BoundaryCompensateEventActivityBehavior createBoundaryCompensateEventActivityBehavior(BoundaryEvent boundaryEvent, CompensateEventDefinition compensateEventDefinition, boolean interrupting);

    ThrowMessageEndEventActivityBehavior createThrowMessageEndEventActivityBehavior(EndEvent endEvent, MessageEventDefinition messageEventDefinition, Message message);

    /**
     * 创建凯特用户任务行为
     * @param kaiteUserTask
     * @return
     */
    KaiteUserTaskActivityBehavior createKaiteUserTaskActivityBehavior(KaiteUserTask kaiteUserTask);

    /**
     * 多人审批用户任务行为
     * @param kaiteMultiUserTask
     * @return
     */
    KaiteMultiUserTaskActivityBehavior createKaiteMultiUserTaskActivityBehavior(KaiteMultiUserTask kaiteMultiUserTask);

    /**
     * 创建自由（循环）节点活动行为
     * @param kaiteLoopUserTask
     * @return
     */
    KaiteLoopUserTaskActivityBehavior createKaiteLoopUserTaskActivityBehavior(KaiteLoopUserTask kaiteLoopUserTask);

    /**
     * 创建固定人节点活动行为
     * @param kaiteFixedUserTask
     * @return
     */
    KaiteFixedUserTaskActivityBehavior createKaiteFixedUserTaskActivityBehavior(KaiteFixedUserTask kaiteFixedUserTask);

    /**
     * 创建随机节点活动行为
     * @param kaiteRandomUserTask
     * @return
     */
    KaiteRandomUserTaskActivityBehavior createKaiteRandomUserTaskActivityBehavior(KaiteRandomUserTask kaiteRandomUserTask);

    /**
     * 创建选择节点活动行为
     * @param kaiteDecideUserTask
     * @return
     */
    KaiteDecideUserTaskActivityBehavior createKaiteDecideUserTaskActivityBehavior(KaiteDecideUserTask kaiteDecideUserTask);

    /**
     * 创建会签节点活动行为
     * @param kaiteCounterSignUserTask
     * @return
     */
    KaiteCounterSignUserTaskActivityBehavior createKaiteCounterSignUserTaskActivityBehavior(KaiteCounterSignUserTask kaiteCounterSignUserTask);

    /**
     * 创建候选节点活动行为
     * @param kaiteCandidateUserTask
     * @return
     */
    KaiteCandidateUserTaskActivityBehavior createKaiteCandidateUserTaskActivityBehavior(KaiteCandidateUserTask kaiteCandidateUserTask);
}
