/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.runtime;

import com.je.bpm.engine.internal.Internal;
import java.util.Date;

/**
 * Represents one job (timer, async job, etc.).
 */
@Internal
public interface Job {

    String JOB_TYPE_TIMER = "timer";
    String JOB_TYPE_MESSAGE = "message";

    boolean DEFAULT_EXCLUSIVE = true;
    int MAX_EXCEPTION_MESSAGE_LENGTH = 255;

    /**
     * Returns the unique identifier for this job.
     */
    String getId();

    /**
     * Returns the date on which this job is supposed to be processed.
     */
    Date getDuedate();

    /**
     * Returns the id of the process instance which execution created the job.
     */
    String getProcessInstanceId();

    /**
     * Returns the specific execution on which the job was created.
     */
    String getExecutionId();

    /**
     * Returns the specific process definition on which the job was created
     */
    String getProcessDefinitionId();

    /**
     * Returns the number of retries this job has left. Whenever the jobexecutor fails to execute the job, this value is decremented. When it hits zero, the job is supposed to be dead and not retried
     * again (ie a manual retry is required then).
     */
    int getRetries();

    /**
     * Returns the message of the exception that occurred, the last time the job was executed. Returns null when no exception occurred.
     * <p>
     * To get the full exception stacktrace, use {@link com.je.bpm.engine.ManagementService#getJobExceptionStacktrace(String)}
     */
    String getExceptionMessage();

    /**
     * Get the tenant identifier for this job.
     */
    String getTenantId();

    /**
     * Is the job exclusive?
     */
    boolean isExclusive();

    /**
     * Get the job type for this job.
     */
    String getJobType();

    /**
     * Get the job handler type.
     */
    String getJobHandlerType();

    /**
     * Get the job configuration.
     */
    String getJobHandlerConfiguration();

}
