/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.persistence.entity.data.DataManager;
import com.je.bpm.engine.impl.persistence.entity.data.ProcessDefinitionInfoDataManager;

public class ProcessDefinitionInfoEntityManagerImpl extends AbstractEntityManager<ProcessDefinitionInfoEntity> implements ProcessDefinitionInfoEntityManager {

    protected ProcessDefinitionInfoDataManager processDefinitionInfoDataManager;

    public ProcessDefinitionInfoEntityManagerImpl(ProcessEngineConfigurationImpl processEngineConfiguration, ProcessDefinitionInfoDataManager processDefinitionInfoDataManager) {

        super(processEngineConfiguration);
        this.processDefinitionInfoDataManager = processDefinitionInfoDataManager;
    }

    @Override
    protected DataManager<ProcessDefinitionInfoEntity> getDataManager() {
        return processDefinitionInfoDataManager;
    }

    @Override
    public void insertProcessDefinitionInfo(ProcessDefinitionInfoEntity processDefinitionInfo) {
        insert(processDefinitionInfo);
    }

    @Override
    public void updateProcessDefinitionInfo(ProcessDefinitionInfoEntity updatedProcessDefinitionInfo) {
        update(updatedProcessDefinitionInfo, true);
    }

    @Override
    public void deleteProcessDefinitionInfo(String processDefinitionId) {
        ProcessDefinitionInfoEntity processDefinitionInfo = findProcessDefinitionInfoByProcessDefinitionId(processDefinitionId);
        if (processDefinitionInfo != null) {
            delete(processDefinitionInfo);
            deleteInfoJson(processDefinitionInfo);
        }
    }

    @Override
    public void updateInfoJson(String id, byte[] json) {
        ProcessDefinitionInfoEntity processDefinitionInfo = findById(id);
        if (processDefinitionInfo != null) {
            ByteArrayRef ref = new ByteArrayRef(processDefinitionInfo.getInfoJsonId());
            ref.setValue("json", json,false);

            if (processDefinitionInfo.getInfoJsonId() == null) {
                processDefinitionInfo.setInfoJsonId(ref.getId());
                updateProcessDefinitionInfo(processDefinitionInfo);
            }
        }
    }

    @Override
    public void deleteInfoJson(ProcessDefinitionInfoEntity processDefinitionInfo) {
        if (processDefinitionInfo.getInfoJsonId() != null) {
            ByteArrayRef ref = new ByteArrayRef(processDefinitionInfo.getInfoJsonId());
            ref.delete(false);
        }
    }

    @Override
    public ProcessDefinitionInfoEntity findProcessDefinitionInfoByProcessDefinitionId(String processDefinitionId) {
        return processDefinitionInfoDataManager.findProcessDefinitionInfoByProcessDefinitionId(processDefinitionId);
    }

    @Override
    public byte[] findInfoJsonById(String infoJsonId) {
        ByteArrayRef ref = new ByteArrayRef(infoJsonId);
        return ref.getBytes(false);
    }
}
