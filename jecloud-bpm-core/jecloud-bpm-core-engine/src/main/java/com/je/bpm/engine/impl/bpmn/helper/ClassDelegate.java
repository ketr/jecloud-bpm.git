/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.helper;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.MapExceptionEntry;
import com.je.bpm.core.model.task.Task;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.ActivitiIllegalArgumentException;
import com.je.bpm.engine.DynamicBpmnConstants;
import com.je.bpm.engine.delegate.*;
import com.je.bpm.engine.impl.bpmn.behavior.AbstractBpmnActivityBehavior;
import com.je.bpm.engine.impl.bpmn.behavior.ServiceTaskJavaDelegateActivityBehavior;
import com.je.bpm.engine.impl.bpmn.parser.FieldDeclaration;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.delegate.ActivityBehavior;
import com.je.bpm.engine.impl.delegate.SubProcessActivityBehavior;
import com.je.bpm.engine.impl.delegate.TriggerableActivityBehavior;
import com.je.bpm.engine.impl.delegate.invocation.ExecutionListenerInvocation;
import com.je.bpm.engine.impl.delegate.invocation.TaskListenerInvocation;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.util.ReflectUtil;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * Helper class for bpmn constructs that allow class delegation.
 * <p>
 * This class will lazily instantiate the referenced classes when needed at runtime.
 */
public class ClassDelegate extends AbstractBpmnActivityBehavior implements TaskListener, ExecutionListener, TransactionDependentExecutionListener, TransactionDependentTaskListener, SubProcessActivityBehavior, CustomPropertiesResolver {

    private static final long serialVersionUID = 1L;

    protected String serviceTaskId;
    protected String className;
    protected List<FieldDeclaration> fieldDeclarations;
    protected ExecutionListener executionListenerInstance;
    protected TransactionDependentExecutionListener transactionDependentExecutionListenerInstance;
    protected TaskListener taskListenerInstance;
    protected TransactionDependentTaskListener transactionDependentTaskListenerInstance;
    protected ActivityBehavior activityBehaviorInstance;
    protected Expression skipExpression;
    protected List<MapExceptionEntry> mapExceptions;
    protected CustomPropertiesResolver customPropertiesResolverInstance;

    public ClassDelegate(String className, List<FieldDeclaration> fieldDeclarations, Expression skipExpression) {
        this.className = className;
        this.fieldDeclarations = fieldDeclarations;
        this.skipExpression = skipExpression;
    }

    public ClassDelegate(String id, String className, List<FieldDeclaration> fieldDeclarations, Expression skipExpression, List<MapExceptionEntry> mapExceptions) {
        this(className, fieldDeclarations, skipExpression);
        this.serviceTaskId = id;
        this.mapExceptions = mapExceptions;
    }

    public ClassDelegate(String className, List<FieldDeclaration> fieldDeclarations) {
        this(className, fieldDeclarations, null);
    }

    public ClassDelegate(Class<?> clazz, List<FieldDeclaration> fieldDeclarations) {
        this(clazz.getName(), fieldDeclarations, null);
    }

    public ClassDelegate(Class<?> clazz, List<FieldDeclaration> fieldDeclarations, Expression skipExpression) {
        this(clazz.getName(), fieldDeclarations, skipExpression);
    }

    // Execution listener
    @Override
    public void notify(DelegateExecution execution) {
        if (executionListenerInstance == null) {
            executionListenerInstance = getExecutionListenerInstance();
        }
        Context.getProcessEngineConfiguration().getDelegateInterceptor().handleInvocation(new ExecutionListenerInvocation(executionListenerInstance, execution));
    }

    // Transaction Dependent execution listener
    @Override
    public void notify(String processInstanceId, String executionId, FlowElement flowElement, Map<String, Object> executionVariables, Map<String, Object> customPropertiesMap) {
        if (transactionDependentExecutionListenerInstance == null) {
            transactionDependentExecutionListenerInstance = getTransactionDependentExecutionListenerInstance();
        }

        // Note that we can't wrap it in the delegate interceptor like usual here due to being executed when the context is already removed.
        transactionDependentExecutionListenerInstance.notify(processInstanceId, executionId, flowElement, executionVariables, customPropertiesMap);
    }

    @Override
    public Map<String, Object> getCustomPropertiesMap(DelegateExecution execution) {
        if (customPropertiesResolverInstance == null) {
            customPropertiesResolverInstance = getCustomPropertiesResolverInstance();
        }
        return customPropertiesResolverInstance.getCustomPropertiesMap(execution);
    }

    // Task listener
    @Override
    public void notify(DelegateTask delegateTask) {
        if (taskListenerInstance == null) {
            taskListenerInstance = getTaskListenerInstance();
        }
        try {
            Context.getProcessEngineConfiguration().getDelegateInterceptor().handleInvocation(new TaskListenerInvocation(taskListenerInstance, delegateTask));
        } catch (Exception e) {
            throw new ActivitiException("Exception while invoking TaskListener: " + e.getMessage(), e);
        }
    }

    @Override
    public void notify(String processInstanceId, String executionId, Task task, Map<String, Object> executionVariables, Map<String, Object> customPropertiesMap) {
        if (transactionDependentTaskListenerInstance == null) {
            transactionDependentTaskListenerInstance = getTransactionDependentTaskListenerInstance();
        }
        transactionDependentTaskListenerInstance.notify(processInstanceId, executionId, task, executionVariables, customPropertiesMap);
    }


    protected ExecutionListener getExecutionListenerInstance() {
        Object delegateInstance = instantiateDelegate(className, fieldDeclarations);
        if (delegateInstance instanceof ExecutionListener) {
            return (ExecutionListener) delegateInstance;
        } else if (delegateInstance instanceof JavaDelegate) {
            return new ServiceTaskJavaDelegateActivityBehavior((JavaDelegate) delegateInstance);
        } else {
            throw new ActivitiIllegalArgumentException(delegateInstance.getClass().getName() + " doesn't implement " + ExecutionListener.class + " nor " + JavaDelegate.class);
        }
    }

    protected TransactionDependentExecutionListener getTransactionDependentExecutionListenerInstance() {
        Object delegateInstance = instantiateDelegate(className, fieldDeclarations);
        if (delegateInstance instanceof TransactionDependentExecutionListener) {
            return (TransactionDependentExecutionListener) delegateInstance;
        } else {
            throw new ActivitiIllegalArgumentException(delegateInstance.getClass().getName() + " doesn't implement " + TransactionDependentExecutionListener.class);
        }
    }

    protected CustomPropertiesResolver getCustomPropertiesResolverInstance() {
        Object delegateInstance = instantiateDelegate(className, fieldDeclarations);
        if (delegateInstance instanceof CustomPropertiesResolver) {
            return (CustomPropertiesResolver) delegateInstance;
        } else {
            throw new ActivitiIllegalArgumentException(delegateInstance.getClass().getName() + " doesn't implement " + CustomPropertiesResolver.class);
        }
    }

    protected TaskListener getTaskListenerInstance() {
        Object delegateInstance = instantiateDelegate(className, fieldDeclarations);
        if (delegateInstance instanceof TaskListener) {
            return (TaskListener) delegateInstance;
        } else {
            throw new ActivitiIllegalArgumentException(delegateInstance.getClass().getName() + " doesn't implement " + TaskListener.class);
        }
    }

    protected TransactionDependentTaskListener getTransactionDependentTaskListenerInstance() {
        Object delegateInstance = instantiateDelegate(className, fieldDeclarations);
        if (delegateInstance instanceof TransactionDependentTaskListener) {
            return (TransactionDependentTaskListener) delegateInstance;
        } else {
            throw new ActivitiIllegalArgumentException(delegateInstance.getClass().getName() + " doesn't implement " + TransactionDependentTaskListener.class);
        }
    }

    // Activity Behavior
    @Override
    public void execute(DelegateExecution execution) {
        boolean isSkipExpressionEnabled = SkipExpressionUtil.isSkipExpressionEnabled(execution, skipExpression);
        if (!isSkipExpressionEnabled || (isSkipExpressionEnabled && !SkipExpressionUtil.shouldSkipFlowElement(execution, skipExpression))) {

            if (Context.getProcessEngineConfiguration().isEnableProcessDefinitionInfoCache()) {
                ObjectNode taskElementProperties = Context.getBpmnOverrideElementProperties(serviceTaskId, execution.getProcessDefinitionId());
                if (taskElementProperties != null && taskElementProperties.has(DynamicBpmnConstants.SERVICE_TASK_CLASS_NAME)) {
                    String overrideClassName = taskElementProperties.get(DynamicBpmnConstants.SERVICE_TASK_CLASS_NAME).asText();
                    if (StringUtils.isNotEmpty(overrideClassName) && !overrideClassName.equals(className)) {
                        className = overrideClassName;
                        activityBehaviorInstance = null;
                    }
                }
            }

            if (activityBehaviorInstance == null) {
                activityBehaviorInstance = getActivityBehaviorInstance();
            }

            try {
                activityBehaviorInstance.execute(execution);
            } catch (BpmnError error) {
                ErrorPropagation.propagateError(error, execution);
            } catch (RuntimeException e) {
                if (!ErrorPropagation.mapException(e, (ExecutionEntity) execution, mapExceptions)) {
                    throw e;
                }
            }
        }
    }

    // Signallable activity behavior
    @Override
    public void trigger(DelegateExecution execution, String signalName, Object signalData) {
        if (activityBehaviorInstance == null) {
            activityBehaviorInstance = getActivityBehaviorInstance();
        }

        if (activityBehaviorInstance instanceof TriggerableActivityBehavior) {
            ((TriggerableActivityBehavior) activityBehaviorInstance).trigger(execution, signalName, signalData);
        } else {
            throw new ActivitiException("signal() can only be called on a " + TriggerableActivityBehavior.class.getName() + " instance");
        }
    }

    // Subprocess activityBehaviour

    @Override
    public void completing(DelegateExecution execution, DelegateExecution subProcessInstance) throws Exception {
        if (activityBehaviorInstance == null) {
            activityBehaviorInstance = getActivityBehaviorInstance();
        }

        if (activityBehaviorInstance instanceof SubProcessActivityBehavior) {
            ((SubProcessActivityBehavior) activityBehaviorInstance).completing(execution, subProcessInstance);
        } else {
            throw new ActivitiException("completing() can only be called on a " + SubProcessActivityBehavior.class.getName() + " instance");
        }
    }

    @Override
    public void completed(DelegateExecution execution) throws Exception {
        if (activityBehaviorInstance == null) {
            activityBehaviorInstance = getActivityBehaviorInstance();
        }

        if (activityBehaviorInstance instanceof SubProcessActivityBehavior) {
            ((SubProcessActivityBehavior) activityBehaviorInstance).completed(execution);
        } else {
            throw new ActivitiException("completed() can only be called on a " + SubProcessActivityBehavior.class.getName() + " instance");
        }
    }

    protected ActivityBehavior getActivityBehaviorInstance() {
        Object delegateInstance = instantiateDelegate(className, fieldDeclarations);

        if (delegateInstance instanceof ActivityBehavior) {
            return determineBehaviour((ActivityBehavior) delegateInstance);
        } else if (delegateInstance instanceof JavaDelegate) {
            return determineBehaviour(new ServiceTaskJavaDelegateActivityBehavior((JavaDelegate) delegateInstance));
        } else {
            throw new ActivitiIllegalArgumentException(delegateInstance.getClass().getName() + " doesn't implement " + JavaDelegate.class.getName() + " nor " + ActivityBehavior.class.getName());
        }
    }

    // Adds properties to the given delegation instance (eg multi instance) if
    // needed
    protected ActivityBehavior determineBehaviour(ActivityBehavior delegateInstance) {
        if (hasMultiInstanceCharacteristics()) {
            multiInstanceActivityBehavior.setInnerActivityBehavior((AbstractBpmnActivityBehavior) delegateInstance);
            return multiInstanceActivityBehavior;
        }
        return delegateInstance;
    }

    protected Object instantiateDelegate(String className, List<FieldDeclaration> fieldDeclarations) {
        return ClassDelegate.defaultInstantiateDelegate(className, fieldDeclarations);
    }

    // --HELPER METHODS (also usable by external classes)
    // ----------------------------------------

    public static Object defaultInstantiateDelegate(Class<?> clazz, List<FieldDeclaration> fieldDeclarations) {
        return defaultInstantiateDelegate(clazz.getName(), fieldDeclarations);
    }

    public static Object defaultInstantiateDelegate(String className, List<FieldDeclaration> fieldDeclarations) {
        Object object = ReflectUtil.instantiate(className);
        applyFieldDeclaration(fieldDeclarations, object);
        return object;
    }

    public static void applyFieldDeclaration(List<FieldDeclaration> fieldDeclarations, Object target) {
        applyFieldDeclaration(fieldDeclarations, target, true);
    }

    public static void applyFieldDeclaration(List<FieldDeclaration> fieldDeclarations, Object target, boolean throwExceptionOnMissingField) {
        if (fieldDeclarations != null) {
            for (FieldDeclaration declaration : fieldDeclarations) {
                applyFieldDeclaration(declaration, target, throwExceptionOnMissingField);
            }
        }
    }

    public static void applyFieldDeclaration(FieldDeclaration declaration, Object target) {
        applyFieldDeclaration(declaration, target, true);
    }

    public static void applyFieldDeclaration(FieldDeclaration declaration, Object target, boolean throwExceptionOnMissingField) {
        Method setterMethod = ReflectUtil.getSetter(declaration.getName(),
                target.getClass(), declaration.getValue().getClass());

        if (setterMethod != null) {
            try {
                setterMethod.invoke(target, declaration.getValue());
            } catch (IllegalArgumentException e) {
                throw new ActivitiException("Error while invoking '" + declaration.getName() + "' on class " + target.getClass().getName(), e);
            } catch (IllegalAccessException e) {
                throw new ActivitiException("Illegal acces when calling '" + declaration.getName() + "' on class " + target.getClass().getName(), e);
            } catch (InvocationTargetException e) {
                throw new ActivitiException("Exception while invoking '" + declaration.getName() + "' on class " + target.getClass().getName(), e);
            }
        } else {
            Field field = ReflectUtil.getField(declaration.getName(), target);
            if (field == null) {
                if (throwExceptionOnMissingField) {
                    throw new ActivitiIllegalArgumentException("Field definition uses unexisting field '" + declaration.getName() + "' on class " + target.getClass().getName());
                } else {
                    return;
                }
            }

            // Check if the delegate field's type is correct
            if (!fieldTypeCompatible(declaration, field)) {
                throw new ActivitiIllegalArgumentException("Incompatible type set on field declaration '" + declaration.getName()
                        + "' for class " + target.getClass().getName()
                        + ". Declared value has type " + declaration.getValue().getClass().getName()
                        + ", while expecting " + field.getType().getName());
            }
            ReflectUtil.setField(field, target, declaration.getValue());

        }
    }

    public static boolean fieldTypeCompatible(FieldDeclaration declaration, Field field) {
        if (declaration.getValue() != null) {
            return field.getType().isAssignableFrom(declaration.getValue().getClass());
        } else {
            // Null can be set any field type
            return true;
        }
    }

    /**
     * returns the class name this {@link ClassDelegate} is configured to. Comes in handy if you want to check which delegates you already have e.g. in a list of listeners
     */
    public String getClassName() {
        return className;
    }

}
