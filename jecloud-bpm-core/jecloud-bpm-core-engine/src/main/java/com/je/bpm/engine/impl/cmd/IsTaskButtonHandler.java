/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.je.bpm.engine.impl.bpmn.behavior.KaiteBaseUserTaskActivityBehavior;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.engine.impl.interceptor.Command;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.task.Task;
import java.io.Serializable;
import java.util.List;
import static com.je.bpm.engine.delegate.DelegateHelper.isCountersignApproval;


/**
 * 是不是节点 按钮的可处理人 （前置处理人，候选，所有人，处理人）
 */
public class IsTaskButtonHandler implements Command<Boolean>, Serializable {

    private static final long serialVersionUID = 1L;

    private Task task;
    private boolean isMulti;
    private boolean getModel;
    private boolean isOwner;
    private String assignee;
    private String owner;
    private List<String> candidateIds;

    public IsTaskButtonHandler(Task task, boolean isMulti, boolean getModel, boolean isOwner, String assignee, String owner, List<String> candidateIds) {
        this.task = task;
        this.isMulti = isMulti;
        this.getModel = getModel;
        this.isOwner = isOwner;
        this.assignee = assignee;
        this.owner = owner;
        this.candidateIds = candidateIds;
    }

    @Override
    public Boolean execute(CommandContext commandContext) {
        String logUserId = Authentication.getAuthenticatedUser().getDeptId();
        if (task instanceof TaskEntity) {
            TaskEntity taskEntity = (TaskEntity) task;
            Boolean isPrevAssignee = false;
            if (!isMulti) {
                Object prevAssigneeObject = taskEntity.getVariable(KaiteBaseUserTaskActivityBehavior.PREV_ASSIGNEE);
                if (prevAssigneeObject != null) {
                    String prevAssignee = (String) prevAssigneeObject;
                    if (prevAssignee.equals(logUserId)) {
                        isPrevAssignee = true;
                    }
                }
                if (isPrevAssignee && getModel) {
                    return true;
                }
            }
        }
        //当前登录人在会签节点上
        if (getModel && isCountersignApproval(task)) {
            return true;
        }
        //节点处理人等于当前登录人
        if (assignee != null && assignee.equals(logUserId)) {
            return true;
        }
        //候选人选包含当前登录人
        if (candidateIds.contains(logUserId)) {
            return true;
        }
        //节点所有者
        if ((owner != null && owner.equals(logUserId)) && isOwner) {
            return true;
        }
        return false;
    }

}
