/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.interceptor;

import com.je.bpm.engine.internal.Internal;

/**
 * A listener that can be used to be notified of lifecycle events of the {@link CommandContext}.
 */
@Internal
public interface CommandContextCloseListener {

    /**
     * Called when the {@link CommandContext} is being closed, but no 'close logic' has been executed.
     * <p>
     * At this point, the {@link com.je.bpm.engine.impl.cfg.TransactionContext} (if applicable) has not yet been committed/rolledback
     * and none of the {@link Session} instances have been flushed.
     * <p>
     * If an exception happens and it is not caught in this method:
     * - The {@link Session} instances will *not* be flushed
     * - The {@link com.je.bpm.engine.impl.cfg.TransactionContext} will be rolled back (if applicable)
     */
    void closing(CommandContext commandContext);

    /**
     * Called when the {@link Session} have been successfully flushed.
     * When an exception happened during the flushing of the sessions, this method will not be called.
     * <p>
     * If an exception happens and it is not caught in this method:
     * - The {@link Session} instances will *not* be flushed
     * - The {@link com.je.bpm.engine.impl.cfg.TransactionContext} will be rolled back (if applicable)
     */
    void afterSessionsFlush(CommandContext commandContext);

    /**
     * Called when the {@link CommandContext} is successfully closed.
     * <p>
     * At this point, the {@link com.je.bpm.engine.impl.cfg.TransactionContext} (if applicable) has been successfully committed
     * and no rollback has happened. All {@link Session} instances have been closed.
     * <p>
     * Note that throwing an exception here does *not* affect the transaction.
     * The {@link CommandContext} will log the exception though.
     */
    void closed(CommandContext commandContext);

    /**
     * Called when the {@link CommandContext} has not been successully closed due to an exception that happened.
     * <p>
     * Note that throwing an exception here does *not* affect the transaction.
     * The {@link CommandContext} will log the exception though.
     */
    void closeFailure(CommandContext commandContext);

}
