/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.el;

import com.je.bpm.engine.delegate.VariableScope;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.el.variable.*;

import javax.el.ELContext;
import javax.el.ELResolver;
import java.beans.FeatureDescriptor;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Implementation of an {@link ELResolver} that resolves expressions with the
 * process variables of a given {@link VariableScope} as context. <br>
 * Also exposes the currently logged in username to be used in expressions (if
 * any)
 */
public class VariableScopeElResolver extends ELResolver {

    protected VariableScope variableScope;
    private List<VariableScopeItemELResolver> variableScopeItemELResolvers;

    public VariableScopeElResolver(VariableScope variableScope) {
        this.variableScope = variableScope;
    }

    @Override
    public Object getValue(ELContext context, Object base, Object property) {
        if (base == null) {
            String variable = (String) property; // according to javadoc, can
            // only be a String

            for (VariableScopeItemELResolver variableScopeItemELResolver : getVariableScopeItemELResolvers()) {
                if (variableScopeItemELResolver.canResolve(variable, variableScope)) {
                    // if not set, the next elResolver in the CompositeElResolver
                    // will be called
                    context.setPropertyResolved(true);

                    return variableScopeItemELResolver.resolve(variable, variableScope);
                }
            }
        }

        // property resolution (eg. bean.value) will be done by the
        // BeanElResolver (part of the CompositeElResolver)
        // It will use the bean resolved in this resolver as base.

        return null;
    }

    protected List<VariableScopeItemELResolver> getVariableScopeItemELResolvers() {
        if (variableScopeItemELResolvers == null) {
            variableScopeItemELResolvers = Arrays.asList(
                    new ExecutionElResolver(),
                    new TaskElResolver(),
                    new AuthenticatedUserELResolver(),
                    new ProcessInitiatorELResolver(),
                    new VariableElResolver(Context.getProcessEngineConfiguration().getObjectMapper()));
        }
        return variableScopeItemELResolvers;
    }

    @Override
    public boolean isReadOnly(ELContext context, Object base, Object property) {
        if (base == null) {
            String variable = (String) property;
            return !variableScope.hasVariable(variable);
        }
        return true;
    }

    @Override
    public void setValue(ELContext context, Object base, Object property, Object value) {
        if (base == null) {
            String variable = (String) property;
            if (variableScope.hasVariable(variable)) {
                variableScope.setVariable(variable, value);
            }
        }
    }

    @Override
    public Class<?> getCommonPropertyType(ELContext arg0, Object arg1) {
        return Object.class;
    }

    @Override
    public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext arg0, Object arg1) {
        return null;
    }

    @Override
    public Class<?> getType(ELContext arg0, Object arg1, Object arg2) {
        return Object.class;
    }

}
