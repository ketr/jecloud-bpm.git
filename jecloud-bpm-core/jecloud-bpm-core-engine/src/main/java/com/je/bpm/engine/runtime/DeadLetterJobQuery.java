/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.runtime;

import com.je.bpm.engine.internal.Internal;
import com.je.bpm.engine.query.Query;

import java.util.Date;

/**
 * Allows programmatic querying of {@link Job}s.
 */
@Internal
public interface DeadLetterJobQuery extends Query<DeadLetterJobQuery, Job> {

  /** Only select jobs with the given id */
  DeadLetterJobQuery jobId(String jobId);

  /** Only select jobs which exist for the given process instance. **/
  DeadLetterJobQuery processInstanceId(String processInstanceId);

  /** Only select jobs which exist for the given execution */
  DeadLetterJobQuery executionId(String executionId);

  /** Only select jobs which exist for the given process definition id */
  DeadLetterJobQuery processDefinitionId(String processDefinitionid);

  /**
   * Only select jobs which are executable, ie. duedate is null or duedate is in the past
   **/
  DeadLetterJobQuery executable();

  /**
   * Only select jobs that are timers. Cannot be used together with {@link #messages()}
   */
  DeadLetterJobQuery timers();

  /**
   * Only select jobs that are messages. Cannot be used together with {@link #timers()}
   */
  DeadLetterJobQuery messages();

  /** Only select jobs where the duedate is lower than the given date. */
  DeadLetterJobQuery duedateLowerThan(Date date);

  /** Only select jobs where the duedate is higher then the given date. */
  DeadLetterJobQuery duedateHigherThan(Date date);

  /** Only select jobs that failed due to an exception. */
  DeadLetterJobQuery withException();

  /** Only select jobs that failed due to an exception with the given message. */
  DeadLetterJobQuery exceptionMessage(String exceptionMessage);

  /**
   * Only select jobs that have the given tenant id.
   */
  DeadLetterJobQuery jobTenantId(String tenantId);

  /**
   * Only select jobs with a tenant id like the given one.
   */
  DeadLetterJobQuery jobTenantIdLike(String tenantIdLike);

  /**
   * Only select jobs that do not have a tenant id.
   */
  DeadLetterJobQuery jobWithoutTenantId();

  // sorting //////////////////////////////////////////

  /**
   * Order by job id (needs to be followed by {@link #asc()} or {@link #desc()}).
   */
  DeadLetterJobQuery orderByJobId();

  /**
   * Order by duedate (needs to be followed by {@link #asc()} or {@link #desc()}).
   */
  DeadLetterJobQuery orderByJobDuedate();

  /**
   * Order by retries (needs to be followed by {@link #asc()} or {@link #desc()}).
   */
  DeadLetterJobQuery orderByJobRetries();

  /**
   * Order by process instance id (needs to be followed by {@link #asc()} or {@link #desc()}).
   */
  DeadLetterJobQuery orderByProcessInstanceId();

  /**
   * Order by execution id (needs to be followed by {@link #asc()} or {@link #desc()}).
   */
  DeadLetterJobQuery orderByExecutionId();

  /**
   * Order by tenant id (needs to be followed by {@link #asc()} or {@link #desc()}).
   */
  DeadLetterJobQuery orderByTenantId();

}
