/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.delegate.VariableScope;
import com.je.bpm.engine.impl.Page;
import com.je.bpm.engine.impl.TimerJobQueryImpl;
import com.je.bpm.engine.internal.Internal;
import com.je.bpm.engine.runtime.Job;

import java.util.List;

/**
 * {@link EntityManager} responsible for {@link TimerJobEntity} instances.
 */
@Internal
public interface TimerJobEntityManager extends EntityManager<TimerJobEntity> {

    /**
     * Insert the {@link TimerJobEntity}, similar to {@link #insert(TimerJobEntity)},
     * but returns a boolean in case the insert did not go through.
     * This could happen if the execution related to the {@link TimerJobEntity}
     * has been removed (for example due to a task complete for a timer boundary on that task).
     */
    boolean insertTimerJobEntity(TimerJobEntity timerJobEntity);

    /**
     * Returns the {@link TimerJobEntity} instances that are elegible to execute,
     * meaning the due date of the timer has been passed.
     */
    List<TimerJobEntity> findTimerJobsToExecute(Page page);

    /**
     * Returns the {@link TimerJobEntity} for a given process definition.
     * <p>
     * This is for example used when deleting a process definition: it finds
     * the {@link TimerJobEntity} representing the timer start events.
     */
    List<TimerJobEntity> findJobsByTypeAndProcessDefinitionId(String type, String processDefinitionId);

    /**
     * The same as {@link #findJobsByTypeAndProcessDefinitionId(String, String)}, but
     * by key and for a specific tenantId.
     */
    List<TimerJobEntity> findJobsByTypeAndProcessDefinitionKeyAndTenantId(String type, String processDefinitionKey, String tenantId);

    /**
     * The same as {@link #findJobsByTypeAndProcessDefinitionId(String, String)}, but
     * by key and specifically for the 'no tenant' mode.
     */
    List<TimerJobEntity> findJobsByTypeAndProcessDefinitionKeyNoTenantId(String type, String processDefinitionKey);

    /**
     * Returns all {@link TimerJobEntity} instances related to on {@link ExecutionEntity}.
     */
    List<TimerJobEntity> findJobsByExecutionId(String id);

    /**
     * Returns all {@link TimerJobEntity} instances related to on {@link ExecutionEntity}.
     */
    List<TimerJobEntity> findJobsByProcessInstanceId(String id);

    /**
     * Executes a {@link com.je.bpm.engine.impl.JobQueryImpl} and returns the matching {@link TimerJobEntity} instances.
     */
    List<Job> findJobsByQueryCriteria(TimerJobQueryImpl jobQuery, Page page);

    /**
     * Same as {@link #findJobsByQueryCriteria(TimerJobQueryImpl, Page)}, but only returns a count
     * and not the instances itself.
     */
    long findJobCountByQueryCriteria(TimerJobQueryImpl jobQuery);

    /**
     * Creates a new {@link TimerJobEntity}, typically when a timer is used in a
     * repeating way. The returns {@link TimerJobEntity} is not yet inserted.
     * <p>
     * Returns null if the timer has finished its repetitions.
     */
    TimerJobEntity createAndCalculateNextTimer(JobEntity timerEntity, VariableScope variableScope);

    /**
     * Changes the tenantId for all jobs related to a given {@link DeploymentEntity}.
     */
    void updateJobTenantIdForDeployment(String deploymentId, String newTenantId);

}
