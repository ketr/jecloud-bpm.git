/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.delegate;

import com.je.bpm.core.model.ActivitiListener;
import com.je.bpm.engine.task.DelegationState;
import com.je.bpm.engine.task.IdentityLink;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

/**

 */
public interface DelegateTask extends VariableScope {

  /** DB id of the task. */
  String getId();

  /** Name or title of the task. */
  String getName();

  /** Change the name of the task. */
  void setName(String name);

  /** Free text description of the task. */
  String getDescription();

  /** Change the description of the task */
  void setDescription(String description);

  /**
   * indication of how important/urgent this task is with a number between 0 and 100 where higher values mean a higher priority and lower values mean lower priority: [0..19] lowest, [20..39] low,
   * [40..59] normal, [60..79] high [80..100] highest
   */
  int getPriority();

  /**
   * indication of how important/urgent this task is with a number between 0 and 100 where higher values mean a higher priority and lower values mean lower priority: [0..19] lowest, [20..39] low,
   * [40..59] normal, [60..79] high [80..100] highest
   */
  void setPriority(int priority);

  /**
   * Reference to the process instance or null if it is not related to a process instance.
   */
  String getProcessInstanceId();

  /**
   * Reference to the path of execution or null if it is not related to a process instance.
   */
  String getExecutionId();

  /**
   * Reference to the process definition or null if it is not related to a process.
   */
  String getProcessDefinitionId();

  /** The date/time when this task was created */
  Date getCreateTime();

  /**
   * The id of the activity in the process defining this task or null if this is not related to a process
   */
  String getTaskDefinitionKey();

  /** Indicated whether this task is suspended or not. */
  boolean isSuspended();

  /** The tenant identifier of this task */
  String getTenantId();

  /** The form key for the user task */
  String getFormKey();

  /** Change the form key of the task */
  void setFormKey(String formKey);

  /** Returns the execution currently at the task. */
  DelegateExecution getExecution();

  /**
   * Returns the event name which triggered the task listener to fire for this task.
   */
  String getEventName();

  ActivitiListener getCurrentActivitiListener();

  /**
   * The current {@link com.je.bpm.engine.task.DelegationState} for this task.
   */
  DelegationState getDelegationState();

  /** Adds the given user as a candidate user to this task. */
  void addCandidateUser(String userId);

  /** Adds multiple users as candidate user to this task. */
  void addCandidateUsers(Collection<String> candidateUsers);

  /** Adds the given group as candidate group to this task */
  void addCandidateGroup(String groupId);

  /** Adds multiple groups as candidate group to this task. */
  void addCandidateGroups(Collection<String> candidateGroups);

  /** The {User.getId() userId} of the person responsible for this task. */
  String getOwner();

  /** The {User.getId() userId} of the person responsible for this task. */
  void setOwner(String owner);

  /**
   * The {User.getId() userId} of the person to which this task is delegated.
   */
  String getAssignee();

  /**
   * The {User.getId() userId} of the person to which this task is delegated.
   */
  void setAssignee(String assignee);

  /** Due date of the task. */
  Date getDueDate();

  /** Change due date of the task. */
  void setDueDate(Date dueDate);

  /**
   * The category of the task. This is an optional field and allows to 'tag' tasks as belonging to a certain category.
   */
  String getCategory();

  /**
   * Change the category of the task. This is an optional field and allows to 'tag' tasks as belonging to a certain category.
   */
  void setCategory(String category);

  /**
   * Involves a user with a task. The type of identity link is defined by the given identityLinkType.
   *
   * @param userId
   *          id of the user involve, cannot be null.
   * @param identityLinkType
   *          type of identityLink, cannot be null (@see {@link com.je.bpm.engine.task.IdentityLinkType}).
   * @throws com.je.bpm.engine.ActivitiObjectNotFoundException
   *           when the task or user doesn't exist.
   */
  void addUserIdentityLink(String userId, String identityLinkType);

  /**
   * Involves a group with group task. The type of identityLink is defined by the given identityLink.
   *
   * @param groupId
   *          id of the group to involve, cannot be null.
   * @param identityLinkType
   *          type of identity, cannot be null (@see {@link com.je.bpm.engine.task.IdentityLinkType}).
   * @throws com.je.bpm.engine.ActivitiObjectNotFoundException
   *           when the task or group doesn't exist.
   */
  void addGroupIdentityLink(String groupId, String identityLinkType);

  /**
   * Convenience shorthand for {@link #deleteUserIdentityLink(String, String)} ; with type {@link com.je.bpm.engine.task.IdentityLinkType#CANDIDATE}
   *
   * @param userId
   *          id of the user to use as candidate, cannot be null.
   * @throws com.je.bpm.engine.ActivitiObjectNotFoundException
   *           when the task or user doesn't exist.
   */
  void deleteCandidateUser(String userId);

  /**
   * Convenience shorthand for {@link #deleteGroupIdentityLink(String, String, String)}; with type {@link com.je.bpm.engine.task.IdentityLinkType#CANDIDATE}
   *
   * @param groupId
   *          id of the group to use as candidate, cannot be null.
   * @throws com.je.bpm.engine.ActivitiObjectNotFoundException
   *           when the task or group doesn't exist.
   */
  void deleteCandidateGroup(String groupId);

  /**
   * Removes the association between a user and a task for the given identityLinkType.
   *
   * @param userId
   *          id of the user involve, cannot be null.
   * @param identityLinkType
   *          type of identityLink, cannot be null (@see {@link com.je.bpm.engine.task.IdentityLinkType}).
   * @throws com.je.bpm.engine.ActivitiObjectNotFoundException
   *           when the task or user doesn't exist.
   */
  void deleteUserIdentityLink(String userId, String identityLinkType);

  /**
   * Removes the association between a group and a task for the given identityLinkType.
   *
   * @param groupId
   *          id of the group to involve, cannot be null.
   * @param identityLinkType
   *          type of identity, cannot be null (@see {@link com.je.bpm.engine.task.IdentityLinkType}).
   * @throws com.je.bpm.engine.ActivitiObjectNotFoundException
   *           when the task or group doesn't exist.
   */
  void deleteGroupIdentityLink(String groupId, String identityLinkType);

  /**
   * Retrieves the candidate users and groups associated with the task.
   *
   * @return set of {@link IdentityLink}s of type {@link com.je.bpm.engine.task.IdentityLinkType#CANDIDATE}.
   */
  Set<IdentityLink> getCandidates();
}
