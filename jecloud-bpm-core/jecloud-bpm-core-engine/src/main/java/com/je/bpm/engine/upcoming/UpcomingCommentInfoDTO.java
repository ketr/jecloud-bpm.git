/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.upcoming;

import com.je.bpm.engine.impl.cmd.SubmitTypeEnum;
import com.je.bpm.engine.impl.identity.Authentication;

import java.util.HashMap;
import java.util.Map;

public class UpcomingCommentInfoDTO {
    private Map<String, Object> bean;
    private String beanId;
    private String comment;
    private SubmitTypeEnum submitType;
    private String submitUserName;
    private String submitUserId;
    private String taskId;
    private Map<String, String> params = new HashMap<>();
    private String assigneeJson;
    private String nodeId;
    private String piid;

    public UpcomingCommentInfoDTO() {

    }

    public static UpcomingCommentInfoDTO build(SubmitTypeEnum submitType, Map<String, Object> bean, String beanId,
                                               String comment, String taskId, Map<String, String> params, String assigneeJson) {
        UpcomingCommentInfoDTO upcomingCommentInfoDTO = new UpcomingCommentInfoDTO();
        upcomingCommentInfoDTO.setSubmitType(submitType);
        upcomingCommentInfoDTO.setBean(bean);
        upcomingCommentInfoDTO.setBeanId(beanId);
        upcomingCommentInfoDTO.setSubmitUserId(Authentication.getAuthenticatedUser().getDeptId());
        upcomingCommentInfoDTO.setSubmitUserName(Authentication.getAuthenticatedUser().getName());
        upcomingCommentInfoDTO.setComment(comment);
        upcomingCommentInfoDTO.setTaskId(taskId);
        upcomingCommentInfoDTO.setParams(params);
        upcomingCommentInfoDTO.setAssigneeJson(assigneeJson);
        return upcomingCommentInfoDTO;
    }

    public static UpcomingCommentInfoDTO build(SubmitTypeEnum submitType, String nodeId, String piid) {
        UpcomingCommentInfoDTO upcomingCommentInfoDTO = new UpcomingCommentInfoDTO();
        upcomingCommentInfoDTO.setSubmitType(submitType);
        upcomingCommentInfoDTO.setNodeId(nodeId);
        upcomingCommentInfoDTO.setPiid(piid);
        return upcomingCommentInfoDTO;
    }


    public Map<String, Object> getBean() {
        return bean;
    }

    public UpcomingCommentInfoDTO setBean(Map<String, Object> bean) {
        this.bean = bean;
        return this;
    }

    public String getBeanId() {
        return beanId;
    }

    public UpcomingCommentInfoDTO setBeanId(String beanId) {
        this.beanId = beanId;
        return this;
    }

    public String getComment() {
        return comment;
    }

    public UpcomingCommentInfoDTO setComment(String comment) {
        this.comment = comment;
        return this;
    }

    public SubmitTypeEnum getSubmitType() {
        return submitType;
    }

    public void setSubmitType(SubmitTypeEnum submitType) {
        this.submitType = submitType;
    }

    public String getSubmitUserName() {
        return submitUserName;
    }

    public void setSubmitUserName(String submitUserName) {
        this.submitUserName = submitUserName;
    }

    public String getSubmitUserId() {
        return submitUserId;
    }

    public void setSubmitUserId(String submitUserId) {
        this.submitUserId = submitUserId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public String getAssigneeJson() {
        return assigneeJson;
    }

    public void setAssigneeJson(String assigneeJson) {
        this.assigneeJson = assigneeJson;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getPiid() {
        return piid;
    }

    public void setPiid(String piid) {
        this.piid = piid;
    }
}
