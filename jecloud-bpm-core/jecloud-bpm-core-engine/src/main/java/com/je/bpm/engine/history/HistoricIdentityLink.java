/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.history;

import com.je.bpm.engine.internal.Internal;

/**
 * Historic counterpart of {@link com.je.bpm.engine.task.IdentityLink} that represents the current state if any runtime link. Will be preserved when the runtime process instance or task is finished.
 */
@Internal
public interface HistoricIdentityLink {

    /**
     * Returns the type of link. See {@link com.je.bpm.engine.task.IdentityLinkType} for the native supported types by Activiti.
     */
    String getType();

    /**
     * If the identity link involves a user, then this will be a non-null id of a user. That userId can be used to query for user information through the {@link UserQuery} API.
     */
    String getUserId();

    /**
     * If the identity link involves a group, then this will be a non-null id of a group. That groupId can be used to query for user information through the {@link GroupQuery} API.
     */
    String getGroupId();

    /**
     * The id of the task associated with this identity link.
     */
    String getTaskId();

    /**
     * The id of the process instance associated with this identity link.
     */
    String getProcessInstanceId();
}
