/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity.data;

import com.je.bpm.engine.impl.persistence.entity.VariableInstanceEntity;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface VariableInstanceDataManager extends DataManager<VariableInstanceEntity> {

    List<VariableInstanceEntity> findVariableInstancesByTaskId(String taskId);

    List<VariableInstanceEntity> findVariableInstancesByTaskIds(Set<String> taskIds);

    List<VariableInstanceEntity> findVariableInstancesByExecutionId(String executionId);

    List<VariableInstanceEntity> findVariableInstancesByExecutionIds(Set<String> executionIds);

    VariableInstanceEntity findVariableInstanceByExecutionAndName(String executionId, String variableName);

    List<VariableInstanceEntity> findVariableInstancesByExecutionAndNames(String executionId, Collection<String> names);

    VariableInstanceEntity findVariableInstanceByTaskAndName(String taskId, String variableName);

    List<VariableInstanceEntity> findVariableInstancesByTaskAndNames(String taskId, Collection<String> names);

    List<VariableInstanceEntity> findVariableInstancesByProcessInstanceId(String piid);

}
