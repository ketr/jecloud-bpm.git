/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.ActivitiIllegalArgumentException;
import com.je.bpm.engine.ActivitiObjectNotFoundException;
import com.je.bpm.engine.ProcessEngineConfiguration;
import com.je.bpm.engine.impl.ProcessDefinitionQueryImpl;
import com.je.bpm.engine.impl.ProcessInstanceQueryImpl;
import com.je.bpm.engine.impl.interceptor.Command;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.jobexecutor.TimerChangeProcessDefinitionSuspensionStateJobHandler;
import com.je.bpm.engine.impl.persistence.entity.*;
import com.je.bpm.engine.repository.ProcessDefinition;
import com.je.bpm.engine.runtime.ProcessInstance;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.util.Collections.singletonList;

/**
 *
 */
public abstract class AbstractSetProcessDefinitionStateCmd implements Command<Void> {

    protected String processDefinitionId;
    protected String processDefinitionKey;
    protected ProcessDefinitionEntity processDefinitionEntity;
    protected boolean includeProcessInstances = false;
    protected Date executionDate;
    protected String tenantId;

    public AbstractSetProcessDefinitionStateCmd(ProcessDefinitionEntity processDefinitionEntity, boolean includeProcessInstances, Date executionDate, String tenantId) {
        this.processDefinitionEntity = processDefinitionEntity;
        this.includeProcessInstances = includeProcessInstances;
        this.executionDate = executionDate;
        this.tenantId = tenantId;
    }

    public AbstractSetProcessDefinitionStateCmd(String processDefinitionId, String processDefinitionKey, boolean includeProcessInstances, Date executionDate, String tenantId) {
        this.processDefinitionId = processDefinitionId;
        this.processDefinitionKey = processDefinitionKey;
        this.includeProcessInstances = includeProcessInstances;
        this.executionDate = executionDate;
        this.tenantId = tenantId;
    }

    @Override
    public Void execute(CommandContext commandContext) {

        List<ProcessDefinitionEntity> processDefinitions = findProcessDefinition(commandContext);

        if (executionDate != null) { // Process definition state change is delayed
            createTimerForDelayedExecution(commandContext, processDefinitions);
        } else { // Process definition state is changed now
            changeProcessDefinitionState(commandContext, processDefinitions);
        }

        return null;
    }

    protected List<ProcessDefinitionEntity> findProcessDefinition(CommandContext commandContext) {

        // If process definition is already provided (eg. when command is called through the DeployCmd)
        // we don't need to do an extra database fetch and we can simply return it, wrapped in a list
        if (processDefinitionEntity != null) {
            return singletonList(processDefinitionEntity);
        }

        // Validation of input parameters
        if (processDefinitionId == null && processDefinitionKey == null) {
            throw new ActivitiIllegalArgumentException("Process definition id or key cannot be null");
        }

        List<ProcessDefinitionEntity> processDefinitionEntities = new ArrayList<ProcessDefinitionEntity>();
        ProcessDefinitionEntityManager processDefinitionManager = commandContext.getProcessDefinitionEntityManager();

        if (processDefinitionId != null) {

            ProcessDefinitionEntity processDefinitionEntity = processDefinitionManager.findById(processDefinitionId);
            if (processDefinitionEntity == null) {
                throw new ActivitiObjectNotFoundException("Cannot find process definition for id '" + processDefinitionId + "'", ProcessDefinition.class);
            }
            processDefinitionEntities.add(processDefinitionEntity);

        } else {

            ProcessDefinitionQueryImpl query = new ProcessDefinitionQueryImpl(commandContext).processDefinitionKey(processDefinitionKey);

            if (tenantId == null || ProcessEngineConfiguration.NO_TENANT_ID.equals(tenantId)) {
                query.processDefinitionWithoutTenantId();
            } else {
                query.processDefinitionTenantId(tenantId);
            }

            List<ProcessDefinition> processDefinitions = query.list();
            if (processDefinitions.isEmpty()) {
                throw new ActivitiException("Cannot find process definition for key '" + processDefinitionKey + "'");
            }

            for (ProcessDefinition processDefinition : processDefinitions) {
                processDefinitionEntities.add((ProcessDefinitionEntity) processDefinition);
            }

        }
        return processDefinitionEntities;
    }

    protected void createTimerForDelayedExecution(CommandContext commandContext, List<ProcessDefinitionEntity> processDefinitions) {
        for (ProcessDefinitionEntity processDefinition : processDefinitions) {

            TimerJobEntity timer = commandContext.getTimerJobEntityManager().create();
            timer.setJobType(JobEntity.JOB_TYPE_TIMER);
            timer.setProcessDefinitionId(processDefinition.getId());

            // Inherit tenant identifier (if applicable)
            if (processDefinition.getTenantId() != null) {
                timer.setTenantId(processDefinition.getTenantId());
            }

            timer.setDuedate(executionDate);
            timer.setJobHandlerType(getDelayedExecutionJobHandlerType());
            timer.setJobHandlerConfiguration(TimerChangeProcessDefinitionSuspensionStateJobHandler.createJobHandlerConfiguration(includeProcessInstances));
            commandContext.getJobManager().scheduleTimerJob(timer);
        }
    }

    protected void changeProcessDefinitionState(CommandContext commandContext, List<ProcessDefinitionEntity> processDefinitions) {
        for (ProcessDefinitionEntity processDefinition : processDefinitions) {

            SuspensionState.SuspensionStateUtil.setSuspensionState(processDefinition, getProcessDefinitionSuspensionState());

            // Evict cache
            commandContext.getProcessEngineConfiguration().getDeploymentManager().getProcessDefinitionCache().remove(processDefinition.getId());

            // Suspend process instances (if needed)
            if (includeProcessInstances) {

                int currentStartIndex = 0;
                List<ProcessInstance> processInstances = fetchProcessInstancesPage(commandContext, processDefinition, currentStartIndex);
                while (!processInstances.isEmpty()) {

                    for (ProcessInstance processInstance : processInstances) {
                        AbstractSetProcessInstanceStateCmd processInstanceCmd = getProcessInstanceChangeStateCmd(processInstance);
                        processInstanceCmd.execute(commandContext);
                    }

                    // Fetch new batch of process instances
                    currentStartIndex += processInstances.size();
                    processInstances = fetchProcessInstancesPage(commandContext, processDefinition, currentStartIndex);
                }
            }
        }
    }

    protected List<ProcessInstance> fetchProcessInstancesPage(CommandContext commandContext, ProcessDefinition processDefinition, int currentPageStartIndex) {

        if (SuspensionState.ACTIVE.equals(getProcessDefinitionSuspensionState())) {
            return new ProcessInstanceQueryImpl(commandContext).processDefinitionId(processDefinition.getId()).suspended()
                    .listPage(currentPageStartIndex, commandContext.getProcessEngineConfiguration().getBatchSizeProcessInstances());
        } else {
            return new ProcessInstanceQueryImpl(commandContext).processDefinitionId(processDefinition.getId()).active()
                    .listPage(currentPageStartIndex, commandContext.getProcessEngineConfiguration().getBatchSizeProcessInstances());
        }
    }

    // ABSTRACT METHODS
    // ////////////////////////////////////////////////////////////////////

    /**
     * Subclasses should return the wanted {@link SuspensionState} here.
     */
    protected abstract SuspensionState getProcessDefinitionSuspensionState();

    /**
     * Subclasses should return the type of the {@link JobHandler} here. it will be used when the user provides an execution date on which the actual state change will happen.
     */
    protected abstract String getDelayedExecutionJobHandlerType();

    /**
     * Subclasses should return a {@link Command} implementation that matches the process definition state change.
     */
    protected abstract AbstractSetProcessInstanceStateCmd getProcessInstanceChangeStateCmd(ProcessInstance processInstance);

}
