/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity.data.impl.util;

import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;

import java.util.List;

/**
 * Prints a nicely tree-looking overview of the executions.
 */
public class ExecutionTreeStringBuilder {

    protected ExecutionEntity executionEntity;

    public ExecutionTreeStringBuilder(ExecutionEntity executionEntity) {
        this.executionEntity = executionEntity;
    }

    /* See http://stackoverflow.com/questions/4965335/how-to-print-binary-tree-diagram */
    @Override
    public String toString() {
        StringBuilder strb = new StringBuilder();
        strb.append(executionEntity.getId()).append(" : ")
                .append(executionEntity.getActivityId())
                .append(", parent id ")
                .append(executionEntity.getParentId())
                .append("\r\n");

        List<? extends ExecutionEntity> children = executionEntity.getExecutions();
        if (children != null) {
            for (ExecutionEntity childExecution : children) {
                internalToString(childExecution, strb, "", true);
            }
        }
        return strb.toString();
    }

    protected void internalToString(ExecutionEntity execution, StringBuilder strb, String prefix, boolean isTail) {
        strb.append(prefix)
                .append(isTail ? "└── " : "├── ")
                .append(execution.getId()).append(" : ")
                .append("activityId=" + execution.getActivityId())
                .append(", parent id ")
                .append(execution.getParentId())
                .append(execution.isScope() ? " (scope)" : "")
                .append(execution.isMultiInstanceRoot() ? " (multi instance root)" : "")
                .append("\r\n");

        List<? extends ExecutionEntity> children = executionEntity.getExecutions();
        if (children != null) {
            for (int i = 0; i < children.size() - 1; i++) {
                internalToString(children.get(i), strb, prefix + (isTail ? "    " : "│   "), false);
            }
            if (children.size() > 0) {
                internalToString(children.get(children.size() - 1), strb, prefix + (isTail ? "    " : "│   "), true);
            }
        }
    }

}
