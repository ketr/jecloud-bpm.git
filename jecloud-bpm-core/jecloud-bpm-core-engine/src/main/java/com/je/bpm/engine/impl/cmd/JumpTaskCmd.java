/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.delegate.DelegateExecution;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.interceptor.Command;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntityManager;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.impl.util.CollectionUtil;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

import static com.je.bpm.engine.impl.bpmn.behavior.MultiInstanceActivityBehavior.NUMBER_OF_COMPLETED_INSTANCES;

/**
 * 跳跃命令
 */
public class JumpTaskCmd implements Command<Void>, Serializable {

    /**
     * 当前任务ID
     */
    private String currentTaskId;
    /**
     * 目标任务定义key
     */
    private String targetTaskDefinitionKey;

    private String comment;

    private Map<String, Object> variables;

    public JumpTaskCmd(String currentTaskId, String targetTaskDefinitionKey) {
        this.currentTaskId = currentTaskId;
        this.targetTaskDefinitionKey = targetTaskDefinitionKey;
    }

    public JumpTaskCmd(String currentTaskId, String targetTaskDefinitionKey, Map<String, Object> variables) {
        this.currentTaskId = currentTaskId;
        this.targetTaskDefinitionKey = targetTaskDefinitionKey;
        this.variables = variables;
    }

    public JumpTaskCmd(String currentTaskId, String targetTaskDefinitionKey, String comment) {
        this.currentTaskId = currentTaskId;
        this.targetTaskDefinitionKey = targetTaskDefinitionKey;
        this.comment = comment;
    }

    public JumpTaskCmd(String currentTaskId, String targetTaskDefinitionKey, String comment, Map<String, Object> variables) {
        this.currentTaskId = currentTaskId;
        this.targetTaskDefinitionKey = targetTaskDefinitionKey;
        this.comment = comment;
        this.variables = variables;
    }

    @Override
    public Void execute(CommandContext commandContext) {
        TaskEntity currentTask = commandContext.getProcessEngineConfiguration().getTaskEntityManager().findById(currentTaskId);
        if (currentTask == null) {
            throw new ActivitiException("Cant't find the current task.");
        }

        ExecutionEntity execution = currentTask.getExecution();
        BpmnModel bpmnModel = commandContext.getProcessEngineConfiguration().getRepositoryService().getBpmnModel(currentTask.getProcessDefinitionId(), execution.getProcessInstanceId(),execution.getProcessInstanceBusinessKey());
        FlowElement flowElement = bpmnModel.getFlowElement(currentTask.getTaskDefinitionKey());
        if (flowElement == null) {
            throw new ActivitiException("Can't find the flow element in the process definition.");
        }

        if (!(flowElement instanceof KaiteBaseUserTask)) {
            throw new ActivitiException("Only kaite user task can be jumped.");
        }
        KaiteBaseUserTask kaiteBaseUserTask = (KaiteBaseUserTask) flowElement;
//        List<String> canJumpedTaskDefinitionList = kaiteBaseUserTask.getTaskJumpConfig().getJumpTasks();
//        if (!canJumpedTaskDefinitionList.contains(targetTaskDefinitionKey)) {
//            throw new ActivitiException("Can't jump to the target definiton task!");
//        }

        FlowElement targetFlowElement = bpmnModel.getFlowElement(targetTaskDefinitionKey);
        if (!(targetFlowElement instanceof KaiteBaseUserTask)) {
            throw new ActivitiException("Only kaite user task can jump.");
        }

        KaiteBaseUserTask targetBaseUserTask = (KaiteBaseUserTask) targetFlowElement;
        ExecutionEntity finalExecutionEntity;
        //如果属于多实例节点，在没有开始审批前可以跳转，但是一旦有人审批，则不可以跳转
        if (kaiteBaseUserTask.hasMultiInstanceLoopCharacteristics()) {
            Integer completed = getLoopVariable(execution, NUMBER_OF_COMPLETED_INSTANCES);
            if (completed != null && completed > 0) {
                throw new ActivitiException("The multi instance task already have approve record.");
            }
            //删除所有任务
            finalExecutionEntity = deleteAllMultiTask(commandContext, execution, targetBaseUserTask);
        } else {
            //删除当前任务
            commandContext.getTaskEntityManager().deleteTask(currentTask, comment, false, false);
            finalExecutionEntity = execution;
        }

        finalExecutionEntity.setCurrentFlowElement(targetFlowElement);
        finalExecutionEntity.setVariablesLocal(variables);
        //如果目标任务属于多实例节点，则要根据多实例要求创建多个任务（串行创建一个，并行创建多个）
        if (targetBaseUserTask.hasMultiInstanceLoopCharacteristics()) {
            Context.getAgenda().planContinueMultiInstanceOperation(finalExecutionEntity);
        } else {
            Context.getAgenda().planContinueProcessOperation(finalExecutionEntity);
        }
        return null;
    }

    private ExecutionEntity deleteAllMultiTask(CommandContext commandContext, ExecutionEntity executionEntity, KaiteBaseUserTask kaiteBaseUserTask) {
        ExecutionEntity rootExecutionEntity = (ExecutionEntity) getMultiInstanceRootExecution(executionEntity);
        deleteChildExecutions(rootExecutionEntity, true, commandContext, kaiteBaseUserTask);
        return rootExecutionEntity;
    }

    protected void deleteChildExecutions(ExecutionEntity parentExecution, boolean deleteExecution, CommandContext commandContext, KaiteBaseUserTask kaiteBaseUserTask) {
        // Delete all child executions
        ExecutionEntityManager executionEntityManager = commandContext.getExecutionEntityManager();
        Collection<ExecutionEntity> childExecutions = executionEntityManager.findChildExecutionsByParentExecutionId(parentExecution.getId());
        if (CollectionUtil.isNotEmpty(childExecutions)) {
            for (ExecutionEntity childExecution : childExecutions) {
                deleteChildExecutions(childExecution, true, commandContext, kaiteBaseUserTask);
            }
        }

        if (deleteExecution) {
            executionEntityManager.deleteExecutionAndRelatedData(parentExecution, comment);
        }

    }

    protected DelegateExecution getMultiInstanceRootExecution(DelegateExecution executionEntity) {
        DelegateExecution multiInstanceRootExecution = null;
        DelegateExecution currentExecution = executionEntity;
        while (currentExecution != null && multiInstanceRootExecution == null && currentExecution.getParent() != null) {
            if (currentExecution.isMultiInstanceRoot()) {
                multiInstanceRootExecution = currentExecution;
            } else {
                currentExecution = currentExecution.getParent();
            }
        }
        return multiInstanceRootExecution;
    }

    private Integer getLoopVariable(DelegateExecution execution, String variableName) {
        Object value = execution.getVariableLocal(variableName);
        DelegateExecution parent = execution.getParent();
        while (value == null && parent != null) {
            value = parent.getVariableLocal(variableName);
            parent = parent.getParent();
        }
        return (Integer) (value != null ? value : 0);
    }

}
