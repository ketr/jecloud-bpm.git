/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cfg;

import com.je.bpm.engine.ActivitiException;
import org.springframework.beans.factory.BeanFactory;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class SpringBeanFactoryProxyMap implements Map<Object, Object> {

    protected BeanFactory beanFactory;

    public SpringBeanFactoryProxyMap(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    @Override
    public Object get(Object key) {
        if ((key == null) || (!String.class.isAssignableFrom(key.getClass()))) {
            return null;
        }
        return beanFactory.getBean((String) key);
    }

    @Override
    public boolean containsKey(Object key) {
        if ((key == null) || (!String.class.isAssignableFrom(key.getClass()))) {
            return false;
        }
        return beanFactory.containsBean((String) key);
    }

    @Override
    public Set<Object> keySet() {
        throw new ActivitiException("unsupported operation on configuration beans");
        // List<String> beanNames =
        // asList(beanFactory.getBeanDefinitionNames());
        // return new HashSet<Object>(beanNames);
    }

    @Override
    public void clear() {
        throw new ActivitiException("can't clear configuration beans");
    }

    @Override
    public boolean containsValue(Object value) {
        throw new ActivitiException("can't search values in configuration beans");
    }

    @Override
    public Set<Entry<Object, Object>> entrySet() {
        throw new ActivitiException("unsupported operation on configuration beans");
    }

    @Override
    public boolean isEmpty() {
        throw new ActivitiException("unsupported operation on configuration beans");
    }

    @Override
    public Object put(Object key, Object value) {
        throw new ActivitiException("unsupported operation on configuration beans");
    }

    @Override
    public void putAll(Map<? extends Object, ? extends Object> m) {
        throw new ActivitiException("unsupported operation on configuration beans");
    }

    @Override
    public Object remove(Object key) {
        throw new ActivitiException("unsupported operation on configuration beans");
    }

    @Override
    public int size() {
        throw new ActivitiException("unsupported operation on configuration beans");
    }

    @Override
    public Collection<Object> values() {
        throw new ActivitiException("unsupported operation on configuration beans");
    }
}
