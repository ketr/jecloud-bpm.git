/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity.data.impl;

import com.je.bpm.engine.impl.JobQueryImpl;
import com.je.bpm.engine.impl.Page;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.persistence.CachedEntityMatcher;
import com.je.bpm.engine.impl.persistence.entity.JobEntity;
import com.je.bpm.engine.impl.persistence.entity.JobEntityImpl;
import com.je.bpm.engine.impl.persistence.entity.data.AbstractDataManager;
import com.je.bpm.engine.impl.persistence.entity.data.JobDataManager;
import com.je.bpm.engine.impl.persistence.entity.data.impl.cachematcher.JobsByExecutionIdMatcher;
import com.je.bpm.engine.runtime.Job;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MybatisJobDataManager extends AbstractDataManager<JobEntity> implements JobDataManager {

    protected CachedEntityMatcher<JobEntity> jobsByExecutionIdMatcher = new JobsByExecutionIdMatcher();

    public MybatisJobDataManager(ProcessEngineConfigurationImpl processEngineConfiguration) {
        super(processEngineConfiguration);
    }

    @Override
    public Class<? extends JobEntity> getManagedEntityClass() {
        return JobEntityImpl.class;
    }

    @Override
    public JobEntity create() {
        return new JobEntityImpl();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<JobEntity> findJobsToExecute(Page page) {
        return getDbSqlSession().selectList("selectJobsToExecute", null, page);
    }

    @Override
    public List<JobEntity> findJobsByExecutionId(final String executionId) {
        return getList("selectJobsByExecutionId", executionId, jobsByExecutionIdMatcher, true);
    }

    @Override
    public List<JobEntity> findJobsByProcessDefinitionId(final String processDefinitionId) {
        Map<String, String> params = new HashMap<String, String>(1);
        params.put("processDefinitionId", processDefinitionId);
        return getDbSqlSession().selectList("selectJobByProcessDefinitionId", params);
    }

    @Override
    public List<JobEntity> findJobsByTypeAndProcessDefinitionId(final String jobHandlerType, final String processDefinitionId) {
        Map<String, String> params = new HashMap<String, String>(2);
        params.put("handlerType", jobHandlerType);
        params.put("processDefinitionId", processDefinitionId);
        return getDbSqlSession().selectList("selectJobByTypeAndProcessDefinitionId", params);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<JobEntity> findJobsByProcessInstanceId(final String processInstanceId) {
        return getDbSqlSession().selectList("selectJobsByProcessInstanceId", processInstanceId);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<JobEntity> findExpiredJobs(Page page) {
        Date now = getClock().getCurrentTime();
        return getDbSqlSession().selectList("selectExpiredJobs", now, page);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Job> findJobsByQueryCriteria(JobQueryImpl jobQuery, Page page) {
        final String query = "selectJobByQueryCriteria";
        return getDbSqlSession().selectList(query, jobQuery, page);
    }

    @Override
    public long findJobCountByQueryCriteria(JobQueryImpl jobQuery) {
        return (Long) getDbSqlSession().selectOne("selectJobCountByQueryCriteria", jobQuery);
    }

    @Override
    public void updateJobTenantIdForDeployment(String deploymentId, String newTenantId) {
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("deploymentId", deploymentId);
        params.put("tenantId", newTenantId);
        getDbSqlSession().update("updateJobTenantIdForDeployment", params);
    }

    @Override
    public void resetExpiredJob(String jobId) {
        Map<String, Object> params = new HashMap<String, Object>(2);
        params.put("id", jobId);
        getDbSqlSession().update("resetExpiredJob", params);
    }

}
