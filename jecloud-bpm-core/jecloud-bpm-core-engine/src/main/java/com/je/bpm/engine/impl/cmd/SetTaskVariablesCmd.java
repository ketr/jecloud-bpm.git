/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;

import java.util.Map;

public class SetTaskVariablesCmd extends NeedsActiveTaskCmd<Object> {

    private static final long serialVersionUID = 1L;

    protected Map<String, ? extends Object> variables;
    protected boolean isLocal;

    public SetTaskVariablesCmd(String taskId, Map<String, ? extends Object> variables, boolean isLocal) {
        super(taskId, null, null);
        this.taskId = taskId;
        this.variables = variables;
        this.isLocal = isLocal;
    }

    @Override
    protected Object execute(CommandContext commandContext, TaskEntity task) {
        if (isLocal) {
            if (variables != null) {
                for (String variableName : variables.keySet()) {
                    task.setVariableLocal(variableName, variables.get(variableName), false);
                }
            }
        } else {
            if (variables != null) {
                for (String variableName : variables.keySet()) {
                    task.setVariable(variableName, variables.get(variableName), false);
                }
            }
        }

        // ACT-1887: Force an update of the task's revision to prevent
        // simultaneous inserts of the same variable. If not, duplicate variables may occur since optimistic
        // locking doesn't work on inserts
        task.forceUpdate();
        return null;
    }

    @Override
    protected String getSuspendedTaskException() {
        return "Cannot add variables to a suspended task";
    }

}
