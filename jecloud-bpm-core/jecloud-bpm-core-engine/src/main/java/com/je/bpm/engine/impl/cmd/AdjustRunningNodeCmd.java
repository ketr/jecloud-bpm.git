/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.config.task.assignment.TaskAssigneeConfigImpl;
import com.je.bpm.core.model.task.*;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.delegate.DelegateHelper;
import com.je.bpm.engine.impl.bpmn.behavior.KaiteBaseUserTaskActivityBehavior;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.engine.task.Comment;
import com.je.bpm.engine.task.Task;
import com.je.bpm.engine.upcoming.ActivitiUpcomingRun;
import com.je.bpm.engine.upcoming.UpcomingCommentInfoDTO;
import com.je.bpm.runtime.shared.identity.BO.ParserUserBo;
import com.je.bpm.runtime.shared.identity.ResultUserParser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 调整运行节点
 */
public class AdjustRunningNodeCmd extends AbstractJumpTask {

    private static final DateFormat nodeDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    private String piId;
    private String currentNodeId;
    private String toNodeId;
    private Map<String, Object> bean;
    private String prod;

    public AdjustRunningNodeCmd(String piId, String currentNodeId, String toNodeId, Map<String, Object> bean, String prod) {
        super("", "", null);
        isTaskIdValid = false;
        this.piId = piId;
        this.currentNodeId = currentNodeId;
        this.toNodeId = toNodeId;
        this.bean = bean;
        this.prod = prod;
    }

    @Override
    protected Void execute(CommandContext commandContext, TaskEntity task) {
        ProcessEngineConfigurationImpl processEngineConfiguration = commandContext.getProcessEngineConfiguration();

        List<ExecutionEntity> list = processEngineConfiguration.getExecutionDataManager().findChildExecutionsByProcessInstanceId(piId);
        ExecutionEntity currentExecution = null;
        for (ExecutionEntity executionEntity : list) {
            if (!Strings.isNullOrEmpty(executionEntity.getActivityId()) && executionEntity.getActivityId().equals(currentNodeId)) {
                currentExecution = executionEntity;
            }
        }
        adjust(piId, currentExecution, commandContext);
        return null;
    }


    public void adjust(String piId, ExecutionEntity execution, CommandContext commandContext) {
        String comment = String.format("(%s)任务调整", Authentication.getAuthenticatedUser().getName());
        String nodeId = execution.getActivityId();

        List<TaskEntity> list = new ArrayList<>();
        if (nodeId.startsWith("countersign") || nodeId.startsWith("batchtask")) {
            ExecutionEntity root = (ExecutionEntity) getMultiInstanceRootExecution(execution);
            for (ExecutionEntity executionEntity : root.getExecutions()) {
                list.addAll(executionEntity.getTasks());
            }
        } else {
            list = execution.getTasks();
        }

        String beanId = "";
        for (Task task : list) {
            beanId = task.getBusinessKey();
            commandContext.getProcessEngineConfiguration().getTaskService().addComment(task.getId(), task.getProcessInstanceId(),
                    Comment.NODE_TYPE, "调整");
            commandContext.getProcessEngineConfiguration().getTaskService().addComment(task.getId(), task.getProcessInstanceId(),
                    Comment.USER_COMMENT, comment);
        }


        //执行清理首页待办逻辑
        List<TaskEntity> taskEntityList = commandContext.getProcessEngineConfiguration().getTaskEntityManager()
                .findTasksByProcessInstanceId(execution.getProcessInstanceId());
        List<String> taskIds = new ArrayList<>();
        for (TaskEntity task : taskEntityList) {
            taskIds.add(task.getId());
        }
        ActivitiUpcomingRun activitiUpcomingRun = commandContext.getProcessEngineConfiguration().getActivitiUpcomingRun();
        UpcomingCommentInfoDTO upcomingInfo = DelegateHelper.buildUpcomingInfo(null, "", SubmitTypeEnum.ADJUST,
                beanId, String.join(",", taskIds), null);
        upcomingInfo.setPiid(piId);
        activitiUpcomingRun.completeUpcoming(upcomingInfo);

        //删除所有的任务
        if (nodeId.startsWith("countersign") || nodeId.startsWith("batchtask")) {
            execution = deleteAllMultiTask(commandContext, execution);
        } else {
            List<TaskEntity> tasks = execution.getTasks();
            for (TaskEntity taskEntity1 : tasks) {
                commandContext.getTaskEntityManager().deleteTask(taskEntity1, comment, false, false);
            }
        }

        if (execution == null) {
            throw new ActivitiException("调整任务异常，请联系管理员！");
        }

        BpmnModel bpmnModel = commandContext.getProcessEngineConfiguration().getRepositoryService()
                .getBpmnModel(execution.getProcessDefinitionId(), piId,execution.getProcessInstanceBusinessKey());

        KaiteBaseUserTask targetBaseUserTask = (KaiteBaseUserTask) bpmnModel.getFlowElement(toNodeId);

        //获取人员信息
        String assignee = getUsers(execution, commandContext, targetBaseUserTask);
        JSONArray jsonArray = new JSONArray();
        JSONObject assigneeUserJsonObject = new JSONObject();
        assigneeUserJsonObject.put("nodeId", targetBaseUserTask.getId());
        assigneeUserJsonObject.put("nodeName", targetBaseUserTask.getName());
        assigneeUserJsonObject.put("assignee", assignee);
        assigneeUserJsonObject.put("assigneeName", "");
        if (!Strings.isNullOrEmpty(assignee)) {
            ResultUserParser resultUserParser = commandContext.getProcessEngineConfiguration().getResultUserParser();
            String userName = resultUserParser.getUserNameByUserId(assignee);
            assigneeUserJsonObject.put("assigneeName", userName);
        }

        jsonArray.add(assigneeUserJsonObject);
        UpcomingCommentInfoDTO upcomingInfo2 = UpcomingCommentInfoDTO.build
                (SubmitTypeEnum.ADJUST, Context.getCommandContext().getBean(), beanId,
                        comment, taskId, null, jsonArray.toJSONString());
        Context.getCommandContext().addAttribute(KaiteBaseUserTaskActivityBehavior.UPCOMINGINFO, upcomingInfo2);

        execution.setCurrentFlowElement(targetBaseUserTask);
        Map<String, Object> transientVariables = new HashMap<>();
        //设置人员
        transientVariables.put(targetBaseUserTask.getId(), assignee);
        execution.setTransientVariables(transientVariables);
        //如果目标任务属于多实例节点，则要根据多实例要求创建多个任务（串行创建一个，并行创建多个）
        if (targetBaseUserTask.hasMultiInstanceLoopCharacteristics()) {
            Context.getAgenda().planContinueMultiInstanceOperation(execution);
        } else {
            execution.setActive(true);
            execution.setMultiInstanceRoot(false);
            Context.getAgenda().planContinueProcessOperation(execution);
        }
    }

    private String getUsers(ExecutionEntity execution, CommandContext commandContext, KaiteBaseUserTask targetBaseUserTask) {
        TaskAssigneeConfigImpl taskAssigneeConfig = targetBaseUserTask.getTaskAssigneeConfig();

        //人员参照
        String userId = "";
        TaskAssigneeConfigImpl.ReferToEnum referToEnum = taskAssigneeConfig.getReferTo();
        if (referToEnum != null && referToEnum.toString().equals(TaskAssigneeConfigImpl.ReferToEnum.STARTUSER.toString())) {
            userId = execution.getStartUserId();
        } else {
            userId = Authentication.getAuthenticatedUser().getDeptId();
        }

        String starter = execution.getProcessInstance().getStartUserId();
        Object userObject = commandContext.getProcessEngineConfiguration().getResultUserParser().parserResultUser(
                ParserUserBo.build(true, targetBaseUserTask, false, taskAssigneeConfig, userId, "",
                        bean, prod, "", "", starter, ""));
        if (userObject instanceof Map) {
            //多人节点，直接返回空，手动调拨指定人员
            if (targetBaseUserTask instanceof KaiteMultiUserTask || targetBaseUserTask instanceof KaiteCandidateUserTask ||
                    targetBaseUserTask instanceof KaiteCounterSignUserTask || targetBaseUserTask instanceof KaiteRandomUserTask) {
                return "";
            }
            List<String> userIdList = commandContext.getProcessEngineConfiguration().getResultUserParser().
                    parserResultUserToListString(userObject, true);
            if (userIdList.size() == 1) {
                return userIdList.get(0);
            } else {
                return "";
            }
        } else {
            return "";
        }

    }


    @Override
    public String getComment() {
        return null;
    }

}
