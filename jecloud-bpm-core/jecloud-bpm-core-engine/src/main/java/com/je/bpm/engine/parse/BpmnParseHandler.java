/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.parse;

import com.je.bpm.core.model.BaseElement;
import com.je.bpm.engine.impl.bpmn.parser.BpmnParse;
import com.je.bpm.engine.internal.Internal;

import java.util.Collection;

/**
 * Allows to hook into the parsing of one or more elements during the parsing of a BPMN 2.0 process. For more details, see the userguide section on bpmn parse handlers.
 *
 * Instances of this class can be injected into the {@link com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl}. The handler will then be called whenever a BPMN 2.0 element is parsed that matches the types returned by the
 * {@link #getHandledTypes()} method.
 *
 * @see com.je.bpm.engine.impl.bpmn.parser.handler.AbstractBpmnParseHandler
 */
@Internal
public interface BpmnParseHandler {

    /**
     * The types for which this handler must be called during process parsing.
     */
    Collection<Class<? extends BaseElement>> getHandledTypes();

    /**
     * The actual delegation method. The parser will calls this method on a match with the {@link #getHandledTypes()} return value.
     *
     * @param bpmnParse The {@link com.je.bpm.engine.impl.bpmn.parser.BpmnParse} instance that acts as container for all things produced during the parsing.
     */
    void parse(BpmnParse bpmnParse, BaseElement element);

}
