/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity.data.impl.cachematcher;

import com.je.bpm.engine.impl.persistence.CachedEntityMatcher;
import com.je.bpm.engine.impl.persistence.cache.CachedEntity;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;

import java.util.Collection;

public class ExecutionsWithSameRootProcessInstanceIdMatcher implements CachedEntityMatcher<ExecutionEntity> {

    @Override
    public boolean isRetained(Collection<ExecutionEntity> databaseEntities, Collection<CachedEntity> cachedEntities, ExecutionEntity entity, Object param) {
        ExecutionEntity executionEntity = getMatchingExecution(databaseEntities, cachedEntities, (String) param);
        return (executionEntity.getRootProcessInstanceId() != null
                && executionEntity.getRootProcessInstanceId().equals(entity.getRootProcessInstanceId()));
    }

    public ExecutionEntity getMatchingExecution(Collection<ExecutionEntity> databaseEntities, Collection<CachedEntity> cachedEntities, String executionId) {

        // Doing some preprocessing here: we need to find the execution that matches the provided execution id,
        // as we need to match the root process instance id later on.

        if (cachedEntities != null) {
            for (CachedEntity cachedEntity : cachedEntities) {
                ExecutionEntity executionEntity = (ExecutionEntity) cachedEntity.getEntity();
                if (executionId.equals(executionEntity.getId())) {
                    return executionEntity;
                }
            }
        }

        if (databaseEntities != null) {
            for (ExecutionEntity databaseExecutionEntity : databaseEntities) {
                if (executionId.equals(databaseExecutionEntity.getId())) {
                    return databaseExecutionEntity;
                }
            }
        }

        return null;
    }

}
