/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.history.HistoricDetail;
import com.je.bpm.engine.impl.HistoricDetailQueryImpl;
import com.je.bpm.engine.impl.Page;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.history.HistoryLevel;
import com.je.bpm.engine.impl.persistence.entity.data.DataManager;
import com.je.bpm.engine.impl.persistence.entity.data.HistoricDetailDataManager;

import java.util.List;
import java.util.Map;

public class HistoricDetailEntityManagerImpl extends AbstractEntityManager<HistoricDetailEntity> implements HistoricDetailEntityManager {

  protected HistoricDetailDataManager historicDetailDataManager;

  public HistoricDetailEntityManagerImpl(ProcessEngineConfigurationImpl processEngineConfiguration, HistoricDetailDataManager historicDetailDataManager) {
    super(processEngineConfiguration);
    this.historicDetailDataManager = historicDetailDataManager;
  }

  @Override
  protected DataManager<HistoricDetailEntity> getDataManager() {
    return historicDetailDataManager;
  }

  @Override
  public HistoricDetailVariableInstanceUpdateEntity copyAndInsertHistoricDetailVariableInstanceUpdateEntity(VariableInstanceEntity variableInstance) {
    HistoricDetailVariableInstanceUpdateEntity historicVariableUpdate = historicDetailDataManager.createHistoricDetailVariableInstanceUpdate();
    historicVariableUpdate.setProcessInstanceId(variableInstance.getProcessInstanceId());
    historicVariableUpdate.setExecutionId(variableInstance.getExecutionId());
    historicVariableUpdate.setTaskId(variableInstance.getTaskId());
    historicVariableUpdate.setTime(getClock().getCurrentTime());
    historicVariableUpdate.setRevision(variableInstance.getRevision());
    historicVariableUpdate.setName(variableInstance.getName());
    historicVariableUpdate.setVariableType(variableInstance.getType());
    historicVariableUpdate.setTextValue(variableInstance.getTextValue());
    historicVariableUpdate.setTextValue2(variableInstance.getTextValue2());
    historicVariableUpdate.setDoubleValue(variableInstance.getDoubleValue());
    historicVariableUpdate.setLongValue(variableInstance.getLongValue());

    if (variableInstance.getBytes() != null) {
      historicVariableUpdate.setBytes(variableInstance.getBytes());
    }

    insert(historicVariableUpdate);
    return historicVariableUpdate;
  }

  @Override
  public void delete(HistoricDetailEntity entity, boolean fireDeleteEvent) {
    super.delete(entity, fireDeleteEvent);

    if (entity instanceof HistoricDetailVariableInstanceUpdateEntity) {
      HistoricDetailVariableInstanceUpdateEntity historicDetailVariableInstanceUpdateEntity = ((HistoricDetailVariableInstanceUpdateEntity) entity);
      if (historicDetailVariableInstanceUpdateEntity.getByteArrayRef() != null) {
        historicDetailVariableInstanceUpdateEntity.getByteArrayRef().delete(false);
      }
    }
  }

  @Override
  public void deleteHistoricDetailsByProcessInstanceId(String historicProcessInstanceId) {
    if (getHistoryManager().isHistoryLevelAtLeast(HistoryLevel.AUDIT)) {
      List<HistoricDetailEntity> historicDetails = historicDetailDataManager.findHistoricDetailsByProcessInstanceId(historicProcessInstanceId);

      for (HistoricDetailEntity historicDetail : historicDetails) {
        delete(historicDetail);
      }
    }
  }

  @Override
  public long findHistoricDetailCountByQueryCriteria(HistoricDetailQueryImpl historicVariableUpdateQuery) {
    return historicDetailDataManager.findHistoricDetailCountByQueryCriteria(historicVariableUpdateQuery);
  }

  @Override
  public List<HistoricDetail> findHistoricDetailsByQueryCriteria(HistoricDetailQueryImpl historicVariableUpdateQuery, Page page) {
    return historicDetailDataManager.findHistoricDetailsByQueryCriteria(historicVariableUpdateQuery, page);
  }

  @Override
  public void deleteHistoricDetailsByTaskId(String taskId) {
    if (getHistoryManager().isHistoryLevelAtLeast(HistoryLevel.FULL)) {
      List<HistoricDetailEntity> details = historicDetailDataManager.findHistoricDetailsByTaskId(taskId);
      for (HistoricDetail detail : details) {
        delete((HistoricDetailEntity) detail);
      }
    }
  }

  @Override
  public List<HistoricDetail> findHistoricDetailsByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults) {
    return historicDetailDataManager.findHistoricDetailsByNativeQuery(parameterMap, firstResult, maxResults);
  }

  @Override
  public long findHistoricDetailCountByNativeQuery(Map<String, Object> parameterMap) {
    return historicDetailDataManager.findHistoricDetailCountByNativeQuery(parameterMap);
  }

  public HistoricDetailDataManager getHistoricDetailDataManager() {
    return historicDetailDataManager;
  }

  public void setHistoricDetailDataManager(HistoricDetailDataManager historicDetailDataManager) {
    this.historicDetailDataManager = historicDetailDataManager;
  }

}
