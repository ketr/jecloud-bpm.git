/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.interceptor;

import com.je.bpm.core.model.config.process.ProcessBasicConfigImpl;
import com.je.bpm.engine.*;
import com.je.bpm.engine.delegate.event.ActivitiEventDispatcher;
import com.je.bpm.engine.impl.asyncexecutor.JobManager;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.db.DbSqlSession;
import com.je.bpm.engine.impl.history.HistoryManager;
import com.je.bpm.engine.impl.jobexecutor.FailedJobCommandFactory;
import com.je.bpm.engine.impl.persistence.cache.EntityCache;
import com.je.bpm.engine.impl.persistence.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class CommandContext {

    private static Logger log = LoggerFactory.getLogger(CommandContext.class);
    //审批告知变量key
    public static final String APPROVALNOTICE = "approvalNotice";
    //是否跳跃变量key
    public static final String IS_JUMP = "isJump";
    //是否跳跃变量意见key
    public static final String IS_JUMP_COMMENT = "isJumpComment";
    //跳跃节点key变量key
    public static final String JUMP_TARGET_NODE_ID = "jumpTargetNodeId";
    //是否是发起变量key
    public static final String IS_SPONSOR = "sponsor";
    //kaiteDynaBean变量key
    public static final String BEAN = "bean";
    //产品标识变量key
    public static final String PROD = "prod";
    //审批意见
    public static final String COMMENT = "comment";
    //审批意见
    public static final String FILES = "files";
    //前端提交意见
    public static final String SUBMIT_COMMENT = "submitComment";


    protected Command<?> command;
    protected Map<Class<?>, SessionFactory> sessionFactories;
    protected Map<Class<?>, Session> sessions = new HashMap<>();
    protected Throwable exception;
    protected ProcessEngineConfigurationImpl processEngineConfiguration;
    protected FailedJobCommandFactory failedJobCommandFactory;
    protected List<CommandContextCloseListener> closeListeners;
    protected Map<String, Object> attributes; // General-purpose storing of anything during the lifetime of a command context
    protected boolean reused;

    protected ActivitiEngineAgenda agenda;
    protected Map<String, ExecutionEntity> involvedExecutions = new HashMap<>(1); // The executions involved with the command
    protected LinkedList<Object> resultStack = new LinkedList<>(); // needs to be a stack, as JavaDelegates can do api calls again

    public CommandContext(Command<?> command, ProcessEngineConfigurationImpl processEngineConfiguration) {
        this.command = command;
        this.processEngineConfiguration = processEngineConfiguration;
        this.failedJobCommandFactory = processEngineConfiguration.getFailedJobCommandFactory();
        this.sessionFactories = processEngineConfiguration.getSessionFactories();
        this.agenda = processEngineConfiguration.getEngineAgendaFactory().createAgenda(this);
    }

    public void close() {
        // The intention of this method is that all resources are closed properly, even if exceptions occur
        // in close or flush methods of the sessions or the transaction context.
        try {
            try {
                try {
                    executeCloseListenersClosing();
                    if (exception == null) {
                        flushSessions();
                    }
                } catch (Throwable exception) {
                    exception(exception);
                } finally {

                    try {
                        if (exception == null) {
                            executeCloseListenersAfterSessionFlushed();
                        }
                    } catch (Throwable exception) {
                        exception(exception);
                    }

                    if (exception != null) {
                        logException();
                        executeCloseListenersCloseFailure();
                    } else {
                        executeCloseListenersClosed();
                    }
                }
            } catch (Throwable exception) {
                // Catch exceptions during rollback
                exception(exception);
            } finally {
                // Sessions need to be closed, regardless of exceptions/commit/rollback
                closeSessions();
            }
        } catch (Throwable exception) {
            // Catch exceptions during session closing
            exception(exception);
        }

        if (exception != null) {
            rethrowExceptionIfNeeded();
        }
    }

    protected void logException() {
        if (exception instanceof JobNotFoundException || exception instanceof ActivitiTaskAlreadyClaimedException) {
            // reduce log level, because this may have been caused because of job deletion due to cancelActiviti="true"
            log.info("Error while closing command context", exception);
        } else if (exception instanceof ActivitiOptimisticLockingException) {
            // reduce log level, as normally we're not interested in logging this exception
            log.debug("Optimistic locking exception : " + exception);
        } else {
            log.error("Error while closing command context", exception);
        }
    }

    protected void rethrowExceptionIfNeeded() throws Error {
        if (exception instanceof Error) {
            throw (Error) exception;
        } else if (exception instanceof RuntimeException) {
            throw (RuntimeException) exception;
        } else {
            throw new ActivitiException("exception while executing command " + command, exception);
        }
    }

    public void addCloseListener(CommandContextCloseListener commandContextCloseListener) {
        if (closeListeners == null) {
            closeListeners = new ArrayList<>(1);
        }
        closeListeners.add(commandContextCloseListener);
    }

    public List<CommandContextCloseListener> getCloseListeners() {
        return closeListeners;
    }

    public boolean hasCloseListener(Class<?> type) {
        if (closeListeners != null && closeListeners.size() != 0) {
            for (CommandContextCloseListener listener : closeListeners) {
                if (type.isInstance(listener)) {
                    return true;
                }
            }
        }
        return false;
    }

    protected void executeCloseListenersClosing() {
        if (closeListeners != null) {
            try {
                for (CommandContextCloseListener listener : closeListeners) {
                    listener.closing(this);
                }
            } catch (Throwable exception) {
                exception(exception);
            }
        }
    }

    protected void executeCloseListenersAfterSessionFlushed() {
        if (closeListeners != null) {
            try {
                for (CommandContextCloseListener listener : closeListeners) {
                    listener.afterSessionsFlush(this);
                }
            } catch (Throwable exception) {
                exception(exception);
            }
        }
    }

    protected void executeCloseListenersClosed() {
        if (closeListeners != null) {
            try {
                for (CommandContextCloseListener listener : closeListeners) {
                    listener.closed(this);
                }
            } catch (Throwable exception) {
                exception(exception);
            }
        }
    }

    protected void executeCloseListenersCloseFailure() {
        if (closeListeners != null) {
            try {
                for (CommandContextCloseListener listener : closeListeners) {
                    listener.closeFailure(this);
                }
            } catch (Throwable exception) {
                exception(exception);
            }
        }
    }

    protected void flushSessions() {
        for (Session session : sessions.values()) {
            session.flush();
        }
    }

    protected void closeSessions() {
        for (Session session : sessions.values()) {
            try {
                session.close();
            } catch (Throwable exception) {
                exception(exception);
            }
        }
    }

    /**
     * Stores the provided exception on this {@link CommandContext} instance.
     * That exception will be rethrown at the end of closing the {@link CommandContext} instance.
     * <p>
     * If there is already an exception being stored, a 'masked exception' message will be logged.
     */
    public void exception(Throwable exception) {
        if (this.exception == null) {
            this.exception = exception;
        } else {
            log.error("masked exception in command context. for root cause, see below as it will be rethrown later.", exception);
        }
    }

    public void addAttribute(String key, Object value) {
        if (attributes == null) {
            attributes = new HashMap<>(1);
        }
        attributes.put(key, value);
    }

    public Object getAttribute(String key) {
        if (attributes != null) {
            return attributes.get(key);
        }
        return null;
    }

    public <T> T getGenericAttribute(String key) {
        if (attributes != null) {
            return (T) getAttribute(key);
        }
        return null;
    }

    public <T> T getSession(Class<T> sessionClass) {
        Session session = sessions.get(sessionClass);
        if (session == null) {
            SessionFactory sessionFactory = sessionFactories.get(sessionClass);
            if (sessionFactory == null) {
                throw new ActivitiException("no session factory configured for " + sessionClass.getName());
            }
            session = sessionFactory.openSession(this);
            sessions.put(sessionClass, session);
        }
        return (T) session;
    }

    public Map<Class<?>, SessionFactory> getSessionFactories() {
        return sessionFactories;
    }

    public DbSqlSession getDbSqlSession() {
        return getSession(DbSqlSession.class);
    }

    public EntityCache getEntityCache() {
        return getSession(EntityCache.class);
    }

    public DeploymentEntityManager getDeploymentEntityManager() {
        return processEngineConfiguration.getDeploymentEntityManager();
    }

    public ResourceEntityManager getResourceEntityManager() {
        return processEngineConfiguration.getResourceEntityManager();
    }


    public CsResourceEntityManager getCsResourceEntityManager() {
        return processEngineConfiguration.getCsResourceEntityManager();
    }

    public ByteArrayEntityManager getByteArrayEntityManager() {
        return processEngineConfiguration.getByteArrayEntityManager();
    }

    public CsByteArrayEntityManager getCsByteArrayEntityManager() {
        return processEngineConfiguration.getCsByteArrayEntityManager();
    }

    public ProcessDefinitionEntityManager getProcessDefinitionEntityManager() {
        return processEngineConfiguration.getProcessDefinitionEntityManager();
    }

    public ModelEntityManager getModelEntityManager() {
        return processEngineConfiguration.getModelEntityManager();
    }

    public ProcessDefinitionInfoEntityManager getProcessDefinitionInfoEntityManager() {
        return processEngineConfiguration.getProcessDefinitionInfoEntityManager();
    }

    public ExecutionEntityManager getExecutionEntityManager() {
        return processEngineConfiguration.getExecutionEntityManager();
    }

    public TaskEntityManager getTaskEntityManager() {
        return processEngineConfiguration.getTaskEntityManager();
    }

    public IdentityLinkEntityManager getIdentityLinkEntityManager() {
        return processEngineConfiguration.getIdentityLinkEntityManager();
    }

    public VariableInstanceEntityManager getVariableInstanceEntityManager() {
        return processEngineConfiguration.getVariableInstanceEntityManager();
    }

    public HistoricProcessInstanceEntityManager getHistoricProcessInstanceEntityManager() {
        return processEngineConfiguration.getHistoricProcessInstanceEntityManager();
    }

    public HistoricDetailEntityManager getHistoricDetailEntityManager() {
        return processEngineConfiguration.getHistoricDetailEntityManager();
    }

    public HistoricVariableInstanceEntityManager getHistoricVariableInstanceEntityManager() {
        return processEngineConfiguration.getHistoricVariableInstanceEntityManager();
    }

    public HistoricActivityInstanceEntityManager getHistoricActivityInstanceEntityManager() {
        return processEngineConfiguration.getHistoricActivityInstanceEntityManager();
    }

    public HistoricTaskInstanceEntityManager getHistoricTaskInstanceEntityManager() {
        return processEngineConfiguration.getHistoricTaskInstanceEntityManager();
    }

    public HistoricIdentityLinkEntityManager getHistoricIdentityLinkEntityManager() {
        return processEngineConfiguration.getHistoricIdentityLinkEntityManager();
    }

    public EventLogEntryEntityManager getEventLogEntryEntityManager() {
        return processEngineConfiguration.getEventLogEntryEntityManager();
    }

    public JobEntityManager getJobEntityManager() {
        return processEngineConfiguration.getJobEntityManager();
    }

    public TimerJobEntityManager getTimerJobEntityManager() {
        return processEngineConfiguration.getTimerJobEntityManager();
    }

    public SuspendedJobEntityManager getSuspendedJobEntityManager() {
        return processEngineConfiguration.getSuspendedJobEntityManager();
    }

    public DeadLetterJobEntityManager getDeadLetterJobEntityManager() {
        return processEngineConfiguration.getDeadLetterJobEntityManager();
    }

    public AttachmentEntityManager getAttachmentEntityManager() {
        return processEngineConfiguration.getAttachmentEntityManager();
    }

    public TableDataManager getTableDataManager() {
        return processEngineConfiguration.getTableDataManager();
    }

    public CommentEntityManager getCommentEntityManager() {
        return processEngineConfiguration.getCommentEntityManager();
    }

    public PropertyEntityManager getPropertyEntityManager() {
        return processEngineConfiguration.getPropertyEntityManager();
    }

    public EventSubscriptionEntityManager getEventSubscriptionEntityManager() {
        return processEngineConfiguration.getEventSubscriptionEntityManager();
    }

    public HistoryManager getHistoryManager() {
        return processEngineConfiguration.getHistoryManager();
    }

    public JobManager getJobManager() {
        return processEngineConfiguration.getJobManager();
    }

    // Involved executions ////////////////////////////////////////////////////////

    public void addInvolvedExecution(ExecutionEntity executionEntity) {
        if (executionEntity.getId() != null) {
            involvedExecutions.put(executionEntity.getId(), executionEntity);
        }
    }

    public boolean hasInvolvedExecutions() {
        return involvedExecutions.size() > 0;
    }

    public Collection<ExecutionEntity> getInvolvedExecutions() {
        return involvedExecutions.values();
    }

    // getters and setters
    // //////////////////////////////////////////////////////

    public Command<?> getCommand() {
        return command;
    }

    public Map<Class<?>, Session> getSessions() {
        return sessions;
    }

    public Throwable getException() {
        return exception;
    }

    public FailedJobCommandFactory getFailedJobCommandFactory() {
        return failedJobCommandFactory;
    }

    public ProcessEngineConfigurationImpl getProcessEngineConfiguration() {
        return processEngineConfiguration;
    }

    public ActivitiEventDispatcher getEventDispatcher() {
        return processEngineConfiguration.getEventDispatcher();
    }

    public ActivitiEngineAgenda getAgenda() {
        return agenda;
    }

    public Object getResult() {
        return resultStack.pollLast();
    }

    public void setResult(Object result) {
        resultStack.add(result);
    }

    public boolean isReused() {
        return reused;
    }

    public void setReused(boolean reused) {
        this.reused = reused;
    }

    public void updateBean(Map<String, Object> bean, String pkValue, ProcessBasicConfigImpl processBasicConfig) {
        if (bean == null) {
            return;
        }
        processEngineConfiguration.getRemoteCallServeManager().doUpdate(
                bean, pkValue, processBasicConfig, (String) getAttribute(PROD), null);
        Object beanObj = processEngineConfiguration.getRemoteCallServeManager().doGet((String) getAttribute(PROD), pkValue,
                processBasicConfig.getTableCode(), null);
        if (beanObj != null && beanObj instanceof HashMap) {
            addAttribute(BEAN, ((HashMap) beanObj).get("values"));
        }
    }

    public Map<String, Object> getBean() {
        return (Map<String, Object>) getAttribute(BEAN);
    }

    public void addBeanValue(String key, Object value) {
        Map<String, Object> bean = (Map<String, Object>) getAttribute(BEAN);
        bean.put(key, value);
        addAttribute(BEAN, bean);
    }

}
