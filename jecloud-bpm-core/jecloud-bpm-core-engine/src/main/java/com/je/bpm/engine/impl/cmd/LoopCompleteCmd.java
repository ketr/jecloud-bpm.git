/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.task.KaiteLoopUserTask;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.common.auth.AuthAccount;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class LoopCompleteCmd extends AbstractJumpTask {

    private static final long serialVersionUID = 1L;

    protected String comment;
    protected Map<String, Object> variables;
    protected Map<String, Object> transientVariables;
    /**
     * 下个节点指派人
     */
    protected String assigneeUser;
    /**
     * 是否本任务作用域
     */
    protected boolean localScope;

    public LoopCompleteCmd(String taskId, String prod, Map<String, Object> bean) {
        super(taskId, prod, bean);
        this.taskId = taskId;
    }

    public LoopCompleteCmd(String taskId, String prod, Map<String, Object> bean, Map<String, Object> variables) {
        this(taskId, prod, bean);
        this.variables = variables;
    }

    public LoopCompleteCmd(String taskId, String prod, Map<String, Object> bean, String assigneeUser, Map<String, Object> variables) {
        this(taskId, prod, bean);
        this.variables = variables;
        this.assigneeUser = assigneeUser;
    }

    public LoopCompleteCmd(String taskId, String prod, Map<String, Object> bean, String assigneeUser, Map<String, Object> variables, boolean localScope) {
        this(taskId, prod, bean);
        this.localScope = localScope;
        this.assigneeUser = assigneeUser;
        this.variables = variables;
    }

    public LoopCompleteCmd(String taskId, String prod, Map<String, Object> bean, String assigneeUser, Map<String, Object> variables, Map<String, Object> transientVariables) {
        this(taskId, prod, bean);
        this.transientVariables = transientVariables;
        this.assigneeUser = assigneeUser;
        this.variables = variables;
    }

    @Override
    public String getComment() {
        return comment;
    }

    @Override
    public Void execute(CommandContext commandContext, TaskEntity taskEntity) {
        ExecutionEntity execution = taskEntity.getExecution();
        BpmnModel bpmnModel = commandContext.getProcessEngineConfiguration().getRepositoryService().getBpmnModel(taskEntity.getProcessDefinitionId(), taskEntity.getProcessInstanceId(),taskEntity.getBusinessKey());
        FlowElement flowElement = bpmnModel.getFlowElement(taskEntity.getTaskDefinitionKey());
        if (flowElement == null) {
            throw new ActivitiException("Can't find the flow element in the process definition.");
        }

        if (!(flowElement instanceof KaiteLoopUserTask)) {
            throw new ActivitiException("Only kaite loop user task can be jumped.");
        }

        //删除当前任务
        commandContext.getTaskEntityManager().deleteTask(taskEntity, comment, false, false);

        if (StringUtils.isEmpty(this.assigneeUser)) {
            AuthAccount activitiUser = Authentication.getAuthenticatedUser();
            if (activitiUser == null) {
                throw new ActivitiException("Please login while use this command method.");
            }
            this.assigneeUser = activitiUser.getDeptId();
        }

        //设置变量，继续执行
        if (transientVariables != null) {
            execution.setTransientVariables(transientVariables);
        }
        execution.setTransientVariable(flowElement.getId(), this.assigneeUser);
        Context.getAgenda().planContinueProcessOperation(execution);
        return null;
    }

}
