/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.delegate.event.impl;

import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.engine.delegate.event.*;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.util.ProcessDefinitionUtil;
import com.je.bpm.engine.repository.ProcessDefinition;

/**
 * Class capable of dispatching events.
 */
public class ActivitiEventDispatcherImpl implements ActivitiEventDispatcher {

    protected ActivitiEventSupport eventSupport;
    protected boolean enabled = true;

    public ActivitiEventDispatcherImpl() {
        eventSupport = new ActivitiEventSupport();
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void addEventListener(ActivitiEventListener listenerToAdd) {
        eventSupport.addEventListener(listenerToAdd);
    }

    @Override
    public void addEventListener(ActivitiEventListener listenerToAdd, ActivitiEventType... types) {
        eventSupport.addEventListener(listenerToAdd, types);
    }

    @Override
    public void removeEventListener(ActivitiEventListener listenerToRemove) {
        eventSupport.removeEventListener(listenerToRemove);
    }

    @Override
    public void dispatchEvent(ActivitiEvent event) {
        if (enabled) {
            eventSupport.dispatchEvent(event);
        }

        if (event.getType() == ActivitiEventType.ENTITY_DELETED && event instanceof ActivitiEntityEvent) {
            ActivitiEntityEvent entityEvent = (ActivitiEntityEvent) event;
            if (entityEvent.getEntity() instanceof ProcessDefinition) {
                // process definition deleted event doesn't need to be dispatched to event listeners
                return;
            }
        }

        // Try getting hold of the Process definition, based on the process definition key, if a context is active
        // 如果上下文处于活动状态，请尝试根据流程定义键获取流程定义
        CommandContext commandContext = Context.getCommandContext();
        if (commandContext != null) {
            BpmnModel bpmnModel = extractBpmnModelFromEvent(event);
            if (bpmnModel != null) {
                ((ActivitiEventSupport) bpmnModel.getEventSupport()).dispatchEvent(event);
            }
        }
    }

    /**
     * In case no process-context is active, this method attempts to extract a process-definition based on the event. In case it's an event related to an entity, this can be deducted by inspecting the
     * entity, without additional queries to the database.
     * 如果没有流程上下文处于活动状态，此方法将尝试基于事件提取流程定义。如果是与实体相关的事件，可以通过检查实体，无需对数据库进行其他查询。
     * If not an entity-related event, the process-definition will be retrieved based on the processDefinitionId (if filled in). This requires an additional query to the database in case not already
     * cached. However, queries will only occur when the definition is not yet in the cache, which is very unlikely to happen, unless evicted.
     * 如果不是实体相关事件，则将根据processDefinitionId（如果已填写）检索流程定义。如果尚未缓存，则需要对数据库进行额外的查询。但是，只有当定义尚未在缓存中时，才会发生查询，除非被逐出，否则这种情况不太可能发生。
     * @param event
     * @return
     */
    protected BpmnModel extractBpmnModelFromEvent(ActivitiEvent event) {
        BpmnModel result = null;
        if (result == null && event.getProcessDefinitionId() != null) {
            ProcessDefinition processDefinition = ProcessDefinitionUtil.getProcessDefinition(event.getProcessDefinitionId(), true);
            if (processDefinition != null) {
                result = Context.getProcessEngineConfiguration().getDeploymentManager().resolveProcessDefinition(processDefinition).getBpmnModel();
            }
        }

        return result;
    }

}
