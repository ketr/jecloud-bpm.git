/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.event.logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.je.bpm.engine.delegate.event.ActivitiEntityEvent;
import com.je.bpm.engine.delegate.event.ActivitiEvent;
import com.je.bpm.engine.delegate.event.ActivitiEventListener;
import com.je.bpm.engine.delegate.event.ActivitiEventType;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.event.logger.handler.*;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.interceptor.CommandContextCloseListener;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.runtime.Clock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class EventLogger implements ActivitiEventListener {

    private static final Logger logger = LoggerFactory.getLogger(EventLogger.class);

    private static final String EVENT_FLUSHER_KEY = "eventFlusher";

    protected Clock clock;
    protected ObjectMapper objectMapper;

    // Mapping of type -> handler
    protected Map<ActivitiEventType, Class<? extends EventLoggerEventHandler>> eventHandlers
            = new HashMap<ActivitiEventType, Class<? extends EventLoggerEventHandler>>();

    // Listeners for new events
    protected List<EventLoggerListener> listeners;

    public EventLogger() {
        initializeDefaultHandlers();
    }

    public EventLogger(Clock clock, ObjectMapper objectMapper) {
        this();
        this.clock = clock;
        this.objectMapper = objectMapper;
    }

    protected void initializeDefaultHandlers() {
        addEventHandler(ActivitiEventType.TASK_CREATED, TaskCreatedEventHandler.class);
        addEventHandler(ActivitiEventType.TASK_COMPLETED, TaskCompletedEventHandler.class);
        addEventHandler(ActivitiEventType.TASK_ASSIGNED, TaskAssignedEventHandler.class);

        addEventHandler(ActivitiEventType.SEQUENCEFLOW_TAKEN, SequenceFlowTakenEventHandler.class);
        addEventHandler(ActivitiEventType.ACTIVITY_COMPLETED, ActivityCompletedEventHandler.class);
        addEventHandler(ActivitiEventType.ACTIVITY_STARTED, ActivityStartedEventHandler.class);
        addEventHandler(ActivitiEventType.ACTIVITY_SIGNALED, ActivitySignaledEventHandler.class);
        addEventHandler(ActivitiEventType.ACTIVITY_MESSAGE_RECEIVED, ActivityMessageEventHandler.class);
        addEventHandler(ActivitiEventType.ACTIVITY_COMPENSATE, ActivityCompensatedEventHandler.class);
        addEventHandler(ActivitiEventType.ACTIVITY_ERROR_RECEIVED, ActivityErrorReceivedEventHandler.class);

        addEventHandler(ActivitiEventType.VARIABLE_CREATED, VariableCreatedEventHandler.class);
        addEventHandler(ActivitiEventType.VARIABLE_DELETED, VariableDeletedEventHandler.class);
        addEventHandler(ActivitiEventType.VARIABLE_UPDATED, VariableUpdatedEventHandler.class);
    }

    @Override
    public void onEvent(ActivitiEvent event) {
        EventLoggerEventHandler eventHandler = getEventHandler(event);
        if (eventHandler != null) {

            // Events are flushed when command context is closed
            CommandContext currentCommandContext = Context.getCommandContext();
            EventFlusher eventFlusher = (EventFlusher) currentCommandContext.getAttribute(EVENT_FLUSHER_KEY);

            if (eventFlusher == null) {

                eventFlusher = createEventFlusher();
                if (eventFlusher == null) {
                    eventFlusher = new DatabaseEventFlusher(); // Default
                }
                currentCommandContext.addAttribute(EVENT_FLUSHER_KEY, eventFlusher);

                currentCommandContext.addCloseListener(eventFlusher);
                currentCommandContext
                        .addCloseListener(new CommandContextCloseListener() {

                            @Override
                            public void closing(CommandContext commandContext) {
                            }

                            @Override
                            public void closed(CommandContext commandContext) {
                                // For those who are interested: we can now broadcast the events were added
                                if (listeners != null) {
                                    for (EventLoggerListener listener : listeners) {
                                        listener.eventsAdded(EventLogger.this);
                                    }
                                }
                            }

                            @Override
                            public void afterSessionsFlush(CommandContext commandContext) {
                            }

                            @Override
                            public void closeFailure(CommandContext commandContext) {
                            }

                        });
            }

            eventFlusher.addEventHandler(eventHandler);
        }
    }

    // Subclasses can override this if defaults are not ok
    protected EventLoggerEventHandler getEventHandler(ActivitiEvent event) {

        Class<? extends EventLoggerEventHandler> eventHandlerClass = null;
        if (event.getType().equals(ActivitiEventType.ENTITY_INITIALIZED)) {
            Object entity = ((ActivitiEntityEvent) event).getEntity();
            if (entity instanceof ExecutionEntity) {
                ExecutionEntity executionEntity = (ExecutionEntity) entity;
                if (executionEntity.getProcessInstanceId().equals(executionEntity.getId())) {
                    eventHandlerClass = ProcessInstanceStartedEventHandler.class;
                }
            }
        } else if (event.getType().equals(ActivitiEventType.ENTITY_DELETED)) {
            Object entity = ((ActivitiEntityEvent) event).getEntity();
            if (entity instanceof ExecutionEntity) {
                ExecutionEntity executionEntity = (ExecutionEntity) entity;
                if (executionEntity.getProcessInstanceId().equals(executionEntity.getId())) {
                    eventHandlerClass = ProcessInstanceEndedEventHandler.class;
                }
            }
        } else {
            // Default: dedicated mapper for the type
            eventHandlerClass = eventHandlers.get(event.getType());
        }

        if (eventHandlerClass != null) {
            return instantiateEventHandler(event, eventHandlerClass);
        }

        return null;
    }

    protected EventLoggerEventHandler instantiateEventHandler(ActivitiEvent event, Class<? extends EventLoggerEventHandler> eventHandlerClass) {
        try {
            EventLoggerEventHandler eventHandler = eventHandlerClass.newInstance();
            eventHandler.setTimeStamp(clock.getCurrentTime());
            eventHandler.setEvent(event);
            eventHandler.setObjectMapper(objectMapper);
            return eventHandler;
        } catch (Exception e) {
            logger.warn("Could not instantiate " + eventHandlerClass + ", this is most likely a programmatic error");
        }
        return null;
    }

    @Override
    public boolean isFailOnException() {
        return false;
    }

    public void addEventHandler(ActivitiEventType eventType, Class<? extends EventLoggerEventHandler> eventHandlerClass) {
        eventHandlers.put(eventType, eventHandlerClass);
    }

    public void addEventLoggerListener(EventLoggerListener listener) {
        if (listeners == null) {
            listeners = new ArrayList<EventLoggerListener>(1);
        }
        listeners.add(listener);
    }

    /**
     * Subclasses that want something else than the database flusher should override this method
     */
    protected EventFlusher createEventFlusher() {
        return null;
    }

    public Clock getClock() {
        return clock;
    }

    public void setClock(Clock clock) {
        this.clock = clock;
    }

    public ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public List<EventLoggerListener> getListeners() {
        return listeners;
    }

    public void setListeners(List<EventLoggerListener> listeners) {
        this.listeners = listeners;
    }

}
