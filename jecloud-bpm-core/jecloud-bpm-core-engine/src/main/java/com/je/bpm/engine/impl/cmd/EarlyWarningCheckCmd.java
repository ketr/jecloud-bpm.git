/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.FlowNode;
import com.je.bpm.core.model.SequenceFlow;
import com.je.bpm.core.model.config.task.EarlyWarningAndPostponementSource;
import com.je.bpm.core.model.config.task.TaskEarlyWarningAndPostponementConfigImpl;
import com.je.bpm.core.model.task.*;
import com.je.bpm.engine.TaskService;
import com.je.bpm.engine.earlyWarning.EarlyWarningPush;
import com.je.bpm.engine.impl.bpmn.behavior.FlowNodeActivityBehavior;
import com.je.bpm.engine.impl.context.Context;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.engine.impl.interceptor.Command;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.EarlyWarningEntity;
import com.je.bpm.engine.impl.persistence.entity.EarlyWarningEntityManager;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.runtime.shared.dto.EventEarlyWarningDTO;
import com.je.common.auth.AuthAccount;
import com.je.common.auth.AuthOrg;
import com.je.common.auth.AuthRealUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 执行预警检查
 */
public class EarlyWarningCheckCmd implements Command<Void>, Serializable {

    private static Logger logger = LoggerFactory.getLogger(EarlyWarningCheckCmd.class);

    private static final String EARLY_WARNING_DESC = "【%s】任务预警，请及时处理！";

    private String processInstanceId;
    private TaskService taskService;

    public EarlyWarningCheckCmd() {

    }

    public EarlyWarningCheckCmd(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    private void completeEarlyWarning(EarlyWarningEntity earlyWarningEntity) {
        earlyWarningEntity.setState("0");
        EarlyWarningEntityManager earlyWarningEntityManager = Context.getCommandContext().getProcessEngineConfiguration().getEarlyWarningEntityManager();
        earlyWarningEntityManager.update(earlyWarningEntity);
    }

    @Override
    public Void execute(CommandContext commandContext) {
        EarlyWarningEntityManager earlyWarningEntityManager = commandContext.getProcessEngineConfiguration().getEarlyWarningEntityManager();
        List<EarlyWarningEntity> earlyWarnings = earlyWarningEntityManager.findExecutableEarlyWarning();
        Date nowTime = new Date();
        for (EarlyWarningEntity earlyWarningEntity : earlyWarnings) {
            TaskEntity taskEntity = commandContext.getProcessEngineConfiguration().getTaskDataManager().findById(earlyWarningEntity.getTaskId());
            //查询不到任务后，设置state为0
            if (taskEntity == null) {
                completeEarlyWarning(earlyWarningEntity);
                continue;
            }
            BpmnModel bpmnModel = commandContext.getProcessEngineConfiguration().getRepositoryService().
                    getBpmnModel(earlyWarningEntity.getProcessDefinitionId(), taskEntity.getProcessInstanceId(), taskEntity.getBusinessKey());
            String prod = Context.getCommandContext().getProcessEngineConfiguration().getRemoteCallServeManager().
                    getProdByFuncCode(bpmnModel.getMainProcess().getProcessConfig().getFuncCode());
            //处理时限到期，进行处理
            Map<String, Object> beanValue = getBean(prod, bpmnModel, taskEntity);
            if (beanValue == null) {
                return null;
            }
            if (nowTime.getTime() > earlyWarningEntity.getProcessingTime().getTime()) {
                logger.info("任务到达处理时限，开始处理任务=" + earlyWarningEntity.getTaskId() + earlyWarningEntity.getTaskName());
                FlowElement flowElement = bpmnModel.getMainProcess().getFlowElement(taskEntity.getTaskDefinitionKey());
                KaiteBaseUserTask kaiteBaseUserTask = (KaiteBaseUserTask) flowElement;
                List<EarlyWarningAndPostponementSource> eventList = kaiteBaseUserTask.getTaskEarlyWarningAndPostponementConfig().getSource();
                earlyWarningEntity.setState("0");
                earlyWarningEntityManager.update(earlyWarningEntity);
                for (EarlyWarningAndPostponementSource source : eventList) {
                    if (source.getExecutionType() == EarlyWarningAndPostponementSource.ExecutionTypeEnum.AUTO_APPROVAL) {
                        autoApproval(bpmnModel, taskEntity, prod, beanValue);
                    } else if (source.getExecutionType() == EarlyWarningAndPostponementSource.ExecutionTypeEnum.NOTIFICATION) {
                        alertArrivalReminder(bpmnModel, kaiteBaseUserTask, earlyWarningEntity, taskEntity, beanValue);
                    } else if (source.getExecutionType() == EarlyWarningAndPostponementSource.ExecutionTypeEnum.EXECUTE_CUSTOM_METHOD) {
                        String serviceName = source.getService();
                        String methodName = source.getMethod();
                        EventEarlyWarningDTO eventEarlyWarningDTO = EventEarlyWarningDTO.build(taskEntity.getId(), taskEntity.getName(),
                                taskEntity.getAssignee(), bpmnModel, taskEntity.getBusinessKey());
                        executeEventEarlyWarningCustomMethod(serviceName, methodName, eventEarlyWarningDTO, prod);
                    }
                }
                log(nowTime);
            } else if (nowTime.getTime() > earlyWarningEntity.getNextTime().getTime()) {
                FlowElement flowElement = bpmnModel.getMainProcess().getFlowElement(taskEntity.getTaskDefinitionKey());
                KaiteBaseUserTask kaiteBaseUserTask = (KaiteBaseUserTask) flowElement;
                TaskEarlyWarningAndPostponementConfigImpl taskEarlyWarningAndPostponementConfig =
                        kaiteBaseUserTask.getTaskEarlyWarningAndPostponementConfig();
                logger.info("任务达到预警时间，开始预警提醒=" + earlyWarningEntity.getTaskId() + earlyWarningEntity.getTaskName());
                earlyWarningEntity.setNextTime(FlowNodeActivityBehavior.calculateTime(
                        new Date(),
                        Integer.valueOf(taskEarlyWarningAndPostponementConfig.getReminderFrequencyDuration()),
                        taskEarlyWarningAndPostponementConfig.getReminderFrequencyUnitCode(), true)
                );
                earlyWarningEntityManager.update(earlyWarningEntity);
                warningReminder(bpmnModel, kaiteBaseUserTask, earlyWarningEntity, taskEntity, beanValue);
                log(nowTime);
            }
        }
        return null;
    }

    /**
     * 自动通过
     */
    private void autoApproval(BpmnModel bpmnModel, TaskEntity taskEntity, String prod, Map<String, Object> beanValue) {
        if (beanValue == null) {
            return;
        }
        logInAs(taskEntity.getAssignee());
        if (taskService == null) {
            taskService = Context.getCommandContext().getProcessEngineConfiguration().getTaskService();
        }
        FlowElement flowElement = bpmnModel.getMainProcess().getFlowElement(taskEntity.getTaskDefinitionKey());
        if (flowElement instanceof FlowNode) {
            List<SequenceFlow> list = ((FlowNode) flowElement).getOutgoingFlows();
            if (list.size() != 1) {
                return;
            }
            SequenceFlow sequenceFlow = list.get(0);
            if (!(sequenceFlow.getTargetFlowElement() instanceof KaiteFixedUserTask
                    || sequenceFlow.getTargetFlowElement() instanceof KaiteRandomUserTask
                    || sequenceFlow.getTargetFlowElement() instanceof KaiteMultiUserTask
                    || sequenceFlow.getTargetFlowElement() instanceof KaiteCounterSignUserTask
                    || sequenceFlow.getTargetFlowElement() instanceof KaiteCandidateUserTask)) {
                return;
            }
            taskService.complete(taskEntity.getId(), "", sequenceFlow.getId(),
                    prod, beanValue, "任务到达预警期限，自动流转",
                    "[]", "0", "");
        }
    }

    private Map<String, Object> getBean(String prod, BpmnModel bpmnModel, TaskEntity taskEntity) {
        Object beanObj = Context.getCommandContext().getProcessEngineConfiguration().getRemoteCallServeManager().doGet(
                prod, taskEntity.getBusinessKey(),
                bpmnModel.getMainProcess().getProcessConfig().getTableCode(),
                null);
        if (!(beanObj instanceof Map)) {
            return null;
        }
        Map<String, Object> bean = (Map<String, Object>) beanObj;
        Map<String, Object> beanValue = (Map<String, Object>) bean.get("values");
        return beanValue;
    }

    public void logInAs(String userId) {
        AuthAccount user = new AuthAccount() {
            @Override
            public String getId() {
                return userId;
            }

            @Override
            public String getCode() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public AuthOrg getAuthOrg() {
                return null;
            }

            @Override
            public AuthRealUser getRealUser() {
                return null;
            }

            @Override
            public String getDeptId() {
                return userId;
            }
        };
        Authentication.setAuthenticatedUser(user);
    }

    /**
     * 执行自定义方法
     */
    private void executeEventEarlyWarningCustomMethod(String serviceName, String methodName, EventEarlyWarningDTO eventEarlyWarningDTO, String prod) {
        Context.getCommandContext().getProcessEngineConfiguration().getRemoteCallServeManager()
                .executeCustomMethod(serviceName, methodName, eventEarlyWarningDTO, prod);
    }

    /**
     * 延期到达提醒
     */
    private void alertArrivalReminder(BpmnModel bpmnModel, KaiteBaseUserTask kaiteBaseUserTask, EarlyWarningEntity earlyWarningEntity, TaskEntity taskEntity, Map<String, Object> beanValue) {
        if (beanValue == null) {
            return;
        }
        EarlyWarningPush push = Context.getCommandContext().getProcessEngineConfiguration().getEarlyWarningPush();
        String submitUserId = "";
        if (taskEntity.getVariable("prevAssignee") != null) {
            submitUserId = (String) taskEntity.getVariable("prevAssignee");
        }
        push.alertArrivalReminder(bpmnModel, kaiteBaseUserTask, earlyWarningEntity, taskEntity, beanValue, submitUserId);
    }

    /**
     * 延期提醒
     */
    private void warningReminder(BpmnModel bpmnModel, KaiteBaseUserTask kaiteBaseUserTask, EarlyWarningEntity earlyWarningEntity, TaskEntity taskEntity, Map<String, Object> beanValue) {
        if (beanValue == null) {
            return;
        }
        String submitUserId = "";
        if (taskEntity.getVariable("prevAssignee") != null) {
            submitUserId = (String) taskEntity.getVariable("prevAssignee");
        }
        EarlyWarningPush push = Context.getCommandContext().getProcessEngineConfiguration().getEarlyWarningPush();
        push.warningReminder(bpmnModel, kaiteBaseUserTask, earlyWarningEntity, taskEntity, beanValue, submitUserId);
    }


    private void log(Date nowTime) {
        //如果当前时间在nextTime之后或在dueDate设置之前的时间后，则执行预警
//        EarlyWarningLogEntity earlyWarningLogEntity = new EarlyWarningLogEntityImpl();
//        earlyWarningLogEntity.setCreateTime(nowTime);
//        earlyWarningLogEntity.setProcessDefinitionId(eachTask.getProcessDefinitionId());
//        earlyWarningLogEntity.setProcessInstanceId(eachTask.getProcessInstanceId());
//        earlyWarningLogEntity.setTaskId(eachTask.getId());
//        earlyWarningLogEntity.setTaskName(eachTask.getName());
//        Context.getCommandContext().getProcessEngineConfiguration().getEarlyWarningLogEntityManager().insert(earlyWarningLogEntity);
    }

}
