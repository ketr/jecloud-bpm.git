/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine;

import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Service providing access to the repository of process definitions and deployments.
 * 一个新增的服务，用于动态修改流程中的一些参数信息等，是引擎中的一个辅助的服务
 */
public interface DynamicBpmnService {

    ObjectNode getProcessDefinitionInfo(String processDefinitionId);

    void saveProcessDefinitionInfo(String processDefinitionId, ObjectNode infoNode);

    ObjectNode changeServiceTaskClassName(String id, String className);

    void changeServiceTaskClassName(String id, String className, ObjectNode infoNode);

    ObjectNode changeServiceTaskExpression(String id, String expression);

    void changeServiceTaskExpression(String id, String expression, ObjectNode infoNode);

    ObjectNode changeServiceTaskDelegateExpression(String id, String expression);

    void changeServiceTaskDelegateExpression(String id, String expression, ObjectNode infoNode);

    ObjectNode changeScriptTaskScript(String id, String script);

    void changeScriptTaskScript(String id, String script, ObjectNode infoNode);

    ObjectNode changeUserTaskName(String id, String name);

    void changeUserTaskName(String id, String name, ObjectNode infoNode);

    ObjectNode changeUserTaskDescription(String id, String description);

    void changeUserTaskDescription(String id, String description, ObjectNode infoNode);

    ObjectNode changeUserTaskDueDate(String id, String dueDate);

    void changeUserTaskDueDate(String id, String dueDate, ObjectNode infoNode);

    ObjectNode changeUserTaskPriority(String id, String priority);

    void changeUserTaskPriority(String id, String priority, ObjectNode infoNode);

    ObjectNode changeUserTaskCategory(String id, String category);

    void changeUserTaskCategory(String id, String category, ObjectNode infoNode);

    ObjectNode changeUserTaskFormKey(String id, String formKey);

    void changeUserTaskFormKey(String id, String formKey, ObjectNode infoNode);

    ObjectNode changeUserTaskAssignee(String id, String assignee);

    void changeUserTaskAssignee(String id, String assignee, ObjectNode infoNode);

    ObjectNode changeUserTaskOwner(String id, String owner);

    void changeUserTaskOwner(String id, String owner, ObjectNode infoNode);

    ObjectNode changeUserTaskCandidateUser(String id, String candidateUser, boolean overwriteOtherChangedEntries);

    void changeUserTaskCandidateUser(String id, String candidateUser, boolean overwriteOtherChangedEntries, ObjectNode infoNode);

    ObjectNode changeUserTaskCandidateGroup(String id, String candidateGroup, boolean overwriteOtherChangedEntries);

    void changeUserTaskCandidateGroup(String id, String candidateGroup, boolean overwriteOtherChangedEntries, ObjectNode infoNode);

    ObjectNode changeDmnTaskDecisionTableKey(String id, String decisionTableKey);

    void changeDmnTaskDecisionTableKey(String id, String decisionTableKey, ObjectNode infoNode);

    ObjectNode changeSequenceFlowCondition(String id, String condition);

    void changeSequenceFlowCondition(String id, String condition, ObjectNode infoNode);

    ObjectNode getBpmnElementProperties(String id, ObjectNode infoNode);

    ObjectNode changeLocalizationName(String language, String id, String value);

    void changeLocalizationName(String language, String id, String value, ObjectNode infoNode);

    ObjectNode changeLocalizationDescription(String language, String id, String value);

    void changeLocalizationDescription(String language, String id, String value, ObjectNode infoNode);

    ObjectNode getLocalizationElementProperties(String language, String id, ObjectNode infoNode);
}
