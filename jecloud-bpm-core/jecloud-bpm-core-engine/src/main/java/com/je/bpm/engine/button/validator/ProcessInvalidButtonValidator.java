/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.button.validator;

import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.button.Button;
import com.je.bpm.core.model.button.ProcessInvalidButton;
import com.je.bpm.core.model.task.KaiteCounterSignUserTask;
import com.je.bpm.core.model.task.KaiteMultiUserTask;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.engine.impl.persistence.entity.TaskEntityImpl;

/**
 * 流程作废按钮
 */
public class ProcessInvalidButtonValidator extends ButtonValidator<ProcessInvalidButton> {
    @Override
    public ProcessInvalidButton exec(ButtonValidateParam param, Button button, BpmnModel bpmnModel, TaskEntityImpl taskEntity) {
        if (!param.getStarting()) {
            return null;
        }
        //当前任务处理人是申请人
        if (!param.getStarter().equals(Authentication.getAuthenticatedUser().getDeptId())) {
            return null;
        }

        FlowElement flowElement = bpmnModel.getMainProcess().getFlowElement(taskEntity.getId());
        if (flowElement instanceof KaiteCounterSignUserTask || flowElement instanceof KaiteMultiUserTask) {
            return null;
        }
        ProcessInvalidButton processInvalidButton = (ProcessInvalidButton) getModelProcessButtonByCode(button, bpmnModel);
        return processInvalidButton;
    }
}
