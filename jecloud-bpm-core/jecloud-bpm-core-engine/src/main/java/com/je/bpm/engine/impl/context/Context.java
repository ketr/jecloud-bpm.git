/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.context;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.je.bpm.engine.ActivitiEngineAgenda;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.cfg.TransactionContext;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.deploy.ProcessDefinitionInfoCacheObject;

import java.util.*;


public class Context {

  protected static ThreadLocal<Stack<CommandContext>> commandContextThreadLocal = new ThreadLocal<Stack<CommandContext>>();
  protected static ThreadLocal<Stack<ProcessEngineConfigurationImpl>> processEngineConfigurationStackThreadLocal = new ThreadLocal<Stack<ProcessEngineConfigurationImpl>>();
  protected static ThreadLocal<Stack<TransactionContext>> transactionContextThreadLocal = new ThreadLocal<Stack<TransactionContext>>();
  protected static ThreadLocal<Map<String, ObjectNode>> bpmnOverrideContextThreadLocal = new ThreadLocal<Map<String, ObjectNode>>();

  protected static ResourceBundle.Control resourceBundleControl = new ResourceBundleControl();

  public static CommandContext getCommandContext() {
    Stack<CommandContext> stack = getStack(commandContextThreadLocal);
    if (stack.isEmpty()) {
      return null;
    }
    return stack.peek();
  }

  public static ActivitiEngineAgenda getAgenda() {
    return getCommandContext().getAgenda();
  }

  public static void setCommandContext(CommandContext commandContext) {
    getStack(commandContextThreadLocal).push(commandContext);
  }

  public static void removeCommandContext() {
    getStack(commandContextThreadLocal).pop();
  }

  public static ProcessEngineConfigurationImpl getProcessEngineConfiguration() {
    Stack<ProcessEngineConfigurationImpl> stack = getStack(processEngineConfigurationStackThreadLocal);
    if (stack.isEmpty()) {
      return null;
    }
    return stack.peek();
  }

  public static void setProcessEngineConfiguration(ProcessEngineConfigurationImpl processEngineConfiguration) {
    getStack(processEngineConfigurationStackThreadLocal).push(processEngineConfiguration);
  }

  public static void removeProcessEngineConfiguration() {
    getStack(processEngineConfigurationStackThreadLocal).pop();
  }

  public static TransactionContext getTransactionContext() {
    Stack<TransactionContext> stack = getStack(transactionContextThreadLocal);
    if (stack.isEmpty()) {
      return null;
    }
    return stack.peek();
  }

  public static void setTransactionContext(TransactionContext transactionContext) {
    getStack(transactionContextThreadLocal).push(transactionContext);
  }

  public static void removeTransactionContext() {
    getStack(transactionContextThreadLocal).pop();
  }

  protected static <T> Stack<T> getStack(ThreadLocal<Stack<T>> threadLocal) {
    Stack<T> stack = threadLocal.get();
    if (stack == null) {
      stack = new Stack<T>();
      threadLocal.set(stack);
    }
    return stack;
  }

  public static ObjectNode getBpmnOverrideElementProperties(String id, String processDefinitionId) {
    ObjectNode definitionInfoNode = getProcessDefinitionInfoNode(processDefinitionId);
    ObjectNode elementProperties = null;
    if (definitionInfoNode != null) {
      elementProperties = getProcessEngineConfiguration().getDynamicBpmnService().getBpmnElementProperties(id, definitionInfoNode);
    }
    return elementProperties;
  }

  public static ObjectNode getLocalizationElementProperties(String language, String id, String processDefinitionId, boolean useFallback) {
    ObjectNode definitionInfoNode = getProcessDefinitionInfoNode(processDefinitionId);
    ObjectNode localizationProperties = null;
    if (definitionInfoNode != null) {
      if (!useFallback) {
        localizationProperties = getProcessEngineConfiguration().getDynamicBpmnService().getLocalizationElementProperties(
            language, id, definitionInfoNode);

      } else {
        HashSet<Locale> candidateLocales = new LinkedHashSet<Locale>();
        candidateLocales.addAll(resourceBundleControl.getCandidateLocales(id, Locale.forLanguageTag(language)));
        for (Locale locale : candidateLocales) {
          localizationProperties = getProcessEngineConfiguration().getDynamicBpmnService().getLocalizationElementProperties(
              locale.toLanguageTag(), id, definitionInfoNode);

          if (localizationProperties != null) {
            break;
          }
        }
      }
    }
    return localizationProperties;
  }

  public static void removeBpmnOverrideContext() {
    bpmnOverrideContextThreadLocal.remove();
  }

  protected static ObjectNode getProcessDefinitionInfoNode(String processDefinitionId) {
    Map<String, ObjectNode> bpmnOverrideMap = getBpmnOverrideContext();
    if (!bpmnOverrideMap.containsKey(processDefinitionId)) {
      ProcessDefinitionInfoCacheObject cacheObject = getProcessEngineConfiguration().getDeploymentManager()
          .getProcessDefinitionInfoCache()
          .get(processDefinitionId);

      addBpmnOverrideElement(processDefinitionId, cacheObject.getInfoNode());
    }

    return getBpmnOverrideContext().get(processDefinitionId);
  }

  protected static Map<String, ObjectNode> getBpmnOverrideContext() {
    Map<String, ObjectNode> bpmnOverrideMap = bpmnOverrideContextThreadLocal.get();
    if (bpmnOverrideMap == null) {
      bpmnOverrideMap = new HashMap<String, ObjectNode>();
    }
    return bpmnOverrideMap;
  }

  protected static void addBpmnOverrideElement(String id, ObjectNode infoNode) {
    Map<String, ObjectNode> bpmnOverrideMap = bpmnOverrideContextThreadLocal.get();
    if (bpmnOverrideMap == null) {
      bpmnOverrideMap = new HashMap<String, ObjectNode>();
      bpmnOverrideContextThreadLocal.set(bpmnOverrideMap);
    }
    bpmnOverrideMap.put(id, infoNode);
  }

  public static class ResourceBundleControl extends ResourceBundle.Control {
    @Override
    public List<Locale> getCandidateLocales(String baseName, Locale locale) {
      return super.getCandidateLocales(baseName, locale);
    }
  }
}
