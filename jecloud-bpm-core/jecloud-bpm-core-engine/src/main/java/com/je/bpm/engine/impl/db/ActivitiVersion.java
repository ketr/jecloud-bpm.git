/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.db;

import java.util.List;
import static java.util.Collections.singletonList;

/**
 * This class is used for auto-upgrade purposes.
 * <p>
 * The idea is that instances of this class are put in a sequential order, and that the current version is determined from the ACT_GE_PROPERTY table.
 * <p>
 * Since sometimes in the past, a version is ambiguous (eg. 5.12 => 5.12, 5.12.1, 5.12T) this class act as a wrapper with a smarter matches() method.
 */
public class ActivitiVersion {

    protected String mainVersion;
    protected List<String> alternativeVersionStrings;

    public ActivitiVersion(String mainVersion) {
        this.mainVersion = mainVersion;
        this.alternativeVersionStrings = singletonList(mainVersion);
    }

    public ActivitiVersion(String mainVersion, List<String> alternativeVersionStrings) {
        this.mainVersion = mainVersion;
        this.alternativeVersionStrings = alternativeVersionStrings;
    }

    public String getMainVersion() {
        return mainVersion;
    }

    public boolean matches(String version) {
        if (version.equals(mainVersion)) {
            return true;
        } else if (!alternativeVersionStrings.isEmpty()) {
            return alternativeVersionStrings.contains(version);
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ActivitiVersion)) {
            return false;
        }
        ActivitiVersion other = (ActivitiVersion) obj;
        boolean mainVersionEqual = mainVersion.equals(other.mainVersion);
        if (!mainVersionEqual) {
            return false;
        } else {
            if (alternativeVersionStrings != null) {
                return alternativeVersionStrings.equals(other.alternativeVersionStrings);
            } else {
                return other.alternativeVersionStrings == null;
            }
        }
    }

}
