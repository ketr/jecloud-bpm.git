/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.repository;

import com.je.bpm.engine.internal.Internal;
import com.je.bpm.engine.query.Query;

/**
 * Allows programmatic querying of {@link Deployment}s.
 * <p>
 * Note that it is impossible to retrieve the deployment resources through the results of this operation, since that would cause a huge transfer of (possibly) unneeded bytes over the wire.
 * <p>
 * To retrieve the actual bytes of a deployment resource use the operations on the {@link com.je.bpm.engine.RepositoryService#getDeploymentResourceNames(String)} and
 * {@link com.je.bpm.engine.RepositoryService#getResourceAsStream(String, String)}
 */
@Internal
public interface DeploymentQuery extends Query<DeploymentQuery, Deployment> {

    /**
     * Only select deployments with the given deployment id.
     */
    DeploymentQuery deploymentId(String deploymentId);

    /**
     * Only select deployments with the given name.
     */
    DeploymentQuery deploymentName(String name);

    /**
     * Only select deployments with a name like the given string.
     */
    DeploymentQuery deploymentNameLike(String nameLike);

    /**
     * Only select deployments with the given category.
     *
     * @see DeploymentBuilder#category(String)
     */
    DeploymentQuery deploymentCategory(String category);

    /**
     * Only select deployments with a category like the given string.
     */
    DeploymentQuery deploymentCategoryLike(String categoryLike);

    /**
     * Only select deployments that have a different category then the given one.
     *
     * @see DeploymentBuilder#category(String)
     */
    DeploymentQuery deploymentCategoryNotEquals(String categoryNotEquals);

    /**
     * Only select deployments with the given key.
     */
    DeploymentQuery deploymentKey(String key);

    /**
     * Only select deployments with a key like the given string.
     */
    DeploymentQuery deploymentKeyLike(String keyLike);

    /**
     * Only select deployment that have the given tenant id.
     */
    DeploymentQuery deploymentTenantId(String tenantId);

    /**
     * Only select deployments with a tenant id like the given one.
     */
    DeploymentQuery deploymentTenantIdLike(String tenantIdLike);

    /**
     * Only select deployments that do not have a tenant id.
     */
    DeploymentQuery deploymentWithoutTenantId();

    /**
     * Only select deployments with the given process definition key.
     */
    DeploymentQuery processDefinitionKey(String key);

    /**
     * Only select deployments with a process definition key like the given string.
     */
    DeploymentQuery processDefinitionKeyLike(String keyLike);

    /**
     * Only select deployments where the deployment time is the latest value.
     * Can only be used together with the deployment key.
     */
    DeploymentQuery latest();

    /**
     * Only select deployments where the deployment version is the latest value
     */
    DeploymentQuery latestVersion();

    // sorting ////////////////////////////////////////////////////////

    /**
     * Order by deployment id (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    DeploymentQuery orderByDeploymentId();

    /**
     * Order by deployment name (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    DeploymentQuery orderByDeploymentName();

    /**
     * Order by deployment time (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    DeploymentQuery orderByDeploymenTime();

    /**
     * Order by tenant id (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    DeploymentQuery orderByTenantId();
}
