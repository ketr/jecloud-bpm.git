/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.je.bpm.engine.impl.TaskQueryImpl;
import com.je.bpm.engine.internal.Internal;
import com.je.bpm.engine.task.Task;

import java.util.List;
import java.util.Map;

@Internal
public interface TaskEntityManager extends EntityManager<TaskEntity> {

    void insert(TaskEntity taskEntity, ExecutionEntity execution);

    void changeTransferAssignee(TaskEntity taskEntity, String assignee, boolean fireEvents);
    void changeTaskAssignee(TaskEntity taskEntity, String assignee);

    void changeTaskAssigneeNoEvents(TaskEntity taskEntity, String assignee);

    void changeTaskOwner(TaskEntity taskEntity, String owner);

    List<TaskEntity> findTasksByExecutionId(String executionId);

    List<TaskEntity> findTasksByProcessInstanceId(String processInstanceId);

    List<Task> findTasksByQueryCriteria(TaskQueryImpl taskQuery);

    List<Task> findTasksAndVariablesByQueryCriteria(TaskQueryImpl taskQuery);

    long findTaskCountByQueryCriteria(TaskQueryImpl taskQuery);

    List<Task> findTasksByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults);

    long findTaskCountByNativeQuery(Map<String, Object> parameterMap);

    List<Task> findTasksByParentTaskId(String parentTaskId);

    void updateTaskTenantIdForDeployment(String deploymentId, String newTenantId);

    void deleteTask(String taskId, String deleteReason, boolean cascade);

    void deleteTask(String taskId, String deleteReason, boolean cascade, boolean cancel);

    void deleteTasksByProcessInstanceId(String processInstanceId, String deleteReason, boolean cascade);

    void deleteTask(TaskEntity task, String deleteReason, boolean cascade, boolean cancel);

}
