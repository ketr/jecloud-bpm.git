/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.repository;

import com.je.bpm.engine.internal.Internal;
import com.je.bpm.engine.query.Query;

/**
 * Allows programmatic querying of {@link Model}s.
 */
@Internal
public interface ModelQuery extends Query<ModelQuery, Model> {

    /**
     * Only select model with the given id.
     */
    ModelQuery modelId(String modelId);

    /**
     * Only select models with the given category.
     */
    ModelQuery modelCategory(String modelCategory);

    /**
     * Only select models where the category matches the given parameter. The syntax that should be used is the same as in SQL, eg. %activiti%
     */
    ModelQuery modelCategoryLike(String modelCategoryLike);

    /**
     * Only select models that have a different category then the given one.
     */
    ModelQuery modelCategoryNotEquals(String categoryNotEquals);

    /**
     * Only select models with the given name.
     */
    ModelQuery modelName(String modelName);

    /**
     * Only select models where the name matches the given parameter. The syntax that should be used is the same as in SQL, eg. %activiti%
     */
    ModelQuery modelNameLike(String modelNameLike);

    /**
     * Only selects models with the given key.
     */
    ModelQuery modelKey(String key);

    /**
     * Only select model with a certain version.
     */
    ModelQuery modelVersion(Integer modelVersion);

    /**
     * Only selects models with the disabled.
     */
    ModelQuery disabled(Integer disabled);

    /**
     * Only selects models with the deployStatus.
     */
    ModelQuery deployStatus(Integer deployStatus);
    /**
     * Only selects models with the funcCode.
     */
    ModelQuery modelFuncCode(String funcCode);

    /**
     * Only selects models with the funcName.
     */
    ModelQuery modelFuncName(String funcName);

    /**
     * Only select models which has the highest version.
     * <p>
     * Note: if modelKey(key) is not used in this query, all the models with the highest version for each key will be returned (similar to process definitions)
     */
    ModelQuery latestVersion();

    /**
     * Only select models that are the source for the provided deployment
     */
    ModelQuery deploymentId(String deploymentId);

    /**
     * Only select models that are deployed (ie deploymentId != null)
     */
    ModelQuery deployed();

    /**
     * Only select models that are not yet deployed
     */
    ModelQuery notDeployed();

    /**
     * Only select models that have the given tenant id.
     */
    ModelQuery modelTenantId(String tenantId);

    /**
     * Only select models with a tenant id like the given one.
     */
    ModelQuery modelTenantIdLike(String tenantIdLike);

    /**
     * Only select models that do not have a tenant id.
     */
    ModelQuery modelWithoutTenantId();

    // ordering ////////////////////////////////////////////////////////////

    /**
     * Order by the category of the models (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    ModelQuery orderByModelCategory();

    /**
     * Order by the id of the models (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    ModelQuery orderByModelId();

    /**
     * Order by the key of the models (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    ModelQuery orderByModelKey();

    /**
     * Order by the version of the models (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    ModelQuery orderByModelVersion();

    /**
     * Order by the name of the models (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    ModelQuery orderByModelName();

    /**
     * Order by the creation time of the models (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    ModelQuery orderByCreateTime();

    /**
     * Order by the last update time of the models (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    ModelQuery orderByLastUpdateTime();

    /**
     * Order by tenant id (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    ModelQuery orderByTenantId();

    ModelQuery multipleConditionsOrQuery();

}
