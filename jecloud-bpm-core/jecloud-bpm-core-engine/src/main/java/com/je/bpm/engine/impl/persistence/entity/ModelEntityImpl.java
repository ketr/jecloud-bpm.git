/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity;

import com.google.common.base.Strings;
import com.je.bpm.core.model.config.ProcessDeployTypeEnum;
import com.je.bpm.engine.ProcessEngineConfiguration;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ModelEntityImpl extends AbstractEntity implements ModelEntity, Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 名称
     */
    protected String name;
    /**
     * 流程key
     */
    protected String key;
    /**
     * 流程分类
     */
    protected String category;
    /**
     * 创建时间
     */
    protected Date createTime;
    /**
     * 最后更新时间
     */
    protected Date lastUpdateTime;
    /**
     * 流程版本
     */
    protected Integer version = 1;
    /**
     * 以json格式保存流程定义信息
     */
    protected String metaInfo;
    /**
     * 部署id
     */
    protected String deploymentId;
    /**
     * 对应bytearray的id，表示对应的模型文件
     */
    protected String editorSourceValueId;
    /**
     * 对应bytearray的id，表示该模型生成的图片文件
     */
    protected String editorSourceExtraValueId;
    /**
     * 对应加签bytearray的id，表示对应的模型文件
     */
    protected String editorCsSourceValueId;
    /**
     * 租户id
     */
    protected String tenantId = ProcessEngineConfiguration.NO_TENANT_ID;
    /**
     * 功能code
     */
    protected String funcCode;
    /**
     * 功能名称
     */
    protected String funcName;
    /**
     * 部署模式CODE
     */
    protected String runModeCode = ProcessDeployTypeEnum.PRODUCT.name();
    /**
     * 部署模式
     */
    protected String runModeName = "生产环境模式";
    /**
     * 是否部署
     */
    protected int deployStatus = 0;
    /**
     * 是否禁用
     */
    protected int disabled = 0;
    /**
     * 源
     */
    protected ByteArrayEntity resource;
    /**
     * 源
     */
    protected CsByteArrayEntity csResource;

    public ModelEntityImpl() {

    }

    @Override
    public Object getPersistentState() {
        Map<String, Object> persistentState = new HashMap<String, Object>();
        persistentState.put("name", this.name);
        persistentState.put("key", key);
        persistentState.put("category", this.category);
        persistentState.put("createTime", this.createTime);
        persistentState.put("lastUpdateTime", lastUpdateTime);
        persistentState.put("version", this.version);
        persistentState.put("metaInfo", this.metaInfo);
        persistentState.put("deploymentId", deploymentId);
        persistentState.put("editorSourceValueId", this.editorSourceValueId);
        persistentState.put("editorSourceExtraValueId", this.editorSourceExtraValueId);
        persistentState.put("editorCsSourceValueId", this.editorSourceValueId);
        return persistentState;
    }

    // getters and setters ////////////////////////////////////////////////////////

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    @Override
    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String getMetaInfo() {
        return metaInfo;
    }

    @Override
    public void setMetaInfo(String metaInfo) {
        this.metaInfo = metaInfo;
    }

    @Override
    public String getDeploymentId() {
        return deploymentId;
    }

    @Override
    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }

    @Override
    public String getEditorSourceValueId() {
        return editorSourceValueId;
    }

    @Override
    public void setEditorSourceValueId(String editorSourceValueId) {
        this.editorSourceValueId = editorSourceValueId;
    }

    public String getEditorCsSourceValueId() {
        return editorCsSourceValueId;
    }

    public void setEditorCsSourceValueId(String editorCsSourceValueId) {
        this.editorCsSourceValueId = editorCsSourceValueId;
    }

    @Override
    public String getEditorSourceExtraValueId() {
        return editorSourceExtraValueId;
    }

    @Override
    public void setEditorSourceExtraValueId(String editorSourceExtraValueId) {
        this.editorSourceExtraValueId = editorSourceExtraValueId;
    }

    @Override
    public void buildResource(ByteArrayEntity resourceEntity, String id) {
        resourceEntity.setName(String.format("%s%s", name, ".bpmn20.xml"));
        resource = resourceEntity;
        resource.setName(this.getResource().getName());
        if (!Strings.isNullOrEmpty(metaInfo)) {
            resource.setBytes(metaInfo.getBytes());
        }
        resource.setId(id);
        editorSourceValueId = id;
        metaInfo = "";
    }

    @Override
    public void buildCsResource(CsByteArrayEntity resourceEntity, String id) {
        resourceEntity.setName(String.format("%s%s", name, ".bpmn20.xml"));
        csResource = resourceEntity;
        csResource.setName(this.getCsResource().getName());
        if (!Strings.isNullOrEmpty(metaInfo)) {
            csResource.setBytes(metaInfo.getBytes());
        }
        csResource.setId(id);
        editorCsSourceValueId = id;
        metaInfo = "";
    }

    @Override
    public void addResource(ByteArrayEntity resource) {
        this.resource = resource;
    }

    @Override
    public void addCsResource(CsByteArrayEntity resource) {
        this.csResource = resource;
    }

    @Override
    public ByteArrayEntity getResource() {
        return resource;
    }

    @Override
    public CsByteArrayEntity getCsResource() {
        return csResource;
    }

    @Override
    public String getTenantId() {
        return tenantId;
    }

    @Override
    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public boolean hasEditorSource() {
        return this.editorSourceValueId != null;
    }

    @Override
    public boolean hasEditorSourceExtra() {
        return this.editorSourceExtraValueId != null;
    }

    @Override
    public String getRunModeCode() {
        return runModeCode;
    }

    @Override
    public void setRunModeCode(String runModeCode) {
        this.runModeCode = runModeCode;
    }

    @Override
    public String getRunModeName() {
        return runModeName;
    }

    @Override
    public void setRunModeName(String runModeName) {
        this.runModeName = runModeName;
    }

    @Override
    public String getFuncCode() {
        return funcCode;
    }

    @Override
    public void setFuncCode(String funcCode) {
        this.funcCode = funcCode;
    }

    @Override
    public String getFuncName() {
        return funcName;
    }

    @Override
    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

    public int getDeployStatus() {
        return deployStatus;
    }

    public void setDeployStatus(int deployStatus) {
        this.deployStatus = deployStatus;
    }

    public int getDisabled() {
        return disabled;
    }

    public void setDisabled(int disabled) {
        this.disabled = disabled;
    }
}
