/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.persistence.entity.data.impl;

import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.persistence.CachedEntityMatcher;
import com.je.bpm.engine.impl.persistence.entity.VariableInstanceEntity;
import com.je.bpm.engine.impl.persistence.entity.VariableInstanceEntityImpl;
import com.je.bpm.engine.impl.persistence.entity.data.AbstractDataManager;
import com.je.bpm.engine.impl.persistence.entity.data.VariableInstanceDataManager;
import com.je.bpm.engine.impl.persistence.entity.data.impl.cachematcher.VariableByExecutionIdMatcher;

import java.util.*;

public class MybatisVariableInstanceDataManager extends AbstractDataManager<VariableInstanceEntity> implements VariableInstanceDataManager {

    protected CachedEntityMatcher<VariableInstanceEntity> variableInstanceEntity = new VariableByExecutionIdMatcher();

    public MybatisVariableInstanceDataManager(ProcessEngineConfigurationImpl processEngineConfiguration) {
        super(processEngineConfiguration);
    }

    @Override
    public Class<? extends VariableInstanceEntity> getManagedEntityClass() {
        return VariableInstanceEntityImpl.class;
    }

    @Override
    public VariableInstanceEntity create() {
        VariableInstanceEntityImpl variableInstanceEntity = new VariableInstanceEntityImpl();
        variableInstanceEntity.setRevision(0); // For backwards compatibility, variables / HistoricVariableUpdate assumes revision 0 for the first time
        return variableInstanceEntity;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<VariableInstanceEntity> findVariableInstancesByTaskId(String taskId) {
        return getDbSqlSession().selectList("selectVariablesByTaskId", taskId);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<VariableInstanceEntity> findVariableInstancesByTaskIds(Set<String> taskIds) {
        return getDbSqlSession().selectList("selectVariablesByTaskIds", taskIds);
    }

    @Override
    public List<VariableInstanceEntity> findVariableInstancesByExecutionId(final String executionId) {
        return getList("selectVariablesByExecutionId", executionId, variableInstanceEntity, true);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<VariableInstanceEntity> findVariableInstancesByExecutionIds(Set<String> executionIds) {
        return getDbSqlSession().selectList("selectVariablesByExecutionIds", executionIds);
    }

    @Override
    public VariableInstanceEntity findVariableInstanceByExecutionAndName(String executionId, String variableName) {
        Map<String, String> params = new HashMap<String, String>(2);
        params.put("executionId", executionId);
        params.put("name", variableName);
        return (VariableInstanceEntity) getDbSqlSession().selectOne("selectVariableInstanceByExecutionAndName", params);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<VariableInstanceEntity> findVariableInstancesByExecutionAndNames(String executionId, Collection<String> names) {
        Map<String, Object> params = new HashMap<String, Object>(2);
        params.put("executionId", executionId);
        params.put("names", names);
        return getDbSqlSession().selectList("selectVariableInstancesByExecutionAndNames", params);
    }

    @Override
    public VariableInstanceEntity findVariableInstanceByTaskAndName(String taskId, String variableName) {
        Map<String, String> params = new HashMap<String, String>(2);
        params.put("taskId", taskId);
        params.put("name", variableName);
        return (VariableInstanceEntity) getDbSqlSession().selectOne("selectVariableInstanceByTaskAndName", params);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<VariableInstanceEntity> findVariableInstancesByTaskAndNames(String taskId, Collection<String> names) {
        Map<String, Object> params = new HashMap<String, Object>(2);
        params.put("taskId", taskId);
        params.put("names", names);
        return getDbSqlSession().selectList("selectVariableInstancesByTaskAndNames", params);
    }

    @Override
    public List<VariableInstanceEntity> findVariableInstancesByProcessInstanceId(String piid) {
        Map<String, Object> params = new HashMap<String, Object>(1);
        params.put("piid", piid);
        return getDbSqlSession().selectList("findVariableInstancesByProcessInstanceId", params);
    }
}
