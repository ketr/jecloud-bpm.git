/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.history;

import com.je.bpm.engine.internal.Internal;
import com.je.bpm.engine.task.TaskInfoQuery;

import java.util.Date;

/**
 * Allows programmatic querying for {@link HistoricTaskInstance}s.
 */
@Internal
public interface HistoricTaskInstanceQuery extends TaskInfoQuery<HistoricTaskInstanceQuery, HistoricTaskInstance> {

    /**
     * Only select historic task instances with the given task delete reason.
     */
    HistoricTaskInstanceQuery taskDeleteReason(String taskDeleteReason);

    /**
     * Only select historic task instances with a task description like the given value. The syntax that should be used is the same as in SQL, eg. %activiti%.
     */
    HistoricTaskInstanceQuery taskDeleteReasonLike(String taskDeleteReasonLike);

    /**
     * Only select historic task instances which are finished.
     */
    HistoricTaskInstanceQuery finished();

    /**
     * Only select historic task instances which aren't finished yet.
     */
    HistoricTaskInstanceQuery unfinished();

    /**
     * Only select historic task instances which are part of a process instance which is already finished.
     */
    HistoricTaskInstanceQuery processFinished();

    /**
     * Only select historic task instances which are part of a process instance which is not finished yet.
     */
    HistoricTaskInstanceQuery processUnfinished();

    /**
     * Only select historic task instances which are completed on the given date
     */
    HistoricTaskInstanceQuery taskCompletedOn(Date endDate);

    /**
     * Only select historic task instances which are completed before the given date
     */
    HistoricTaskInstanceQuery taskCompletedBefore(Date endDate);

    /**
     * Only select historic task instances which are completed after the given date
     */
    HistoricTaskInstanceQuery taskCompletedAfter(Date endDate);

    // ORDERING

    /**
     * Order by the historic activity instance id this task was used in (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    HistoricTaskInstanceQuery orderByHistoricActivityInstanceId();

    /**
     * Order by duration (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    HistoricTaskInstanceQuery orderByHistoricTaskInstanceDuration();

    /**
     * Order by end time (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    HistoricTaskInstanceQuery orderByHistoricTaskInstanceEndTime();

    /**
     * Order by start time (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    HistoricTaskInstanceQuery orderByHistoricTaskInstanceStartTime();

    /**
     * Order by task delete reason (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    HistoricTaskInstanceQuery orderByDeleteReason();

}
