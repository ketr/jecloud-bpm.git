/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.history;

import com.je.bpm.engine.internal.Internal;
import com.je.bpm.engine.query.Query;

/**
 * Programmatic querying for {@link HistoricDetail}s.
 */
@Internal
public interface HistoricDetailQuery extends Query<HistoricDetailQuery, HistoricDetail> {

    /**
     * Only select historic info with the given id.
     */
    HistoricDetailQuery id(String id);

    /**
     * Only select historic variable updates with the given process instance. {@link com.je.bpm.engine.runtime.ProcessInstance) ids and {@link HistoricProcessInstance} ids match.
     */
    HistoricDetailQuery processInstanceId(String processInstanceId);

    /**
     * Only select historic variable updates with the given execution. Note that {@link com.je.bpm.engine.runtime.Execution} ids are not stored in the history as first class citizen, only process instances are.
     */
    HistoricDetailQuery executionId(String executionId);

    /**
     * Only select historic variable updates associated to the given {@link HistoricActivityInstance activity instance}.
     */
    HistoricDetailQuery activityInstanceId(String activityInstanceId);

    /**
     * Only select historic variable updates associated to the given {@link HistoricTaskInstance historic task instance}.
     */
    HistoricDetailQuery taskId(String taskId);

    /**
     * Only select {@link HistoricVariableUpdate}s.
     */
    HistoricDetailQuery variableUpdates();

    /**
     * Exclude all task-related {@link HistoricDetail}s, so only items which have no task-id set will be selected. When used together with {@link #taskId(String)}, this call is ignored task details are
     * NOT excluded.
     */
    HistoricDetailQuery excludeTaskDetails();

    HistoricDetailQuery orderByProcessInstanceId();

    HistoricDetailQuery orderByVariableName();

    HistoricDetailQuery orderByFormPropertyId();

    HistoricDetailQuery orderByVariableType();

    HistoricDetailQuery orderByVariableRevision();

    HistoricDetailQuery orderByTime();
}
