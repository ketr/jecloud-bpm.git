/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.parser.handler;

import com.je.bpm.core.model.*;
import com.je.bpm.core.model.error.Error;
import com.je.bpm.core.model.event.EndEvent;
import com.je.bpm.core.model.message.Message;
import com.je.bpm.engine.impl.bpmn.parser.BpmnParse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EndEventParseHandler extends AbstractActivityBpmnParseHandler<EndEvent> {

    private static final Logger logger = LoggerFactory.getLogger(EndEventParseHandler.class);

    @Override
    public Class<? extends BaseElement> getHandledType() {
        return EndEvent.class;
    }

    @Override
    protected void executeParse(BpmnParse bpmnParse, EndEvent endEvent) {
        EventDefinition eventDefinition = null;
        if (endEvent.getEventDefinitions().size() > 0) {
            eventDefinition = endEvent.getEventDefinitions().get(0);
            if (eventDefinition instanceof ErrorEventDefinition) {
                ErrorEventDefinition errorDefinition = (ErrorEventDefinition) eventDefinition;
                if (bpmnParse.getBpmnModel().containsErrorRef(errorDefinition.getErrorRef())) {
                    for (Error error : bpmnParse.getBpmnModel().getErrors().values()) {
                        String errorCode = null;
                        if (error.getId().equals(errorDefinition.getErrorRef())) {
                            errorCode = error.getErrorCode();
                        }
                        if (StringUtils.isEmpty(errorCode)) {
                            logger.warn("errorCode is required for an error event " + endEvent.getId());
                        }
                    }
                }
                endEvent.setBehavior(bpmnParse.getActivityBehaviorFactory().createErrorEndEventActivityBehavior(endEvent, errorDefinition));
            } else if (eventDefinition instanceof TerminateEventDefinition) {
                endEvent.setBehavior(bpmnParse.getActivityBehaviorFactory().createTerminateEndEventActivityBehavior(endEvent));
            } else if (eventDefinition instanceof CancelEventDefinition) {
                endEvent.setBehavior(bpmnParse.getActivityBehaviorFactory().createCancelEndEventActivityBehavior(endEvent));
            } else if (eventDefinition instanceof MessageEventDefinition) {
                MessageEventDefinition messageEventDefinition = MessageEventDefinition.class
                        .cast(eventDefinition);
                Message message = bpmnParse.getBpmnModel()
                        .getMessage(messageEventDefinition.getMessageRef());

                BpmnModel bpmnModel = bpmnParse.getBpmnModel();
                if (bpmnModel.containsMessageId(messageEventDefinition.getMessageRef())) {
                    messageEventDefinition.setMessageRef(message.getName());
                    messageEventDefinition.setExtensionElements(message.getExtensionElements());
                }

                endEvent.setBehavior(bpmnParse.getActivityBehaviorFactory()
                        .createThrowMessageEndEventActivityBehavior(endEvent,
                                messageEventDefinition,
                                message));
            } else {
                endEvent.setBehavior(bpmnParse.getActivityBehaviorFactory().createNoneEndEventActivityBehavior(endEvent));
            }

        } else {
            endEvent.setBehavior(bpmnParse.getActivityBehaviorFactory().createNoneEndEventActivityBehavior(endEvent));
        }
    }

}
