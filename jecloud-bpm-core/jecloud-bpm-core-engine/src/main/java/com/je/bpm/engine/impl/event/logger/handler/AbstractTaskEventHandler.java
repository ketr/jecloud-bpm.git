/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.event.logger.handler;

import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public abstract class AbstractTaskEventHandler extends AbstractDatabaseEventLoggerEventHandler {

    protected Map<String, Object> handleCommonTaskFields(TaskEntity task) {
        Map<String, Object> data = new HashMap<String, Object>();
        putInMapIfNotNull(data, Fields.ID, task.getId());
        putInMapIfNotNull(data, Fields.NAME, task.getName());
        putInMapIfNotNull(data, Fields.TASK_DEFINITION_KEY, task.getTaskDefinitionKey());
        putInMapIfNotNull(data, Fields.DESCRIPTION, task.getDescription());
        putInMapIfNotNull(data, Fields.ASSIGNEE, task.getAssignee());
        putInMapIfNotNull(data, Fields.OWNER, task.getOwner());
        putInMapIfNotNull(data, Fields.CATEGORY, task.getCategory());
        putInMapIfNotNull(data, Fields.CREATE_TIME, task.getCreateTime());
        putInMapIfNotNull(data, Fields.DUE_DATE, task.getDueDate());
        putInMapIfNotNull(data, Fields.FORM_KEY, task.getFormKey());
        putInMapIfNotNull(data, Fields.PRIORITY, task.getPriority());
        putInMapIfNotNull(data, Fields.PROCESS_DEFINITION_ID, task.getProcessDefinitionId());
        putInMapIfNotNull(data, Fields.PROCESS_INSTANCE_ID, task.getProcessInstanceId());
        putInMapIfNotNull(data, Fields.EXECUTION_ID, task.getExecutionId());
        if (task.getTenantId() != null && !ProcessEngineConfigurationImpl.NO_TENANT_ID.equals(task.getTenantId())) {
            putInMapIfNotNull(data, Fields.TENANT_ID, task.getTenantId()); // Important for standalone tasks
        }
        return data;
    }

}
