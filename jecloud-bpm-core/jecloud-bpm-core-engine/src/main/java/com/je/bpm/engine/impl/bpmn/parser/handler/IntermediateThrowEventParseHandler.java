/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.bpmn.parser.handler;

import com.je.bpm.core.model.*;
import com.je.bpm.core.model.event.ThrowEvent;
import com.je.bpm.core.model.message.Message;
import com.je.bpm.engine.impl.bpmn.parser.BpmnParse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IntermediateThrowEventParseHandler extends AbstractActivityBpmnParseHandler<ThrowEvent> {

    private static final Logger logger = LoggerFactory.getLogger(IntermediateThrowEventParseHandler.class);

    @Override
    public Class<? extends BaseElement> getHandledType() {
        return ThrowEvent.class;
    }

    @Override
    protected void executeParse(BpmnParse bpmnParse, ThrowEvent intermediateEvent) {
        EventDefinition eventDefinition = null;
        if (!intermediateEvent.getEventDefinitions().isEmpty()) {
            eventDefinition = intermediateEvent.getEventDefinitions().get(0);
        }

        if (eventDefinition instanceof SignalEventDefinition) {
            SignalEventDefinition signalEventDefinition = (SignalEventDefinition) eventDefinition;
            intermediateEvent.setBehavior(bpmnParse.getActivityBehaviorFactory().createIntermediateThrowSignalEventActivityBehavior(intermediateEvent, signalEventDefinition,
                    bpmnParse.getBpmnModel().getSignal(signalEventDefinition.getSignalRef())));

        } else if (eventDefinition instanceof CompensateEventDefinition) {
            CompensateEventDefinition compensateEventDefinition = (CompensateEventDefinition) eventDefinition;
            intermediateEvent.setBehavior(bpmnParse.getActivityBehaviorFactory().createIntermediateThrowCompensationEventActivityBehavior(intermediateEvent, compensateEventDefinition));

        } else if (eventDefinition instanceof MessageEventDefinition) {

            MessageEventDefinition messageEventDefinition = (MessageEventDefinition) eventDefinition;
            Message message = bpmnParse.getBpmnModel().getMessage(messageEventDefinition.getMessageRef());

            BpmnModel bpmnModel = bpmnParse.getBpmnModel();
            if (bpmnModel.containsMessageId(messageEventDefinition.getMessageRef())) {
                messageEventDefinition.setMessageRef(message.getName());
                messageEventDefinition.setExtensionElements(message.getExtensionElements());
            }

            intermediateEvent.setBehavior(bpmnParse.getActivityBehaviorFactory()
                    .createThrowMessageEventActivityBehavior(intermediateEvent,
                            messageEventDefinition,
                            message));
        } else if (eventDefinition == null) {
            intermediateEvent.setBehavior(bpmnParse.getActivityBehaviorFactory().createIntermediateThrowNoneEventActivityBehavior(intermediateEvent));
        } else {
            logger.warn("Unsupported intermediate throw event type for throw event " + intermediateEvent.getId());
        }
    }

    //
    // Seems not to be used anymore?
    //
    // protected CompensateEventDefinition
    // createCompensateEventDefinition(BpmnParse bpmnParse,
    // org.activiti.bpmn.model.CompensateEventDefinition eventDefinition,
    // ScopeImpl scopeElement) {
    // if(StringUtils.isNotEmpty(eventDefinition.getActivityRef())) {
    // if(scopeElement.findActivity(eventDefinition.getActivityRef()) == null) {
    // bpmnParse.getBpmnModel().addProblem("Invalid attribute value for 'activityRef': no activity with id '"
    // + eventDefinition.getActivityRef() +
    // "' in current scope " + scopeElement.getId(), eventDefinition);
    // }
    // }
    //
    // CompensateEventDefinition compensateEventDefinition = new
    // CompensateEventDefinition();
    // compensateEventDefinition.setActivityRef(eventDefinition.getActivityRef());
    // compensateEventDefinition.setWaitForCompletion(eventDefinition.isWaitForCompletion());
    //
    // return compensateEventDefinition;
    // }

}
