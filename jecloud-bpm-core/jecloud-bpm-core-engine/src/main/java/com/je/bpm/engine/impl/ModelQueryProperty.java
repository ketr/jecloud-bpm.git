/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl;

import com.je.bpm.engine.query.QueryProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains the possible properties that can be used in a {@link ModelQuery}.
 */
public class ModelQueryProperty implements QueryProperty {

    private static final long serialVersionUID = 1L;

    private static final Map<String, ModelQueryProperty> properties = new HashMap<String, ModelQueryProperty>();

    public static final ModelQueryProperty MODEL_CATEGORY = new ModelQueryProperty("RES.CATEGORY_");
    public static final ModelQueryProperty MODEL_ID = new ModelQueryProperty("RES.ID_");
    public static final ModelQueryProperty MODEL_VERSION = new ModelQueryProperty("RES.VERSION_");
    public static final ModelQueryProperty MODEL_NAME = new ModelQueryProperty("RES.NAME_");
    public static final ModelQueryProperty MODEL_CREATE_TIME = new ModelQueryProperty("RES.CREATE_TIME_");
    public static final ModelQueryProperty MODEL_LAST_UPDATE_TIME = new ModelQueryProperty("RES.LAST_UPDATE_TIME_");
    public static final ModelQueryProperty MODEL_KEY = new ModelQueryProperty("RES.KEY_");
    public static final ModelQueryProperty MODEL_TENANT_ID = new ModelQueryProperty("RES.TENANT_ID_");

    private String name;

    public ModelQueryProperty(String name) {
        this.name = name;
        properties.put(name, this);
    }
    
    @Override
    public String getName() {
        return name;
    }

    public static ModelQueryProperty findByName(String propertyName) {
        return properties.get(propertyName);
    }

}
