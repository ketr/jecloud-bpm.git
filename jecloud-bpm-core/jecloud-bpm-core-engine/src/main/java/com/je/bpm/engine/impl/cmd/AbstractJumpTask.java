/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.impl.cmd;

import com.alibaba.fastjson2.JSONArray;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.engine.delegate.DelegateExecution;
import com.je.bpm.engine.history.HistoricTaskInstance;
import com.je.bpm.engine.history.HistoricVariableInstance;
import com.je.bpm.engine.impl.HistoricTaskInstanceQueryImpl;
import com.je.bpm.engine.impl.HistoricVariableInstanceQueryImpl;
import com.je.bpm.engine.impl.Page;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntity;
import com.je.bpm.engine.impl.persistence.entity.ExecutionEntityManager;
import com.je.bpm.engine.impl.util.CollectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 跳跃节点统一方法
 */
public abstract class AbstractJumpTask extends NeedsActiveTaskCmd<Void> implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(AbstractJumpTask.class);

    public AbstractJumpTask(String taskId, String prod, Map<String, Object> bean) {
        super(taskId, prod, bean);
    }

    public abstract String getComment();

    protected ExecutionEntity deleteAllMultiTask(CommandContext commandContext, ExecutionEntity executionEntity) {
        ExecutionEntity rootExecutionEntity = (ExecutionEntity) getMultiInstanceRootExecution(executionEntity);
        deleteChildExecutions(rootExecutionEntity, true, commandContext);
        return rootExecutionEntity;
    }

    protected void deleteChildExecutions(ExecutionEntity parentExecution, boolean deleteExecution, CommandContext commandContext) {
        // Delete all child executions
        ExecutionEntityManager executionEntityManager = commandContext.getExecutionEntityManager();
        Collection<ExecutionEntity> childExecutions = executionEntityManager.findChildExecutionsByParentExecutionId(parentExecution.getId());
        if (CollectionUtil.isNotEmpty(childExecutions)) {
            for (ExecutionEntity childExecution : childExecutions) {
                deleteChildExecutions(childExecution, true, commandContext);
            }
        }

        if (deleteExecution) {
            if (!parentExecution.isMultiInstanceRoot()) {
                executionEntityManager.deleteExecutionAndRelatedData(parentExecution, getComment());
            }
        }

    }

    public String getHistoryAssigneeByTask(CommandContext commandContext, String piid, FlowElement userTask) {
        HistoricTaskInstanceQueryImpl historicTaskInstanceQuery = new HistoricTaskInstanceQueryImpl();
        historicTaskInstanceQuery.processInstanceId(piid);
        historicTaskInstanceQuery.taskDefinitionKey(userTask.getId());
        historicTaskInstanceQuery.orderByTaskCreateTime().desc();
        List<HistoricTaskInstance> links = commandContext.getProcessEngineConfiguration().getHistoricTaskInstanceEntityManager()
                .findHistoricTaskInstancesByQueryCriteria(historicTaskInstanceQuery);
        if (links.size() == 0) {
            return "";
        }
        String prevAssignee = "";
        KaiteBaseUserTask kaiteBaseUserTask = (KaiteBaseUserTask) userTask;
        if (kaiteBaseUserTask.hasMultiInstanceLoopCharacteristics()) {
            Page page = new Page(0, 10);
            HistoricVariableInstanceQueryImpl historicVariableInstanceQuery = new HistoricVariableInstanceQueryImpl();
            historicVariableInstanceQuery.processInstanceId(piid);
            historicVariableInstanceQuery.variableName("datas");
            historicVariableInstanceQuery.executionId(links.get(0).getExecutionId());
            List<HistoricVariableInstance> historicVariableInstanceList = commandContext.getProcessEngineConfiguration()
                    .getHistoricVariableInstanceEntityManager().
                            findHistoricVariableInstancesByQueryCriteria(historicVariableInstanceQuery, page);
            if (historicVariableInstanceList.size() > 0) {
                Object value = historicVariableInstanceList.get(0).getValue();
                if (value instanceof List) {
                    List<String> userIds = (List<String>) value;
                    prevAssignee = userIds.stream().collect(Collectors.joining(","));
                }
                if (value instanceof JSONArray) {
                    List<String> userIds = new ArrayList<>();
                    JSONArray jsonArray = (JSONArray) value;
                    for (Object object : jsonArray) {
                        userIds.add(object.toString());
                    }
                    prevAssignee = userIds.stream().collect(Collectors.joining(","));
                }
            }
        } else {
            prevAssignee = links.get(0).getAssignee();
        }
        return prevAssignee;
    }

    protected DelegateExecution getMultiInstanceRootExecution(DelegateExecution executionEntity) {
        DelegateExecution multiInstanceRootExecution = null;
        DelegateExecution currentExecution = executionEntity;
        while (currentExecution != null && multiInstanceRootExecution == null && currentExecution.getParent() != null) {
            if (currentExecution.isMultiInstanceRoot()) {
                multiInstanceRootExecution = currentExecution;
            } else {
                currentExecution = currentExecution.getParent();
            }
        }
        return multiInstanceRootExecution;
    }

    protected Integer getLoopVariable(DelegateExecution execution, String variableName) {
        Object value = execution.getVariableLocal(variableName);
        DelegateExecution parent = execution.getParent();
        while (value == null && parent != null) {
            value = parent.getVariableLocal(variableName);
            parent = parent.getParent();
        }
        return (Integer) (value != null ? value : 0);
    }

}