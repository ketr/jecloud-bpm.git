/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.history;

import com.je.bpm.engine.internal.Internal;

/**
 * Allows to fetch the {@link ProcessInstanceHistoryLog} for a process instance.
 *
 * Note that every includeXXX() method below will lead to an additional query.
 *
 * This class is actually a convenience on top of the other specific queries such as {@link HistoricTaskInstanceQuery}, {@link HistoricActivityInstanceQuery}, ... It will execute separate queries for
 * each included type, order the data according to the date (ascending) and wrap the results in the {@link ProcessInstanceHistoryLog}.
 *
 */
@Internal
public interface ProcessInstanceHistoryLogQuery {

  /**
   * The {@link ProcessInstanceHistoryLog} will contain the {@link HistoricTaskInstance} instances.
   */
  ProcessInstanceHistoryLogQuery includeTasks();

  /**
   * The {@link ProcessInstanceHistoryLog} will contain the {@link HistoricActivityInstance} instances.
   */
  ProcessInstanceHistoryLogQuery includeActivities();

  /**
   * The {@link ProcessInstanceHistoryLog} will contain the {@link HistoricVariableInstance} instances.
   */
  ProcessInstanceHistoryLogQuery includeVariables();

  /**
   * The {@link ProcessInstanceHistoryLog} will contain the {@link com.je.bpm.engine.task.Comment} instances.
   */
  ProcessInstanceHistoryLogQuery includeComments();

  /**
   * The {@link ProcessInstanceHistoryLog} will contain the {@link HistoricVariableUpdate} instances.
   */
  ProcessInstanceHistoryLogQuery includeVariableUpdates();

  /**
   * The {@link ProcessInstanceHistoryLog} will contain the {@link HistoricFormProperty} instances.
   */
  ProcessInstanceHistoryLogQuery includeFormProperties();

  /** Executes the query. */
  ProcessInstanceHistoryLog singleResult();

}
