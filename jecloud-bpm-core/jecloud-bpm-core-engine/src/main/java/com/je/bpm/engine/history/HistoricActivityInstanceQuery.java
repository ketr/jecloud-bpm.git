/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.history;

import com.je.bpm.engine.internal.Internal;
import com.je.bpm.engine.query.Query;

/**
 * Programmatic querying for {@link HistoricActivityInstance}s.
 */
@Internal
public interface HistoricActivityInstanceQuery extends Query<HistoricActivityInstanceQuery, HistoricActivityInstance> {

    /**
     * Only select historic activity instances with the given id (primary key within history tables).
     */
    HistoricActivityInstanceQuery activityInstanceId(String activityInstanceId);

    /**
     * Only select historic activity instances with the given process instance. {@link com.je.bpm.engine.runtime.ProcessInstance) ids and {@link HistoricProcessInstance} ids match.
     */
    HistoricActivityInstanceQuery processInstanceId(String processInstanceId);

    /**
     * Only select historic activity instances for the given process definition
     */
    HistoricActivityInstanceQuery processDefinitionId(String processDefinitionId);

    /**
     * Only select historic activity instances for the given execution
     */
    HistoricActivityInstanceQuery executionId(String executionId);

    /**
     * Only select historic activity instances for the given activity (id from BPMN 2.0 XML)
     */
    HistoricActivityInstanceQuery activityId(String activityId);

    /**
     * Only select historic activity instances for activities with the given name
     */
    HistoricActivityInstanceQuery activityName(String activityName);

    /**
     * Only select historic activity instances for activities with the given activity type
     */
    HistoricActivityInstanceQuery activityType(String activityType);

    /**
     * Only select historic activity instances for userTask activities assigned to the given user
     */
    HistoricActivityInstanceQuery taskAssignee(String userId);

    /**
     * Only select historic activity instances that are finished.
     */
    HistoricActivityInstanceQuery finished();

    /**
     * Only select historic activity instances that are not finished yet.
     */
    HistoricActivityInstanceQuery unfinished();

    /**
     * Obly select historic activity instances with a specific delete reason.
     */
    HistoricActivityInstanceQuery deleteReason(String deleteReason);

    /**
     * Obly select historic activity instances with a delete reason that matches the provided parameter.
     */
    HistoricActivityInstanceQuery deleteReasonLike(String deleteReasonLike);

    /**
     * Only select historic activity instances that have the given tenant id.
     */
    HistoricActivityInstanceQuery activityTenantId(String tenantId);

    /**
     * Only select historic activity instances with a tenant id like the given one.
     */
    HistoricActivityInstanceQuery activityTenantIdLike(String tenantIdLike);

    /**
     * Only select historic activity instances that do not have a tenant id.
     */
    HistoricActivityInstanceQuery activityWithoutTenantId();

    // ordering
    // /////////////////////////////////////////////////////////////////

    /**
     * Order by id (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    HistoricActivityInstanceQuery orderByHistoricActivityInstanceId();

    /**
     * Order by processInstanceId (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    HistoricActivityInstanceQuery orderByProcessInstanceId();

    /**
     * Order by executionId (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    HistoricActivityInstanceQuery orderByExecutionId();

    /**
     * Order by activityId (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    HistoricActivityInstanceQuery orderByActivityId();

    /**
     * Order by activityName (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    HistoricActivityInstanceQuery orderByActivityName();

    /**
     * Order by activityType (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    HistoricActivityInstanceQuery orderByActivityType();

    /**
     * Order by start (needs to be followed by {@link #asc()} or {@link #desc()} ).
     */
    HistoricActivityInstanceQuery orderByHistoricActivityInstanceStartTime();

    /**
     * Order by end (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    HistoricActivityInstanceQuery orderByHistoricActivityInstanceEndTime();

    /**
     * Order by duration (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    HistoricActivityInstanceQuery orderByHistoricActivityInstanceDuration();

    /**
     * Order by processDefinitionId (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    HistoricActivityInstanceQuery orderByProcessDefinitionId();

    /**
     * Order by tenant id (needs to be followed by {@link #asc()} or {@link #desc()}).
     */
    HistoricActivityInstanceQuery orderByTenantId();

}
