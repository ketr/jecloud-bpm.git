/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.query;

import com.je.bpm.engine.internal.Internal;
import java.util.List;

/**
 * Describes basic methods for querying.
 * 定义查询的基本方法
 */
@Internal
public interface Query<T extends Query<?, ?>, U> {

    /**
     * Order the results ascending on the given property as defined in this class (needs to come after a call to one of the orderByXxxx methods).
     */
    T asc();

    /**
     * Order the results descending on the given property as defined in this class (needs to come after a call to one of the orderByXxxx methods).
     */
    T desc();

    /**
     * Order the results by the given {@link QueryProperty} (needs to be followed by {@link #asc()} or {@link #desc()})
     *
     * @param property the {@code QueryProperty} to be used to order the results
     * @return itself
     */
    T orderBy(QueryProperty property);

    /**
     * Executes the query and returns the number of results
     */
    long count();

    /**
     * Executes the query and returns the resulting entity or null if no entity matches the query criteria.
     *
     * @throws com.je.bpm.engine.ActivitiException when the query results in more than one entities.
     */
    U singleResult();

    /**
     * Executes the query and get a list of entities as the result.
     */
    List<U> list();

    /**
     * Executes the query and get a list of entities as the result.
     */
    List<U> listPage(int firstResult, int maxResults);
}
