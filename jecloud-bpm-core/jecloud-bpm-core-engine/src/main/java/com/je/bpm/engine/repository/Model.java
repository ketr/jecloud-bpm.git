/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.repository;

import com.je.bpm.engine.impl.persistence.entity.ByteArrayEntity;
import com.je.bpm.engine.impl.persistence.entity.CsByteArrayEntity;
import com.je.bpm.engine.internal.Internal;

import java.util.Date;

/**
 * Represents a model that is stored in the model repository. In addition, a model can be deployed to the Activiti Engine in a separate deployment step.
 * A model is a container for the meta data and sources of a process model that typically can be edited in a modeling environment.
 */
@Internal
public interface Model {

    String getId();

    void setId(String id);

    String getName();

    void setName(String name);

    String getKey();

    void setKey(String key);

    String getCategory();

    void setCategory(String category);

    Date getCreateTime();

    void setCreateTime(Date createTime);

    String getEditorCsSourceValueId();

    void setEditorCsSourceValueId(String editorSourceValueId);

    Date getLastUpdateTime();

    void setLastUpdateTime(Date lastUpdateTime);

    Integer getVersion();

    void setVersion(Integer version);

    String getMetaInfo();

    void setMetaInfo(String metaInfo);

    String getDeploymentId();

    void setDeploymentId(String deploymentId);

    String getEditorSourceValueId();

    void setEditorSourceValueId(String editorSourceValueId);

    String getEditorSourceExtraValueId();

    void setEditorSourceExtraValueId(String editorSourceExtraValueId);

    String getTenantId();

    void setTenantId(String tenantId);

    /**
     * 获取运行模式编码
     *
     * @return
     */
    String getRunModeCode();

    /**
     * 设置运行模式编码
     *
     * @param modeCode
     */
    void setRunModeCode(String modeCode);

    /**
     * 设置运行模式名称
     *
     * @return
     */
    String getRunModeName();

    /**
     * 设置模式名称
     *
     * @param modeName
     */
    void setRunModeName(String modeName);

    /**
     * 获取功能名称
     *
     * @return
     */
    String getFuncCode();

    /**
     * 设置功能编码
     *
     * @param funcCode
     */
    void setFuncCode(String funcCode);

    /**
     * 获取功能名称
     *
     * @return
     */
    String getFuncName();

    /**
     * 设置功能名称
     *
     * @param funcName
     */
    void setFuncName(String funcName);

    boolean hasEditorSource();

    boolean hasEditorSourceExtra();

    void addResource(ByteArrayEntity resource);

    void addCsResource(CsByteArrayEntity resource);

    ByteArrayEntity getResource();

    CsByteArrayEntity getCsResource();

    int getDeployStatus();

    void setDeployStatus(int deployStatus);

    int getDisabled();

    void setDisabled(int disabled);
}
