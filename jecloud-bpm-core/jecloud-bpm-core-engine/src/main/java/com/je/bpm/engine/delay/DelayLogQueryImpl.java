/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.engine.delay;

import com.je.bpm.engine.impl.AbstractQuery;
import com.je.bpm.engine.impl.Page;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import java.util.Date;
import java.util.List;

public class DelayLogQueryImpl extends AbstractQuery<DelayLogQuery, DelayLog> implements DelayLogQuery {

    private String taskId;
    private String taskName;
    private Date dueTime;
    private Date beforeDueTime;
    private Date afterDueTime;
    private String description;
    private String readState;
    private String processInstanceId;
    private String processDefinitionId;
    private Date createTime;
    private Date beforeCreateTime;
    private Date afterCreateTime;
    private String assignee;

    @Override
    public DelayLogQuery taskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    @Override
    public DelayLogQuery taskName(String taskName) {
        this.taskName = taskName;
        return this;
    }

    @Override
    public DelayLogQuery processInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
        return this;
    }

    @Override
    public DelayLogQuery processDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
        return this;
    }

    @Override
    public DelayLogQuery dueTime(Date date) {
        this.dueTime = dueTime;
        return this;
    }

    @Override
    public DelayLogQuery createTime(Date date) {
        this.createTime = date;
        return this;
    }

    @Override
    public DelayLogQuery description(String description) {
        this.description = description;
        return this;
    }

    @Override
    public DelayLogQuery readState(String readState) {
        this.readState = readState;
        return this;
    }

    @Override
    public DelayLogQuery beforeDueTime(Date date) {
        this.beforeDueTime = date;
        return this;
    }

    @Override
    public DelayLogQuery afterDueTime(Date date) {
        this.afterDueTime = date;
        return this;
    }

    @Override
    public DelayLogQuery beforeCreateTime(Date date) {
        this.beforeCreateTime = date;
        return this;
    }

    @Override
    public DelayLogQuery afterCreateTime(Date date) {
        this.afterCreateTime = date;
        return this;
    }

    @Override
    public DelayLogQuery assignee(String assignee) {
        this.assignee = assignee;
        return this;
    }

    @Override
    public long executeCount(CommandContext commandContext) {
        return 0;
    }

    @Override
    public List<DelayLog> executeList(CommandContext commandContext, Page page) {
        return null;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public Date getDueTime() {
        return dueTime;
    }

    public Date getBeforeDueTime() {
        return beforeDueTime;
    }

    public Date getAfterDueTime() {
        return afterDueTime;
    }

    public String getDescription() {
        return description;
    }

    public String getReadState() {
        return readState;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Date getBeforeCreateTime() {
        return beforeCreateTime;
    }

    public Date getAfterCreateTime() {
        return afterCreateTime;
    }
}
