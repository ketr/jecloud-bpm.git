/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model;

import com.je.bpm.core.model.event.BoundaryEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * 活动定义
 */
public abstract class Activity extends FlowNode {

    /**
     * 默认流向
     * 默认流向
     */
    protected String defaultFlow;

    /**
     * 是否补偿
     */
    protected boolean forCompensation;

    /**
     * 多实例属性
     */
    protected MultiInstanceLoopCharacteristics loopCharacteristics;

    /**
     * IO规范
     */
    protected IOSpecification ioSpecification;

    /**
     * 数据输入关联
     */
    protected List<DataAssociation> dataInputAssociations = new ArrayList<DataAssociation>();

    /**
     * 数据输出关联
     */
    protected List<DataAssociation> dataOutputAssociations = new ArrayList<DataAssociation>();

    /**
     * 边界事件
     */
    protected List<BoundaryEvent> boundaryEvents = new ArrayList<BoundaryEvent>();

    /**
     *
     */
    protected String failedJobRetryTimeCycleValue;

    /**
     *
     */
    protected List<MapExceptionEntry> mapExceptions = new ArrayList<MapExceptionEntry>();

    public String getFailedJobRetryTimeCycleValue() {
        return failedJobRetryTimeCycleValue;
    }

    public void setFailedJobRetryTimeCycleValue(String failedJobRetryTimeCycleValue) {
        this.failedJobRetryTimeCycleValue = failedJobRetryTimeCycleValue;
    }

    public boolean isForCompensation() {
        return forCompensation;
    }

    public void setForCompensation(boolean forCompensation) {
        this.forCompensation = forCompensation;
    }

    public List<BoundaryEvent> getBoundaryEvents() {
        return boundaryEvents;
    }

    public void setBoundaryEvents(List<BoundaryEvent> boundaryEvents) {
        this.boundaryEvents = boundaryEvents;
    }

    public String getDefaultFlow() {
        return defaultFlow;
    }

    public void setDefaultFlow(String defaultFlow) {
        this.defaultFlow = defaultFlow;
    }

    public MultiInstanceLoopCharacteristics getLoopCharacteristics() {
        return loopCharacteristics;
    }

    public void setLoopCharacteristics(MultiInstanceLoopCharacteristics loopCharacteristics) {
        this.loopCharacteristics = loopCharacteristics;
    }

    public boolean hasMultiInstanceLoopCharacteristics() {
        return getLoopCharacteristics() != null;
    }

    public IOSpecification getIoSpecification() {
        return ioSpecification;
    }

    public void setIoSpecification(IOSpecification ioSpecification) {
        this.ioSpecification = ioSpecification;
    }

    public List<DataAssociation> getDataInputAssociations() {
        return dataInputAssociations;
    }

    public void setDataInputAssociations(List<DataAssociation> dataInputAssociations) {
        this.dataInputAssociations = dataInputAssociations;
    }

    public List<DataAssociation> getDataOutputAssociations() {
        return dataOutputAssociations;
    }

    public void setDataOutputAssociations(List<DataAssociation> dataOutputAssociations) {
        this.dataOutputAssociations = dataOutputAssociations;
    }

    public List<MapExceptionEntry> getMapExceptions() {
        return mapExceptions;
    }

    public void setMapExceptions(List<MapExceptionEntry> mapExceptions) {
        this.mapExceptions = mapExceptions;
    }

    public void setValues(Activity otherActivity) {
        super.setValues(otherActivity);
        setFailedJobRetryTimeCycleValue(otherActivity.getFailedJobRetryTimeCycleValue());
        setDefaultFlow(otherActivity.getDefaultFlow());
        setForCompensation(otherActivity.isForCompensation());
        if (otherActivity.getLoopCharacteristics() != null) {
            setLoopCharacteristics(otherActivity.getLoopCharacteristics().clone());
        }
        if (otherActivity.getIoSpecification() != null) {
            setIoSpecification(otherActivity.getIoSpecification().clone());
        }

        dataInputAssociations = new ArrayList<DataAssociation>();
        if (otherActivity.getDataInputAssociations() != null && !otherActivity.getDataInputAssociations().isEmpty()) {
            for (DataAssociation association : otherActivity.getDataInputAssociations()) {
                dataInputAssociations.add(association.clone());
            }
        }

        dataOutputAssociations = new ArrayList<DataAssociation>();
        if (otherActivity.getDataOutputAssociations() != null && !otherActivity.getDataOutputAssociations().isEmpty()) {
            for (DataAssociation association : otherActivity.getDataOutputAssociations()) {
                dataOutputAssociations.add(association.clone());
            }
        }

        boundaryEvents.clear();
        for (BoundaryEvent event : otherActivity.getBoundaryEvents()) {
            boundaryEvents.add(event);
        }
    }
}
