/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task.assignment;

/**
 * 人员权限
 */
public class AssignmentPermission {
    /**
     * 公司内可见
     */
    private Boolean company;
    /**
     * 公司监管可见
     */
    private Boolean companySupervision;
    /**
     * 本部门
     */
    private Boolean dept;
    /**
     * 部门内包含子部门
     */
    private Boolean deptAll;
    /**
     * 直属领导可见
     */
    private Boolean directLeader;
    /**
     * 部门领导
     */
    private Boolean deptLeader;
    /**
     * 监管领导可见
     */
    private Boolean supervisionLeader;
    /**
     * sql
     */
    private String sql;
    /**
     * sql备注
     */
    private String sqlRemarks;

    public Boolean getCompany() {
        return company;
    }

    public void setCompany(Boolean company) {
        this.company = company;
    }

    public Boolean getCompanySupervision() {
        return companySupervision;
    }

    public void setCompanySupervision(Boolean companySupervision) {
        this.companySupervision = companySupervision;
    }

    public Boolean getDept() {
        return dept;
    }

    public void setDept(Boolean dept) {
        this.dept = dept;
    }

    public Boolean getDeptAll() {
        return deptAll;
    }

    public void setDeptAll(Boolean deptAll) {
        this.deptAll = deptAll;
    }

    public Boolean getDirectLeader() {
        return directLeader;
    }

    public void setDirectLeader(Boolean directLeader) {
        this.directLeader = directLeader;
    }

    public Boolean getDeptLeader() {
        return deptLeader;
    }

    public void setDeptLeader(Boolean deptLeader) {
        this.deptLeader = deptLeader;
    }

    public Boolean getSupervisionLeader() {
        return supervisionLeader;
    }

    public void setSupervisionLeader(Boolean supervisionLeader) {
        this.supervisionLeader = supervisionLeader;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public String getSqlRemarks() {
        return sqlRemarks;
    }

    public void setSqlRemarks(String sqlRemarks) {
        this.sqlRemarks = sqlRemarks;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AssignmentPermission{");
        sb.append("company=").append(company);
        sb.append(", companySupervision=").append(companySupervision);
        sb.append(", dept=").append(dept);
        sb.append(", deptAll=").append(deptAll);
        sb.append(", directLeader=").append(directLeader);
        sb.append(", deptLeader=").append(deptLeader);
        sb.append(", supervisionLeader=").append(supervisionLeader);
        sb.append(", sql='").append(sql).append('\'');
        sb.append(", sqlRemarks='").append(sqlRemarks).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
