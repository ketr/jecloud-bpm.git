/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.je.bpm.core.model.config.sequenceflow.SequenceFlowStyleConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * 普通顺序流
 * 顺序流是流程中两个元素间的连接器。当流程执行中，一个元素被访问后，会沿着所有的出口顺序流继续。
 * 这意味着BPMN 2.0的默认性质是并行的：两个出口顺序流，会创建两个独立的，并行的执行路径。
 * <p>
 * 条件顺序流
 * 在顺序流上可以定义条件。当离开BPMN 2.0活动时，默认行为是计算其出口顺序流上的条件。
 * 当条件计算为true时，选择该出口顺序流。如果该方法选择了多条顺序流，则会生成多个执行，流程会以并行方式继续。
 * <p>
 * 默认顺序流
 * 所有的BPMN 2.0任务与网关，都可以使用默认顺序流。这种顺序流只有当没有其他顺序流可以选择时，才会被选择为活动的出口顺序流。
 * 默认顺序流上的条件会被忽略。
 */
public class SequenceFlow extends FlowElement {

    /**
     * 条件表达式
     */
    protected String conditionExpression;
    /**
     * 源对象引用
     */
    protected String sourceRef;
    /**
     * 目标对象引用
     */
    protected String targetRef;
    /**
     * 跳过表达式
     */
    protected String skipExpression;

    /**
     * 样式
     */
    protected SequenceFlowStyleConfig styleConfig = new SequenceFlowStyleConfig();

    /**
     * Actual flow elements that match the source and target ref
     * Set during process definition parsing
     * 源流程元素
     * 此元素对应sourceRef引用，是真实对象，只有在流程定义解析的时候会设置此值
     */
    @JsonIgnore
    protected FlowElement sourceFlowElement;

    /**
     * Actual flow elements that match the source and target ref
     * Set during process definition parsing
     * 目标流程元素
     * 此元素对应targetRef引用，是真实对象，只有在流程定义解析的时候会设置此值
     */
    @JsonIgnore
    protected FlowElement targetFlowElement;

    /**
     * Graphical information: a list of waypoints: x1, y1, x2, y2, x3, y3, ..
     * <p>
     * Added during parsing of a process definition.
     * 图形信息：一些基准点集合，只有在流程定义解析的时候会设置
     */
    protected List<Integer> waypoints = new ArrayList<Integer>();

    public SequenceFlow() {

    }

    public SequenceFlow(String sourceRef, String targetRef) {
        this.sourceRef = sourceRef;
        this.targetRef = targetRef;
    }

    public String getConditionExpression() {
        return conditionExpression;
    }

    public void setConditionExpression(String conditionExpression) {
        this.conditionExpression = conditionExpression;
    }

    public String getSourceRef() {
        return sourceRef;
    }

    public void setSourceRef(String sourceRef) {
        this.sourceRef = sourceRef;
    }

    public String getTargetRef() {
        return targetRef;
    }

    public void setTargetRef(String targetRef) {
        this.targetRef = targetRef;
    }

    public String getSkipExpression() {
        return skipExpression;
    }

    public void setSkipExpression(String skipExpression) {
        this.skipExpression = skipExpression;
    }

    public FlowElement getSourceFlowElement() {
        return sourceFlowElement;
    }

    public void setSourceFlowElement(FlowElement sourceFlowElement) {
        this.sourceFlowElement = sourceFlowElement;
    }

    public FlowElement getTargetFlowElement() {
        return targetFlowElement;
    }

    public void setTargetFlowElement(FlowElement targetFlowElement) {
        this.targetFlowElement = targetFlowElement;
    }

    public List<Integer> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<Integer> waypoints) {
        this.waypoints = waypoints;
    }

    public SequenceFlowStyleConfig getStyleConfig() {
        return styleConfig;
    }

    public void setStyleConfig(SequenceFlowStyleConfig styleConfig) {
        this.styleConfig = styleConfig;
    }

    @Override
    public String toString() {
        return sourceRef + " --> " + targetRef;
    }

    @Override
    public SequenceFlow clone() {
        SequenceFlow clone = new SequenceFlow();
        clone.setValues(this);
        return clone;
    }

    public void setValues(SequenceFlow otherFlow) {
        super.setValues(otherFlow);
        setConditionExpression(otherFlow.getConditionExpression());
        setSourceRef(otherFlow.getSourceRef());
        setTargetRef(otherFlow.getTargetRef());
        setSkipExpression(otherFlow.getSkipExpression());
    }
}
