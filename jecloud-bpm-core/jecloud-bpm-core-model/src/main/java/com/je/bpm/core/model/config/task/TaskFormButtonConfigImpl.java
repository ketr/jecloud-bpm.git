/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task;

import com.je.bpm.core.model.BaseElement;

import java.util.ArrayList;
import java.util.List;

/**
 * 表单按钮配置
 */
public class TaskFormButtonConfigImpl extends AbstractTaskConfigImpl {

    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "taskFormButton";

    private List<ButtonConfig> buttonConfigs = new ArrayList<>();

    public TaskFormButtonConfigImpl() {
        super(CONFIG_TYPE);
    }

    public void add(ButtonConfig buttonConfig) {
        buttonConfigs.add(buttonConfig);
    }

    public void remove(ButtonConfig buttonConfig) {
        buttonConfigs.remove(buttonConfig);
    }

    public List<ButtonConfig> getButtonConfigs() {
        return buttonConfigs;
    }

    public void setButtonConfigs(List<ButtonConfig> buttonConfigs) {
        this.buttonConfigs = buttonConfigs;
    }

    public static class ButtonConfig {

        /**
         * 按钮名称
         */
        private String name;
        /**
         * 按钮编码
         */
        private String code;
        /**
         * 是否可用
         */
        private Boolean enable;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Boolean getEnable() {
            return enable;
        }

        public void setEnable(Boolean enable) {
            this.enable = enable;
        }

    }

    @Override
    public BaseElement clone() {
        TaskFormButtonConfigImpl taskFormButtonConfig = new TaskFormButtonConfigImpl();
        List<ButtonConfig> cloneButtonConfigs = new ArrayList<>();
        ButtonConfig buttonConfig;
        for (ButtonConfig each : getButtonConfigs()) {
            buttonConfig = new ButtonConfig();
            buttonConfig.setCode(each.getCode());
            buttonConfig.setName(each.getName());
            buttonConfig.setEnable(each.getEnable());
            taskFormButtonConfig.add(each);
        }
        return taskFormButtonConfig;
    }
}
