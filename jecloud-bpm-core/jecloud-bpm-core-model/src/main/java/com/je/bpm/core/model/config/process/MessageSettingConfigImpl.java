/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.process;

import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.config.ProcessRemindTemplateTypeEnum;
import com.je.bpm.core.model.config.ProcessRemindTypeEnum;

import java.util.ArrayList;
import java.util.List;

public class MessageSettingConfigImpl extends AbstractProcessConfig {
    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "messageSetting";

    /**
     * 推送模板
     */
    private List<ProcessRemindTemplate> messageDefinitions = new ArrayList<>();

    /**
     * 流程提醒配置，默认为推送
     */
    private List<ProcessRemindTypeEnum> messages = ProcessRemindTypeEnum.getDefaultRemindTypes();

    private String jeCloudDingTalkId;
    private String jeCloudDingTalkName;


    public MessageSettingConfigImpl() {
        super(CONFIG_TYPE);
    }

    public String getConfigType() {
        return CONFIG_TYPE;
    }

    public List<ProcessRemindTemplate> getMessageDefinitions() {
        return messageDefinitions;
    }

    public ProcessRemindTemplate getMessageUpcomingDefinitions() {
        for (ProcessRemindTemplate processRemindTemplate : this.messageDefinitions) {
            if (processRemindTemplate.getType().equals(ProcessRemindTemplateTypeEnum.FLOWBACKLOG)) {
                return processRemindTemplate;
            }
        }
        return null;
    }

    public void addProcessRemindTemplate(ProcessRemindTemplate processRemindTemplate) {
        this.messageDefinitions.add(processRemindTemplate);
    }

    public void setMessageDefinitions(List<ProcessRemindTemplate> messageDefinitions) {
        this.messageDefinitions = messageDefinitions;
    }

    public List<ProcessRemindTypeEnum> getMessages() {
        return messages;
    }

    public void addProcessRemindTypeEnum(String remindType) {
        this.messages.add(ProcessRemindTypeEnum.getType(remindType));
    }

    public void setMessages(List<ProcessRemindTypeEnum> messages) {
        this.messages = messages;
    }

    public String getJeCloudDingTalkId() {
        return jeCloudDingTalkId;
    }

    public void setJeCloudDingTalkId(String jeCloudDingTalkId) {
        this.jeCloudDingTalkId = jeCloudDingTalkId;
    }

    public String getJeCloudDingTalkName() {
        return jeCloudDingTalkName;
    }

    public void setJeCloudDingTalkName(String jeCloudDingTalkName) {
        this.jeCloudDingTalkName = jeCloudDingTalkName;
    }

    @Override
    public BaseElement clone() {
        return null;
    }
}
