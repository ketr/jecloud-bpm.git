/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.task;

import com.je.bpm.core.model.button.*;
import com.je.bpm.core.model.config.ButtonsConfig;
import com.je.bpm.core.model.config.CustomEvent;
import com.je.bpm.core.model.config.task.*;
import com.je.bpm.core.model.config.task.assignment.TaskAssigneeConfigImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * 抽象基础用户任务
 */
public abstract class KaiteBaseUserTask extends KaiteTask {

    /**
     * 指派人，单人
     */
    private String assignee;
    /**
     * 拥有人
     */
    private String owner;
    /**
     * 优先级
     */
    private String priority;
    /**
     * 到期时间
     */
    private String dueDate;
    /**
     * 提交分解
     */
    private List<TaskCommitBreakdownButton> commitBreakdownButtonList = new ArrayList<>();
    /**
     * 驳回配置
     */
    private TaskDismissConfigImpl taskDismissConfig = new TaskDismissConfigImpl();
    /**
     * 预警与延期
     */
    private TaskEarlyWarningAndPostponementConfigImpl taskEarlyWarningAndPostponementConfig = new TaskEarlyWarningAndPostponementConfigImpl();
    /**
     * 任务传阅配置
     */
    private TaskPassRoundConfigImpl taskPassRoundConfig = new TaskPassRoundConfigImpl();
    /**
     * 流程按钮
     */
    private ButtonsConfig<TaskButton> buttons = buildUserTaskButton(new ButtonsConfig());
    /**
     * 事件配置
     */
    protected List<CustomEvent> customEventListeners = new ArrayList<>();
    /**
     * 加签
     */
    protected AddSignatureConfigImpl addSignatureConfig = new AddSignatureConfigImpl();
    /**
     * 审批告知
     */
    protected TaskApprovalNoticeConfigImpl taskApprovalNoticeConfig = new TaskApprovalNoticeConfigImpl();
    /**
     * 随机配置
     */
    protected TaskRandomConfigImpl taskRandomConfig = new TaskRandomConfigImpl();
    /**
     * 表单基本配置
     */
    private TaskFormBasicConfigImpl taskFormBasicConfig = new TaskFormBasicConfigImpl();

    public TaskDismissConfigImpl getTaskDismissConfig() {
        return taskDismissConfig;
    }

    public ButtonsConfig buildUserTaskButton(ButtonsConfig<TaskButton> baseUserTask) {
        return baseUserTask.addButton(new TaskDelegateButton())
                .addButton(new TaskCancelDelegateButton()).addButton(new TaskSubmitButton())
                .addButton(new TaskDirectSendButton()).addButton(new TaskDismissButton())
                .addButton(new TaskRetrieveButton()).addButton(new TaskTransferButton())
                .addButton(new TaskGobackButton()).addButton(new TaskPassroundButton())
                .addButton(new TaskPassroundReadButton()).addButton(new TaskChangeAssigneeButton())
                .addButton(new TaskUrgeButton()).addButton(new TaskAbstainButton())
                .addButton(new TaskPassButton()).addButton(new TaskVetoButton())
                .addButton(new TaskCountersignedRebookReductionButton()).addButton(new TaskCountersignedAddSignatureButton())
                .addButton(new TaskReceiveButton())
                .addButton(new TaskCountersignButton())
                .addButton(new TaskSignBackButton())
                .addButton(new TaskStagingButton())
                .addButton(new TaskAddSignatureNodeButton())
                .addButton(new TaskDelSignatureNodeButton())
                .addButton(new TaskWithdrawButton())
                ;
    }

    public abstract String getType();

    public void setTaskDismissConfig(TaskDismissConfigImpl taskDismissConfig) {
        this.taskDismissConfig = taskDismissConfig;
    }

    public TaskPassRoundConfigImpl getTaskPassRoundConfig() {
        return taskPassRoundConfig;
    }

    public void setTaskPassRoundConfig(TaskPassRoundConfigImpl taskPassRoundConfig) {
        this.taskPassRoundConfig = taskPassRoundConfig;
    }

    public TaskFormBasicConfigImpl getTaskFormBasicConfig() {
        return taskFormBasicConfig;
    }

    public void setTaskFormBasicConfig(TaskFormBasicConfigImpl taskFormBasicConfig) {
        this.taskFormBasicConfig = taskFormBasicConfig;
    }

    public List<TaskCommitBreakdownButton> getCommitBreakdownButtonList() {
        return commitBreakdownButtonList;
    }

    public void setCommitBreakdownButtonList(List<TaskCommitBreakdownButton> commitBreakdownButtonList) {
        this.commitBreakdownButtonList = commitBreakdownButtonList;
    }

    public TaskEarlyWarningAndPostponementConfigImpl getTaskEarlyWarningAndPostponementConfig() {
        return taskEarlyWarningAndPostponementConfig;
    }

    public void setTaskEarlyWarningAndPostponementConfig(TaskEarlyWarningAndPostponementConfigImpl taskEarlyWarningAndPostponementConfig) {
        this.taskEarlyWarningAndPostponementConfig = taskEarlyWarningAndPostponementConfig;
    }

    public List<CustomEvent> getCustomEventListeners() {
        return customEventListeners;
    }

    public void setCustomEventListeners(List<CustomEvent> customEventListeners) {
        this.customEventListeners = customEventListeners;
    }

    public AddSignatureConfigImpl getAddSignatureConfig() {
        return addSignatureConfig;
    }

    public void setAddSignatureConfig(AddSignatureConfigImpl addSignatureConfig) {
        this.addSignatureConfig = addSignatureConfig;
    }

    public TaskApprovalNoticeConfigImpl getTaskApprovalNoticeConfig() {
        return taskApprovalNoticeConfig;
    }

    public void setTaskApprovalNoticeConfig(TaskApprovalNoticeConfigImpl taskApprovalNoticeConfig) {
        this.taskApprovalNoticeConfig = taskApprovalNoticeConfig;
    }

    public TaskRandomConfigImpl getTaskRandomConfig() {
        return taskRandomConfig;
    }

    public void setTaskRandomConfig(TaskRandomConfigImpl taskRandomConfig) {
        this.taskRandomConfig = taskRandomConfig;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public ButtonsConfig<TaskButton> getButtons() {
        return buttons;
    }

    public void setButtons(ButtonsConfig<TaskButton> buttons) {
        this.buttons = buttons;
    }

    public ButtonsConfig addButton(TaskButton taskButton) {
        return this.addButton(taskButton);
    }

    protected abstract void setTaskAssigneeConfig(TaskAssigneeConfigImpl taskAssigneeConfig);

    public void setValues(KaiteBaseUserTask otherElement) {
        super.setValues(otherElement);
        setCategory(otherElement.getCategory());
        setExtensionId(otherElement.getExtensionId());
        setTaskDismissConfig(otherElement.getTaskDismissConfig());
        setTaskFormBasicConfig(otherElement.getTaskFormBasicConfig());
        setTaskListenerConfig(otherElement.getTaskListenerConfig());
        setTaskPassRoundConfig(otherElement.getTaskPassRoundConfig());
        setButtons(otherElement.getButtons());
    }

    public abstract TaskAssigneeConfigImpl getTaskAssigneeConfig();
}
