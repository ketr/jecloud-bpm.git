/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model;

/**
 * 扩展属性
 */
public class ExtensionAttribute {

    /**
     * 属性名称
     */
    protected String name;
    /**
     * 属性值
     */
    protected String value;
    /**
     * 命名空间前缀
     */
    protected String namespacePrefix;
    /**
     * 命名空间
     */
    protected String namespace;

    public ExtensionAttribute() { }

    public ExtensionAttribute(String name) {
        this.name = name;
    }

    public ExtensionAttribute(String namespace, String name) {
        this.namespace = namespace;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getNamespacePrefix() {
        return namespacePrefix;
    }

    public void setNamespacePrefix(String namespacePrefix) {
        this.namespacePrefix = namespacePrefix;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (namespacePrefix != null) {
            sb.append(namespacePrefix);
            if (name != null) {
                sb.append(":").append(name);
            }
        } else {
            sb.append(name);
        }
        if (value != null) {
            sb.append("=").append(value);
        }
        return sb.toString();
    }

    @Override
    public ExtensionAttribute clone() {
        ExtensionAttribute clone = new ExtensionAttribute();
        clone.setValues(this);
        return clone;
    }

    public void setValues(ExtensionAttribute otherAttribute) {
        setName(otherAttribute.getName());
        setValue(otherAttribute.getValue());
        setNamespacePrefix(otherAttribute.getNamespacePrefix());
        setNamespace(otherAttribute.getNamespace());
    }
}
