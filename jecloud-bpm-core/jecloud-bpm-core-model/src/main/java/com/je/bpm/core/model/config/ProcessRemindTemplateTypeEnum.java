/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config;

import java.util.ArrayList;
import java.util.List;

/**
 * 流程提醒模板枚举
 */
public enum ProcessRemindTemplateTypeEnum {

    FLOWBACKLOG("首页待办", "flowBacklog"),
    TITLE("标题", "title"),
    DWR("首页模板", "dwr"),
    EMAIL("邮件", "email"),
    NOTE("短信", "note"),
    DINGTALK("钉钉", "dingTalk"),
    WECHAT("企业微信", "weChat"),
    WEWORK("企业微信", "wework"),
    CANURGED("催办", "canUrged"),
    THIRDPARTY("三方", "thirdparty");

    private String name;

    private String type;

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    ProcessRemindTemplateTypeEnum(String name, String type) {
        this.name = name;
        this.type = type;
    }

    /**
     * 获取默认提醒类型
     *
     * @return
     */
    public static List<ProcessRemindTemplateTypeEnum> getDefaultRemindTypes() {
        List<ProcessRemindTemplateTypeEnum> remindTypeEnumList = new ArrayList<>();
        return remindTypeEnumList;
    }

    /**
     * 根据类型获取
     *
     * @param type
     * @return
     */
    public static ProcessRemindTemplateTypeEnum getType(String type) {
        if (FLOWBACKLOG.getType().equalsIgnoreCase(type)) {
            return FLOWBACKLOG;
        } else if (TITLE.getType().equalsIgnoreCase(type)) {
            return TITLE;
        } else if (DWR.getType().equalsIgnoreCase(type)) {
            return DWR;
        } else if (EMAIL.getType().equalsIgnoreCase(type)) {
            return EMAIL;
        } else if (NOTE.getType().equalsIgnoreCase(type)) {
            return NOTE;
        } else if (THIRDPARTY.getType().equalsIgnoreCase(type)) {
            return THIRDPARTY;
        } else if (DINGTALK.getType().equalsIgnoreCase(type)) {
            return DINGTALK;
        } else if (CANURGED.getType().equalsIgnoreCase(type)) {
            return CANURGED;
        } else if (WECHAT.getType().equalsIgnoreCase(type)) {
            return WECHAT;
        } else if (WEWORK.getType().equalsIgnoreCase(type)) {//做兼容
            return WECHAT;
        }
        return null;
    }
}
