/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model;

/**
 * 信号事件定义
 * <p>
 *     信号事件，是引用具名信号的事件。信号是全局范围（广播）的事件，并会被传递给所有激活的处理器（等待中的流程实例/捕获信号事件 catching signal events）。
 *     信号事件定义使用signalEventDefinition元素声明。其signalRef属性引用一个signal元素，该signal元素需要声明为definitions根元素的子元素。下面摘录一个流程，使用中间事件（intermediate event）抛出与捕获信号事件。
 * </p>
 */
public class SignalEventDefinition extends EventDefinition {

    protected String signalRef;
    protected String signalExpression;
    protected boolean async;

    public String getSignalRef() {
        return signalRef;
    }

    public void setSignalRef(String signalRef) {
        this.signalRef = signalRef;
    }

    public String getSignalExpression() {
        return signalExpression;
    }

    public void setSignalExpression(String signalExpression) {
        this.signalExpression = signalExpression;
    }

    public boolean isAsync() {
        return async;
    }

    public void setAsync(boolean async) {
        this.async = async;
    }

    @Override
    public SignalEventDefinition clone() {
        SignalEventDefinition clone = new SignalEventDefinition();
        clone.setValues(this);
        return clone;
    }

    public void setValues(SignalEventDefinition otherDefinition) {
        super.setValues(otherDefinition);
        setSignalRef(otherDefinition.getSignalRef());
        setSignalExpression(otherDefinition.getSignalExpression());
        setAsync(otherDefinition.isAsync());
    }
}
