/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model;

import org.apache.commons.lang3.StringUtils;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 扩展元素
 */
public class ExtensionElement extends BaseElement {

    /**
     * 扩展元素名称
     */
    protected String name;
    /**
     * 扩展元素前缀
     */
    protected String namespacePrefix;
    /**
     * 扩展元素命名空间
     */
    protected String namespace;
    /**
     * 元素文本内容
     */
    protected String elementText;
    /**
     * 子扩展元素集合
     */
    protected Map<String, List<ExtensionElement>> childElements = new LinkedHashMap<String, List<ExtensionElement>>();

    public String getElementText() {
        return elementText;
    }

    public void setElementText(String elementText) {
        this.elementText = elementText;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamespacePrefix() {
        return namespacePrefix;
    }

    public void setNamespacePrefix(String namespacePrefix) {
        this.namespacePrefix = namespacePrefix;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public Map<String, List<ExtensionElement>> getChildElements() {
        return childElements;
    }

    public void addChildElement(ExtensionElement childElement) {
        if (childElement != null && StringUtils.isNotEmpty(childElement.getName())) {
            List<ExtensionElement> elementList = null;
            if (!this.childElements.containsKey(childElement.getName())) {
                elementList = new ArrayList<ExtensionElement>();
                this.childElements.put(childElement.getName(), elementList);
            }
            this.childElements.get(childElement.getName()).add(childElement);
        }
    }

    public void setChildElements(Map<String, List<ExtensionElement>> childElements) {
        this.childElements = childElements;
    }

    @Override
    public ExtensionElement clone() {
        ExtensionElement clone = new ExtensionElement();
        clone.setValues(this);
        return clone;
    }

    public void setValues(ExtensionElement otherElement) {
        setName(otherElement.getName());
        setNamespacePrefix(otherElement.getNamespacePrefix());
        setNamespace(otherElement.getNamespace());
        setElementText(otherElement.getElementText());
        setAttributes(otherElement.getAttributes());

        childElements = new LinkedHashMap<String, List<ExtensionElement>>();
        if (otherElement.getChildElements() != null && !otherElement.getChildElements().isEmpty()) {
            for (String key : otherElement.getChildElements().keySet()) {
                List<ExtensionElement> otherElementList = otherElement.getChildElements().get(key);
                if (otherElementList != null && !otherElementList.isEmpty()) {
                    List<ExtensionElement> elementList = new ArrayList<ExtensionElement>();
                    for (ExtensionElement extensionElement : otherElementList) {
                        elementList.add(extensionElement.clone());
                    }
                    childElements.put(key, elementList);
                }
            }
        }
    }
}
