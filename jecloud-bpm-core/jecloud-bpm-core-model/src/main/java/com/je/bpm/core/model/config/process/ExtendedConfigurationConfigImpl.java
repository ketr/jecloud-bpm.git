/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.process;

import com.je.bpm.core.model.BaseElement;

/**
 * 拓展配置
 */
public class ExtendedConfigurationConfigImpl extends AbstractProcessConfig {

    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "extendedConfiguration";

    public ExtendedConfigurationConfigImpl() {
        super(CONFIG_TYPE);
    }

    public String getCustomeEventType() {
        return CONFIG_TYPE;
    }

    /**
     * 是否可发起，起效后，流程启动会流转到第二个节点
     */
    private boolean canSponsor = false;
    /**
     * 是否可取回
     */
    private boolean canRetrieve = false;
    /**
     * 是否可退回
     */
    private boolean canReturn = false;
    /**
     * 是否可催办
     */
    private boolean canUrged = false;
    /**
     * 是否可撤销
     */
    private boolean canCancel = false;
    /**
     * 可作废
     */
    private boolean canInvalid = false;
    /**
     * 是否可转办
     */
    private boolean canTransfer = false;
    /**
     * 是否可委托
     */
    private boolean canDelegate = false;
    /**
     * 加签节点
     */
    private boolean countersignNode = false;
    /**
     * 简易发起
     */
    private boolean easyLaunch = false;
    /**
     * 简易审批
     */
    private boolean simpleApproval = false;
    /**
     * 是否开启简易审批意见
     */
    private boolean enableSimpleComments = false;
    /**
     * 紧急状态
     */
    private boolean exigency = false;
    /**
     * 隐藏状态
     */
    private boolean hideStateInfo = false;
    /**
     * 审批耗时时间
     */
    private boolean hideTimeInfo = false;


    public boolean isCanSponsor() {
        return canSponsor;
    }

    public void setCanSponsor(boolean canSponsor) {
        this.canSponsor = canSponsor;
    }

    public boolean isCanRetrieve() {
        return canRetrieve;
    }

    public void setCanRetrieve(boolean canRetrieve) {
        this.canRetrieve = canRetrieve;
    }

    public boolean isCanReturn() {
        return canReturn;
    }

    public void setCanReturn(boolean canReturn) {
        this.canReturn = canReturn;
    }

    public boolean isCanUrged() {
        return canUrged;
    }

    public void setCanUrged(boolean canUrged) {
        this.canUrged = canUrged;
    }

    public boolean isCanCancel() {
        return canCancel;
    }

    public void setCanCancel(boolean canCancel) {
        this.canCancel = canCancel;
    }

    public boolean isCanInvalid() {
        return canInvalid;
    }

    public void setCanInvalid(boolean canInvalid) {
        this.canInvalid = canInvalid;
    }

    public boolean isCanTransfer() {
        return canTransfer;
    }

    public void setCanTransfer(boolean canTransfer) {
        this.canTransfer = canTransfer;
    }

    public boolean isCanDelegate() {
        return canDelegate;
    }

    public void setCanDelegate(boolean canDelegate) {
        this.canDelegate = canDelegate;
    }

    public boolean isEasyLaunch() {
        return easyLaunch;
    }

    public void setEasyLaunch(boolean easyLaunch) {
        this.easyLaunch = easyLaunch;
    }

    public boolean isSimpleApproval() {
        return simpleApproval;
    }

    public void setSimpleApproval(boolean simpleApproval) {
        this.simpleApproval = simpleApproval;
    }

    public boolean isHideStateInfo() {
        return hideStateInfo;
    }

    public void setHideStateInfo(boolean hideStateInfo) {
        this.hideStateInfo = hideStateInfo;
    }

    public boolean isHideTimeInfo() {
        return hideTimeInfo;
    }

    public void setHideTimeInfo(boolean hideTimeInfo) {
        this.hideTimeInfo = hideTimeInfo;
    }

    public boolean isCountersignNode() {
        return countersignNode;
    }

    public void setCountersignNode(boolean countersignNode) {
        this.countersignNode = countersignNode;
    }

    public boolean isExigency() {
        return exigency;
    }

    public void setExigency(boolean exigency) {
        this.exigency = exigency;
    }

    public boolean isEnableSimpleComments() {
        return enableSimpleComments;
    }

    public void setEnableSimpleComments(boolean enableSimpleComments) {
        this.enableSimpleComments = enableSimpleComments;
    }

    @Override
    public BaseElement clone() {

        return null;
    }
}
