/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config;

import java.util.ArrayList;
import java.util.List;

/**
 * 流程提醒枚举
 */
public enum ProcessRemindTypeEnum {

    WEB("推送"),
    EMAIL("邮件"),
    NOTE("短信"),
    WECHAT("企业微信"),
    DINGTALK("钉钉"),
    FLYBOOK("飞书"),
    WELINK("WELINK"),
    CUSTOM("自定义"),
    ALL("所有");

    private String name;

    public String getName() {
        return name;
    }

    ProcessRemindTypeEnum(String name) {
        this.name = name;
    }

    /**
     * 获取默认提醒类型
     *
     * @return
     */
    public static List<ProcessRemindTypeEnum> getDefaultRemindTypes() {
        List<ProcessRemindTypeEnum> remindTypeEnumList = new ArrayList<>();
        return remindTypeEnumList;
    }

    /**
     * 根据类型获取
     *
     * @param type
     * @return
     */
    public static ProcessRemindTypeEnum getType(String type) {
        if (WEB.toString().equalsIgnoreCase(type)) {
            return WEB;
        } else if (EMAIL.toString().equalsIgnoreCase(type)) {
            return EMAIL;
        } else if (NOTE.toString().equalsIgnoreCase(type)) {
            return NOTE;
        } else if (WECHAT.toString().equalsIgnoreCase(type)) {
            return WECHAT;
        } else if (DINGTALK.toString().equalsIgnoreCase(type)) {
            return DINGTALK;
        } else if (FLYBOOK.toString().equalsIgnoreCase(type)) {
            return FLYBOOK;
        } else if (WELINK.toString().equalsIgnoreCase(type)) {
            return WELINK;
        } else if (CUSTOM.toString().equalsIgnoreCase(type)) {
            return WEB;
        } else if (ALL.toString().equalsIgnoreCase(type)) {
            return ALL;
        }
        return null;
    }
}
