/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task;

import com.je.bpm.core.model.BaseElement;

/**
 * 随机配置
 */
public class TaskRandomConfigImpl extends AbstractTaskConfigImpl {

    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "TaskRandomConfig";

    /**
     * 轮询派发
     */
    private boolean distributedPolling = false;
    /**
     * 随机派发
     */
    private boolean distributeRandom = false;

    private String teamName;

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public TaskRandomConfigImpl() {
        super(CONFIG_TYPE);
    }

    public  String getDistributeRandom() {
        return CONFIG_TYPE;
    }

    public boolean isDistributedPolling() {
        return distributedPolling;
    }

    public void setDistributedPolling(boolean distributedPolling) {
        this.distributedPolling = distributedPolling;
    }

    public boolean isDistributeRandom() {
        return distributeRandom;
    }

    public void setDistributeRandom(boolean distributeRandom) {
        this.distributeRandom = distributeRandom;
    }

    @Override
    public BaseElement clone() {
        TaskRandomConfigImpl taskRemindConfig = new TaskRandomConfigImpl();
        return taskRemindConfig;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TaskRandomConfigImpl{");
        sb.append("distributedPolling=").append(distributedPolling);
        sb.append(", distributeRandom=").append(distributeRandom);
        sb.append('}');
        return sb.toString();
    }
}
