/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.task;

import com.je.bpm.core.model.FieldExtension;

import java.util.ArrayList;

public class SendTask extends TaskWithFieldExtensions {

  protected String type;
  protected String implementationType;
  protected String operationRef;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getImplementationType() {
    return implementationType;
  }

  public void setImplementationType(String implementationType) {
    this.implementationType = implementationType;
  }

  public String getOperationRef() {
    return operationRef;
  }

  public void setOperationRef(String operationRef) {
    this.operationRef = operationRef;
  }

  @Override
  public SendTask clone() {
    SendTask clone = new SendTask();
    clone.setValues(this);
    return clone;
  }

  public void setValues(SendTask otherElement) {
    super.setValues(otherElement);
    setType(otherElement.getType());
    setImplementationType(otherElement.getImplementationType());
    setOperationRef(otherElement.getOperationRef());

    fieldExtensions = new ArrayList<FieldExtension>();
    if (otherElement.getFieldExtensions() != null && !otherElement.getFieldExtensions().isEmpty()) {
      for (FieldExtension extension : otherElement.getFieldExtensions()) {
        fieldExtensions.add(extension.clone());
      }
    }
  }
}
