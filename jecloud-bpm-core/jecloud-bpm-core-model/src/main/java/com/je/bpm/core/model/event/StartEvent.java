/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.event;

import com.je.bpm.core.model.FormProperty;
import com.je.bpm.core.model.ReferenceOverrider;

import java.util.ArrayList;
import java.util.List;

/**
 * 启动事件
 * <p>
 *     消息事件，是指引用具名消息的事件。消息具有名字与载荷。与信号不同，消息事件只有一个接收者。
 *     消息事件定义使用messageEventDefinition元素声明。其messageRef属性引用一个message元素，该message元素需要声明为definitions根元素的子元素。
 *     启动事件主要包括：
 *     1. “空”启动事件，技术上指的是没有特别指定启动流程实例的触发器。这意味着引擎无法预知何时启动流程实例。空启动事件用于流程实例通过调用下列startProcessInstanceByXXX API方法启动的情况。
 *     2. 定时器启动事件，用于在指定时间创建流程实例。在流程只需要启动一次，或者流程需要在特定的时间间隔重复启动时，都可以使用。
 *     3. 消息启动事件，使用具名消息启动流程实例。它让我们可以使用消息名，有效地在一组可选的启动事件中选择正确的启动事件。
 *     4. 信号启动事件，使用具名信号启动流程实例。这个信号可以由流程实例中的信号抛出中间事件（intermediary signal throw event），或者API（runtimeService.signalEventReceivedXXX方法）触发。这些情况下，所有拥有相同名字信号启动事件的流程定义都会被启动。
 *      请注意这些情况下，都可以选择异步还是同步启动流程实例。
 *     5. 错误启动事件，可用于触发事件子流程（Event Sub-Process）。错误启动事件不能用于启动流程实例。
 * </p>
 */
public class StartEvent extends Event {

    /**
     * 启动人
     */
    protected String initiator;
    /**
     * 表单键
     */
    protected String formKey;
    /**
     *
     */
    protected boolean isInterrupting;
    /**
     * 表单属性
     */
    protected List<FormProperty> formProperties = new ArrayList<FormProperty>();

    public String getInitiator() {
        return initiator;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    public String getFormKey() {
        return formKey;
    }

    public void setFormKey(String formKey) {
        this.formKey = formKey;
    }

    public boolean isInterrupting() {
        return isInterrupting;
    }

    public void setInterrupting(boolean isInterrupting) {
        this.isInterrupting = isInterrupting;
    }

    public List<FormProperty> getFormProperties() {
        return formProperties;
    }

    public void setFormProperties(List<FormProperty> formProperties) {
        this.formProperties = formProperties;
    }

    @Override
    public StartEvent clone() {
        StartEvent clone = new StartEvent();
        clone.setValues(this);
        return clone;
    }

    public void setValues(StartEvent otherEvent) {
        super.setValues(otherEvent);
        setInitiator(otherEvent.getInitiator());
        setFormKey(otherEvent.getFormKey());
        setInterrupting(otherEvent.isInterrupting);

        formProperties = new ArrayList<FormProperty>();
        if (otherEvent.getFormProperties() != null && !otherEvent.getFormProperties().isEmpty()) {
            for (FormProperty property : otherEvent.getFormProperties()) {
                formProperties.add(property.clone());
            }
        }
    }

    @Override
    public void accept(ReferenceOverrider referenceOverrider) {
        referenceOverrider.override(this);
    }
}
