/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.event;

import com.je.bpm.core.model.config.task.TaskFormBasicConfigImpl;

/**
 * 结束事件
 * <p>
 *     结束事件标志着（子）流程的（分支的）结束。结束事件总是抛出（型）事件。这意味着当流程执行到达结束事件时，会抛出一个结果。
 *     结果的类型由事件内部的黑色图标描绘。在XML表示中，类型由子元素声明给出。
 *     结束事件主要包括：
 *     1. “空”结束事件，意味着当到达这个事件时，抛出的结果没有特别指定。因此，引擎除了结束当前执行分支之外，不会多做任何事情。
 *     2. 错误结束事件，当流程执行到达错误结束事件时，结束执行的当前分支，并抛出错误。这个错误可以使用匹配的错误边界中间事件 intermediate boundary error event 捕获。如果找不到匹配的错误边界事件，将会抛出异常。
 *     3. 终止结束事件，当到达终止结束事件时，当前的流程实例或子流程会被终止。概念上说，当执行到达终止结束事件时，会判断第一个范围 scope（流程或子流程）并终止它。请注意在BPMN 2.0中，子流程可以是嵌入式子流程，调用活动，事件子流程，或事务子流程。有一条通用规则：当存在多实例的调用过程或嵌入式子流程时，只会终止一个实例，其他的实例与流程实例不会受影响。
 *     4. 取消结束事件，只能与bpmn事务子流程（bpmn transaction subprocess）一起使用。当到达取消结束事件时，会抛出取消事件，且必须由取消边界事件（cancel boundary event）捕获。之后这个取消边界事件将取消事务，并触发补偿（compensation）。
 * </p>
 */
public class EndEvent extends Event {

    /**
     * 表单基本配置
     */
    private TaskFormBasicConfigImpl taskFormBasicConfig = new TaskFormBasicConfigImpl();

    @Override
    public EndEvent clone() {
        EndEvent clone = new EndEvent();
        clone.setValues(this);
        return clone;
    }

    public void setValues(EndEvent otherEvent) {
        super.setValues(otherEvent);
    }

    public TaskFormBasicConfigImpl getTaskFormBasicConfig() {
        return taskFormBasicConfig;
    }

    public void setTaskFormBasicConfig(TaskFormBasicConfigImpl taskFormBasicConfig) {
        this.taskFormBasicConfig = taskFormBasicConfig;
    }

}
