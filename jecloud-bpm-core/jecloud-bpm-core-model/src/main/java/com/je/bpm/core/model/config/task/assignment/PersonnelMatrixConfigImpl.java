/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task.assignment;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.bpm.core.model.BaseElement;

import java.util.ArrayList;
import java.util.List;

/**
 * 人员矩阵配置实现
 */
public class PersonnelMatrixConfigImpl extends BasicAssignmentConfigImpl {
    private static final String CONFIG_TYPE = "matrixConfig";

    private static final String CONFIG_TYPE_NAME = "人员矩阵";

    /**
     * 资源
     */
    private List<AssigneeResource> resource = new ArrayList<>();
    /**
     * 权限
     */
    private AssignmentPermission permission;
    /**
     * 矩阵配置
     */
    private Config config;

    public void addConditionalInfo(Config.ConditionalInfo conditionalInfo) {
        this.getConfig().getConditionalInfos().add(conditionalInfo);
    }

    public static class Config {
        /**
         * 矩阵名称
         */
        private String matrixName;
        /**
         * 矩阵id
         */
        private String matrixId;
        /**
         * 结果字段
         */
        private String resultField;
        /**
         * 结果字段ID
         */
        private String resultFieldId;

        private List<ConditionalInfo> conditionalInfos = new ArrayList<>();

        public static class ConditionalInfo {
            private String condition;
            private String conditionValue;
            private String valueType;

            public String getCondition() {
                return condition;
            }

            public void setCondition(String condition) {
                this.condition = condition;
            }

            public String getConditionValue() {
                return conditionValue;
            }

            public void setConditionValue(String conditionValue) {
                this.conditionValue = conditionValue;
            }

            public String getValueType() {
                return valueType;
            }

            public void setValueType(String valueType) {
                this.valueType = valueType;
            }
        }


        public String getMatrixName() {
            return matrixName;
        }

        public void setMatrixName(String matrixName) {
            this.matrixName = matrixName;
        }

        public String getMatrixId() {
            return matrixId;
        }

        public void setMatrixId(String matrixId) {
            this.matrixId = matrixId;
        }

        public String getResultField() {
            return resultField;
        }

        public void setResultField(String resultField) {
            this.resultField = resultField;
        }

        public String getResultFieldId() {
            return resultFieldId;
        }

        public void setResultFieldId(String resultFieldId) {
            this.resultFieldId = resultFieldId;
        }

        public List<ConditionalInfo> getConditionalInfos() {
            return conditionalInfos;
        }

        public void setConditionalInfos(List<ConditionalInfo> conditionalInfos) {
            this.conditionalInfos = conditionalInfos;
        }

        protected static List<ConditionalInfo> buildInfo(String gridData) {
            List<ConditionalInfo> list = new ArrayList<>();
            if (Strings.isNullOrEmpty(gridData)) {
                return list;
            }
            JSONArray jsonArray = JSONArray.parseArray(gridData);
            for (Object o : jsonArray) {
                JSONObject value = JSONObject.parseObject(o.toString());
                ConditionalInfo conditionalInfo = new ConditionalInfo();
                conditionalInfo.setCondition(value.getString("condition"));
                conditionalInfo.setConditionValue(value.getString("conditionValue"));
                conditionalInfo.setValueType(value.getString("valueType"));
                list.add(conditionalInfo);
            }

            return list;
        }
    }

    public static Config buildConfig(String matrixName, String matrixId, String resultField, String resultFieldId, String gridData) {
        Config config = new Config();
        config.setMatrixName(matrixName);
        config.setMatrixId(matrixId);
        config.setResultField(resultField);
        config.setResultFieldId(resultFieldId);
        config.setConditionalInfos(config.buildInfo(gridData));
        return config;
    }

    public void addResource(AssigneeResource assigneeResource) {
        resource.add(assigneeResource);
    }

    public PersonnelMatrixConfigImpl() {
        super(CONFIG_TYPE);
    }

    @Override
    public String getConfigType() {
        return CONFIG_TYPE;
    }

    @Override
    public String getConfigTypeName() {
        return CONFIG_TYPE_NAME;
    }

    public List<AssigneeResource> getResource() {
        return resource;
    }

    public void setResource(List<AssigneeResource> resource) {
        this.resource = resource;
    }

    public AssignmentPermission getPermission() {
        return permission;
    }

    public void setPermission(AssignmentPermission permission) {
        this.permission = permission;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    @Override
    public void addResource(List<AssigneeResource> list) {
        resource.addAll(list);
    }

    @Override
    public BaseElement clone() {
        PersonnelMatrixConfigImpl personnelMatrixConfigImpl = new PersonnelMatrixConfigImpl();
        return personnelMatrixConfigImpl;
    }

}
