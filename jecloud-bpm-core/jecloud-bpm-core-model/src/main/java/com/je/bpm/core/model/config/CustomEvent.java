/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config;

import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.config.process.AbstractProcessConfig;

/**
 * 事件配置
 */
public class CustomEvent extends AbstractProcessConfig {

    public CustomEvent() {
        super(CONFIG_TYPE);
    }

    @Override
    public BaseElement clone() {
        return null;
    }

    public enum CustomEventEnum {
        PROCESS_START("流程启动"),
        PROCESS_END("流程结束"),
        PROCESS_REVOKE("流程撤销"),
        PROCESS_VOID("流程作废"),
        TASK_SUBMIT_BEFORE("提交之前"),
        TASK_SUBMIT_AFTER("提交之后"),
        TASK_STRAIGHT_BEFORE("直接提交之前"),
        TASK_STRAIGHT_AFTER("直接提交之后"),
        TASK_RETURN_BEFORE("退回之前"),
        TASK_RETURN_AFTER("退回之后"),
        TASK_TURN_DOWN_BEFORE("驳回之前"),
        TASK_TURN_DOWN_AFTER("驳回之后"),
        TASK_TRUN_TO_DO_BEFORE("转办之前"),
        TASK_TRUN_TO_DO_AFTER("转办之后"),
        TASK_RETRIEVE_BEFORE("取回之前"),
        TASK_RETRIEVE_AFTER("取回之后"),
        TASK_ENTRUST_BEFORE("委托之前"),
        TASK_ENTRUST_AFTER("委托之后"),
        TASK_CIRCULATED_BEFORE("传阅之前"),
        TASK_CIRCULATED_AFTER("传阅之后"),
        TASK_ROUND_BEFORE("审阅之前"),
        TASK_ROUND_AFTER("审阅之后"),
        TASK_END_BEFORE("结束之前"),
        TASK_END_AFTER("结束之后"),
        TASK_RECEIVE("领取任务"),
        TASK_CHANGE_PERSON("切换负责人"),
        TASK_COUNTERSIGNED_PLUS_MINUS_SIGN("人员调整");

        private String name;

        public String getName() {
            return name;
        }

        CustomEventEnum(String name) {
            this.name = name;
        }


        public static CustomEventEnum getEventByCode(String type) {
            if (PROCESS_START.toString().equalsIgnoreCase(type)) {
                return PROCESS_START;
            } else if (PROCESS_END.toString().equalsIgnoreCase(type)) {
                return PROCESS_END;
            } else if (PROCESS_REVOKE.toString().equalsIgnoreCase(type)) {
                return PROCESS_REVOKE;
            } else if (PROCESS_VOID.toString().equalsIgnoreCase(type)) {
                return PROCESS_VOID;
            } else if (TASK_SUBMIT_BEFORE.toString().equalsIgnoreCase(type)) {
                return TASK_SUBMIT_BEFORE;
            } else if (TASK_SUBMIT_AFTER.toString().equalsIgnoreCase(type)) {
                return TASK_SUBMIT_AFTER;
            } else if (TASK_STRAIGHT_BEFORE.toString().equalsIgnoreCase(type)) {
                return TASK_STRAIGHT_BEFORE;
            } else if (TASK_STRAIGHT_AFTER.toString().equalsIgnoreCase(type)) {
                return TASK_STRAIGHT_AFTER;
            } else if (TASK_RETURN_BEFORE.toString().equalsIgnoreCase(type)) {
                return TASK_RETURN_BEFORE;
            } else if (TASK_RETURN_AFTER.toString().equalsIgnoreCase(type)) {
                return TASK_RETURN_AFTER;
            } else if (TASK_TURN_DOWN_BEFORE.toString().equalsIgnoreCase(type)) {
                return TASK_TURN_DOWN_BEFORE;
            } else if (TASK_TURN_DOWN_AFTER.toString().equalsIgnoreCase(type)) {
                return TASK_TURN_DOWN_AFTER;
            } else if (TASK_TRUN_TO_DO_BEFORE.toString().equalsIgnoreCase(type)) {
                return TASK_TRUN_TO_DO_BEFORE;
            } else if (TASK_TRUN_TO_DO_AFTER.toString().equalsIgnoreCase(type)) {
                return TASK_TRUN_TO_DO_AFTER;
            } else if (TASK_RETRIEVE_BEFORE.toString().equalsIgnoreCase(type)) {
                return TASK_RETRIEVE_BEFORE;
            } else if (TASK_RETRIEVE_AFTER.toString().equalsIgnoreCase(type)) {
                return TASK_RETRIEVE_AFTER;
            } else if (TASK_ENTRUST_BEFORE.toString().equalsIgnoreCase(type)) {
                return TASK_ENTRUST_BEFORE;
            } else if (TASK_ENTRUST_AFTER.toString().equalsIgnoreCase(type)) {
                return TASK_ENTRUST_AFTER;
            } else if (TASK_CIRCULATED_BEFORE.toString().equalsIgnoreCase(type)) {
                return TASK_CIRCULATED_BEFORE;
            } else if (TASK_CIRCULATED_AFTER.toString().equalsIgnoreCase(type)) {
                return TASK_CIRCULATED_AFTER;
            } else if (TASK_END_BEFORE.toString().equalsIgnoreCase(type)) {
                return TASK_END_BEFORE;
            } else if (TASK_END_AFTER.toString().equalsIgnoreCase(type)) {
                return TASK_END_AFTER;
            } else if (TASK_RECEIVE.toString().equalsIgnoreCase(type)) {
                return TASK_RECEIVE;
            } else if (TASK_CHANGE_PERSON.toString().equalsIgnoreCase(type)) {
                return TASK_CHANGE_PERSON;
            } else if (TASK_COUNTERSIGNED_PLUS_MINUS_SIGN.toString().equalsIgnoreCase(type)) {
                return TASK_COUNTERSIGNED_PLUS_MINUS_SIGN;
            } else if (TASK_ROUND_AFTER.toString().equalsIgnoreCase(type)) {
                return TASK_ROUND_AFTER;
            } else if (TASK_ROUND_BEFORE.toString().equalsIgnoreCase(type)) {
                return TASK_ROUND_BEFORE;
            }
            return null;
        }


    }

    public enum CustomExecutionStrategyEnum {
        FIELD_ASSIGNMENT("字段赋值"),
        EXECUTION_METHOD("执行方法"),
        EXECUTE_SQL_TEMPLATE("执行方法");
        private String name;

        public String getName() {
            return name;
        }

        CustomExecutionStrategyEnum(String name) {
            this.name = name;
        }

        public static CustomExecutionStrategyEnum getExecutionStrategyByCode(String type) {
            if (FIELD_ASSIGNMENT.toString().equalsIgnoreCase(type)) {
                return FIELD_ASSIGNMENT;
            } else if (EXECUTION_METHOD.toString().equalsIgnoreCase(type)) {
                return EXECUTION_METHOD;
            } else if (EXECUTE_SQL_TEMPLATE.toString().equalsIgnoreCase(type)) {
                return EXECUTE_SQL_TEMPLATE;
            }
            return null;
        }

    }

    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "eventListener";
    /**
     * 事件类型
     */
    private CustomEventEnum type;

    /**
     * 执行策略
     */
    private CustomExecutionStrategyEnum executionStrategy;

    /**
     * 赋值字段配置
     */
    private String assignmentFieldConfiguration;

    /**
     * 业务bean
     */
    private String serviceName;

    /**
     * 业务方法
     */
    private String method;

    /**
     * 是否存在参数
     */
    private Boolean existenceParameter;

    public String getConfigType() {
        return CONFIG_TYPE;
    }

    public CustomEventEnum getCustomeEventType() {
        return type;
    }

    public void setType(CustomEventEnum type) {
        this.type = type;
    }

    public String getAssignmentFieldConfiguration() {
        return assignmentFieldConfiguration;
    }

    public CustomExecutionStrategyEnum getExecutionStrategy() {
        return executionStrategy;
    }

    public void setExecutionStrategy(CustomExecutionStrategyEnum executionStrategy) {
        this.executionStrategy = executionStrategy;
    }

    public void setAssignmentFieldConfiguration(String assignmentFieldConfiguration) {
        this.assignmentFieldConfiguration = assignmentFieldConfiguration;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Boolean getExistenceParameter() {
        return existenceParameter;
    }

    public void setExistenceParameter(Boolean existenceParameter) {
        this.existenceParameter = existenceParameter;
    }

}
