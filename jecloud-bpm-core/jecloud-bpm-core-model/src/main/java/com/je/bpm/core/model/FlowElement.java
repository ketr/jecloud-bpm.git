/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.je.bpm.core.model.process.SubProcess;
import java.util.ArrayList;
import java.util.List;

/**
 * 流程元素抽象实现
 */
public abstract class FlowElement extends BaseElement implements HasExecutionListeners, AcceptUpdates {

    /**
     * 元素名称
     */
    protected String name;
    /**
     * 元素描述
     */
    protected String documentation;
    /**
     * 元素执行监听
     */
    protected List<ActivitiListener> executionListeners = new ArrayList<ActivitiListener>();
    /**
     * 父容器
     */
    protected FlowElementsContainer parentContainer;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocumentation() {
        return documentation;
    }

    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    @Override
    public List<ActivitiListener> getExecutionListeners() {
        return executionListeners;
    }

    @Override
    public void setExecutionListeners(List<ActivitiListener> executionListeners) {
        this.executionListeners = executionListeners;
    }

    @JsonIgnore
    public FlowElementsContainer getParentContainer() {
        return parentContainer;
    }

    @JsonIgnore
    public SubProcess getSubProcess() {
        SubProcess subProcess = null;
        if (parentContainer instanceof SubProcess) {
            subProcess = (SubProcess) parentContainer;
        }

        return subProcess;
    }

    public void setParentContainer(FlowElementsContainer parentContainer) {
        this.parentContainer = parentContainer;
    }

    @Override
    public abstract FlowElement clone();

    public void setValues(FlowElement otherElement) {
        super.setValues(otherElement);
        setName(otherElement.getName());
        setDocumentation(otherElement.getDocumentation());
        executionListeners = new ArrayList<ActivitiListener>();
        if (otherElement.getExecutionListeners() != null && !otherElement.getExecutionListeners().isEmpty()) {
            for (ActivitiListener listener : otherElement.getExecutionListeners()) {
                executionListeners.add(listener.clone());
            }
        }
    }
}
