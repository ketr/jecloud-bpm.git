/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task;

import com.je.bpm.core.model.BaseElement;

public class EarlyWarningAndPostponementSource extends AbstractTaskConfigImpl {

    private static final String CONFIG_TYPE = "earlyWarningStrategyResourceConfig";

    public EarlyWarningAndPostponementSource() {
        super(CONFIG_TYPE);
    }

    @Override
    public BaseElement clone() {
        return null;
    }


    public enum EarlyWarningAndPostponementSourceType {
        EARLY_WARNING_WORKING_DAY("预警(工作日)"),
        EARLY_WARNING("预警（自然日)"),
        POSTPONEMENT_WORKING_DAY("延期(工作日)"),
        POSTPONEMENT("延期(自然日)");

        private String name;

        public String getName() {
            return name;
        }

        EarlyWarningAndPostponementSourceType(String name) {
            this.name = name;
        }

        public static EarlyWarningAndPostponementSourceType getType(String type) {
            if (EARLY_WARNING.toString().equalsIgnoreCase(type)) {
                return EARLY_WARNING;
            } else if (POSTPONEMENT.toString().equalsIgnoreCase(type)) {
                return POSTPONEMENT;
            }
            return null;
        }

    }

    //执行类型枚举
    public enum ExecutionTypeEnum {
        AUTO_APPROVAL("autoApproval", "自动通过"),
        NOTIFICATION("notification", "发送通知"),
        EXECUTE_CUSTOM_METHOD("executeCustomMethod", "自定义方法");
        private String name;
        private String value;

        ExecutionTypeEnum(String value, String name) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public static ExecutionTypeEnum getType(String type) {
            if (AUTO_APPROVAL.getValue().equalsIgnoreCase(type)) {
                return AUTO_APPROVAL;
            } else if (NOTIFICATION.getValue().equalsIgnoreCase(type)) {
                return NOTIFICATION;
            } else if (EXECUTE_CUSTOM_METHOD.getValue().equalsIgnoreCase(type)) {
                return EXECUTE_CUSTOM_METHOD;
            }
            return null;
        }
    }

    private ExecutionTypeEnum executionType;

    /**
     * service
     */
    private String service;

    /**
     * 方法名
     */
    private String method;

    /**
     * 类型
     */
    private EarlyWarningAndPostponementSourceType sourceType;


    public EarlyWarningAndPostponementSourceType getSourceType() {
        return sourceType;
    }

    public void setSourceType(EarlyWarningAndPostponementSourceType sourceType) {
        this.sourceType = sourceType;
    }

    public ExecutionTypeEnum getExecutionType() {
        return executionType;
    }

    public void setExecutionType(ExecutionTypeEnum executionType) {
        this.executionType = executionType;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
