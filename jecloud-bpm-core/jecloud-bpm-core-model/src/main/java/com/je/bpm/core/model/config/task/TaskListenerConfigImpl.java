/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task;

import com.je.bpm.core.model.BaseElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 任务事件配置
 */
public class TaskListenerConfigImpl extends AbstractTaskConfigImpl {
    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "taskListenerConfig";
    /**
     * 监听配置
     */
    private List<ListenerConfig> listenerConfigs = new ArrayList<>();

    public List<ListenerConfig> getListenerConfigs() {
        return listenerConfigs;
    }

    public void setListenerConfigs(List<ListenerConfig> listenerConfigs) {
        this.listenerConfigs = listenerConfigs;
    }

    public TaskListenerConfigImpl() {
        super(CONFIG_TYPE);
    }

    public void add(ListenerConfig listenerConfig) {
        listenerConfigs.add(listenerConfig);
    }

    public void remove(ListenerConfig listenerConfig) {
        listenerConfigs.remove(listenerConfig);
    }

    public static class ListenerConfig {
        /**
         * 事件类型
         */
        private String type;
        /**
         * 策略
         */
        private String stategy;
        /**
         * 赋值字段
         */
        private String fieldSetValueStr;
        /**
         * bean名称
         */
        private String beanName;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getStategy() {
            return stategy;
        }

        public void setStategy(String stategy) {
            this.stategy = stategy;
        }

        public String getFieldSetValueStr() {
            return fieldSetValueStr;
        }

        public void setFieldSetValueStr(String fieldSetValueStr) {
            this.fieldSetValueStr = fieldSetValueStr;
        }

        public String getBeanName() {
            return beanName;
        }

        public void setBeanName(String beanName) {
            this.beanName = beanName;
        }
    }

    @Override
    public BaseElement clone() {
        TaskListenerConfigImpl taskListenerConfig = new TaskListenerConfigImpl();
        List<ListenerConfig> copyListenerConfigs = new ArrayList<>();
        ListenerConfig eachCopyListenerConfig = null;
        for (ListenerConfig eachConfig : getListenerConfigs()) {
            eachCopyListenerConfig = new ListenerConfig();
            eachCopyListenerConfig.setBeanName(eachConfig.getBeanName());
            eachCopyListenerConfig.setStategy(eachConfig.getStategy());
            eachCopyListenerConfig.setType(eachConfig.getType());
            eachCopyListenerConfig.setFieldSetValueStr(eachConfig.getFieldSetValueStr());
            copyListenerConfigs.add(eachCopyListenerConfig);
        }
        taskListenerConfig.setListenerConfigs(copyListenerConfigs);
        return taskListenerConfig;
    }
}
