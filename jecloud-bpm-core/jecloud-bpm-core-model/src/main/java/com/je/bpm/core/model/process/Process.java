/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.process;

import com.je.bpm.core.model.EventListener;
import com.je.bpm.core.model.*;
import com.je.bpm.core.model.artifact.Artifact;
import com.je.bpm.core.model.artifact.Association;
import com.je.bpm.core.model.button.*;
import com.je.bpm.core.model.config.ButtonsConfig;
import com.je.bpm.core.model.config.CustomEvent;
import com.je.bpm.core.model.config.process.ExtendedConfigurationConfigImpl;
import com.je.bpm.core.model.config.process.MessageSettingConfigImpl;
import com.je.bpm.core.model.config.process.ProcessBasicConfigImpl;
import com.je.bpm.core.model.config.process.StartupSettingsConfigImpl;
import com.je.bpm.core.model.lane.Lane;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * 流程
 */
public class Process extends BaseElement implements FlowElementsContainer, HasExecutionListeners {

    /**
     * 流程名称
     */
    protected String name;
    /**
     * 是否可执行
     */
    protected boolean executable = true;
    /**
     * 流程说明
     */
    protected String documentation;
    /**
     * IO规范
     */
    protected IOSpecification ioSpecification;
    /**
     * 流程监听
     */
    protected List<ActivitiListener> executionListeners = new ArrayList();
    /**
     * 泳道
     */
    protected List<Lane> lanes = new ArrayList();
    /**
     * 流程元素集合
     */
    protected List<FlowElement> flowElementList = new ArrayList();

    protected List<ValuedDataObject> dataObjects = new ArrayList();
    /**
     * 流程工件集合
     */
    protected List<Artifact> artifactList = new ArrayList();
    /**
     * 候选启动人
     */
    protected List<String> candidateStarterUsers = new ArrayList();
    /**
     * 候选启动组
     */
    protected List<String> candidateStarterGroups = new ArrayList();
    protected List<com.je.bpm.core.model.EventListener> eventListeners = new ArrayList<com.je.bpm.core.model.EventListener>();
    /**
     * 流程元素映射表
     */
    protected Map<String, FlowElement> flowElementMap = new LinkedHashMap<String, FlowElement>();

    /**
     * Added during process definition parsing
     */
    protected FlowElement initialFlowElement;
    /**
     * 流程配置
     */
    protected ProcessBasicConfigImpl processConfig = new ProcessBasicConfigImpl();
    /**
     * 启动配置
     */
    protected StartupSettingsConfigImpl startupSettings = new StartupSettingsConfigImpl();
    /**
     * 拓展配置
     */
    protected ExtendedConfigurationConfigImpl extendedConfiguration = new ExtendedConfigurationConfigImpl();
    /**
     * 事件配置
     */
    protected List<CustomEvent> customEventListeners = new ArrayList<>();
    /**
     * 通知提醒
     */
    protected MessageSettingConfigImpl messageSetting = new MessageSettingConfigImpl();
    /**
     * 按钮配置
     */
    protected ButtonsConfig<ProcessButton> buttons = buildProcessButton(new ButtonsConfig<ProcessButton>());

    public ButtonsConfig buildProcessButton(ButtonsConfig<ProcessButton> baseUserTask) {
        return baseUserTask.addButton(new ProcessStartButton())
                .addButton(new ProcessSponsorButton()).addButton(new ProcessInvalidButton())
                .addButton(new ProcessHangButton()).addButton(new ProcessCancelButton())
                .addButton(new ProcessActivateButton());
    }

    public Process() {

    }

    public ProcessBasicConfigImpl getProcessConfig() {
        return processConfig;
    }

    public void setProcessConfig(ProcessBasicConfigImpl processConfig) {
        this.processConfig = processConfig;
    }

    public String getDocumentation() {
        return documentation;
    }

    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isExecutable() {
        return executable;
    }

    public void setExecutable(boolean executable) {
        this.executable = executable;
    }

    public IOSpecification getIoSpecification() {
        return ioSpecification;
    }

    public void setIoSpecification(IOSpecification ioSpecification) {
        this.ioSpecification = ioSpecification;
    }

    @Override
    public List<ActivitiListener> getExecutionListeners() {
        return executionListeners;
    }

    @Override
    public void setExecutionListeners(List<ActivitiListener> executionListeners) {
        this.executionListeners = executionListeners;
    }

    public List<Lane> getLanes() {
        return lanes;
    }

    public void setLanes(List<Lane> lanes) {
        this.lanes = lanes;
    }

    public ButtonsConfig<ProcessButton> getButtons() {
        return buttons;
    }

    public void setButtons(ButtonsConfig<ProcessButton> buttons) {
        this.buttons = buttons;
    }

    @Override
    public Map<String, FlowElement> getFlowElementMap() {
        return flowElementMap;
    }

    public void setFlowElementMap(Map<String, FlowElement> flowElementMap) {
        this.flowElementMap = flowElementMap;
    }


    public boolean containsFlowElementId(String id) {
        return flowElementMap.containsKey(id);
    }

    @Override
    public FlowElement getFlowElement(String flowElementId) {
        return getFlowElement(flowElementId, false);
    }

    public List<FlowElement> getFlowElementList() {
        return flowElementList;
    }

    public void setFlowElementList(List<FlowElement> flowElementList) {
        this.flowElementList = flowElementList;
    }

    public List<Artifact> getArtifactList() {
        return artifactList;
    }

    public void setArtifactList(List<Artifact> artifactList) {
        this.artifactList = artifactList;
    }

    public StartupSettingsConfigImpl getStartupSettings() {
        return startupSettings;
    }

    public void setStartupSettings(StartupSettingsConfigImpl startupSettings) {
        this.startupSettings = startupSettings;
    }

    public ExtendedConfigurationConfigImpl getExtendedConfiguration() {
        return extendedConfiguration;
    }

    public void setExtendedConfiguration(ExtendedConfigurationConfigImpl extendedConfiguration) {
        this.extendedConfiguration = extendedConfiguration;
    }

    public List<CustomEvent> getCustomEventListeners() {
        return customEventListeners;
    }

    public void addCustomEvent(CustomEvent customEvent) {
        this.customEventListeners.add(customEvent);
    }

    public void setCustomEventListeners(List<CustomEvent> customEventListeners) {
        this.customEventListeners = customEventListeners;
    }

    public MessageSettingConfigImpl getMessageSetting() {
        return messageSetting;
    }

    public void setMessageSetting(MessageSettingConfigImpl messageSetting) {
        this.messageSetting = messageSetting;
    }

    /**
     * @param searchRecurive: searches the whole process, including subprocesses
     */
    public FlowElement getFlowElement(String flowElementId, boolean searchRecurive) {
        if (searchRecurive) {
            return flowElementMap.get(flowElementId);
        } else {
            return findFlowElementInList(flowElementId);
        }
    }

    public List<Association> findAssociationsWithSourceRefRecursive(String sourceRef) {
        return findAssociationsWithSourceRefRecursive(this, sourceRef);
    }

    protected List<Association> findAssociationsWithSourceRefRecursive(FlowElementsContainer flowElementsContainer, String sourceRef) {
        List<Association> associations = new ArrayList<Association>();
        for (Artifact artifact : flowElementsContainer.getArtifacts()) {
            if (artifact instanceof Association) {
                Association association = (Association) artifact;
                if (association.getSourceRef() != null && association.getTargetRef() != null && association.getSourceRef().equals(sourceRef)) {
                    associations.add(association);
                }
            }
        }

        for (FlowElement flowElement : flowElementsContainer.getFlowElements()) {
            if (flowElement instanceof FlowElementsContainer) {
                associations.addAll(findAssociationsWithSourceRefRecursive((FlowElementsContainer) flowElement, sourceRef));
            }
        }
        return associations;
    }

    public List<Association> findAssociationsWithTargetRefRecursive(String targetRef) {
        return findAssociationsWithTargetRefRecursive(this, targetRef);
    }

    protected List<Association> findAssociationsWithTargetRefRecursive(FlowElementsContainer flowElementsContainer, String targetRef) {
        List<Association> associations = new ArrayList<Association>();
        for (Artifact artifact : flowElementsContainer.getArtifacts()) {
            if (artifact instanceof Association) {
                Association association = (Association) artifact;
                if (association.getTargetRef() != null && association.getTargetRef().equals(targetRef)) {
                    associations.add(association);
                }
            }
        }

        for (FlowElement flowElement : flowElementsContainer.getFlowElements()) {
            if (flowElement instanceof FlowElementsContainer) {
                associations.addAll(findAssociationsWithTargetRefRecursive((FlowElementsContainer) flowElement, targetRef));
            }
        }
        return associations;
    }

    /**
     * Searches the whole process, including subprocesses
     */
    public FlowElementsContainer getFlowElementsContainer(String flowElementId) {
        return getFlowElementsContainer(this, flowElementId);
    }

    protected FlowElementsContainer getFlowElementsContainer(FlowElementsContainer flowElementsContainer, String flowElementId) {
        for (FlowElement flowElement : flowElementsContainer.getFlowElements()) {
            if (flowElement.getId() != null && flowElement.getId().equals(flowElementId)) {
                return flowElementsContainer;
            } else if (flowElement instanceof FlowElementsContainer) {
                FlowElementsContainer result = getFlowElementsContainer((FlowElementsContainer) flowElement, flowElementId);
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }

    protected FlowElement findFlowElementInList(String flowElementId) {
        for (FlowElement f : flowElementList) {
            if (f.getId() != null && f.getId().equals(flowElementId)) {
                return f;
            }
        }
        return null;
    }

    @Override
    public Collection<FlowElement> getFlowElements() {
        return flowElementList;
    }

    @Override
    public void addFlowElement(FlowElement element) {
        flowElementList.add(element);
        element.setParentContainer(this);
        if (StringUtils.isNotEmpty(element.getId())) {
            flowElementMap.put(element.getId(), element);
        }
        if (element instanceof FlowElementsContainer) {
            flowElementMap.putAll(((FlowElementsContainer) element).getFlowElementMap());
        }
    }

    @Override
    public void addFlowElementToMap(FlowElement element) {
        if (element != null && StringUtils.isNotEmpty(element.getId())) {
            flowElementMap.put(element.getId(), element);
        }
    }

    @Override
    public void removeFlowElement(String elementId) {
        FlowElement element = flowElementMap.get(elementId);
        if (element != null) {
            flowElementList.remove(element);
            flowElementMap.remove(element.getId());
        }
    }

    @Override
    public void removeFlowElementFromMap(String elementId) {
        if (StringUtils.isNotEmpty(elementId)) {
            flowElementMap.remove(elementId);
        }
    }

    @Override
    public Artifact getArtifact(String id) {
        Artifact foundArtifact = null;
        for (Artifact artifact : artifactList) {
            if (id.equals(artifact.getId())) {
                foundArtifact = artifact;
                break;
            }
        }
        return foundArtifact;
    }

    @Override
    public Collection<Artifact> getArtifacts() {
        return artifactList;
    }

    @Override
    public void addArtifact(Artifact artifact) {
        artifactList.add(artifact);
    }

    @Override
    public void removeArtifact(String artifactId) {
        Artifact artifact = getArtifact(artifactId);
        if (artifact != null) {
            artifactList.remove(artifact);
        }
    }

    public List<String> getCandidateStarterUsers() {
        return candidateStarterUsers;
    }

    public void setCandidateStarterUsers(List<String> candidateStarterUsers) {
        this.candidateStarterUsers = candidateStarterUsers;
    }

    public List<String> getCandidateStarterGroups() {
        return candidateStarterGroups;
    }

    public void setCandidateStarterGroups(List<String> candidateStarterGroups) {
        this.candidateStarterGroups = candidateStarterGroups;
    }

    public List<com.je.bpm.core.model.EventListener> getEventListeners() {
        return eventListeners;
    }

    public void setEventListeners(List<com.je.bpm.core.model.EventListener> eventListeners) {
        this.eventListeners = eventListeners;
    }

    public <FlowElementType extends FlowElement> List<FlowElementType> findFlowElementsOfType(Class<FlowElementType> type) {
        return findFlowElementsOfType(type, true);
    }

    @SuppressWarnings("unchecked")
    public <FlowElementType extends FlowElement> List<FlowElementType> findFlowElementsOfType(Class<FlowElementType> type, boolean goIntoSubprocesses) {
        List<FlowElementType> foundFlowElements = new ArrayList<FlowElementType>();
        for (FlowElement flowElement : this.getFlowElements()) {
            if (type.isInstance(flowElement)) {
                foundFlowElements.add((FlowElementType) flowElement);
            }
            if (flowElement instanceof SubProcess) {
                if (goIntoSubprocesses) {
                    foundFlowElements.addAll(findFlowElementsInSubProcessOfType((SubProcess) flowElement, type));
                }
            }
        }
        return foundFlowElements;
    }

    public <FlowElementType extends FlowElement> List<FlowElementType> findFlowElementsInSubProcessOfType(SubProcess subProcess, Class<FlowElementType> type) {
        return findFlowElementsInSubProcessOfType(subProcess, type, true);
    }

    @SuppressWarnings("unchecked")
    public <FlowElementType extends FlowElement> List<FlowElementType> findFlowElementsInSubProcessOfType(SubProcess subProcess, Class<FlowElementType> type, boolean goIntoSubprocesses) {

        List<FlowElementType> foundFlowElements = new ArrayList<FlowElementType>();
        for (FlowElement flowElement : subProcess.getFlowElements()) {
            if (type.isInstance(flowElement)) {
                foundFlowElements.add((FlowElementType) flowElement);
            }
            if (flowElement instanceof SubProcess) {
                if (goIntoSubprocesses) {
                    foundFlowElements.addAll(findFlowElementsInSubProcessOfType((SubProcess) flowElement, type));
                }
            }
        }
        return foundFlowElements;
    }

    public FlowElementsContainer findParent(FlowElement childElement) {
        return findParent(childElement, this);
    }

    public FlowElementsContainer findParent(FlowElement childElement, FlowElementsContainer flowElementsContainer) {
        for (FlowElement flowElement : flowElementsContainer.getFlowElements()) {
            if (childElement.getId() != null && childElement.getId().equals(flowElement.getId())) {
                return flowElementsContainer;
            }
            if (flowElement instanceof FlowElementsContainer) {
                FlowElementsContainer result = findParent(childElement, (FlowElementsContainer) flowElement);
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }

    @Override
    public Process clone() {
        Process clone = new Process();
        clone.setValues(this);
        return clone;
    }

    public void setValues(Process otherElement) {
        super.setValues(otherElement);

//    setBpmnModel(bpmnModel);
        setName(otherElement.getName());
        setExecutable(otherElement.isExecutable());
        setDocumentation(otherElement.getDocumentation());
        if (otherElement.getIoSpecification() != null) {
            setIoSpecification(otherElement.getIoSpecification().clone());
        }

        executionListeners = new ArrayList<ActivitiListener>();
        if (otherElement.getExecutionListeners() != null && !otherElement.getExecutionListeners().isEmpty()) {
            for (ActivitiListener listener : otherElement.getExecutionListeners()) {
                executionListeners.add(listener.clone());
            }
        }

        candidateStarterUsers = new ArrayList<String>();
        if (otherElement.getCandidateStarterUsers() != null && !otherElement.getCandidateStarterUsers().isEmpty()) {
            candidateStarterUsers.addAll(otherElement.getCandidateStarterUsers());
        }

        candidateStarterGroups = new ArrayList<String>();
        if (otherElement.getCandidateStarterGroups() != null && !otherElement.getCandidateStarterGroups().isEmpty()) {
            candidateStarterGroups.addAll(otherElement.getCandidateStarterGroups());
        }

        eventListeners = new ArrayList<com.je.bpm.core.model.EventListener>();
        if (otherElement.getEventListeners() != null && !otherElement.getEventListeners().isEmpty()) {
            for (EventListener listener : otherElement.getEventListeners()) {
                eventListeners.add(listener.clone());
            }
        }

        /*
         * This is required because data objects in Designer have no DI info and are added as properties, not flow elements
         *
         * Determine the differences between the 2 elements' data object
         */
        for (ValuedDataObject thisObject : getDataObjects()) {
            boolean exists = false;
            for (ValuedDataObject otherObject : otherElement.getDataObjects()) {
                if (thisObject.getId().equals(otherObject.getId())) {
                    exists = true;
                }
            }
            if (!exists) {
                // missing object
                removeFlowElement(thisObject.getId());
            }
        }

        dataObjects = new ArrayList<ValuedDataObject>();
        if (otherElement.getDataObjects() != null && !otherElement.getDataObjects().isEmpty()) {
            for (ValuedDataObject dataObject : otherElement.getDataObjects()) {
                ValuedDataObject clone = dataObject.clone();
                dataObjects.add(clone);
                // add it to the list of FlowElements
                // if it is already there, remove it first so order is same as
                // data object list
                removeFlowElement(clone.getId());
                addFlowElement(clone);
            }
        }
    }

    public List<ValuedDataObject> getDataObjects() {
        return dataObjects;
    }

    public void setDataObjects(List<ValuedDataObject> dataObjects) {
        this.dataObjects = dataObjects;
    }

    public FlowElement getInitialFlowElement() {
        return initialFlowElement;
    }

    public void setInitialFlowElement(FlowElement initialFlowElement) {
        this.initialFlowElement = initialFlowElement;
    }

}
