/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.task;

import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.MultiInstanceLoopCharacteristics;
import com.je.bpm.core.model.config.task.assignment.TaskAssigneeConfigImpl;
import com.je.bpm.core.model.exceptions.ModelException;

/**
 * 多人审批节点
 */
public class KaiteMultiUserTask extends KaiteBaseUserTask {

    private static final String LOOP_INDEX_VARIABLE = "loopIndex";

    private static final String LOOP_INPUT_DATA_ITEM = "${datas}";

    /**
     * 运行时调整
     */
    private boolean runtimeTuning = false;

    @Override
    public String getType() {
        return KaiteTaskCategoryEnum.JSON_KAITE_MULTI_USERTASK.getType();
    }

    /**
     * 指派人配置
     */
    protected TaskAssigneeConfigImpl taskAssigneeConfig = new TaskAssigneeConfigImpl();

    public TaskAssigneeConfigImpl getTaskAssigneeConfig() {
        return taskAssigneeConfig;
    }

    public void setTaskAssigneeConfig(TaskAssigneeConfigImpl taskAssigneeConfig) {
        this.taskAssigneeConfig = taskAssigneeConfig;
    }

    public KaiteMultiUserTask() {
        this(false);
        this.category = KaiteTaskCategoryEnum.KAITE_MULTI_USERTASK.getType();
    }

    public KaiteMultiUserTask(boolean sequential) {
        loopCharacteristics = new MultiInstanceLoopCharacteristics();
        loopCharacteristics.setSequential(sequential);
        loopCharacteristics.setElementIndexVariable(LOOP_INDEX_VARIABLE);
        loopCharacteristics.setElementVariable("eachLoopUser");
        loopCharacteristics.setInputDataItem(LOOP_INPUT_DATA_ITEM);
        loopCharacteristics.setCompletionCondition("${nrOfCompletedInstances == nrOfInstances}");
    }

    @Override
    protected void setCategory(String category) {
        throw new ModelException("非法调用！平台已禁用此方法的调用");
    }

    @Override
    public boolean hasMultiInstanceLoopCharacteristics() {
        return true;
    }


    public boolean isRuntimeTuning() {
        return runtimeTuning;
    }

    public void setRuntimeTuning(boolean runtimeTuning) {
        this.runtimeTuning = runtimeTuning;
    }

    public void setValues(KaiteMultiUserTask otherElement) {
        super.setValues(otherElement);
        setLoopCharacteristics(otherElement.getLoopCharacteristics());
    }

    @Override
    public FlowElement clone() {
        KaiteMultiUserTask clone = new KaiteMultiUserTask();
        clone.setValues(this);
        return clone;
    }

}
