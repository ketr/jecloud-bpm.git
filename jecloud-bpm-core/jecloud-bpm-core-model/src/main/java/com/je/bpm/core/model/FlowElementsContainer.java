/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model;

import com.je.bpm.core.model.artifact.Artifact;
import java.util.Collection;
import java.util.Map;

/**
 * 流程元素容器定义
 */
public interface FlowElementsContainer {

    /**
     * 获取流程元素
     * @param id
     * @return
     */
    FlowElement getFlowElement(String id);

    /**
     * 获取所有流程元素
     * @return
     */
    Collection<FlowElement> getFlowElements();

    /**
     * 获取所有流程元素
     * @return
     */
    Map<String, FlowElement> getFlowElementMap();

    /**
     * 添加流程元素
     * @param element
     */
    void addFlowElement(FlowElement element);

    /**
     * 添加流程元素
     * @param element
     */
    void addFlowElementToMap(FlowElement element);

    /**
     * 移除流程元素
     * @param elementId
     */
    void removeFlowElement(String elementId);

    /**
     * 移除流程元素
     * @param elementId
     */
    void removeFlowElementFromMap(String elementId);

    /**
     * 获取基础工件
     * @param id
     * @return
     */
    Artifact getArtifact(String id);

    /**
     * 获取所有工件
     * @return
     */
    Collection<Artifact> getArtifacts();

    /**
     * 添加工件
     * @param artifact
     */
    void addArtifact(Artifact artifact);

    /**
     * 移除工件
     * @param artifactId
     */
    void removeArtifact(String artifactId);

}
