/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.button.factory;

import com.je.bpm.core.model.button.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 流程按钮
 */
public class ProcessButtonFactory extends Button {

    public ProcessButtonFactory() {
        init();
    }

    private static final Map<String, Button> map = new HashMap<>();

    private static final Map<String, ProcessButton> adminMap = new HashMap<>();

    private static void addButton(String code, Button taskButton) {
        map.put(code, taskButton);
    }

    private static void addAdminButton(String code, ProcessButton taskButton) {
        adminMap.put(code, taskButton);
    }

    public static List<ProcessButton> getAdminButtons() {
        List<ProcessButton> list = new ArrayList<>();
        adminMap.forEach((k, v) -> list.add(v));
        return list;
    }

    public static Button getButton(String code) {
        return map.get(code);
    }

    private void init() {
        initGeneralStaffButton();
        initAdminButton();
    }

    /**
     * 普通员工级别的流程按钮
     */
    private void initGeneralStaffButton() {
        addButton(ButtonEnum.CANCEL_BTN.getCode(), new ProcessCancelButton());
        addButton(ButtonEnum.ADD_SIGNATURE_NODE.getCode(), new TaskAddSignatureNodeButton());
        addButton(ButtonEnum.DEL_SIGNATURE_NODE.getCode(), new TaskDelSignatureNodeButton());
        addButton(ButtonEnum.INVALID_BTN.getCode(), new ProcessInvalidButton());
        addButton(ButtonEnum.URGE_BTN.getCode(), new TaskUrgeButton());
        addButton(ButtonEnum.SPONSOR_BTN.getCode(), new ProcessSponsorButton());
        addButton(ButtonEnum.START_BTN.getCode(), new ProcessStartButton());
        addButton(ButtonEnum.WITHDRAW_BTN.getCode(), new TaskWithdrawButton());
    }

    /**
     * 管理员级别的流程按钮
     */
    private void initAdminButton() {
        addAdminButton(ButtonEnum.ACTIVE_BTN.getCode(), new ProcessActivateButton());
        addAdminButton(ButtonEnum.HANG_BTN.getCode(), new ProcessHangButton());
        addAdminButton(ButtonEnum.INVALID_BTN.getCode(), new ProcessInvalidButton());
    }

}
