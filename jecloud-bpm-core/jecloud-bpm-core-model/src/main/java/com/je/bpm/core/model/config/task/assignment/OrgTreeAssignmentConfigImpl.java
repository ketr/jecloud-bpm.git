/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task.assignment;

import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.config.task.AbstractTaskConfigImpl;

import java.util.List;

/**
 * 组织树指派人配置实现
 */
public class OrgTreeAssignmentConfigImpl extends BasicAssignmentConfigImpl {

    private static final String CONFIG_TYPE = "orgTreeConfig";

    private static final String CONFIG_TYPE_NAME = "组织结构";

    private boolean enable = false;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public OrgTreeAssignmentConfigImpl() {
        super(CONFIG_TYPE);
    }

    @Override
    public void addResource(List<AssigneeResource> list) {
    }

    @Override
    public String getConfigType() {
        return CONFIG_TYPE;
    }

    @Override
    public String getConfigTypeName() {
        return CONFIG_TYPE_NAME;
    }

    @Override
    public List<AssigneeResource> getResource() {
        return null;
    }

    @Override
    public AssignmentPermission getPermission() {
        return null;
    }

    @Override
    public void setPermission(AssignmentPermission assignmentPermission) {

    }

    @Override
    public BaseElement clone() {
        OrgTreeAssignmentConfigImpl orgTreeAssignmentConfig = new OrgTreeAssignmentConfigImpl();
        orgTreeAssignmentConfig.setEnable(isEnable());
        return orgTreeAssignmentConfig;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("OrgTreeAssignmentConfigImpl{");
        sb.append("enable=").append(enable);
        sb.append('}');
        return sb.toString();
    }
}
