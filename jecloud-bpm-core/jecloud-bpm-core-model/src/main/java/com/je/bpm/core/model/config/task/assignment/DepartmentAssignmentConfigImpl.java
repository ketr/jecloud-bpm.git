/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.task.assignment;

import com.je.bpm.core.model.BaseElement;

import java.util.ArrayList;
import java.util.List;

/**
 * 部门指派配置实现
 */
public class DepartmentAssignmentConfigImpl extends BasicAssignmentConfigImpl {

    private static final String CONFIG_TYPE = "departmentConfig";

    private static final String CONFIG_TYPE_NAME = "部门";

    /**
     * 资源
     */
    private List<AssigneeResource> resource = new ArrayList<>();;
    /**
     * 权限
     */
    private AssignmentPermission permission;

    public void addResource(AssigneeResource assigneeResource) {
        resource.add(assigneeResource);
    }

    public DepartmentAssignmentConfigImpl() {
        super(CONFIG_TYPE);
    }

    @Override
    public String getConfigType() {
        return CONFIG_TYPE;
    }

    @Override
    public String getConfigTypeName() {
        return CONFIG_TYPE_NAME;
    }

    public List<AssigneeResource> getResource() {
        return resource;
    }

    public void setResource(List<AssigneeResource> resource) {
        this.resource = resource;
    }

    public AssignmentPermission getPermission() {
        return permission;
    }

    @Override
    public void addResource(List<AssigneeResource> list) {
        resource.addAll(list);
    }

    public void setPermission(AssignmentPermission permission) {
        this.permission = permission;
    }

    @Override
    public BaseElement clone() {
        DepartmentAssignmentConfigImpl departmentAssignmentConfig = new DepartmentAssignmentConfigImpl();

        return departmentAssignmentConfig;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DepartmentAssignmentConfigImpl{");
        sb.append("resource=").append(resource);
        sb.append("\r\n");
        for(AssigneeResource assigneeResource : resource){
            sb.append(", assigneeResource=").append(assigneeResource.toString());
            sb.append("\r\n");
        }
        sb.append(", permission=").append(permission.toString());
        sb.append("\r\n");
        sb.append('}');
        return sb.toString();
    }
}
