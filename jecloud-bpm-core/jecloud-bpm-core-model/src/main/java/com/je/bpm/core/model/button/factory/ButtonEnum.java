/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.button.factory;

public enum ButtonEnum {
    /**
     * 流程激活按钮
     */
    ACTIVE_BTN("activeBtn"),
    /**
     * 流程撤销按钮
     */
    CANCEL_BTN("cancelBtn"),
    /**
     * 流程撤回按钮
     */
    WITHDRAW_BTN("withdrawBtn"),
    /**
     * 流程挂起按钮
     */
    HANG_BTN("hangBtn"),
    /**
     * 流程作废按钮
     */
    INVALID_BTN("invalidBtn"),
    /**
     * 流程发起按钮
     */
    SPONSOR_BTN("sponsorBtn"),
    /**
     * 流程启动按钮
     */
    START_BTN("startBtn"),
    /**
     * 任务取消委托按钮
     */
    CANCEL_DELEGATE_BTN("cancelDelegateBtn"),
    /**
     * 加签节点按钮
     */
    ADD_SIGNATURE_NODE("addSignatureNode"),
    /**
     * 减签节点按钮
     */
    DEL_SIGNATURE_NODE("delSignatureNode"),
    /**
     * 任务更换负责人按钮
     */
    CHANGE_ASSIGNEE_BTN("changeAssigneeBtn"),
    /**
     * 任务领取按钮（候选节点领取）
     */
    RECEIVE_BTN("receiveBtn"),
    /**
     * 任务委托按钮
     */
    DELEGATE_BTN("delegateBtn"),
    /**
     * 任务直送按钮
     */
    DIRECT_SEND_BTN("directSendBtn"),
    /**
     * 任务驳回按钮
     */
    DISMISS_BTN("dismissBtn"),
    /**
     * 任务退回按钮
     */
    GOBACK_BTN("gobackBtn"),
    /**
     * 任务传阅按钮
     */
    PASSROUND_BTN("passroundBtn"),
    /**
     * 任务传阅已阅按钮(根据当前登录人)
     */
    PASSROUND_READ_BTN("passroundReadBtn"),
    /**
     * 任务取回按钮
     */
    RETRIEVE_BTN("retrieveBtn"),
    /**
     * 任务提交按钮
     */
    SUBMIT_BTN("submitBtn"),
    /**
     * 任务提交按钮
     */
    CUSTOM_SUBMIT_BTN("customSubmitBtn"),
    /**
     * 任务转办按钮
     */
    TRANSFER_BTN("transferBtn"),
    /**
     * 任务催办按钮（从当前登录人催办指派人，并发送给抄送人）
     */
    URGE_BTN("urgeBtn"),
    /**
     * 会签-弃权
     */
    COUNTERSIGNED_ABSTAIN_BTN("abstainBtn"),
    /**
     * 会签-通过
     */
    COUNTERSIGNED_PASS_BTN("passBtn"),
    /**
     * 会签-否决
     */
    COUNTERSIGNED_VETO_BTN("vetoBtn"),
    /**
     * 会签-加签——》人员调整
     */
    COUNTERSIGNED_ADD_SIGNATURE_BTN("countersignedAddSignatureBtn"),
    /**
     * 会签-减签
     */
    COUNTERSIGNED_VISA_REDUCTION_BTN("countersignedVisaReductionBtn"),
    /**
     * 会签-改签
     */
    COUNTERSIGNED_REBOOK_REDUCTION_BTN("countersignedRebookReductionBtn"),
    /**
     * 加签按钮
     */
    COUNTERSIGN_BTN("countersignBtn"),
    /**
     * 回签按钮
     */
    SIGN_BACK_BTN("signBackBtn"),
    /**
     * 暂存按钮
     */
    STAGING_BTN("stagingBtn");

    private String code;

    ButtonEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }


}
