/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.config.process;

import com.je.bpm.core.model.BaseElement;

/**
 * 启动配置
 */
public class StartupSettingsConfigImpl extends AbstractProcessConfig {

    /**
     * 配置类型
     */
    private static final String CONFIG_TYPE = "startupSettings";

    public StartupSettingsConfigImpl() {
        super(CONFIG_TYPE);
    }

    public String getConfigType() {
        return CONFIG_TYPE;
    }

    /**
     * 是否任何人可以启动流程
     */
    private boolean canEveryoneStart = false;
    /**
     * 可启动角色id
     */
    private String canEveryoneRolesId = "";
    /**
     * 可启动角色name
     */
    private String canEveryoneRolesName = "";
    /**
     * 启动表达式
     */
    private String startExpression = "";
    /**
     * 启动表达式fn
     */
    private String startExpressionFn = "";

    public boolean isCanEveryoneStart() {
        return canEveryoneStart;
    }

    public void setCanEveryoneStart(boolean canEveryoneStart) {
        this.canEveryoneStart = canEveryoneStart;
    }

    public String getCanEveryoneRolesId() {
        return canEveryoneRolesId;
    }

    public void setCanEveryoneRolesId(String canEveryoneRolesId) {
        this.canEveryoneRolesId = canEveryoneRolesId;
    }

    public String getCanEveryoneRolesName() {
        return canEveryoneRolesName;
    }

    public void setCanEveryoneRolesName(String canEveryoneRolesName) {
        this.canEveryoneRolesName = canEveryoneRolesName;
    }

    public String getStartExpression() {
        return startExpression;
    }

    public void setStartExpression(String startExpression) {
        this.startExpression = startExpression;
    }

    public String getStartExpressionFn() {
        return startExpressionFn;
    }

    public void setStartExpressionFn(String startExpressionFn) {
        this.startExpressionFn = startExpressionFn;
    }

    @Override
    public BaseElement clone() {
        return null;
    }
}
