/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class IOSpecification extends BaseElement {

    /**
     * 输入规范
     */
    protected List<DataSpec> dataInputs = new ArrayList<DataSpec>();
    /**
     * 输出规范
     */
    protected List<DataSpec> dataOutputs = new ArrayList<DataSpec>();
    /**
     * 输入数据引用集合
     */
    protected List<String> dataInputRefs = new ArrayList<String>();
    /**
     * 输出数据引用集合
     */
    protected List<String> dataOutputRefs = new ArrayList<String>();

    public List<DataSpec> getDataInputs() {
        return dataInputs;
    }

    public void setDataInputs(List<DataSpec> dataInputs) {
        this.dataInputs = dataInputs;
    }

    public List<DataSpec> getDataOutputs() {
        return dataOutputs;
    }

    public void setDataOutputs(List<DataSpec> dataOutputs) {
        this.dataOutputs = dataOutputs;
    }

    public List<String> getDataInputRefs() {
        return dataInputRefs;
    }

    public void setDataInputRefs(List<String> dataInputRefs) {
        this.dataInputRefs = dataInputRefs;
    }

    public List<String> getDataOutputRefs() {
        return dataOutputRefs;
    }

    public void setDataOutputRefs(List<String> dataOutputRefs) {
        this.dataOutputRefs = dataOutputRefs;
    }

    @Override
    public IOSpecification clone() {
        IOSpecification clone = new IOSpecification();
        clone.setValues(this);
        return clone;
    }

    public void setValues(IOSpecification otherSpec) {
        dataInputs = new ArrayList<DataSpec>();
        if (otherSpec.getDataInputs() != null && !otherSpec.getDataInputs().isEmpty()) {
            for (DataSpec dataSpec : otherSpec.getDataInputs()) {
                dataInputs.add(dataSpec.clone());
            }
        }

        dataOutputs = new ArrayList<DataSpec>();
        if (otherSpec.getDataOutputs() != null && !otherSpec.getDataOutputs().isEmpty()) {
            for (DataSpec dataSpec : otherSpec.getDataOutputs()) {
                dataOutputs.add(dataSpec.clone());
            }
        }

        dataInputRefs = new ArrayList<String>(otherSpec.getDataInputRefs());
        dataOutputRefs = new ArrayList<String>(otherSpec.getDataOutputRefs());
    }
}
