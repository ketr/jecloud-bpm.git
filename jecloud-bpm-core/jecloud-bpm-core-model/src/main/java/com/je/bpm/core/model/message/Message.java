/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.model.message;

import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.ExtensionAttribute;
import com.je.bpm.core.model.ExtensionElement;

import java.util.List;
import java.util.Map;

import static java.util.Collections.emptyMap;

/**
 * 消息
 */
public class Message extends BaseElement {

    /**
     * 消息名称
     */
    protected String name;
    /**
     * 引用
     */
    protected String itemRef;

    private Message(Builder builder) {
        this.id = builder.id;
        this.xmlRowNumber = builder.xmlRowNumber;
        this.xmlColumnNumber = builder.xmlColumnNumber;
        this.extensionElements = builder.extensionElements;
        this.attributes = builder.attributes;
        this.name = builder.name;
        this.itemRef = builder.itemRef;
    }

    public Message() {
    }

    public Message(String id, String name, String itemRef) {
        this.id = id;
        this.name = name;
        this.itemRef = itemRef;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemRef() {
        return itemRef;
    }

    public void setItemRef(String itemRef) {
        this.itemRef = itemRef;
    }

    @Override
    public Message clone() {
        Message clone = new Message();
        clone.setValues(this);
        return clone;
    }

    public void setValues(Message otherElement) {
        super.setValues(otherElement);
        setName(otherElement.getName());
        setItemRef(otherElement.getItemRef());
    }

    /**
     * Creates builder to build {@link Message}.
     *
     * @return created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Creates a builder to build {@link Message} and initialize it with the given object.
     *
     * @param message to initialize the builder with
     * @return created builder
     */
    public static Builder builderFrom(Message message) {
        return new Builder(message);
    }

    /**
     * Builder to build {@link Message}.
     */
    public static final class Builder {

        private String id;
        private int xmlRowNumber;
        private int xmlColumnNumber;
        private Map<String, List<ExtensionElement>> extensionElements = emptyMap();
        private Map<String, List<ExtensionAttribute>> attributes = emptyMap();
        private String name;
        private String itemRef;

        private Builder() {
        }

        private Builder(Message message) {
            this.id = message.id;
            this.xmlRowNumber = message.xmlRowNumber;
            this.xmlColumnNumber = message.xmlColumnNumber;
            this.extensionElements = message.extensionElements;
            this.attributes = message.attributes;
            this.name = message.name;
            this.itemRef = message.itemRef;
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder xmlRowNumber(int xmlRowNumber) {
            this.xmlRowNumber = xmlRowNumber;
            return this;
        }

        public Builder xmlColumnNumber(int xmlColumnNumber) {
            this.xmlColumnNumber = xmlColumnNumber;
            return this;
        }

        public Builder extensionElements(Map<String, List<ExtensionElement>> extensionElements) {
            this.extensionElements = extensionElements;
            return this;
        }

        public Builder attributes(Map<String, List<ExtensionAttribute>> attributes) {
            this.attributes = attributes;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder itemRef(String itemRef) {
            this.itemRef = itemRef;
            return this;
        }

        public Message build() {
            return new Message(this);
        }
    }
}
