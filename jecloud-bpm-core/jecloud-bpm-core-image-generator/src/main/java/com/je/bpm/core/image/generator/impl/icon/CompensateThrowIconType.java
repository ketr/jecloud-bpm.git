/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.image.generator.impl.icon;

import com.je.bpm.core.image.generator.impl.ProcessDiagramSVGGraphics2D;
import org.apache.batik.svggen.SVGGraphics2D;
import org.w3c.dom.Element;

public class CompensateThrowIconType extends CompensateIconType {

    @Override
    public Integer getWidth() {
        return 15;
    }

    @Override
    public Integer getHeight() {
        return 16;
    }

    @Override
    public String getFillValue() {
        return "#585858";
    }

    @Override
    public void drawIcon(int imageX, int imageY, int iconPadding, ProcessDiagramSVGGraphics2D svgGenerator) {
        Element gTag = svgGenerator.getDOMFactory().createElementNS(null,
                SVGGraphics2D.SVG_G_TAG);
        gTag.setAttributeNS(null,
                "transform",
                "translate(" + (imageX - 8) + "," + (imageY - 6) + ")");

        Element polygonTag1 = svgGenerator.getDOMFactory().createElementNS(null,
                SVGGraphics2D.SVG_POLYGON_TAG);
        polygonTag1.setAttributeNS(null,
                "points",
                "14 8 14 22 7 15 ");
        polygonTag1.setAttributeNS(null,
                "fill",
                this.getFillValue());
        polygonTag1.setAttributeNS(null,
                "stroke",
                this.getStrokeValue());
        polygonTag1.setAttributeNS(null,
                "stroke-width",
                this.getStrokeWidth());
        polygonTag1.setAttributeNS(null,
                "stroke-linecap",
                "butt");
        polygonTag1.setAttributeNS(null,
                "stroke-linejoin",
                "miter");
        polygonTag1.setAttributeNS(null,
                "stroke-miterlimit",
                "10");
        gTag.appendChild(polygonTag1);

        Element polygonTag2 = svgGenerator.getDOMFactory().createElementNS(null,
                SVGGraphics2D.SVG_POLYGON_TAG);
        polygonTag2.setAttributeNS(null,
                "points",
                "21 8 21 22 14 15 ");
        polygonTag2.setAttributeNS(null,
                "fill",
                this.getFillValue());
        polygonTag2.setAttributeNS(null,
                "stroke",
                this.getStrokeValue());
        polygonTag2.setAttributeNS(null,
                "stroke-width",
                this.getStrokeWidth());
        polygonTag2.setAttributeNS(null,
                "stroke-linecap",
                "butt");
        polygonTag2.setAttributeNS(null,
                "stroke-linejoin",
                "miter");
        polygonTag2.setAttributeNS(null,
                "stroke-miterlimit",
                "10");
        gTag.appendChild(polygonTag2);

        svgGenerator.getExtendDOMGroupManager().addElement(gTag);
    }
}
