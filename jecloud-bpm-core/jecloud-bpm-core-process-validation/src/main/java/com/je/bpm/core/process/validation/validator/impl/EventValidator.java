/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.process.validation.validator.impl;

import com.je.bpm.core.model.*;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.core.model.event.Event;
import com.je.bpm.core.process.validation.ValidationError;
import com.je.bpm.core.process.validation.validator.Problems;
import com.je.bpm.core.process.validation.validator.ProcessLevelValidator;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Validates rules that apply to all events (start event, boundary event, etc.)
 *

 */
public class EventValidator extends ProcessLevelValidator {

  @Override
  protected void executeValidation(BpmnModel bpmnModel, Process process, List<ValidationError> errors) {
    List<Event> events = process.findFlowElementsOfType(Event.class);
    for (Event event : events) {
      if (event.getEventDefinitions() != null) {
        for (EventDefinition eventDefinition : event.getEventDefinitions()) {

          if (eventDefinition instanceof MessageEventDefinition) {
            handleMessageEventDefinition(bpmnModel, process, event, eventDefinition, errors);
          } else if (eventDefinition instanceof SignalEventDefinition) {
            handleSignalEventDefinition(bpmnModel, process, event, eventDefinition, errors);
          } else if (eventDefinition instanceof TimerEventDefinition) {
            handleTimerEventDefinition(process, event, eventDefinition, errors);
          } else if (eventDefinition instanceof CompensateEventDefinition) {
            handleCompensationEventDefinition(bpmnModel, process, event, eventDefinition, errors);
          }

        }
      }
    }
  }

  protected void handleMessageEventDefinition(BpmnModel bpmnModel, Process process, Event event, EventDefinition eventDefinition, List<ValidationError> errors) {
    MessageEventDefinition messageEventDefinition = (MessageEventDefinition) eventDefinition;

    if (StringUtils.isEmpty(messageEventDefinition.getMessageRef())) {

      if (StringUtils.isEmpty(messageEventDefinition.getMessageExpression())) {
        // message ref should be filled in
        addError(errors, Problems.MESSAGE_EVENT_MISSING_MESSAGE_REF, process, event, "attribute 'messageRef' is required");
      }

    } else if (!bpmnModel.containsMessageId(messageEventDefinition.getMessageRef())) {
      // message ref should exist
      addError(errors, Problems.MESSAGE_EVENT_INVALID_MESSAGE_REF, process, event, "Invalid 'messageRef': no message with that id can be found in the model");
    }
  }

  protected void handleSignalEventDefinition(BpmnModel bpmnModel, Process process, Event event, EventDefinition eventDefinition, List<ValidationError> errors) {
    SignalEventDefinition signalEventDefinition = (SignalEventDefinition) eventDefinition;

    if (StringUtils.isEmpty(signalEventDefinition.getSignalRef())) {

      if (StringUtils.isEmpty(signalEventDefinition.getSignalExpression())) {
        addError(errors, Problems.SIGNAL_EVENT_MISSING_SIGNAL_REF, process, event, "signalEventDefinition does not have mandatory property 'signalRef'");
      }

    } else if (!bpmnModel.containsSignalId(signalEventDefinition.getSignalRef())) {
      addError(errors, Problems.SIGNAL_EVENT_INVALID_SIGNAL_REF, process, event, "Invalid 'signalRef': no signal with that id can be found in the model");
    }
  }

  protected void handleTimerEventDefinition(Process process, Event event, EventDefinition eventDefinition, List<ValidationError> errors) {
    TimerEventDefinition timerEventDefinition = (TimerEventDefinition) eventDefinition;
    if (StringUtils.isEmpty(timerEventDefinition.getTimeDate()) && StringUtils.isEmpty(timerEventDefinition.getTimeCycle()) && StringUtils.isEmpty(timerEventDefinition.getTimeDuration())) {
      // neither date, cycle or duration configured
      addError(errors, Problems.EVENT_TIMER_MISSING_CONFIGURATION, process, event, "Timer needs configuration (either timeDate, timeCycle or timeDuration is needed)");
    }
  }

  protected void handleCompensationEventDefinition(BpmnModel bpmnModel, Process process, Event event, EventDefinition eventDefinition, List<ValidationError> errors) {
    CompensateEventDefinition compensateEventDefinition = (CompensateEventDefinition) eventDefinition;

    // Check activityRef
    if ((StringUtils.isNotEmpty(compensateEventDefinition.getActivityRef()) && process.getFlowElement(compensateEventDefinition.getActivityRef(), true) == null)) {
      addError(errors, Problems.COMPENSATE_EVENT_INVALID_ACTIVITY_REF, process, event, "Invalid attribute value for 'activityRef': no activity with the given id");
    }
  }

}
