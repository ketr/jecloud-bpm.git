/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.process.validation.validator;

import com.je.bpm.core.process.validation.validator.impl.*;

/**

 */
public class ValidatorSetFactory {

  public ValidatorSet createActivitiExecutableProcessValidatorSet() {
    ValidatorSet validatorSet = new ValidatorSet(ValidatorSetNames.ACTIVITI_EXECUTABLE_PROCESS);

    validatorSet.addValidator(new AssociationValidator());
    validatorSet.addValidator(new SignalValidator());
    validatorSet.addValidator(new OperationValidator());
    validatorSet.addValidator(new ErrorValidator());
    validatorSet.addValidator(new DataObjectValidator());

    validatorSet.addValidator(new BpmnModelValidator());
    validatorSet.addValidator(new FlowElementValidator());

    validatorSet.addValidator(new StartEventValidator());
    validatorSet.addValidator(new SequenceflowValidator());
    validatorSet.addValidator(new UserTaskValidator());
    validatorSet.addValidator(new ServiceTaskValidator());
    validatorSet.addValidator(new ScriptTaskValidator());
    validatorSet.addValidator(new SendTaskValidator());
    validatorSet.addValidator(new ExclusiveGatewayValidator());
    validatorSet.addValidator(new EventGatewayValidator());
    validatorSet.addValidator(new SubprocessValidator());
    validatorSet.addValidator(new EventSubprocessValidator());
    validatorSet.addValidator(new BoundaryEventValidator());
    validatorSet.addValidator(new IntermediateCatchEventValidator());
    validatorSet.addValidator(new IntermediateThrowEventValidator());
    validatorSet.addValidator(new MessageValidator());
    validatorSet.addValidator(new EventValidator());
    validatorSet.addValidator(new EndEventValidator());

    validatorSet.addValidator(new ExecutionListenerValidator());
    validatorSet.addValidator(new ActivitiEventListenerValidator());

    validatorSet.addValidator(new DiagramInterchangeInfoValidator());

    return validatorSet;
  }

}
