/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.process.validation.validator.impl;

import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.core.process.validation.ValidationError;
import com.je.bpm.core.process.validation.validator.Constraints;
import com.je.bpm.core.process.validation.validator.Problems;
import com.je.bpm.core.process.validation.validator.ValidatorImpl;

import java.util.List;

public class BpmnModelValidator extends ValidatorImpl {

    @Override
    public void validate(BpmnModel bpmnModel, List<ValidationError> errors) {
        // If all process definitions of this bpmnModel are not executable, raise an error
        boolean isAtLeastOneExecutable = validateAtLeastOneExecutable(bpmnModel, errors);

        // If at least one process definition is executable, show a warning for each of the none-executables
        if (isAtLeastOneExecutable) {
            for (Process process : bpmnModel.getProcesses()) {
                if (!process.isExecutable()) {
                    addWarning(errors, Problems.PROCESS_DEFINITION_NOT_EXECUTABLE, process, process,
                            "Process definition is not executable. Please verify that this is intentional.");
                }
                handleProcessConstraints(bpmnModel, process, errors);
            }
        }
        handleBPMNModelConstraints(bpmnModel, errors);
    }

    protected void handleProcessConstraints(BpmnModel bpmnModel, Process process, List<ValidationError> errors) {
        if (process.getId() != null && process.getId().length() > Constraints.PROCESS_DEFINITION_ID_MAX_LENGTH) {
            addError(errors, Problems.PROCESS_DEFINITION_ID_TOO_LONG, process,
                    "The id of the process definition must not contain more than " + Constraints.PROCESS_DEFINITION_ID_MAX_LENGTH + " characters");
        }
        if (process.getName() != null && process.getName().length() > Constraints.PROCESS_DEFINITION_NAME_MAX_LENGTH) {
            addError(errors, Problems.PROCESS_DEFINITION_NAME_TOO_LONG, process,
                    "The name of the process definition must not contain more than " + Constraints.PROCESS_DEFINITION_NAME_MAX_LENGTH + " characters");
        }
        if (process.getDocumentation() != null && process.getDocumentation().length() > Constraints.PROCESS_DEFINITION_DOCUMENTATION_MAX_LENGTH) {
            addError(errors, Problems.PROCESS_DEFINITION_DOCUMENTATION_TOO_LONG, process,
                    "The documentation of the process definition must not contain more than " + Constraints.PROCESS_DEFINITION_DOCUMENTATION_MAX_LENGTH + " characters");
        }
    }

    protected void handleBPMNModelConstraints(BpmnModel bpmnModel, List<ValidationError> errors) {
        if (bpmnModel.getTargetNamespace() != null && bpmnModel.getTargetNamespace().length() > Constraints.BPMN_MODEL_TARGET_NAMESPACE_MAX_LENGTH) {
            addError(errors, Problems.BPMN_MODEL_TARGET_NAMESPACE_TOO_LONG,
                    "The targetNamespace of the bpmn model must not contain more than " + Constraints.BPMN_MODEL_TARGET_NAMESPACE_MAX_LENGTH + " characters");
        }
    }

    /**
     * Returns 'true' if at least one process definition in the {@link BpmnModel} is executable.
     */
    protected boolean validateAtLeastOneExecutable(BpmnModel bpmnModel, List<ValidationError> errors) {
        int nrOfExecutableDefinitions = 0;
        for (Process process : bpmnModel.getProcesses()) {
            if (process.isExecutable()) {
                nrOfExecutableDefinitions++;
            }
        }

        if (nrOfExecutableDefinitions == 0) {
            addError(errors, Problems.ALL_PROCESS_DEFINITIONS_NOT_EXECUTABLE,
                    "All process definition are set to be non-executable (property 'isExecutable' on process). This is not allowed.");
        }

        return nrOfExecutableDefinitions > 0;
    }

}
