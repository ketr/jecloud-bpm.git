/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.process.validation.validator.impl;

import com.je.bpm.core.model.*;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.core.model.event.BoundaryEvent;
import com.je.bpm.core.model.process.Transaction;
import com.je.bpm.core.process.validation.ValidationError;
import com.je.bpm.core.process.validation.validator.Problems;
import com.je.bpm.core.process.validation.validator.ProcessLevelValidator;

import java.util.HashMap;
import java.util.List;

/**

 */
public class BoundaryEventValidator extends ProcessLevelValidator {

  @Override
  protected void executeValidation(BpmnModel bpmnModel, Process process, List<ValidationError> errors) {
    List<BoundaryEvent> boundaryEvents = process.findFlowElementsOfType(BoundaryEvent.class);

    // Only one boundary event of type 'cancel' can be attached to the same
    // element, so we store the count temporarily here
    HashMap<String, Integer> cancelBoundaryEventsCounts = new HashMap<String, Integer>();

    // Only one boundary event of type 'compensate' can be attached to the
    // same element, so we store the count temporarily here
    HashMap<String, Integer> compensateBoundaryEventsCounts = new HashMap<String, Integer>();

    for (int i = 0; i < boundaryEvents.size(); i++) {

      BoundaryEvent boundaryEvent = boundaryEvents.get(i);

      if (boundaryEvent.getEventDefinitions() != null && !boundaryEvent.getEventDefinitions().isEmpty()) {

        EventDefinition eventDefinition = boundaryEvent.getEventDefinitions().get(0);
        if (!(eventDefinition instanceof TimerEventDefinition) && !(eventDefinition instanceof ErrorEventDefinition) && !(eventDefinition instanceof SignalEventDefinition)
            && !(eventDefinition instanceof CancelEventDefinition) && !(eventDefinition instanceof MessageEventDefinition) && !(eventDefinition instanceof CompensateEventDefinition)) {

          addError(errors, Problems.BOUNDARY_EVENT_INVALID_EVENT_DEFINITION, process, boundaryEvent, "Invalid or unsupported event definition");

        }

        if (eventDefinition instanceof CancelEventDefinition) {

          FlowElement attachedToFlowElement = bpmnModel.getFlowElement(boundaryEvent.getAttachedToRefId());
          if (!(attachedToFlowElement instanceof Transaction)) {
            addError(errors, Problems.BOUNDARY_EVENT_CANCEL_ONLY_ON_TRANSACTION, process, boundaryEvent, "boundary event with cancelEventDefinition only supported on transaction subprocesses");
          } else {
            if (!cancelBoundaryEventsCounts.containsKey(attachedToFlowElement.getId())) {
              cancelBoundaryEventsCounts.put(attachedToFlowElement.getId(), new Integer(0));
            }
            cancelBoundaryEventsCounts.put(attachedToFlowElement.getId(), new Integer(cancelBoundaryEventsCounts.get(attachedToFlowElement.getId()) + 1));
          }

        } else if (eventDefinition instanceof CompensateEventDefinition) {

          if (!compensateBoundaryEventsCounts.containsKey(boundaryEvent.getAttachedToRefId())) {
            compensateBoundaryEventsCounts.put(boundaryEvent.getAttachedToRefId(), new Integer(0));
          }
          compensateBoundaryEventsCounts.put(boundaryEvent.getAttachedToRefId(), compensateBoundaryEventsCounts.get(boundaryEvent.getAttachedToRefId()) + 1);

        } else if (eventDefinition instanceof MessageEventDefinition) {

          // Check if other message boundary events with same message
          // id
          for (int j = 0; j < boundaryEvents.size(); j++) {
            if (j != i) {
              BoundaryEvent otherBoundaryEvent = boundaryEvents.get(j);
              if (otherBoundaryEvent.getAttachedToRefId() != null && otherBoundaryEvent.getAttachedToRefId().equals(boundaryEvent.getAttachedToRefId())) {
                if (otherBoundaryEvent.getEventDefinitions() != null && !otherBoundaryEvent.getEventDefinitions().isEmpty()) {
                  EventDefinition otherEventDefinition = otherBoundaryEvent.getEventDefinitions().get(0);
                  if (otherEventDefinition instanceof MessageEventDefinition) {
                    MessageEventDefinition currentMessageEventDefinition = (MessageEventDefinition) eventDefinition;
                    MessageEventDefinition otherMessageEventDefinition = (MessageEventDefinition) otherEventDefinition;
                    if (otherMessageEventDefinition.getMessageRef() != null && otherMessageEventDefinition.getMessageRef().equals(currentMessageEventDefinition.getMessageRef())) {
                      addError(errors, Problems.MESSAGE_EVENT_MULTIPLE_ON_BOUNDARY_SAME_MESSAGE_ID, process, boundaryEvent, "Multiple message events with same message id not supported");
                    }
                  }
                }
              }
            }

          }

        }

      } else {

        addError(errors, Problems.BOUNDARY_EVENT_NO_EVENT_DEFINITION, process, boundaryEvent, "Event definition is missing from boundary event");

      }
    }

    for (String elementId : cancelBoundaryEventsCounts.keySet()) {
      if (cancelBoundaryEventsCounts.get(elementId) > 1) {
        addError(errors, Problems.BOUNDARY_EVENT_MULTIPLE_CANCEL_ON_TRANSACTION, process, bpmnModel.getFlowElement(elementId),
            "multiple boundary events with cancelEventDefinition not supported on same transaction subprocess.");
      }
    }

    for (String elementId : compensateBoundaryEventsCounts.keySet()) {
      if (compensateBoundaryEventsCounts.get(elementId) > 1) {
        addError(errors, Problems.COMPENSATE_EVENT_MULTIPLE_ON_BOUNDARY, process, bpmnModel.getFlowElement(elementId), "Multiple boundary events of type 'compensate' is invalid");
      }
    }

  }
}
