/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.process.validation.validator.impl;

import com.je.bpm.core.model.*;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.core.process.validation.ValidationError;
import com.je.bpm.core.process.validation.validator.Problems;
import com.je.bpm.core.process.validation.validator.ProcessLevelValidator;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * A validator for stuff that is shared across all flow elements
 */
public class FlowElementValidator extends ProcessLevelValidator {

    protected static final int ID_MAX_LENGTH = 255;

    @Override
    protected void executeValidation(BpmnModel bpmnModel, Process process, List<ValidationError> errors) {
        for (FlowElement flowElement : process.getFlowElements()) {

            if (flowElement instanceof Activity) {
                Activity activity = (Activity) flowElement;
                handleConstraints(process, activity, errors);
                handleMultiInstanceLoopCharacteristics(process, activity, errors);
                handleDataAssociations(process, activity, errors);
            }

        }

    }

    protected void handleConstraints(Process process, Activity activity, List<ValidationError> errors) {
        if (activity.getId() != null && activity.getId().length() > ID_MAX_LENGTH) {
            addError(errors, Problems.FLOW_ELEMENT_ID_TOO_LONG, process, activity,
                    "The id of a flow element must not contain more than " + ID_MAX_LENGTH + " characters");
        }
    }

    protected void handleMultiInstanceLoopCharacteristics(Process process, Activity activity, List<ValidationError> errors) {
        MultiInstanceLoopCharacteristics multiInstanceLoopCharacteristics = activity.getLoopCharacteristics();
        if (multiInstanceLoopCharacteristics != null) {

            if (StringUtils.isEmpty(multiInstanceLoopCharacteristics.getLoopCardinality())
                    && StringUtils.isEmpty(multiInstanceLoopCharacteristics.getInputDataItem())) {

                addError(errors, Problems.MULTI_INSTANCE_MISSING_COLLECTION, process, activity,
                        "Either loopCardinality or loopDataInputRef/activiti:collection must been set");
            }

        }
    }

    protected void handleDataAssociations(Process process, Activity activity, List<ValidationError> errors) {
        if (activity.getDataInputAssociations() != null) {
            for (DataAssociation dataAssociation : activity.getDataInputAssociations()) {
                if (StringUtils.isEmpty(dataAssociation.getTargetRef())) {
                    addError(errors, Problems.DATA_ASSOCIATION_MISSING_TARGETREF, process, activity,
                            "Targetref is required on a data association");
                }
            }
        }
        if (activity.getDataOutputAssociations() != null) {
            for (DataAssociation dataAssociation : activity.getDataOutputAssociations()) {
                if (StringUtils.isEmpty(dataAssociation.getTargetRef())) {
                    addError(errors, Problems.DATA_ASSOCIATION_MISSING_TARGETREF, process, activity,
                            "Targetref is required on a data association");
                }
            }
        }
    }

}
