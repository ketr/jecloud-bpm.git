/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.process.validation.validator;

import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.core.process.validation.ValidationError;

import java.util.List;

public abstract class ValidatorImpl implements Validator {

    public void addError(List<ValidationError> validationErrors, ValidationError error) {
        validationErrors.add(error);
    }

    protected void addError(List<ValidationError> validationErrors, String problem, String description) {
        addError(validationErrors, problem, null, null, description, false);
    }

    protected void addError(List<ValidationError> validationErrors, String problem, BaseElement baseElement, String description) {
        addError(validationErrors, problem, null, baseElement, description);
    }

    protected void addError(List<ValidationError> validationErrors, String problem, Process process, BaseElement baseElement, String description) {
        addError(validationErrors, problem, process, baseElement, description, false);
    }

    protected void addWarning(List<ValidationError> validationErrors, String problem, Process process, BaseElement baseElement, String description) {
        addError(validationErrors, problem, process, baseElement, description, true);
    }

    protected void addError(List<ValidationError> validationErrors, String problem, Process process, BaseElement baseElement, String description, boolean isWarning) {
        ValidationError error = new ValidationError();
        error.setWarning(isWarning);

        if (process != null) {
            error.setProcessDefinitionId(process.getId());
            error.setProcessDefinitionName(process.getName());
        }

        if (baseElement != null) {
            error.setXmlLineNumber(baseElement.getXmlRowNumber());
            error.setXmlColumnNumber(baseElement.getXmlColumnNumber());
        }
        error.setProblem(problem);
        error.setDefaultDescription(description);

        if (baseElement instanceof FlowElement) {
            FlowElement flowElement = (FlowElement) baseElement;
            error.setActivityId(flowElement.getId());
            error.setActivityName(flowElement.getName());
        }

        addError(validationErrors, error);
    }

    protected void addError(List<ValidationError> validationErrors, String problem, Process process, String id, String description) {
        ValidationError error = new ValidationError();

        if (process != null) {
            error.setProcessDefinitionId(process.getId());
            error.setProcessDefinitionName(process.getName());
        }

        error.setProblem(problem);
        error.setDefaultDescription(description);
        error.setActivityId(id);

        addError(validationErrors, error);
    }

}
