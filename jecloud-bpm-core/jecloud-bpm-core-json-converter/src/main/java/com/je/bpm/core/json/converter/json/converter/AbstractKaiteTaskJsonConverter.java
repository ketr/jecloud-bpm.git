/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.json.converter.json.converter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.config.task.TaskBasicConfigImpl;
import com.je.bpm.core.model.config.task.TaskListenerConfigImpl;
import com.je.bpm.core.model.task.KaiteTask;

/**
 * 凯特任务基础json解析器
 */
public abstract class AbstractKaiteTaskJsonConverter extends BaseBpmnJsonConverter {

    @Override
    protected void convertElementToJson(ObjectNode propertiesNode, BaseElement baseElement) {
        KaiteTask userTask = (KaiteTask) baseElement;
        fillUserTaskListenerConfigToJson(userTask.getTaskListenerConfig(), propertiesNode);
        setPropertyValue(PROPERTY_USERTASK_CATEGORY, userTask.getCategory(), propertiesNode);
    }

    /**
     * 任务监听配置
     *
     * @param taskListenerConfig
     * @return
     */
    protected void fillUserTaskListenerConfigToJson(TaskListenerConfigImpl taskListenerConfig, ObjectNode propertiesNode) {
        ArrayNode userTaskListenerArrayNode = objectMapper.createArrayNode();
        ObjectNode eachNode;
        for (TaskListenerConfigImpl.ListenerConfig eachConfig : taskListenerConfig.getListenerConfigs()) {
            eachNode = objectMapper.createObjectNode();
            eachNode.put(PROPERTY_USERTASK_LISTENER_TYPE, eachConfig.getType());
            eachNode.put(PROPERTY_USERTASK_LISTENER_BEANNAME, eachConfig.getBeanName());
            eachNode.put(PROPERTY_USERTASK_LISTENER_STRATEGY, eachConfig.getStategy());
            eachNode.put(PROPERTY_USERTASK_LISTENER_FIELDVALUES, eachConfig.getFieldSetValueStr());
            userTaskListenerArrayNode.add(eachNode);
        }
        propertiesNode.set(PROPERTY_USERTASK_LISTENER_CONFIG, userTaskListenerArrayNode);
    }

    /**
     * 任务监听解析
     *
     * @param elementNode
     * @return
     */
    protected void parseUserTaskJsonToListenerConfig(JsonNode elementNode, KaiteTask task) {
        ArrayNode listenersJsonNode = (ArrayNode) getProperty(PROPERTY_USERTASK_LISTENER_CONFIG, elementNode);
        TaskListenerConfigImpl taskListenerConfig = task.getTaskListenerConfig();
        if (listenersJsonNode == null) {
            return;
        }
        TaskListenerConfigImpl.ListenerConfig eachConfig;
        for (JsonNode eachNode : listenersJsonNode) {
            eachConfig = new TaskListenerConfigImpl.ListenerConfig();
            eachConfig.setFieldSetValueStr(getValueAsString(PROPERTY_USERTASK_LISTENER_FIELDVALUES, eachNode));
            eachConfig.setStategy(getValueAsString(PROPERTY_USERTASK_LISTENER_STRATEGY, eachNode));
            eachConfig.setBeanName(getValueAsString(PROPERTY_USERTASK_LISTENER_BEANNAME, eachNode));
            eachConfig.setType(getValueAsString(PROPERTY_USERTASK_LISTENER_TYPE, eachNode));
            taskListenerConfig.add(eachConfig);
        }
    }

    /**
     * 转换json到element
     *
     * @param elementNode
     * @param kaiteTask
     */
    protected void convertJsonToElement(JsonNode elementNode, KaiteTask kaiteTask) {
        parseUserTaskJsonToListenerConfig(elementNode, kaiteTask);
        parseUserTaskJsonToPropertiesConfig(elementNode, kaiteTask);
    }

    protected void parseUserTaskJsonToPropertiesConfig(JsonNode elementNode, KaiteTask task) {
        JsonNode propertiesNode = elementNode.get(EDITOR_SHAPE_PROPERTIES);
        TaskBasicConfigImpl taskBasicConfig = new TaskBasicConfigImpl();
        taskBasicConfig.setName(getValueAsString(UNIVERSAL_NAME, propertiesNode));
        taskBasicConfig.setFormSchemeName(getValueAsString(PROPERTY_TASK_PROPERTIES_FORMSCHEMENAME, propertiesNode));
        taskBasicConfig.setFormSchemeId(getValueAsString(PROPERTY_TASK_PROPERTIES_FORMSCHEMEID, propertiesNode));
        taskBasicConfig.setListSynchronization(getValueAsBoolean(PROPERTY_TASK_PROPERTIES_LISTSYNCHRONIZATION, propertiesNode));
        taskBasicConfig.setRetrieve(getValueAsBoolean(PROPERTY_TASK_PROPERTIES_RETRIEVE, propertiesNode));
        taskBasicConfig.setUrge(getValueAsBoolean(PROPERTY_TASK_PROPERTIES_URGE, propertiesNode));
        taskBasicConfig.setInvalid(getValueAsBoolean(PROPERTY_TASK_PROPERTIES_INVALID, propertiesNode));
        taskBasicConfig.setDelegate(getValueAsBoolean(PROPERTY_TASK_PROPERTIES_DELEGATE, propertiesNode));
        taskBasicConfig.setTransfer(getValueAsBoolean(PROPERTY_TASK_PROPERTIES_TRANSFER, propertiesNode));
        taskBasicConfig.setFormEditable(getValueAsBoolean(PROPERTY_TASK_PROPERTIES_FORMEDITABLE, propertiesNode));
        taskBasicConfig.setRemind(getValueAsBoolean(PROPERTY_TASK_PROPERTIES_REMIND, propertiesNode));
        taskBasicConfig.setCloseSimpleApproval(getValueAsBoolean(PROPERTY_TASK_PROPERTIES_SIMPLEAPPROVAL, propertiesNode));
        taskBasicConfig.setSelectAll(getValueAsBoolean(PROPERTY_TASK_PROPERTIES_SELECTALL, propertiesNode));
        taskBasicConfig.setAsynTree(getValueAsBoolean(PROPERTY_TASK_PROPERTIES_ASYNTREE, propertiesNode));
        taskBasicConfig.setJump(getValueAsBoolean(PROPERTY_TASK_PROPERTIES_JUMP, propertiesNode));
        taskBasicConfig.setLogicalJudgment(getValueAsBoolean(PROPERTY_TASK_PROPERTIES_LOGICALJUDGMENT, propertiesNode));
        taskBasicConfig.setJudgeEmpty(getValueAsBoolean(PROPERTY_TASK_PROPERTIES_JUDGE_EMPTY, propertiesNode));
        taskBasicConfig.setCountersignNode(getValueAsBoolean(PROPERTY_PROCESS_COUNTERSIGNNODE, propertiesNode));
        taskBasicConfig.setSequential(getValueAsBoolean(PROPERTY_COUNTERSIGNED_SEQUENTIAL, propertiesNode));
        taskBasicConfig.setSecurityEnable(getValueAsBoolean(SECURITY_ENABLE, propertiesNode));
        taskBasicConfig.setSecurityCode(getValueAsString(SECURITY_CODE, propertiesNode));
        taskBasicConfig.setCountersign(getValueAsBoolean(COUNTERSIGN, propertiesNode));
        taskBasicConfig.setMergeApplication(getValueAsBoolean(MERGEAPPLICATION, propertiesNode));
        taskBasicConfig.setSignBack(getValueAsBoolean(SIGN_BACK, propertiesNode));
        taskBasicConfig.setForcedSignatureBack(getValueAsBoolean(FORCED_SIGNATURE_BACK, propertiesNode));
        taskBasicConfig.setStaging(getValueAsBoolean(STAGING, propertiesNode));
        taskBasicConfig.setBasicRuntimeTuning(getValueAsBoolean(BASIC_RUNTIME_TUNING, propertiesNode));
        taskBasicConfig.setCs(getValueAsBoolean(TASK_IS_CS, propertiesNode));
        taskBasicConfig.setCreateCsUserId(getValueAsString(TASK_CREATE_CS_USERID, propertiesNode));
        taskBasicConfig.setInitiatorCanUrged(getValueAsBoolean(PROPERTY_PROCESS_INITIATOR_CANURGED, propertiesNode));
        taskBasicConfig.setInitiatorCanInvalid(getValueAsBoolean(PROPERTY_PROCESS_INITIATOR_CANINVALID, propertiesNode));
        taskBasicConfig.setInitiatorCanCancel(getValueAsBoolean(PROPERTY_PROCESS_INITIATOR_CANCANCEL, propertiesNode));
        task.setTaskBasicConfig(taskBasicConfig);
    }

}
