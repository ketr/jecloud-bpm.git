/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.json.converter.json.converter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.je.bpm.core.model.*;
import com.je.bpm.core.model.event.EndEvent;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

public class EndEventJsonConverter extends BaseBpmnJsonConverter {

    public static void fillTypes(Map<String, Class<? extends BaseBpmnJsonConverter>> convertersToBpmnMap,
                                 Map<Class<? extends BaseElement>,
                                         Class<? extends BaseBpmnJsonConverter>> convertersToJsonMap) {
        fillJsonTypes(convertersToBpmnMap);
        fillBpmnTypes(convertersToJsonMap);
    }

    public static void fillJsonTypes(Map<String, Class<? extends BaseBpmnJsonConverter>> convertersToBpmnMap) {
        convertersToBpmnMap.put(STENCIL_EVENT_END_NONE, EndEventJsonConverter.class);
        convertersToBpmnMap.put(STENCIL_EVENT_END_ERROR, EndEventJsonConverter.class);
        convertersToBpmnMap.put(STENCIL_EVENT_END_CANCEL, EndEventJsonConverter.class);
        convertersToBpmnMap.put(STENCIL_EVENT_END_TERMINATE, EndEventJsonConverter.class);
    }

    public static void fillBpmnTypes(Map<Class<? extends BaseElement>, Class<? extends BaseBpmnJsonConverter>> convertersToJsonMap) {
        convertersToJsonMap.put(EndEvent.class, EndEventJsonConverter.class);
    }

    @Override
    protected String getStencilId(BaseElement baseElement) {
        EndEvent endEvent = (EndEvent) baseElement;
        List<EventDefinition> eventDefinitions = endEvent.getEventDefinitions();
        if (eventDefinitions.size() != 1) {
            return STENCIL_EVENT_END_NONE;
        }

        EventDefinition eventDefinition = eventDefinitions.get(0);
        if (eventDefinition instanceof ErrorEventDefinition) {
            return STENCIL_EVENT_END_ERROR;
        } else if (eventDefinition instanceof CancelEventDefinition) {
            return STENCIL_EVENT_END_CANCEL;
        } else if (eventDefinition instanceof TerminateEventDefinition) {
            return STENCIL_EVENT_END_TERMINATE;
        } else {
            return STENCIL_EVENT_END_NONE;
        }
    }

    @Override
    protected void convertElementToJson(ObjectNode propertiesNode, BaseElement baseElement) {
        EndEvent endEvent = (EndEvent) baseElement;
        addEventProperties(endEvent, propertiesNode);
    }

    @Override
    protected FlowElement convertJsonToElement(JsonNode elementNode, JsonNode modelNode, Map<String, JsonNode> shapeMap) {
        EndEvent endEvent = new EndEvent();
        String stencilId = BpmnJsonConverterUtil.getStencilId(elementNode);
        if (STENCIL_EVENT_END_ERROR.equals(stencilId)) {
            convertJsonToErrorDefinition(elementNode, endEvent);
        } else if (STENCIL_EVENT_END_CANCEL.equals(stencilId)) {
            CancelEventDefinition eventDefinition = new CancelEventDefinition();
            endEvent.getEventDefinitions().add(eventDefinition);
        } else if (STENCIL_EVENT_END_TERMINATE.equals(stencilId)) {
            TerminateEventDefinition eventDefinition = new TerminateEventDefinition();

            String terminateAllStringValue = getPropertyValueAsString(PROPERTY_TERMINATE_ALL, elementNode);
            if (StringUtils.isNotEmpty(terminateAllStringValue)) {
                eventDefinition.setTerminateAll("true".equals(terminateAllStringValue));
            }

            String terminateMiStringValue = getPropertyValueAsString(PROPERTY_TERMINATE_MULTI_INSTANCE, elementNode);
            if (StringUtils.isNotEmpty(terminateMiStringValue)) {
                eventDefinition.setTerminateMultiInstance("true".equals(terminateMiStringValue));
            }

            endEvent.getEventDefinitions().add(eventDefinition);
        }
        parseJsonToTaskFormBasicConfig(elementNode, endEvent);
        return endEvent;
    }

}
