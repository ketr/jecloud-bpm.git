/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter;

import com.google.common.base.Strings;
import com.je.bpm.core.converter.constants.BpmnXMLConstants;
import com.je.bpm.core.converter.converter.child.BaseChildElementParser;
import com.je.bpm.core.converter.converter.export.ActivitiListenerExport;
import com.je.bpm.core.converter.converter.export.FailedJobRetryCountExport;
import com.je.bpm.core.converter.converter.export.MultiInstanceExport;
import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.*;
import com.je.bpm.core.model.artifact.Artifact;
import com.je.bpm.core.model.config.task.TaskFormBasicConfigImpl;
import com.je.bpm.core.model.config.task.TaskFormButtonConfigImpl;
import com.je.bpm.core.model.config.task.TaskFormChildFuncConfigImpl;
import com.je.bpm.core.model.config.task.TaskFormFieldConfigImpl;
import com.je.bpm.core.model.event.Event;
import com.je.bpm.core.model.event.StartEvent;
import com.je.bpm.core.model.event.ThrowEvent;
import com.je.bpm.core.model.gateway.Gateway;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.core.model.process.SubProcess;
import com.je.bpm.core.model.task.UserTask;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

public abstract class BaseBpmnXMLConverter implements BpmnXMLConstants {

    protected static final Logger LOGGER = LoggerFactory.getLogger(BaseBpmnXMLConverter.class);

    /**
     * 默认元素属性
     */
    protected static final List<ExtensionAttribute> defaultElementAttributes = asList(new ExtensionAttribute(ATTRIBUTE_ID), new ExtensionAttribute(ATTRIBUTE_NAME));

    /**
     * 默认活动属性
     */
    protected static final List<ExtensionAttribute> defaultActivityAttributes = asList(new ExtensionAttribute(ACTIVITI_EXTENSIONS_NAMESPACE, ATTRIBUTE_ACTIVITY_ASYNCHRONOUS),
            new ExtensionAttribute(ACTIVITI_EXTENSIONS_NAMESPACE, ATTRIBUTE_ACTIVITY_EXCLUSIVE),
            new ExtensionAttribute(ATTRIBUTE_DEFAULT),
            new ExtensionAttribute(ACTIVITI_EXTENSIONS_NAMESPACE, ATTRIBUTE_ACTIVITY_ISFORCOMPENSATION));

    /**
     * XML转换为BPMNModel
     *
     * @param xtr
     * @param model
     * @param activeProcess
     * @param activeSubProcessList
     * @throws Exception
     */
    public void convertToBpmnModel(XMLStreamReader xtr, BpmnModel model, Process activeProcess, List<SubProcess> activeSubProcessList) throws Exception {
        String elementId = xtr.getAttributeValue(null, ATTRIBUTE_ID);
        String elementName = xtr.getAttributeValue(null, ATTRIBUTE_NAME);
        boolean async = parseAsync(xtr);
        boolean notExclusive = parseNotExclusive(xtr);
        String defaultFlow = xtr.getAttributeValue(null, ATTRIBUTE_DEFAULT);
        boolean isForCompensation = parseForCompensation(xtr);
        //子类需要实现的方法
        BaseElement parsedElement = convertXMLToElement(xtr, model);

        if (parsedElement instanceof Artifact) {
            Artifact currentArtifact = (Artifact) parsedElement;
            currentArtifact.setId(elementId);
            if (!activeSubProcessList.isEmpty()) {
                activeSubProcessList.get(activeSubProcessList.size() - 1).addArtifact(currentArtifact);
            } else {
                activeProcess.addArtifact(currentArtifact);
            }
        }

        if (parsedElement instanceof FlowElement) {
            FlowElement currentFlowElement = (FlowElement) parsedElement;
            currentFlowElement.setId(elementId);
            currentFlowElement.setName(elementName);
            if (currentFlowElement instanceof FlowNode) {
                FlowNode flowNode = (FlowNode) currentFlowElement;
                flowNode.setAsynchronous(async);
                flowNode.setNotExclusive(notExclusive);
                if (currentFlowElement instanceof Activity) {
                    Activity activity = (Activity) currentFlowElement;
                    activity.setForCompensation(isForCompensation);
                    if (StringUtils.isNotEmpty(defaultFlow)) {
                        activity.setDefaultFlow(defaultFlow);
                    }
                }
                if (currentFlowElement instanceof Gateway) {
                    Gateway gateway = (Gateway) currentFlowElement;
                    if (StringUtils.isNotEmpty(defaultFlow)) {
                        gateway.setDefaultFlow(defaultFlow);
                    }
                }
            }

            if (currentFlowElement instanceof DataObject) {
                if (!activeSubProcessList.isEmpty()) {
                    SubProcess subProcess = activeSubProcessList.get(activeSubProcessList.size() - 1);
                    subProcess.getDataObjects().add((ValuedDataObject) parsedElement);
                } else {
                    activeProcess.getDataObjects().add((ValuedDataObject) parsedElement);
                }
            }

            if (!activeSubProcessList.isEmpty()) {
                SubProcess subProcess = activeSubProcessList.get(activeSubProcessList.size() - 1);
                subProcess.addFlowElement(currentFlowElement);
            } else {
                activeProcess.addFlowElement(currentFlowElement);
            }
        }
    }

    protected void writeFormBasicConfig(TaskFormBasicConfigImpl taskFormBasicConfig, XMLStreamWriter xtw) throws Exception {
        //表单字段设置值配置
        Map<String, String> taskFormFieldSetValueConfig = taskFormBasicConfig.getTaskFormFieldSetValueConfig();
        xtw.writeStartElement(BPMN2_PREFIX, PROPERTY_USERTASK_FORM_FIELD_ASSIGNMENT_CONFIG, BPMN2_NAMESPACE);
        for (String key : taskFormFieldSetValueConfig.keySet()) {
            xtw.writeStartElement(BPMN2_PREFIX, PROPERTY_USERTASK_FORM_FIELD_ASSIGNMENT, BPMN2_NAMESPACE);
            xtw.writeAttribute(PROPERTY_USERTASK_FORM_FIELD_ASSIGNMENT_CODE, key);
            xtw.writeAttribute(PROPERTY_USERTASK_FORM_FIELD_ASSIGNMENT_VALUE, taskFormFieldSetValueConfig.get(key));
            xtw.writeEndElement();
        }
        xtw.writeEndElement();

        //表单字段配置
        TaskFormFieldConfigImpl taskFormFieldConfig = taskFormBasicConfig.getTaskFormFieldConfig();
        xtw.writeStartElement(BPMN2_PREFIX, PROPERTY_USERTASK_FORM_FIELD_CONTROL_CONFIG, BPMN2_NAMESPACE);
        for (TaskFormFieldConfigImpl.FormFieldConfig formFieldConfig : taskFormFieldConfig.getFormFields()) {
            xtw.writeStartElement(BPMN2_PREFIX, PROPERTY_USERTASK_FORM_FIELD_CONTROL, BPMN2_NAMESPACE);
            xtw.writeAttribute(PROPERTY_USERTASK_FORM_FIELD_CONTROL_CODE, formFieldConfig.getCode());
            xtw.writeAttribute(PROPERTY_USERTASK_FORM_FIELD_CONTROL_EDITABLE, formFieldConfig.getEditable() ? ATTRIBUTE_VALUE_TRUE : ATTRIBUTE_VALUE_FALSE);
            xtw.writeAttribute(PROPERTY_USERTASK_FORM_FIELD_CONTROL_READONLY, formFieldConfig.getReadonly() ? ATTRIBUTE_VALUE_TRUE : ATTRIBUTE_VALUE_FALSE);
            xtw.writeAttribute(PROPERTY_USERTASK_FORM_FIELD_CONTROL_HIDDEN, formFieldConfig.getHidden() ? ATTRIBUTE_VALUE_TRUE : ATTRIBUTE_VALUE_FALSE);
            xtw.writeAttribute(PROPERTY_USERTASK_FORM_FIELD_CONTROL_DISPLAY, formFieldConfig.getDisplay() ? ATTRIBUTE_VALUE_TRUE : ATTRIBUTE_VALUE_FALSE);
            xtw.writeAttribute(PROPERTY_USERTASK_FORM_FIELD_CONTROL_REQUIRED, formFieldConfig.getRequired() ? ATTRIBUTE_VALUE_TRUE : ATTRIBUTE_VALUE_FALSE);
            xtw.writeEndElement();
        }
        xtw.writeEndElement();

        //表单按钮配置
        TaskFormButtonConfigImpl taskFormButtonConfig = taskFormBasicConfig.getTaskFormButtonConfig();
        xtw.writeStartElement(BPMN2_PREFIX, PROPERTY_USERTASK_FORM_TASKFORMBUTTON_CONFIG, BPMN2_NAMESPACE);
        for (TaskFormButtonConfigImpl.ButtonConfig buttonConfig : taskFormButtonConfig.getButtonConfigs()) {
            xtw.writeStartElement(BPMN2_PREFIX, PROPERTY_USERTASK_FORM_TASKFORMBUTTON, BPMN2_NAMESPACE);
            xtw.writeAttribute(PROPERTY_USERTASK_FORM_TASKFORMBUTTON_CODE, buttonConfig.getCode());
            xtw.writeAttribute(ATTRIBUTE_ENABLE, buttonConfig.getEnable() ? ATTRIBUTE_VALUE_TRUE : ATTRIBUTE_VALUE_FALSE);
            xtw.writeEndElement();
        }
        xtw.writeEndElement();

        //表单子功能配置
        TaskFormChildFuncConfigImpl taskFormChildFuncConfig = taskFormBasicConfig.getTaskFormChildFuncConfig();
        xtw.writeStartElement(BPMN2_PREFIX, PROPERTY_USERTASK_FORM_TASKCHILDFUNC_CONFIG, BPMN2_NAMESPACE);
        for (TaskFormChildFuncConfigImpl.ChildFuncConfig childFuncConfig : taskFormChildFuncConfig.getChildFuncConfigs()) {
            xtw.writeStartElement(BPMN2_PREFIX, PROPERTY_USERTASK_FORM_TASKCHILDFUNC, BPMN2_NAMESPACE);
            xtw.writeAttribute(PROPERTY_USERTASK_FORM_TASKCHILDFUNC_CODE, childFuncConfig.getCode());
            xtw.writeAttribute(PROPERTY_USERTASK_FORM_TASKCHILDFUNC_EDITABLE, childFuncConfig.getEditable() ? ATTRIBUTE_VALUE_TRUE : ATTRIBUTE_VALUE_FALSE);
            xtw.writeAttribute(PROPERTY_USERTASK_FORM_TASKCHILDFUNC_HIDDEN, childFuncConfig.getHidden() ? ATTRIBUTE_VALUE_TRUE : ATTRIBUTE_VALUE_FALSE);
            xtw.writeAttribute(PROPERTY_USERTASK_FORM_TASKCHILDFUNC_DISPLAY, childFuncConfig.getDisplay() ? ATTRIBUTE_VALUE_TRUE : ATTRIBUTE_VALUE_FALSE);
            xtw.writeEndElement();
        }
        xtw.writeEndElement();
    }

    /**
     * BPMNModel转换为xml
     *
     * @param xtw
     * @param baseElement
     * @param model
     * @throws Exception
     */
    public void convertToXML(XMLStreamWriter xtw, BaseElement baseElement, BpmnModel model) throws Exception {
        xtw.writeStartElement(BPMN2_PREFIX, getXMLElementName(), BPMN2_NAMESPACE);
        boolean didWriteExtensionStartElement = false;
        writeDefaultAttribute(ATTRIBUTE_ID, baseElement.getId(), xtw);

        if (baseElement instanceof FlowElement) {
            writeDefaultAttribute(ATTRIBUTE_NAME, ((FlowElement) baseElement).getName(), xtw);
        }

        if (baseElement instanceof FlowNode) {
            final FlowNode flowNode = (FlowNode) baseElement;
            if (flowNode.isAsynchronous()) {
                writeQualifiedAttribute(ATTRIBUTE_ACTIVITY_ASYNCHRONOUS, ATTRIBUTE_VALUE_TRUE, xtw);
                if (flowNode.isNotExclusive()) {
                    writeQualifiedAttribute(ATTRIBUTE_ACTIVITY_EXCLUSIVE, ATTRIBUTE_VALUE_FALSE, xtw);
                }
            }

            if (baseElement instanceof Activity) {
                final Activity activity = (Activity) baseElement;
                if (activity.isForCompensation()) {
                    writeDefaultAttribute(ATTRIBUTE_ACTIVITY_ISFORCOMPENSATION, ATTRIBUTE_VALUE_TRUE, xtw);
                }
                if (StringUtils.isNotEmpty(activity.getDefaultFlow())) {
                    FlowElement defaultFlowElement = model.getFlowElement(activity.getDefaultFlow());
                    if (defaultFlowElement instanceof SequenceFlow) {
                        writeDefaultAttribute(ATTRIBUTE_DEFAULT, activity.getDefaultFlow(), xtw);
                    }
                }
            }

            if (baseElement instanceof Gateway) {
                final Gateway gateway = (Gateway) baseElement;
                if (StringUtils.isNotEmpty(gateway.getDefaultFlow())) {
                    FlowElement defaultFlowElement = model.getFlowElement(gateway.getDefaultFlow());
                    if (defaultFlowElement instanceof SequenceFlow) {
                        writeDefaultAttribute(ATTRIBUTE_DEFAULT, gateway.getDefaultFlow(), xtw);
                    }
                }
            }
        }

        writeAdditionalAttributes(baseElement, model, xtw);

        if (baseElement instanceof FlowElement) {
            final FlowElement flowElement = (FlowElement) baseElement;
            if (StringUtils.isNotEmpty(flowElement.getDocumentation())) {
                xtw.writeStartElement(BPMN2_PREFIX, ELEMENT_DOCUMENTATION, BPMN2_NAMESPACE);
                xtw.writeCharacters(flowElement.getDocumentation());
                xtw.writeEndElement();
            }
        }

        didWriteExtensionStartElement = writeExtensionChildElements(baseElement, didWriteExtensionStartElement, xtw);
        didWriteExtensionStartElement = writeListeners(baseElement, didWriteExtensionStartElement, xtw);
        didWriteExtensionStartElement = BpmnXMLUtil.writeExtensionElements(baseElement, didWriteExtensionStartElement, model.getNamespaces(), xtw);
        if (baseElement instanceof Activity) {
            final Activity activity = (Activity) baseElement;
            FailedJobRetryCountExport.writeFailedJobRetryCount(activity, xtw);
        }

        if (didWriteExtensionStartElement) {
            xtw.writeEndElement();
        }

        writeIncomingOutgoingFlowElements(baseElement, model, xtw);
        if (baseElement instanceof Activity) {
            final Activity activity = (Activity) baseElement;
            MultiInstanceExport.writeMultiInstance(activity, xtw);
        }

        writeAdditionalChildElements(baseElement, model, xtw);
        xtw.writeEndElement();
    }

    protected abstract Class<? extends BaseElement> getBpmnElementType();

    protected abstract BaseElement convertXMLToElement(XMLStreamReader xtr, BpmnModel model) throws Exception;

    protected abstract String getXMLElementName();

    protected abstract void writeAdditionalAttributes(BaseElement element, BpmnModel model, XMLStreamWriter xtw) throws Exception;

    protected boolean writeExtensionChildElements(BaseElement element, boolean didWriteExtensionStartElement, XMLStreamWriter xtw) throws Exception {
        return didWriteExtensionStartElement;
    }

    protected abstract void writeAdditionalChildElements(BaseElement element, BpmnModel model, XMLStreamWriter xtw) throws Exception;

    // To BpmnModel converter convenience methods
    protected void parseChildElements(String elementName, BaseElement parentElement, BpmnModel model, XMLStreamReader xtr) throws Exception {
        parseChildElements(elementName, parentElement, null, model, xtr);
    }

    protected void parseChildElements(String elementName, BaseElement parentElement, Map<String, BaseChildElementParser> additionalParsers, BpmnModel model, XMLStreamReader xtr) throws Exception {
        Map<String, BaseChildElementParser> childParsers = new HashMap();
        if (additionalParsers != null) {
            childParsers.putAll(additionalParsers);
        }
        BpmnXMLUtil.parseChildElements(elementName, parentElement, xtr, childParsers, model);
    }

    @SuppressWarnings("unchecked")
    protected ExtensionElement parseExtensionElement(XMLStreamReader xtr) throws Exception {
        ExtensionElement extensionElement = new ExtensionElement();
        extensionElement.setName(xtr.getLocalName());
        if (StringUtils.isNotEmpty(xtr.getNamespaceURI())) {
            extensionElement.setNamespace(xtr.getNamespaceURI());
        }
        if (StringUtils.isNotEmpty(xtr.getPrefix())) {
            extensionElement.setNamespacePrefix(xtr.getPrefix());
        }

        BpmnXMLUtil.addCustomAttributes(xtr, extensionElement, defaultElementAttributes);

        boolean readyWithExtensionElement = false;
        while (!readyWithExtensionElement && xtr.hasNext()) {
            xtr.next();
            if (xtr.isCharacters() || XMLStreamReader.CDATA == xtr.getEventType()) {
                if (StringUtils.isNotEmpty(xtr.getText().trim())) {
                    extensionElement.setElementText(xtr.getText().trim());
                }
            } else if (xtr.isStartElement()) {
                ExtensionElement childExtensionElement = parseExtensionElement(xtr);
                extensionElement.addChildElement(childExtensionElement);
            } else if (xtr.isEndElement() && extensionElement.getName().equalsIgnoreCase(xtr.getLocalName())) {
                readyWithExtensionElement = true;
            }
        }
        return extensionElement;
    }

    protected boolean parseAsync(XMLStreamReader xtr) {
        boolean async = false;
        String asyncString = xtr.getAttributeValue(ACTIVITI_EXTENSIONS_NAMESPACE, ATTRIBUTE_ACTIVITY_ASYNCHRONOUS);
        if (ATTRIBUTE_VALUE_TRUE.equalsIgnoreCase(asyncString)) {
            async = true;
        }
        return async;
    }

    protected boolean parseNotExclusive(XMLStreamReader xtr) {
        boolean notExclusive = false;
        String exclusiveString = xtr.getAttributeValue(ACTIVITI_EXTENSIONS_NAMESPACE, ATTRIBUTE_ACTIVITY_EXCLUSIVE);
        if (ATTRIBUTE_VALUE_FALSE.equalsIgnoreCase(exclusiveString)) {
            notExclusive = true;
        }
        return notExclusive;
    }

    protected boolean parseForCompensation(XMLStreamReader xtr) {
        boolean isForCompensation = false;
        String compensationString = xtr.getAttributeValue(null, ATTRIBUTE_ACTIVITY_ISFORCOMPENSATION);
        if (ATTRIBUTE_VALUE_TRUE.equalsIgnoreCase(compensationString)) {
            isForCompensation = true;
        }
        return isForCompensation;
    }

    protected List<String> parseDelimitedList(String expression) {
        return BpmnXMLUtil.parseDelimitedList(expression);
    }

    // To XML converter convenience methods

    protected String convertToDelimitedString(List<String> stringList) {
        return BpmnXMLUtil.convertToDelimitedString(stringList);
    }

    protected boolean writeFormProperties(FlowElement flowElement, boolean didWriteExtensionStartElement, XMLStreamWriter xtw) throws Exception {
        List<FormProperty> propertyList = null;
        if (flowElement instanceof UserTask) {
            propertyList = ((UserTask) flowElement).getFormProperties();
        } else if (flowElement instanceof StartEvent) {
            propertyList = ((StartEvent) flowElement).getFormProperties();
        }

        if (propertyList != null) {

            for (FormProperty property : propertyList) {

                if (StringUtils.isNotEmpty(property.getId())) {

                    if (!didWriteExtensionStartElement) {
                        xtw.writeStartElement(ELEMENT_EXTENSIONS);
                        didWriteExtensionStartElement = true;
                    }

                    xtw.writeStartElement(ACTIVITI_EXTENSIONS_PREFIX, ELEMENT_FORMPROPERTY, ACTIVITI_EXTENSIONS_NAMESPACE);
                    writeDefaultAttribute(ATTRIBUTE_FORM_ID, property.getId(), xtw);

                    writeDefaultAttribute(ATTRIBUTE_FORM_NAME, property.getName(), xtw);
                    writeDefaultAttribute(ATTRIBUTE_FORM_TYPE, property.getType(), xtw);
                    writeDefaultAttribute(ATTRIBUTE_FORM_EXPRESSION, property.getExpression(), xtw);
                    writeDefaultAttribute(ATTRIBUTE_FORM_VARIABLE, property.getVariable(), xtw);
                    writeDefaultAttribute(ATTRIBUTE_FORM_DEFAULT, property.getDefaultExpression(), xtw);
                    writeDefaultAttribute(ATTRIBUTE_FORM_DATEPATTERN, property.getDatePattern(), xtw);
                    if (!property.isReadable()) {
                        writeDefaultAttribute(ATTRIBUTE_FORM_READABLE, ATTRIBUTE_VALUE_FALSE, xtw);
                    }
                    if (!property.isWriteable()) {
                        writeDefaultAttribute(ATTRIBUTE_FORM_WRITABLE, ATTRIBUTE_VALUE_FALSE, xtw);
                    }
                    if (property.isRequired()) {
                        writeDefaultAttribute(ATTRIBUTE_FORM_REQUIRED, ATTRIBUTE_VALUE_TRUE, xtw);
                    }

                    for (FormValue formValue : property.getFormValues()) {
                        if (StringUtils.isNotEmpty(formValue.getId())) {
                            xtw.writeStartElement(ACTIVITI_EXTENSIONS_PREFIX, ELEMENT_VALUE, ACTIVITI_EXTENSIONS_NAMESPACE);
                            xtw.writeAttribute(ATTRIBUTE_ID, formValue.getId());
                            xtw.writeAttribute(ATTRIBUTE_NAME, formValue.getName());
                            xtw.writeEndElement();
                        }
                    }

                    xtw.writeEndElement();
                }
            }
        }

        return didWriteExtensionStartElement;
    }

    protected boolean writeListeners(BaseElement element, boolean didWriteExtensionStartElement, XMLStreamWriter xtw) throws Exception {
        return ActivitiListenerExport.writeListeners(element, didWriteExtensionStartElement, xtw);
    }

    protected void writeEventDefinitions(Event parentEvent, List<EventDefinition> eventDefinitions, BpmnModel model, XMLStreamWriter xtw) throws Exception {
        for (EventDefinition eventDefinition : eventDefinitions) {
            if (eventDefinition instanceof TimerEventDefinition) {
                writeTimerDefinition(parentEvent, (TimerEventDefinition) eventDefinition, xtw);
            } else if (eventDefinition instanceof SignalEventDefinition) {
                writeSignalDefinition(parentEvent, (SignalEventDefinition) eventDefinition, xtw);
            } else if (eventDefinition instanceof MessageEventDefinition) {
                writeMessageDefinition(parentEvent, (MessageEventDefinition) eventDefinition, model, xtw);
            } else if (eventDefinition instanceof ErrorEventDefinition) {
                writeErrorDefinition(parentEvent, (ErrorEventDefinition) eventDefinition, xtw);
            } else if (eventDefinition instanceof TerminateEventDefinition) {
                writeTerminateDefinition(parentEvent, (TerminateEventDefinition) eventDefinition, xtw);
            } else if (eventDefinition instanceof CancelEventDefinition) {
                writeCancelDefinition(parentEvent, (CancelEventDefinition) eventDefinition, xtw);
            } else if (eventDefinition instanceof CompensateEventDefinition) {
                writeCompensateDefinition(parentEvent, (CompensateEventDefinition) eventDefinition, xtw);
            }
        }
    }

    protected void writeTimerDefinition(Event parentEvent, TimerEventDefinition timerDefinition, XMLStreamWriter xtw) throws Exception {
        xtw.writeStartElement(ELEMENT_EVENT_TIMERDEFINITION);
        if (StringUtils.isNotEmpty(timerDefinition.getCalendarName())) {
            writeQualifiedAttribute(ATTRIBUTE_CALENDAR_NAME, timerDefinition.getCalendarName(), xtw);
        }
        boolean didWriteExtensionStartElement = BpmnXMLUtil.writeExtensionElements(timerDefinition, false, xtw);
        if (didWriteExtensionStartElement) {
            xtw.writeEndElement();
        }
        if (StringUtils.isNotEmpty(timerDefinition.getTimeDate())) {
            xtw.writeStartElement(ATTRIBUTE_TIMER_DATE);
            xtw.writeCharacters(timerDefinition.getTimeDate());
            xtw.writeEndElement();

        } else if (StringUtils.isNotEmpty(timerDefinition.getTimeCycle())) {
            xtw.writeStartElement(ATTRIBUTE_TIMER_CYCLE);

            if (StringUtils.isNotEmpty(timerDefinition.getEndDate())) {
                xtw.writeAttribute(ACTIVITI_EXTENSIONS_PREFIX, ACTIVITI_EXTENSIONS_NAMESPACE, ATTRIBUTE_END_DATE, timerDefinition.getEndDate());
            }

            xtw.writeCharacters(timerDefinition.getTimeCycle());
            xtw.writeEndElement();

        } else if (StringUtils.isNotEmpty(timerDefinition.getTimeDuration())) {
            xtw.writeStartElement(ATTRIBUTE_TIMER_DURATION);
            xtw.writeCharacters(timerDefinition.getTimeDuration());
            xtw.writeEndElement();
        }

        xtw.writeEndElement();
    }

    protected void writeSignalDefinition(Event parentEvent, SignalEventDefinition signalDefinition, XMLStreamWriter xtw) throws Exception {
        xtw.writeStartElement(ELEMENT_EVENT_SIGNALDEFINITION);
        writeDefaultAttribute(ATTRIBUTE_SIGNAL_REF, signalDefinition.getSignalRef(), xtw);
        if (parentEvent instanceof ThrowEvent && signalDefinition.isAsync()) {
            BpmnXMLUtil.writeQualifiedAttribute(ATTRIBUTE_ACTIVITY_ASYNCHRONOUS, "true", xtw);
        }
        boolean didWriteExtensionStartElement = BpmnXMLUtil.writeExtensionElements(signalDefinition, false, xtw);
        if (didWriteExtensionStartElement) {
            xtw.writeEndElement();
        }
        xtw.writeEndElement();
    }

    protected void writeCancelDefinition(Event parentEvent, CancelEventDefinition cancelEventDefinition, XMLStreamWriter xtw) throws Exception {
        xtw.writeStartElement(ELEMENT_EVENT_CANCELDEFINITION);
        boolean didWriteExtensionStartElement = BpmnXMLUtil.writeExtensionElements(cancelEventDefinition, false, xtw);
        if (didWriteExtensionStartElement) {
            xtw.writeEndElement();
        }
        xtw.writeEndElement();
    }

    protected void writeCompensateDefinition(Event parentEvent, CompensateEventDefinition compensateEventDefinition, XMLStreamWriter xtw) throws Exception {
        xtw.writeStartElement(ELEMENT_EVENT_COMPENSATEDEFINITION);
        writeDefaultAttribute(ATTRIBUTE_COMPENSATE_ACTIVITYREF, compensateEventDefinition.getActivityRef(), xtw);
        boolean didWriteExtensionStartElement = BpmnXMLUtil.writeExtensionElements(compensateEventDefinition, false, xtw);
        if (didWriteExtensionStartElement) {
            xtw.writeEndElement();
        }
        xtw.writeEndElement();
    }

    protected void writeMessageDefinition(Event parentEvent, MessageEventDefinition messageDefinition, BpmnModel model, XMLStreamWriter xtw) throws Exception {
        xtw.writeStartElement(ELEMENT_EVENT_MESSAGEDEFINITION);
        String messageRef = messageDefinition.getMessageRef();
        if (StringUtils.isNotEmpty(messageRef)) {
            // remove the namespace from the message id if set
            if (messageRef.startsWith(model.getTargetNamespace())) {
                messageRef = messageRef.replace(model.getTargetNamespace(), "");
                messageRef = messageRef.replaceFirst(":", "");
            } else {
                for (String prefix : model.getNamespaces().keySet()) {
                    String namespace = model.getNamespace(prefix);
                    if (messageRef.startsWith(namespace)) {
                        messageRef = messageRef.replace(model.getTargetNamespace(), "");
                        messageRef = prefix + messageRef;
                    }
                }
            }
        }
        writeDefaultAttribute(ATTRIBUTE_MESSAGE_REF, messageRef, xtw);

        if (StringUtils.isNotEmpty(messageDefinition.getCorrelationKey())) {
            xtw.writeAttribute(ACTIVITI_EXTENSIONS_PREFIX,
                    ACTIVITI_EXTENSIONS_NAMESPACE,
                    ATTRIBUTE_MESSAGE_CORRELATION_KEY,
                    messageDefinition.getCorrelationKey());
        }

        if (StringUtils.isNotEmpty(messageDefinition.getMessageExpression())) {
            xtw.writeAttribute(ACTIVITI_EXTENSIONS_PREFIX,
                    ACTIVITI_EXTENSIONS_NAMESPACE,
                    ATTRIBUTE_MESSAGE_EXPRESSION,
                    messageDefinition.getMessageExpression());
        }

        boolean didWriteExtensionStartElement = BpmnXMLUtil.writeExtensionElements(messageDefinition, false, xtw);
        if (didWriteExtensionStartElement) {
            xtw.writeEndElement();
        }
        xtw.writeEndElement();
    }

    protected void writeErrorDefinition(Event parentEvent, ErrorEventDefinition errorDefinition, XMLStreamWriter xtw) throws Exception {
        xtw.writeStartElement(ELEMENT_EVENT_ERRORDEFINITION);
        writeDefaultAttribute(ATTRIBUTE_ERROR_REF, errorDefinition.getErrorRef(), xtw);
        boolean didWriteExtensionStartElement = BpmnXMLUtil.writeExtensionElements(errorDefinition, false, xtw);
        if (didWriteExtensionStartElement) {
            xtw.writeEndElement();
        }
        xtw.writeEndElement();
    }

    protected void writeTerminateDefinition(Event parentEvent, TerminateEventDefinition terminateDefinition, XMLStreamWriter xtw) throws Exception {
        xtw.writeStartElement(ELEMENT_EVENT_TERMINATEDEFINITION);

        if (terminateDefinition.isTerminateAll()) {
            writeQualifiedAttribute(ATTRIBUTE_TERMINATE_ALL, "true", xtw);
        }

        if (terminateDefinition.isTerminateMultiInstance()) {
            writeQualifiedAttribute(ATTRIBUTE_TERMINATE_MULTI_INSTANCE, "true", xtw);
        }

        boolean didWriteExtensionStartElement = BpmnXMLUtil.writeExtensionElements(terminateDefinition, false, xtw);
        if (didWriteExtensionStartElement) {
            xtw.writeEndElement();
        }
        xtw.writeEndElement();
    }

    protected void writeDefaultAttribute(String attributeName, String value, XMLStreamWriter xtw) throws Exception {
        BpmnXMLUtil.writeDefaultAttribute(attributeName, value, xtw);
    }

    protected void writeQualifiedAttribute(String attributeName, String value, XMLStreamWriter xtw) throws Exception {
        BpmnXMLUtil.writeQualifiedAttribute(attributeName, value, xtw);
    }

    protected void writeIncomingOutgoingFlowElements(BaseElement element, BpmnModel model, XMLStreamWriter xtw) throws Exception {
        if (FlowNode.class.isAssignableFrom(element.getClass())) {
            BpmnXMLUtil.writeIncomingAndOutgoingFlowElement((FlowNode) element, xtw);
        }
    }

    protected Boolean getBoolean(String booleanStr) {
        if (Strings.isNullOrEmpty(booleanStr)) {
            return false;
        }

        if (booleanStr.equalsIgnoreCase("true")) {
            return true;
        } else {
            return false;
        }


    }
}
