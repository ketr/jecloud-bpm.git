/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.child.config;

import com.je.bpm.core.converter.converter.child.BaseChildElementParser;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.button.Button;
import com.je.bpm.core.model.button.ProcessButton;
import com.je.bpm.core.model.process.Process;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamReader;
import java.util.List;

public class ProcessEachButtonConfigParser extends BaseChildElementParser {

    @Override
    public String getElementName() {
        return ELEMENT_PROCESS_BUTTON;
    }

    @Override
    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
        Process process = (Process) parentElement;
        String id = null;
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_BUTTON_ID))) {
            id = xtr.getAttributeValue(null, ATTRIBUTE_BUTTON_ID);
        }
        if (id == null) {
            return;
        }
        boolean dislayWhenNotStarted = true;
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_BUTTON_DISLAY_WHEN_NOT_STARTED))) {
            dislayWhenNotStarted = getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_BUTTON_DISLAY_WHEN_NOT_STARTED));
        }
        String displayExpression = null;
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_TASK_BUTTON_DISLAY_EXPRESSION))) {
            displayExpression = xtr.getAttributeValue(null, ATTRIBUTE_TASK_BUTTON_DISLAY_EXPRESSION);
        }

        List<ProcessButton> processButtonList = process.getButtons().getButtons();
        for (Button eachButton : processButtonList) {
            if (eachButton.getId().equals(id)) {
                eachButton.setCustomizeName(xtr.getAttributeValue(null, ATTRIBUTE_BUTTON_CUSTOMIZE_NAME));
                eachButton.setCustomizeComments(xtr.getAttributeValue(null, ATTRIBUTE_BUTTON_CUSTOMIZE_COMMENTS));
                eachButton.setDisplayExpressionWhenStarted(displayExpression);
                eachButton.setPcListeners(xtr.getAttributeValue(null, ATTRIBUTE_BUTTON_PC_LISTENERS));
                break;
            }
        }
    }

}
