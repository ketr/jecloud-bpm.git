/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.child;

import com.je.bpm.core.converter.converter.BaseBpmnXMLConverter;
import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.config.process.ExtendedConfigurationConfigImpl;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;


public class ProcessExtendedConfigParser extends BaseBpmnXMLConverter {

    @Override
    protected Class<? extends BaseElement> getBpmnElementType() {
        return ExtendedConfigurationConfigImpl.class;
    }

    @Override
    protected BaseElement convertXMLToElement(XMLStreamReader xtr, BpmnModel model) throws Exception {
        ExtendedConfigurationConfigImpl extendedConfigurationConfig = new ExtendedConfigurationConfigImpl();
        BpmnXMLUtil.addXMLLocation(extendedConfigurationConfig, xtr);
        extendedConfigurationConfig.setCanSponsor(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANSPONSOR)));
        extendedConfigurationConfig.setCanRetrieve(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANRETRIEVE)));
        extendedConfigurationConfig.setCanReturn(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANRETURN)));
        extendedConfigurationConfig.setCanUrged(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANURGED)));
        extendedConfigurationConfig.setCanCancel(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANCANCEL)));
        extendedConfigurationConfig.setCanInvalid(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANINVALID)));
        extendedConfigurationConfig.setCanTransfer(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANTRANSFER)));
        extendedConfigurationConfig.setCanDelegate(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_CANDELEGATE)));
        extendedConfigurationConfig.setCountersignNode(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_COUNTERSIGNNODE)));
        extendedConfigurationConfig.setEasyLaunch(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_EASYLAUNCH)));
        extendedConfigurationConfig.setSimpleApproval(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_SIMPLEAPPROVAL)));
        extendedConfigurationConfig.setEnableSimpleComments(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_ENABLE_SIMPLE_COMMENTS)));
        extendedConfigurationConfig.setExigency(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_EXIGENCY)));
        extendedConfigurationConfig.setHideStateInfo(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_HIDESTATEINFO)));
        extendedConfigurationConfig.setHideTimeInfo(getBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_HIDETIMEINFO)));
        model.getMainProcess().setExtendedConfiguration(extendedConfigurationConfig);
        return extendedConfigurationConfig;
    }

    @Override
    protected String getXMLElementName() {
        return ELEMENT_PROCESS_EXTENDED_CONFIG;
    }

    @Override
    protected void writeAdditionalAttributes(BaseElement element, BpmnModel model, XMLStreamWriter xtw) throws Exception {

    }

    @Override
    protected void writeAdditionalChildElements(BaseElement element, BpmnModel model, XMLStreamWriter xtw) throws Exception {

    }
}
