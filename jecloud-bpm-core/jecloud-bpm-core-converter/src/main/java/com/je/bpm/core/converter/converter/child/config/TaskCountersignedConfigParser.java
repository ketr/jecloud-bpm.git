/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.child.config;

import com.je.bpm.core.converter.converter.child.BaseChildElementParser;
import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.config.CounterSignPassTypeEnum;
import com.je.bpm.core.model.config.task.TaskCounterSignConfigImpl;
import com.je.bpm.core.model.task.KaiteCounterSignUserTask;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamReader;

/**
 * 会签配置解析器
 */
public class TaskCountersignedConfigParser extends BaseChildElementParser {

    @Override
    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
        KaiteCounterSignUserTask kaiteUserTask = (KaiteCounterSignUserTask) parentElement;
        TaskCounterSignConfigImpl taskCounterSignConfig = kaiteUserTask.getCounterSignConfig();
        BpmnXMLUtil.addXMLLocation(taskCounterSignConfig, xtr);
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, COUNTERSIGNED_COUNTERSIGNPASSTYPE))) {
            String value = xtr.getAttributeValue(null, COUNTERSIGNED_COUNTERSIGNPASSTYPE);
            if (value.equals(CounterSignPassTypeEnum.PASS_PRINCIPAL.toString())) {
                taskCounterSignConfig.setCounterSignPassType(CounterSignPassTypeEnum.PASS_PRINCIPAL);
            } else {
                taskCounterSignConfig.setCounterSignPassType(CounterSignPassTypeEnum.PASS_PERSENT);
            }
        }
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, COUNTERSIGNED_AMOUNT))) {
            taskCounterSignConfig.setAmount(Integer.valueOf(xtr.getAttributeValue(null, COUNTERSIGNED_AMOUNT)));
        }
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, COUNTERSIGNED_ONEBALLOTPASSUSERS))) {
            taskCounterSignConfig.setOneBallotUserId(xtr.getAttributeValue(null, COUNTERSIGNED_ONEBALLOTPASSUSERS));
        }
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, COUNTERSIGNED_VOTEALL))) {
            taskCounterSignConfig.setVoteAll(getBoolean(xtr.getAttributeValue(null, COUNTERSIGNED_VOTEALL)));
        }
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, RUNTIME_TUNING))) {
            taskCounterSignConfig.setRuntimeTuning(getBoolean(xtr.getAttributeValue(null, RUNTIME_TUNING)));
        }
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ADJUST_BEFORE_RUNNING))) {
            taskCounterSignConfig.setAdjustBeforeRunning(getBoolean(xtr.getAttributeValue(null, ADJUST_BEFORE_RUNNING)));
        }
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, DESELECT_ALL))) {
            taskCounterSignConfig.setDeselectAll(getBoolean(xtr.getAttributeValue(null, DESELECT_ALL)));
        }

        //完成条件-》 全部处理
        StringBuilder sb = new StringBuilder();
        //全部处理
        if (taskCounterSignConfig.isVoteAll()) {
            sb.append("${nrOfCompletedInstances == nrOfInstances}");
        } else if (taskCounterSignConfig.getCounterSignPassType() == CounterSignPassTypeEnum.PASS_AMOUNT) {
            sb.append("${ ");
            //数量
            sb.append(" nrOfCompletedInstances >= amount ");
            sb.append("}");
        } else if (taskCounterSignConfig.getCounterSignPassType() == CounterSignPassTypeEnum.PASS_PERSENT) {
            sb.append("${ ");
            sb.append("nrOfCompletedInstances*100/nrOfInstances >= amount");
            sb.append("}");
        } else if (taskCounterSignConfig.getCounterSignPassType() == CounterSignPassTypeEnum.PASS_PRINCIPAL) {
            sb.append("${ ");
            sb.append("oneBallotUserId == prevAssignee");
            sb.append("}");
            kaiteUserTask.getLoopCharacteristics().setCompletionCondition(sb.toString());
        }
//        kaiteUserTask.getLoopCharacteristics().setSequential(kaiteUserTask.getTaskBasicConfig().getSequential());
    }

    @Override
    public String getElementName() {
        return COUNTERSIGNED_TASK_COUNTER_SIGN_CONFIG;
    }

}
