/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter;

import com.je.bpm.core.converter.converter.child.BaseChildElementParser;
import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.CallActivity;
import com.je.bpm.core.model.IOParameter;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**

 */
public class CallActivityXMLConverter extends BaseBpmnXMLConverter {

  protected Map<String, BaseChildElementParser> childParserMap = new HashMap<String, BaseChildElementParser>();

  public CallActivityXMLConverter() {
    InParameterParser inParameterParser = new InParameterParser();
    childParserMap.put(inParameterParser.getElementName(), inParameterParser);
    OutParameterParser outParameterParser = new OutParameterParser();
    childParserMap.put(outParameterParser.getElementName(), outParameterParser);
  }

  @Override
  public Class<? extends BaseElement> getBpmnElementType() {
    return CallActivity.class;
  }

  @Override
  protected String getXMLElementName() {
    return ELEMENT_CALL_ACTIVITY;
  }

  @Override
  protected BaseElement convertXMLToElement(XMLStreamReader xtr, BpmnModel model) throws Exception {
    CallActivity callActivity = new CallActivity();
    BpmnXMLUtil.addXMLLocation(callActivity, xtr);
    callActivity.setCalledElement(xtr.getAttributeValue(null, ATTRIBUTE_CALL_ACTIVITY_CALLEDELEMENT));
    callActivity.setBusinessKey(xtr.getAttributeValue(ACTIVITI_EXTENSIONS_NAMESPACE, ATTRIBUTE_CALL_ACTIVITY_BUSINESS_KEY));
    callActivity.setInheritBusinessKey(Boolean.parseBoolean(xtr.getAttributeValue(
        ACTIVITI_EXTENSIONS_NAMESPACE, ATTRIBUTE_CALL_ACTIVITY_INHERIT_BUSINESS_KEY)));
    callActivity.setInheritVariables(Boolean.valueOf(xtr.getAttributeValue(ACTIVITI_EXTENSIONS_NAMESPACE, ATTRIBUTE_CALL_ACTIVITY_INHERITVARIABLES)));
    parseChildElements(getXMLElementName(), callActivity, childParserMap, model, xtr);
    return callActivity;
  }

  @Override
  protected void writeAdditionalAttributes(BaseElement element, BpmnModel model, XMLStreamWriter xtw) throws Exception {
    CallActivity callActivity = (CallActivity) element;
    if (StringUtils.isNotEmpty(callActivity.getCalledElement())) {
      xtw.writeAttribute(ATTRIBUTE_CALL_ACTIVITY_CALLEDELEMENT, callActivity.getCalledElement());
    }
    if (StringUtils.isNotEmpty(callActivity.getBusinessKey())) {
      writeQualifiedAttribute(ATTRIBUTE_CALL_ACTIVITY_BUSINESS_KEY, callActivity.getBusinessKey(), xtw);
    }
    if (callActivity.isInheritBusinessKey()) {
      writeQualifiedAttribute(ATTRIBUTE_CALL_ACTIVITY_INHERIT_BUSINESS_KEY, "true", xtw);
    }
    if  (callActivity.isInheritVariables()) {
        xtw.writeAttribute(ACTIVITI_EXTENSIONS_NAMESPACE,
                           ATTRIBUTE_CALL_ACTIVITY_INHERITVARIABLES,
                           String.valueOf(callActivity.isInheritVariables()));
    }
  }

  @Override
  protected boolean writeExtensionChildElements(BaseElement element, boolean didWriteExtensionStartElement, XMLStreamWriter xtw) throws Exception {
    CallActivity callActivity = (CallActivity) element;
    didWriteExtensionStartElement = writeIOParameters(ELEMENT_CALL_ACTIVITY_IN_PARAMETERS,
        callActivity.getInParameters(), didWriteExtensionStartElement, xtw);
    didWriteExtensionStartElement = writeIOParameters(ELEMENT_CALL_ACTIVITY_OUT_PARAMETERS,
        callActivity.getOutParameters(), didWriteExtensionStartElement, xtw);
    return didWriteExtensionStartElement;
  }

  @Override
  protected void writeAdditionalChildElements(BaseElement element, BpmnModel model, XMLStreamWriter xtw) throws Exception {
  }

  private boolean writeIOParameters(String elementName, List<IOParameter> parameterList, boolean didWriteExtensionStartElement,
                                    XMLStreamWriter xtw) throws Exception {

    if (parameterList.isEmpty()) {
      return didWriteExtensionStartElement;
    }

    for (IOParameter ioParameter : parameterList) {
      if (!didWriteExtensionStartElement) {
        xtw.writeStartElement(ELEMENT_EXTENSIONS);
        didWriteExtensionStartElement = true;
      }

      xtw.writeStartElement(ACTIVITI_EXTENSIONS_PREFIX, elementName, ACTIVITI_EXTENSIONS_NAMESPACE);
      if (StringUtils.isNotEmpty(ioParameter.getSource())) {
        writeDefaultAttribute(ATTRIBUTE_IOPARAMETER_SOURCE, ioParameter.getSource(), xtw);
      }
      if (StringUtils.isNotEmpty(ioParameter.getSourceExpression())) {
        writeDefaultAttribute(ATTRIBUTE_IOPARAMETER_SOURCE_EXPRESSION, ioParameter.getSourceExpression(), xtw);
      }
      if (StringUtils.isNotEmpty(ioParameter.getTarget())) {
        writeDefaultAttribute(ATTRIBUTE_IOPARAMETER_TARGET, ioParameter.getTarget(), xtw);
      }

      xtw.writeEndElement();
    }

    return didWriteExtensionStartElement;
  }

  public class InParameterParser extends BaseChildElementParser {

    @Override
    public String getElementName() {
      return ELEMENT_CALL_ACTIVITY_IN_PARAMETERS;
    }

    @Override
    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
      String source = xtr.getAttributeValue(null, ATTRIBUTE_IOPARAMETER_SOURCE);
      String sourceExpression = xtr.getAttributeValue(null, ATTRIBUTE_IOPARAMETER_SOURCE_EXPRESSION);
      String target = xtr.getAttributeValue(null, ATTRIBUTE_IOPARAMETER_TARGET);
      if ((StringUtils.isNotEmpty(source) || StringUtils.isNotEmpty(sourceExpression)) && StringUtils.isNotEmpty(target)) {

        IOParameter parameter = new IOParameter();
        if (StringUtils.isNotEmpty(sourceExpression)) {
          parameter.setSourceExpression(sourceExpression);
        } else {
          parameter.setSource(source);
        }

        parameter.setTarget(target);

        ((CallActivity) parentElement).getInParameters().add(parameter);
      }
    }
  }

  public class OutParameterParser extends BaseChildElementParser {

    @Override
    public String getElementName() {
      return ELEMENT_CALL_ACTIVITY_OUT_PARAMETERS;
    }

    @Override
    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
      String source = xtr.getAttributeValue(null, ATTRIBUTE_IOPARAMETER_SOURCE);
      String sourceExpression = xtr.getAttributeValue(null, ATTRIBUTE_IOPARAMETER_SOURCE_EXPRESSION);
      String target = xtr.getAttributeValue(null, ATTRIBUTE_IOPARAMETER_TARGET);
      if ((StringUtils.isNotEmpty(source) || StringUtils.isNotEmpty(sourceExpression)) && StringUtils.isNotEmpty(target)) {

        IOParameter parameter = new IOParameter();
        if (StringUtils.isNotEmpty(sourceExpression)) {
          parameter.setSourceExpression(sourceExpression);
        } else {
          parameter.setSource(source);
        }

        parameter.setTarget(target);

        ((CallActivity) parentElement).getOutParameters().add(parameter);
      }
    }
  }
}
