/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.util.Stack;


public class IndentingXMLStreamWriter extends DelegatingXMLStreamWriter {

  private final static Object SEEN_NOTHING = new Object();
  private final static Object SEEN_ELEMENT = new Object();
  private final static Object SEEN_DATA = new Object();

  private Object state = SEEN_NOTHING;
  private Stack<Object> stateStack = new Stack<Object>();

  private String indentStep = "  ";
  private int depth = 0;

  public IndentingXMLStreamWriter(XMLStreamWriter writer) {
    super(writer);
  }

  /**
   * Return the current indent step.
   *
   * <p>
   * Return the current indent step: each start tag will be indented by this number of spaces times the number of ancestors that the element has.
   * </p>
   *
   * @return The number of spaces in each indentation step, or 0 or less for no indentation.
   * @see #setIndentStep(int)
   *
   * @deprecated Only return the length of the indent string.
   */
  public int getIndentStep() {
    return indentStep.length();
  }

  /**
   * Set the current indent step.
   *
   * @param indentStep
   *          The new indent step (0 or less for no indentation).
   * @see #getIndentStep()
   *
   * @deprecated Should use the version that takes string.
   */
  public void setIndentStep(int indentStep) {
    StringBuilder s = new StringBuilder();
    for (; indentStep > 0; indentStep--){
      s.append(' ');
    }
    setIndentStep(s.toString());
  }

  public void setIndentStep(String s) {
    this.indentStep = s;
  }

  private void onStartElement() throws XMLStreamException {
    stateStack.push(SEEN_ELEMENT);
    state = SEEN_NOTHING;
    if (depth > 0) {
      super.writeCharacters("\n");
    }
    doIndent();
    depth++;
  }

  private void onEndElement() throws XMLStreamException {
    depth--;
    if (state == SEEN_ELEMENT) {
      super.writeCharacters("\n");
      doIndent();
    }
    state = stateStack.pop();
  }

  private void onEmptyElement() throws XMLStreamException {
    state = SEEN_ELEMENT;
    if (depth > 0) {
      super.writeCharacters("\n");
    }
    doIndent();
  }

  /**
   * Print indentation for the current level.
   *
   * @exception org.xml.sax.SAXException
   *              If there is an error writing the indentation characters, or if a filter further down the chain raises an exception.
   */
  private void doIndent() throws XMLStreamException {
    if (depth > 0) {
      for (int i = 0; i < depth; i++){
        super.writeCharacters(indentStep);
      }
    }
  }

  @Override
  public void writeStartDocument() throws XMLStreamException {
    super.writeStartDocument();
    super.writeCharacters("\n");
  }

  @Override
  public void writeStartDocument(String version) throws XMLStreamException {
    super.writeStartDocument(version);
    super.writeCharacters("\n");
  }

  @Override
  public void writeStartDocument(String encoding, String version) throws XMLStreamException {
    super.writeStartDocument(encoding, version);
    super.writeCharacters("\n");
  }

  @Override
  public void writeStartElement(String localName) throws XMLStreamException {
    onStartElement();
    super.writeStartElement(localName);
  }

  @Override
  public void writeStartElement(String namespaceURI, String localName) throws XMLStreamException {
    onStartElement();
    super.writeStartElement(namespaceURI, localName);
  }

  @Override
  public void writeStartElement(String prefix, String localName, String namespaceURI) throws XMLStreamException {
    onStartElement();
    super.writeStartElement(prefix, localName, namespaceURI);
  }

  @Override
  public void writeEmptyElement(String namespaceURI, String localName) throws XMLStreamException {
    onEmptyElement();
    super.writeEmptyElement(namespaceURI, localName);
  }

  @Override
  public void writeEmptyElement(String prefix, String localName, String namespaceURI) throws XMLStreamException {
    onEmptyElement();
    super.writeEmptyElement(prefix, localName, namespaceURI);
  }

  @Override
  public void writeEmptyElement(String localName) throws XMLStreamException {
    onEmptyElement();
    super.writeEmptyElement(localName);
  }

  @Override
  public void writeEndElement() throws XMLStreamException {
    onEndElement();
    super.writeEndElement();
  }

  @Override
  public void writeCharacters(String text) throws XMLStreamException {
    state = SEEN_DATA;
    super.writeCharacters(text);
  }

  @Override
  public void writeCharacters(char[] text, int start, int len) throws XMLStreamException {
    state = SEEN_DATA;
    super.writeCharacters(text, start, len);
  }

  @Override
  public void writeCData(String data) throws XMLStreamException {
    state = SEEN_DATA;
    super.writeCData(data);
  }

}
