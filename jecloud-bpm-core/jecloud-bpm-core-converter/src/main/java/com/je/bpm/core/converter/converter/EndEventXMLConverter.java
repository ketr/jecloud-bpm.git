/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter;

import com.je.bpm.core.converter.converter.child.BaseChildElementParser;
import com.je.bpm.core.converter.converter.child.config.form.TaskFormButtonConfigParser;
import com.je.bpm.core.converter.converter.child.config.form.TaskFormChildFuncConfigParser;
import com.je.bpm.core.converter.converter.child.config.form.TaskFormFieldConfigParser;
import com.je.bpm.core.converter.converter.child.config.form.TaskFormFieldSetValueConfigParser;
import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.event.EndEvent;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import java.util.HashMap;
import java.util.Map;


public class EndEventXMLConverter extends BaseBpmnXMLConverter {

    private Map<String, BaseChildElementParser> childParserMap = new HashMap<String, BaseChildElementParser>();

    public EndEventXMLConverter() {
        //表单控制配置赋值
        TaskFormFieldSetValueConfigParser taskFormFieldSetValueConfigParser = new TaskFormFieldSetValueConfigParser();
        //表单字段控制配置
        TaskFormFieldConfigParser taskFormFieldConfigParser = new TaskFormFieldConfigParser();
        //按钮控制
        TaskFormButtonConfigParser taskFormButtonConfigParser = new TaskFormButtonConfigParser();
        //子功能
        TaskFormChildFuncConfigParser taskFormChildFuncConfigParser = new TaskFormChildFuncConfigParser();
        childParserMap.put(taskFormFieldSetValueConfigParser.getElementName(), taskFormFieldSetValueConfigParser);
        childParserMap.put(taskFormFieldConfigParser.getElementName(), taskFormFieldConfigParser);
        childParserMap.put(taskFormButtonConfigParser.getElementName(), taskFormButtonConfigParser);
        childParserMap.put(taskFormChildFuncConfigParser.getElementName(), taskFormChildFuncConfigParser);
    }

    @Override
    public Class<? extends BaseElement> getBpmnElementType() {
        return EndEvent.class;
    }

    @Override
    protected String getXMLElementName() {
        return ELEMENT_EVENT_END;
    }

    @Override
    protected BaseElement convertXMLToElement(XMLStreamReader xtr, BpmnModel model) throws Exception {
        EndEvent endEvent = new EndEvent();
        BpmnXMLUtil.addXMLLocation(endEvent, xtr);
        parseChildElements(getXMLElementName(), endEvent, childParserMap, model, xtr);
        return endEvent;
    }

    @Override
    protected void writeAdditionalAttributes(BaseElement element, BpmnModel model, XMLStreamWriter xtw) throws Exception {
    }

    @Override
    protected void writeAdditionalChildElements(BaseElement element, BpmnModel model, XMLStreamWriter xtw) throws Exception {
        EndEvent endEvent = (EndEvent) element;
        writeEventDefinitions(endEvent, endEvent.getEventDefinitions(), model, xtw);
        //表单控制配置
        writeFormBasicConfig(endEvent.getTaskFormBasicConfig(), xtw);
    }

}
