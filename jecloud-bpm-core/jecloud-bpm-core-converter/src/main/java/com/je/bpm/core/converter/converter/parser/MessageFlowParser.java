/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.parser;

import com.je.bpm.core.converter.constants.BpmnXMLConstants;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.MessageFlow;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamReader;


public class MessageFlowParser implements BpmnXMLConstants {

    protected static final Logger LOGGER = LoggerFactory.getLogger(MessageFlowParser.class.getName());

    public void parse(XMLStreamReader xtr, BpmnModel model) throws Exception {
        String id = xtr.getAttributeValue(null, ATTRIBUTE_ID);
        if (StringUtils.isNotEmpty(id)) {
            MessageFlow messageFlow = new MessageFlow();
            messageFlow.setId(id);

            String name = xtr.getAttributeValue(null, ATTRIBUTE_NAME);
            if (StringUtils.isNotEmpty(name)) {
                messageFlow.setName(name);
            }

            String sourceRef = xtr.getAttributeValue(null, ATTRIBUTE_FLOW_SOURCE_REF);
            if (StringUtils.isNotEmpty(sourceRef)) {
                messageFlow.setSourceRef(sourceRef);
            }

            String targetRef = xtr.getAttributeValue(null, ATTRIBUTE_FLOW_TARGET_REF);
            if (StringUtils.isNotEmpty(targetRef)) {
                messageFlow.setTargetRef(targetRef);
            }

            String messageRef = xtr.getAttributeValue(null, ATTRIBUTE_MESSAGE_REF);
            if (StringUtils.isNotEmpty(messageRef)) {
                messageFlow.setMessageRef(messageRef);
            }

            model.addMessageFlow(messageFlow);
        }
    }
}
