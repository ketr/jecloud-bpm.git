/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.export;

import com.je.bpm.core.converter.constants.BpmnXMLConstants;
import com.je.bpm.core.model.config.process.ProcessBasicConfigImpl;
import com.je.bpm.core.model.process.Process;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamWriter;

/**
 * 流程配置导出
 */
public class ProccessConfigExport implements BpmnXMLConstants {

    public static void writeConfigs(Process process, XMLStreamWriter xtw) throws Exception {
        ProcessBasicConfigImpl processBasicConfig = process.getProcessConfig();
        if (processBasicConfig != null) {
            xtw.writeStartElement(BPMN2_PREFIX, ELEMENT_PROCESS_CONFIG, BPMN2_NAMESPACE);
            xtw.writeAttribute(ATTRIBUTE_ID, processBasicConfig.getConfigType());

            if (StringUtils.isNoneEmpty(processBasicConfig.getTableCode())) {
                xtw.writeAttribute(ATTRIBUTE_TABLECODE, processBasicConfig.getTableCode());
            }

            if (StringUtils.isNoneEmpty(processBasicConfig.getFuncCode())) {
                xtw.writeAttribute(ATTRIBUTE_FUNCCODE, processBasicConfig.getFuncCode());
            }

            if (StringUtils.isNoneEmpty(processBasicConfig.getFuncName())) {
                xtw.writeAttribute(ATTRIBUTE_FUNCNAME, processBasicConfig.getFuncName());
            }

            if (StringUtils.isNoneEmpty(processBasicConfig.getFuncId())) {
                xtw.writeAttribute(ATTRIBUTE_FUNCID, processBasicConfig.getFuncId());
            }

            if (StringUtils.isNoneEmpty(processBasicConfig.getAttachedFuncIds())) {
                xtw.writeAttribute(ATTRIBUTE_ATTACHEDFUNCIDS, processBasicConfig.getAttachedFuncIds());
            }

            if (StringUtils.isNoneEmpty(processBasicConfig.getAttachedFuncCodes())) {
                xtw.writeAttribute(ATTRIBUTE_ATTACHEDFUNCCODES, processBasicConfig.getAttachedFuncCodes());
            }

            if (StringUtils.isNoneEmpty(processBasicConfig.getAttachedFuncNames())) {
                xtw.writeAttribute(ATTRIBUTE_ATTACHEDFUNCNAMES, processBasicConfig.getAttachedFuncNames());
            }

            if (StringUtils.isNoneEmpty(processBasicConfig.getContentModule())) {
                xtw.writeAttribute(ATTRIBUTE_CONTENTMODULE, processBasicConfig.getContentModule());
            }

            if (StringUtils.isNoneEmpty(processBasicConfig.getProcessClassificationCode())) {
                xtw.writeAttribute(ATTRIBUTE_PROCESS_CLASSIFICATIONCODE, processBasicConfig.getProcessClassificationCode());
            }

            if (StringUtils.isNoneEmpty(processBasicConfig.getProcessClassificationId())) {
                xtw.writeAttribute(ATTRIBUTE_PROCESS_CLASSIFICATIONID, processBasicConfig.getProcessClassificationId());
            }

            if (StringUtils.isNoneEmpty(processBasicConfig.getProcessClassificationName())) {
                xtw.writeAttribute(ATTRIBUTE_PROCESS_CLASSIFICATIONNAME, processBasicConfig.getProcessClassificationName());
            }

            if (processBasicConfig.getDeploymentEnvironment() != null) {
                xtw.writeAttribute(ATTRIBUTE_DEPLOYMENTENVIRONMENT, processBasicConfig.getDeploymentEnvironment().toString());
            }

            xtw.writeEndElement();
        }
    }

}
