/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.parser;

import com.je.bpm.core.converter.constants.BpmnXMLConstants;
import com.je.bpm.core.converter.converter.child.*;
import com.je.bpm.core.converter.converter.child.config.ProcessButtonConfigParser;
import com.je.bpm.core.converter.converter.export.ProcessExport;
import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.process.Process;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ProcessParser implements BpmnXMLConstants {

    private Map<String, BaseChildElementParser> childParserMap = new HashMap();

    public ProcessParser() {
//        //流程基础属性
//        ProcessBasicConfigParser processBasicConfigParser = new ProcessBasicConfigParser();
//        //流程拓展属性
//        ProcessExtendedConfigParser processExtendedConfigParser = new ProcessExtendedConfigParser();
//        //流程自定义事件
//        ProcessEventConfigParser processEventConfigParser = new ProcessEventConfigParser();
//        //流程自定义消息
//        ProcessMessageConfigParser processMessageConfigParser = new ProcessMessageConfigParser();
//        //流程启动配置
//        ProcessStartConfigParser processStartConfigParser = new ProcessStartConfigParser();
//        //流程按钮
//        ProcessButtonConfigParser processButtonConfigParser = new ProcessButtonConfigParser();
//
//        childParserMap.put(processBasicConfigParser.getElementName(), processBasicConfigParser);
//        childParserMap.put(processExtendedConfigParser.getElementName(), processExtendedConfigParser);
//        childParserMap.put(processEventConfigParser.getElementName(), processEventConfigParser);
//        childParserMap.put(processMessageConfigParser.getElementName(), processMessageConfigParser);
//        childParserMap.put(processStartConfigParser.getElementName(), processStartConfigParser);
//        childParserMap.put(processButtonConfigParser.getElementName(), processButtonConfigParser);

    }

    public Process parse(XMLStreamReader xtr, BpmnModel model) throws Exception {
        Process process = null;
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_ID))) {
            String processId = xtr.getAttributeValue(null, ATTRIBUTE_ID);
            process = new Process();
            process.setId(processId);
            BpmnXMLUtil.addXMLLocation(process, xtr);
            process.setName(xtr.getAttributeValue(null, ATTRIBUTE_NAME));
            if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_EXECUTABLE))) {
                process.setExecutable(Boolean.parseBoolean(xtr.getAttributeValue(null, ATTRIBUTE_PROCESS_EXECUTABLE)));
            }
            String candidateUsersString = xtr.getAttributeValue(ACTIVITI_EXTENSIONS_NAMESPACE, ATTRIBUTE_PROCESS_CANDIDATE_USERS);
            if (StringUtils.isNotEmpty(candidateUsersString)) {
                List<String> candidateUsers = BpmnXMLUtil.parseDelimitedList(candidateUsersString);
                process.setCandidateStarterUsers(candidateUsers);
            }
            String candidateGroupsString = xtr.getAttributeValue(ACTIVITI_EXTENSIONS_NAMESPACE, ATTRIBUTE_PROCESS_CANDIDATE_GROUPS);
            if (StringUtils.isNotEmpty(candidateGroupsString)) {
                List<String> candidateGroups = BpmnXMLUtil.parseDelimitedList(candidateGroupsString);
                process.setCandidateStarterGroups(candidateGroups);
            }

//            BpmnXMLUtil.addCustomAttributes(xtr, process, ProcessExport.defaultProcessAttributes);
//            //基础属性 processBasicConfig
//            BpmnXMLUtil.parseChildElements(new ProcessBasicConfigParser().getElementName(), process, xtr, childParserMap, model);
//            //拓展属性 extendedConfiguration
//            BpmnXMLUtil.parseChildElements(new ProcessExtendedConfigParser().getElementName(), process, xtr, childParserMap, model);
//            //自定义事件 customEventlisteners
//            BpmnXMLUtil.parseChildElements(new ProcessEventConfigParser().getElementName(), process, xtr, childParserMap, model);
//            //启动配置 startupSettings
//            BpmnXMLUtil.parseChildElements(new ProcessStartConfigParser().getElementName(), process, xtr, childParserMap, model);
//            //消息 messageSetting
//            BpmnXMLUtil.parseChildElements(new ProcessMessageConfigParser().getElementName(), process, xtr, childParserMap, model);
//            //按钮属性 processButtonsConfig
//            BpmnXMLUtil.parseChildElements(new ProcessButtonConfigParser().getElementName(), process, xtr, childParserMap, model);
            model.getProcesses().add(process);

        }
        return process;
    }

}
