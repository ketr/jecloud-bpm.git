/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.child;

import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.ExtensionAttribute;
import com.je.bpm.core.model.MessageEventDefinition;
import com.je.bpm.core.model.event.Event;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamReader;
import java.util.List;

import static java.util.Collections.singletonMap;

public class MessageEventDefinitionParser extends BaseChildElementParser {

    @Override
    public String getElementName() {
        return ELEMENT_EVENT_MESSAGEDEFINITION;
    }

    @Override
    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
        if (!(parentElement instanceof Event)) {
            return;
        }
        MessageEventDefinition eventDefinition = new MessageEventDefinition();
        BpmnXMLUtil.addXMLLocation(eventDefinition, xtr);
        eventDefinition.setMessageRef(xtr.getAttributeValue(null, ATTRIBUTE_MESSAGE_REF));
        eventDefinition.setMessageExpression(xtr.getAttributeValue(ACTIVITI_EXTENSIONS_NAMESPACE, ATTRIBUTE_MESSAGE_EXPRESSION));
        eventDefinition.setCorrelationKey(xtr.getAttributeValue(ACTIVITI_EXTENSIONS_NAMESPACE, ATTRIBUTE_MESSAGE_CORRELATION_KEY));
        List<ExtensionAttribute> attributes = parseExtensionAttributes(xtr, parentElement, model);
        if (!attributes.isEmpty()) {
            eventDefinition.setAttributes(singletonMap(ACTIVITI_EXTENSIONS_PREFIX, attributes));
        }
        if (!StringUtils.isEmpty(eventDefinition.getMessageRef())) {

            int indexOfP = eventDefinition.getMessageRef().indexOf(':');
            if (indexOfP != -1) {
                String prefix = eventDefinition.getMessageRef().substring(0, indexOfP);
                String resolvedNamespace = model.getNamespace(prefix);
                String messageRef = eventDefinition.getMessageRef().substring(indexOfP + 1);

                if (resolvedNamespace == null) {
                    // if it's an invalid prefix will consider this is not a namespace prefix so will be used as part of the stringReference
                    messageRef = prefix + ":" + messageRef;
                } else if (!resolvedNamespace.equalsIgnoreCase(model.getTargetNamespace())) {
                    // if it's a valid namespace prefix but it's not the targetNamespace then we'll use it as a valid namespace
                    // (even out editor does not support defining namespaces it is still a valid xml file)
                    messageRef = resolvedNamespace + ":" + messageRef;
                }
                eventDefinition.setMessageRef(messageRef);
            } else {
                eventDefinition.setMessageRef(eventDefinition.getMessageRef());
            }
        }
        BpmnXMLUtil.parseChildElements(ELEMENT_EVENT_MESSAGEDEFINITION, eventDefinition, xtr, model);
        ((Event) parentElement).getEventDefinitions().add(eventDefinition);
    }
}
