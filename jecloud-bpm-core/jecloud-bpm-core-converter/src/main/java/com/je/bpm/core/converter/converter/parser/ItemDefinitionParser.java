/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.parser;

import com.je.bpm.core.converter.constants.BpmnXMLConstants;
import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.ItemDefinition;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamReader;


public class ItemDefinitionParser implements BpmnXMLConstants {

    public void parse(XMLStreamReader xtr, BpmnModel model) throws Exception {
        if (StringUtils.isNotEmpty(xtr.getAttributeValue(null, ATTRIBUTE_ID))) {
            String itemDefinitionId = model.getTargetNamespace() + ":" + xtr.getAttributeValue(null, ATTRIBUTE_ID);
            String structureRef = xtr.getAttributeValue(null, ATTRIBUTE_STRUCTURE_REF);
            if (StringUtils.isNotEmpty(structureRef)) {
                ItemDefinition item = new ItemDefinition();
                item.setId(itemDefinitionId);
                BpmnXMLUtil.addXMLLocation(item, xtr);

                int indexOfP = structureRef.indexOf(':');
                if (indexOfP != -1) {
                    String prefix = structureRef.substring(0, indexOfP);
                    String resolvedNamespace = model.getNamespace(prefix);
                    structureRef = resolvedNamespace + ":" + structureRef.substring(indexOfP + 1);
                } else {
                    structureRef = model.getTargetNamespace() + ":" + structureRef;
                }
                item.setStructureRef(structureRef);
                item.setItemKind(xtr.getAttributeValue(null, ATTRIBUTE_ITEM_KIND));
                BpmnXMLUtil.parseChildElements(ELEMENT_ITEM_DEFINITION, item, xtr, model);
                model.addItemDefinition(itemDefinitionId, item);
            }
        }
    }
}
