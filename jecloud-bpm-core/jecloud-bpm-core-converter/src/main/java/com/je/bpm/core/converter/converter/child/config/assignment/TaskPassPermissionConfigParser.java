/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.child.config.assignment;

import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.config.task.PassRoundResource;
import com.je.bpm.core.model.config.task.assignment.AssignmentPermission;

import javax.xml.stream.XMLStreamReader;

/**
 * 传阅人员权限配置实现
 */
public class TaskPassPermissionConfigParser extends AbstractTaskAssignConfigParser {

    @Override
    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
        PassRoundResource userAssignmentConfig = (PassRoundResource) parentElement;
        AssignmentPermission assignmentPermission = new AssignmentPermission();
        assignmentPermission.setCompany(getBoolean(xtr.getAttributeValue(null, PROPERTY_ASSIGNMENT_PERMISSION_COMPANY)));
        assignmentPermission.setCompanySupervision(getBoolean(xtr.getAttributeValue(null, PROPERTY_ASSIGNMENT_PERMISSION_COMPANY_SUPERVISION)));
        assignmentPermission.setDept(getBoolean(xtr.getAttributeValue(null, PROPERTY_ASSIGNMENT_PERMISSION_DEPT)));
        assignmentPermission.setDeptAll(getBoolean(xtr.getAttributeValue(null, PROPERTY_ASSIGNMENT_PERMISSION_DEPT_ALL)));
        assignmentPermission.setDirectLeader(getBoolean(xtr.getAttributeValue(null, PROPERTY_ASSIGNMENT_PERMISSION_DIRECT_LEADER)));
        assignmentPermission.setDeptLeader(getBoolean(xtr.getAttributeValue(null, PROPERTY_ASSIGNMENT_PERMISSION_DEPT_LEADER)));
        assignmentPermission.setSupervisionLeader(getBoolean(xtr.getAttributeValue(null, PROPERTY_ASSIGNMENT_PERMISSION_SUPERVISION_LEADER)));
        assignmentPermission.setSql(xtr.getAttributeValue(null, PROPERTY_ASSIGNMENT_PERMISSION_SQL));
        assignmentPermission.setSqlRemarks(xtr.getAttributeValue(null, PROPERTY_ASSIGNMENT_PERMISSION_SQL_REMARKS));
        userAssignmentConfig.setPermission(assignmentPermission);
    }


    @Override
    public String getElementName() {
        return ELEMENT_ASSIGNMENT_RESOURCE_PERMISSION_CONFIG;
    }

}
