/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter;

import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.*;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.regex.Pattern;


public class ValuedDataObjectXMLConverter extends BaseBpmnXMLConverter {

    private final Pattern xmlChars = Pattern.compile("[<>&]");
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    protected boolean didWriteExtensionStartElement = false;

    @Override
    public Class<? extends BaseElement> getBpmnElementType() {
        return ValuedDataObject.class;
    }

    @Override
    protected String getXMLElementName() {
        return ELEMENT_DATA_OBJECT;
    }

    @Override
    protected BaseElement convertXMLToElement(XMLStreamReader xtr, BpmnModel model) throws Exception {
        ValuedDataObject dataObject = null;
        ItemDefinition itemSubjectRef = new ItemDefinition();
        String structureRef = xtr.getAttributeValue(null, ATTRIBUTE_DATA_ITEM_REF);
        if (StringUtils.isNotEmpty(structureRef) && structureRef.contains(":")) {
            String dataType = structureRef.substring(structureRef.indexOf(':') + 1);
            if (dataType.equals("string")) {
                dataObject = new StringDataObject();
            } else if (dataType.equals("int")) {
                dataObject = new IntegerDataObject();
            } else if (dataType.equals("long")) {
                dataObject = new LongDataObject();
            } else if (dataType.equals("double")) {
                dataObject = new DoubleDataObject();
            } else if (dataType.equals("boolean")) {
                dataObject = new BooleanDataObject();
            } else if (dataType.equals("datetime")) {
                dataObject = new DateDataObject();
            } else {
                LOGGER.error("Error converting {}, invalid data type: " + dataType, xtr.getAttributeValue(null, ATTRIBUTE_DATA_NAME));
            }
        } else {
            // use String as default type
            dataObject = new StringDataObject();
            structureRef = "xsd:string";
        }

        if (dataObject != null) {
            dataObject.setId(xtr.getAttributeValue(null, ATTRIBUTE_DATA_ID));
            dataObject.setName(xtr.getAttributeValue(null, ATTRIBUTE_DATA_NAME));

            BpmnXMLUtil.addXMLLocation(dataObject, xtr);

            itemSubjectRef.setStructureRef(structureRef);
            dataObject.setItemSubjectRef(itemSubjectRef);

            parseChildElements(getXMLElementName(), dataObject, model, xtr);

            List<ExtensionElement> valuesElement = dataObject.getExtensionElements().get("value");
            if (valuesElement != null && !valuesElement.isEmpty()) {
                ExtensionElement valueElement = valuesElement.get(0);
                if (StringUtils.isNotEmpty(valueElement.getElementText())) {
                    if (dataObject instanceof DateDataObject) {
                        try {
                            dataObject.setValue(sdf.parse(valueElement.getElementText()));
                        } catch (Exception e) {
                            LOGGER.error("Error converting {}", dataObject.getName(), e.getMessage());
                        }
                    } else {
                        dataObject.setValue(valueElement.getElementText());
                    }
                }

                // remove value element
                dataObject.getExtensionElements().remove("value");
            }
        }

        return dataObject;
    }

    @Override
    protected void writeAdditionalAttributes(BaseElement element, BpmnModel model, XMLStreamWriter xtw) throws Exception {
        ValuedDataObject dataObject = (ValuedDataObject) element;
        if (dataObject.getItemSubjectRef() != null && StringUtils.isNotEmpty(dataObject.getItemSubjectRef().getStructureRef())) {
            writeDefaultAttribute(ATTRIBUTE_DATA_ITEM_REF, dataObject.getItemSubjectRef().getStructureRef(), xtw);
        }
    }

    @Override
    protected boolean writeExtensionChildElements(BaseElement element, boolean didWriteExtensionStartElement, XMLStreamWriter xtw) throws Exception {
        ValuedDataObject dataObject = (ValuedDataObject) element;
        if (StringUtils.isNotEmpty(dataObject.getId()) && dataObject.getValue() != null) {
            if (!didWriteExtensionStartElement) {
                xtw.writeStartElement(ELEMENT_EXTENSIONS);
                didWriteExtensionStartElement = true;
            }
            xtw.writeStartElement(ACTIVITI_EXTENSIONS_PREFIX, ELEMENT_DATA_VALUE, ACTIVITI_EXTENSIONS_NAMESPACE);
            if (dataObject.getValue() != null) {
                String value = null;
                if (dataObject instanceof DateDataObject) {
                    value = sdf.format(dataObject.getValue());
                } else {
                    value = dataObject.getValue().toString();
                }

                if (dataObject instanceof StringDataObject && xmlChars.matcher(value).find()) {
                    xtw.writeCData(value);
                } else {
                    xtw.writeCharacters(value);
                }
            }
            xtw.writeEndElement();
        }

        return didWriteExtensionStartElement;
    }

    @Override
    protected void writeAdditionalChildElements(BaseElement element, BpmnModel model, XMLStreamWriter xtw) throws Exception {
    }
}
