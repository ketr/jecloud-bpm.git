/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.child.config;

import com.je.bpm.core.converter.converter.child.BaseChildElementParser;
import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.config.task.PassRoundResource;
import com.je.bpm.core.model.task.KaiteBaseUserTask;

import javax.xml.stream.XMLStreamReader;

/**
 * 任务加签指派人配置解析
 */
public class TaskAddSignatureRoundDeaprtmentConfigParser extends BaseChildElementParser {

    @Override
    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
        PassRoundResource passRoundResource = new PassRoundResource();
        BpmnXMLUtil.addXMLLocation(passRoundResource, xtr);
        KaiteBaseUserTask kaiteUserTask = (KaiteBaseUserTask) parentElement;
        passRoundResource.setPassRoundTypeEnum(PassRoundResource.PassRoundTypeEnum.getType(xtr.getAttributeValue(null,
                ELEMENT_USERTASK_PASSROUND_RULE_TYPE)));
        passRoundResource.setResourceId(xtr.getAttributeValue(null, ELEMENT_USERTASK_PASSROUND_ID));
        passRoundResource.setResourceName(xtr.getAttributeValue(null, ELEMENT_USERTASK_PASSROUND_NAME));
        passRoundResource.setService(xtr.getAttributeValue(null, ELEMENT_USERTASK_PASSROUND_SERVICE));
        passRoundResource.setMethod(xtr.getAttributeValue(null, ELEMENT_USERTASK_PASSROUND_METHOD));
        kaiteUserTask.getAddSignatureConfig().getCirculationRules().add(passRoundResource);
    }

    @Override
    public String getElementName() {
        return ELEMENT_USERTASK_PASSROUND_CIRCULATIONRULE;
    }

}
