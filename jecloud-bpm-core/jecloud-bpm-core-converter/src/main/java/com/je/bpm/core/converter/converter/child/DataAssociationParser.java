/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.child;

import com.je.bpm.core.converter.constants.BpmnXMLConstants;
import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.Assignment;
import com.je.bpm.core.model.DataAssociation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamReader;

public class DataAssociationParser implements BpmnXMLConstants {

    protected static final Logger LOGGER = LoggerFactory.getLogger(DataAssociationParser.class.getName());

    public static void parseDataAssociation(DataAssociation dataAssociation, String elementName, XMLStreamReader xtr) {
        boolean readyWithDataAssociation = false;
        Assignment assignment = null;
        try {
            dataAssociation.setId(xtr.getAttributeValue(null, "id"));
            while (!readyWithDataAssociation && xtr.hasNext()) {
                xtr.next();
                if (xtr.isStartElement() && ELEMENT_SOURCE_REF.equals(xtr.getLocalName())) {
                    String sourceRef = xtr.getElementText();
                    if (StringUtils.isNotEmpty(sourceRef)) {
                        dataAssociation.setSourceRef(sourceRef.trim());
                    }

                } else if (xtr.isStartElement() && ELEMENT_TARGET_REF.equals(xtr.getLocalName())) {
                    String targetRef = xtr.getElementText();
                    if (StringUtils.isNotEmpty(targetRef)) {
                        dataAssociation.setTargetRef(targetRef.trim());
                    }

                } else if (xtr.isStartElement() && ELEMENT_TRANSFORMATION.equals(xtr.getLocalName())) {
                    String transformation = xtr.getElementText();
                    if (StringUtils.isNotEmpty(transformation)) {
                        dataAssociation.setTransformation(transformation.trim());
                    }

                } else if (xtr.isStartElement() && ELEMENT_ASSIGNMENT.equals(xtr.getLocalName())) {
                    assignment = new Assignment();
                    BpmnXMLUtil.addXMLLocation(assignment, xtr);

                } else if (xtr.isStartElement() && ELEMENT_FROM.equals(xtr.getLocalName())) {
                    String from = xtr.getElementText();
                    if (assignment != null && StringUtils.isNotEmpty(from)) {
                        assignment.setFrom(from.trim());
                    }

                } else if (xtr.isStartElement() && ELEMENT_TO.equals(xtr.getLocalName())) {
                    String to = xtr.getElementText();
                    if (assignment != null && StringUtils.isNotEmpty(to)) {
                        assignment.setTo(to.trim());
                    }

                } else if (xtr.isEndElement() && ELEMENT_ASSIGNMENT.equals(xtr.getLocalName())) {
                    if (StringUtils.isNotEmpty(assignment.getFrom()) && StringUtils.isNotEmpty(assignment.getTo())) {
                        dataAssociation.getAssignments().add(assignment);
                    }

                } else if (xtr.isEndElement() && elementName.equals(xtr.getLocalName())) {
                    readyWithDataAssociation = true;
                }
            }
        } catch (Exception e) {
            LOGGER.warn("Error parsing data association child elements", e);
        }
    }
}
