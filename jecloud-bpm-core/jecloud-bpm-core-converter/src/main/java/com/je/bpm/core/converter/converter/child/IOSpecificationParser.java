/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.child;

import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.*;
import org.apache.commons.lang3.StringUtils;
import com.je.bpm.core.model.process.Process;

import javax.xml.stream.XMLStreamReader;

/**
 *
 */
public class IOSpecificationParser extends BaseChildElementParser {

    @Override
    public String getElementName() {
        return ELEMENT_IOSPECIFICATION;
    }

    @Override
    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
        if (!(parentElement instanceof Activity) && !(parentElement instanceof Process)) {
            return;
        }

        IOSpecification ioSpecification = new IOSpecification();
        BpmnXMLUtil.addXMLLocation(ioSpecification, xtr);
        boolean readyWithIOSpecification = false;
        try {
            while (!readyWithIOSpecification && xtr.hasNext()) {
                xtr.next();
                if (xtr.isStartElement() && ELEMENT_DATA_INPUT.equalsIgnoreCase(xtr.getLocalName())) {
                    DataSpec dataSpec = new DataSpec();
                    BpmnXMLUtil.addXMLLocation(dataSpec, xtr);
                    dataSpec.setId(xtr.getAttributeValue(null, ATTRIBUTE_ID));
                    dataSpec.setName(xtr.getAttributeValue(null, ATTRIBUTE_NAME));
                    dataSpec.setItemSubjectRef(parseItemSubjectRef(xtr.getAttributeValue(null, ATTRIBUTE_ITEM_SUBJECT_REF), model));
                    ioSpecification.getDataInputs().add(dataSpec);

                } else if (xtr.isStartElement() && ELEMENT_DATA_OUTPUT.equalsIgnoreCase(xtr.getLocalName())) {
                    DataSpec dataSpec = new DataSpec();
                    BpmnXMLUtil.addXMLLocation(dataSpec, xtr);
                    dataSpec.setId(xtr.getAttributeValue(null, ATTRIBUTE_ID));
                    dataSpec.setName(xtr.getAttributeValue(null, ATTRIBUTE_NAME));
                    dataSpec.setItemSubjectRef(parseItemSubjectRef(xtr.getAttributeValue(null, ATTRIBUTE_ITEM_SUBJECT_REF), model));
                    ioSpecification.getDataOutputs().add(dataSpec);

                } else if (xtr.isStartElement() && ELEMENT_DATA_INPUT_REFS.equalsIgnoreCase(xtr.getLocalName())) {
                    String dataInputRefs = xtr.getElementText();
                    if (StringUtils.isNotEmpty(dataInputRefs)) {
                        ioSpecification.getDataInputRefs().add(dataInputRefs.trim());
                    }

                } else if (xtr.isStartElement() && ELEMENT_DATA_OUTPUT_REFS.equalsIgnoreCase(xtr.getLocalName())) {
                    String dataOutputRefs = xtr.getElementText();
                    if (StringUtils.isNotEmpty(dataOutputRefs)) {
                        ioSpecification.getDataOutputRefs().add(dataOutputRefs.trim());
                    }

                } else if (xtr.isEndElement() && getElementName().equalsIgnoreCase(xtr.getLocalName())) {
                    readyWithIOSpecification = true;
                }
            }
        } catch (Exception e) {
            LOGGER.warn("Error parsing ioSpecification child elements", e);
        }

        if (parentElement instanceof Process) {
            ((Process) parentElement).setIoSpecification(ioSpecification);
        } else {
            ((Activity) parentElement).setIoSpecification(ioSpecification);
        }
    }

    protected String parseItemSubjectRef(String itemSubjectRef, BpmnModel model) {
        String result = null;
        if (StringUtils.isNotEmpty(itemSubjectRef)) {
            int indexOfP = itemSubjectRef.indexOf(':');
            if (indexOfP != -1) {
                String prefix = itemSubjectRef.substring(0, indexOfP);
                String resolvedNamespace = model.getNamespace(prefix);
                result = resolvedNamespace + ":" + itemSubjectRef.substring(indexOfP + 1);
            } else {
                result = model.getTargetNamespace() + ":" + itemSubjectRef;
            }
        }
        return result;
    }
}
