/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.child.config.assignment;

import com.je.bpm.core.converter.converter.child.BaseChildElementParser;
import com.je.bpm.core.model.BaseElement;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.config.task.assignment.ProjectDepartmentAssignmentConfigImpl;
import com.je.bpm.core.model.config.task.assignment.TaskAssigneeConfigImpl;

import javax.xml.stream.XMLStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * 部门配置解析
 */
public class ProjectDeptAssignmentConfigParser extends AbstractTaskAssignConfigParser {

    private Map<String, BaseChildElementParser> childParserMap = new HashMap();

    public ProjectDeptAssignmentConfigParser() {
        TaskAssignmentResourceConfigParser taskAssignmentResourceConfigParser = new TaskAssignmentResourceConfigParser();
        TaskAssignmentPermissionConfigParser taskAssignmentPermissionConfigParser = new TaskAssignmentPermissionConfigParser();
        childParserMap.put(taskAssignmentResourceConfigParser.getElementName(), taskAssignmentResourceConfigParser);
        childParserMap.put(taskAssignmentPermissionConfigParser.getElementName(), taskAssignmentPermissionConfigParser);
    }

    @Override
    public void parseChildElement(XMLStreamReader xtr, BaseElement parentElement, BpmnModel model) throws Exception {
        TaskAssigneeConfigImpl taskAssigneeConfig = (TaskAssigneeConfigImpl) parentElement;
        ProjectDepartmentAssignmentConfigImpl userAssignmentConfig = new ProjectDepartmentAssignmentConfigImpl();
        userAssignmentConfig.setEntryPath(xtr.getAttributeValue(null, ELEMENT_ASSIGNMENT_RESOURCE_ENTRYPATH));
        userAssignmentConfig.setSql(xtr.getAttributeValue(null, PROPERTY_ASSIGNMENT_PERMISSION_SQL));
        parseChildElements(getElementName(), userAssignmentConfig, childParserMap, model, xtr);
        taskAssigneeConfig.addResourceConfig(userAssignmentConfig);
    }


    @Override
    public String getElementName() {
        return ELEMENT_PROJECT_DEPARTMENT_CONFIG;
    }

}
