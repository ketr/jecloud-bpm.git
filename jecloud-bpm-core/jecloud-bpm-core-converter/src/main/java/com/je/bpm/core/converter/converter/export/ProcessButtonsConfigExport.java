/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.export;

import com.google.common.base.Strings;
import com.je.bpm.core.converter.constants.BpmnXMLConstants;
import com.je.bpm.core.model.button.Button;
import com.je.bpm.core.model.process.Process;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamWriter;

public class ProcessButtonsConfigExport implements BpmnXMLConstants {

    public static void writeButtonsConfig(Process process, XMLStreamWriter xtw) throws Exception {
        xtw.writeStartElement(BPMN2_PREFIX, ELEMENT_BUTTONS_PROCESS_CONFIG, BPMN2_NAMESPACE);
        for (Button eachProcessButton : process.getButtons().getButtons()) {
            xtw.writeStartElement(BPMN2_PREFIX, ELEMENT_PROCESS_BUTTON, BPMN2_NAMESPACE);
            xtw.writeAttribute(ATTRIBUTE_ID, eachProcessButton.getId());
            xtw.writeAttribute(ATTRIBUTE_BUTTON_ID, eachProcessButton.getId());
            xtw.writeAttribute(ATTRIBUTE_BUTTON_NAME, eachProcessButton.getName());
            xtw.writeAttribute(ATTRIBUTE_BUTTON_CODE, eachProcessButton.getCode());
            xtw.writeAttribute(ATTRIBUTE_BUTTON_OPERATION, eachProcessButton.getOperationId());
            if (!Strings.isNullOrEmpty(eachProcessButton.getCustomizeName())) {
                xtw.writeAttribute(ATTRIBUTE_BUTTON_CUSTOMIZE_NAME, eachProcessButton.getCustomizeName());
            } else {
                xtw.writeAttribute(ATTRIBUTE_BUTTON_CUSTOMIZE_NAME, "");
            }
            if (Strings.isNullOrEmpty(eachProcessButton.getCustomizeComments())) {
                xtw.writeAttribute(ATTRIBUTE_BUTTON_CUSTOMIZE_COMMENTS, "");
            } else {
                xtw.writeAttribute(ATTRIBUTE_BUTTON_CUSTOMIZE_COMMENTS, eachProcessButton.getCustomizeComments());
            }

            if (StringUtils.isNotEmpty(eachProcessButton.getDisplayExpressionWhenStarted())) {
                xtw.writeAttribute(ATTRIBUTE_TASK_BUTTON_DISLAY_EXPRESSION, eachProcessButton.getDisplayExpressionWhenStarted());
            }
            xtw.writeAttribute(ATTRIBUTE_PROCESS_BUTTON_DISLAY_WHEN_NOT_STARTED, false ? ATTRIBUTE_VALUE_TRUE : ATTRIBUTE_VALUE_FALSE);
            if (StringUtils.isNotEmpty(eachProcessButton.getAppListeners())) {
                xtw.writeAttribute(ATTRIBUTE_BUTTON_APP_LISTENERS, eachProcessButton.getAppListeners());
            }
            if (StringUtils.isNotEmpty(eachProcessButton.getPcListeners())) {
                xtw.writeAttribute(ATTRIBUTE_BUTTON_PC_LISTENERS, eachProcessButton.getPcListeners());
            }
            if (StringUtils.isNotEmpty(eachProcessButton.getBackendListeners())) {
                xtw.writeAttribute(ATTRIBUTE_BUTTON_BACKEND_LISTENERS, eachProcessButton.getBackendListeners());
            }
            xtw.writeEndElement();
        }
        xtw.writeEndElement();
    }

}
