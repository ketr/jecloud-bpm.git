/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.core.converter.converter.export;

import com.je.bpm.core.converter.constants.BpmnXMLConstants;
import com.je.bpm.core.converter.converter.util.BpmnXMLUtil;
import com.je.bpm.core.model.ExtensionAttribute;
import com.je.bpm.core.model.process.Process;
import org.apache.commons.lang3.StringUtils;

import javax.xml.stream.XMLStreamWriter;
import java.util.List;

import static java.util.Arrays.asList;

public class ProcessExport implements BpmnXMLConstants {

    /**
     * default attributes taken from process instance attributes
     */
    public static final List<ExtensionAttribute> defaultProcessAttributes = asList(new ExtensionAttribute(ATTRIBUTE_ID),
            new ExtensionAttribute(ATTRIBUTE_NAME),
            new ExtensionAttribute(ATTRIBUTE_PROCESS_EXECUTABLE),
            new ExtensionAttribute("processBasicConfig"),
            new ExtensionAttribute(ATTRIBUTE_TABLECODE),
            new ExtensionAttribute(ATTRIBUTE_FUNCCODE),
            new ExtensionAttribute(ATTRIBUTE_FUNCNAME),
            new ExtensionAttribute(ATTRIBUTE_ATTACHEDFUNCIDS),
            new ExtensionAttribute(ATTRIBUTE_ATTACHEDFUNCNAMES),
            new ExtensionAttribute(ATTRIBUTE_ATTACHEDFUNCCODES),
            new ExtensionAttribute(ATTRIBUTE_CONTENTMODULE),
            new ExtensionAttribute(ATTRIBUTE_PROCESS_CLASSIFICATIONCODE),
            new ExtensionAttribute(ATTRIBUTE_PROCESS_CLASSIFICATIONNAME),
            new ExtensionAttribute(ACTIVITI_EXTENSIONS_NAMESPACE, ATTRIBUTE_PROCESS_CANDIDATE_USERS),
            new ExtensionAttribute(ACTIVITI_EXTENSIONS_NAMESPACE, ATTRIBUTE_PROCESS_CANDIDATE_GROUPS));

    public static void writeProcess(Process process, XMLStreamWriter xtw) throws Exception {
        // start process element
        xtw.writeStartElement(BPMN2_PREFIX, ELEMENT_PROCESS, BPMN2_NAMESPACE);
        xtw.writeAttribute(ATTRIBUTE_ID, process.getId());

        if (StringUtils.isNotEmpty(process.getName())) {
            xtw.writeAttribute(ATTRIBUTE_NAME, process.getName());
        }

        xtw.writeAttribute(ATTRIBUTE_PROCESS_EXECUTABLE, Boolean.toString(process.isExecutable()));

        if (!process.getCandidateStarterUsers().isEmpty()) {
            xtw.writeAttribute(ACTIVITI_EXTENSIONS_PREFIX, ACTIVITI_EXTENSIONS_NAMESPACE, ATTRIBUTE_PROCESS_CANDIDATE_USERS, BpmnXMLUtil.convertToDelimitedString(process.getCandidateStarterUsers()));
        }

        if (!process.getCandidateStarterGroups().isEmpty()) {
            xtw.writeAttribute(ACTIVITI_EXTENSIONS_PREFIX, ACTIVITI_EXTENSIONS_NAMESPACE, ATTRIBUTE_PROCESS_CANDIDATE_GROUPS, BpmnXMLUtil.convertToDelimitedString(process.getCandidateStarterGroups()));
        }

        // write custom attributes
        BpmnXMLUtil.writeCustomAttributes(process.getAttributes().values(), xtw, defaultProcessAttributes);
        if (StringUtils.isNotEmpty(process.getDocumentation())) {
            xtw.writeStartElement(BPMN2_PREFIX, ELEMENT_DOCUMENTATION, BPMN2_NAMESPACE);
            xtw.writeCharacters(process.getDocumentation());
            xtw.writeEndElement();
        }

        boolean didWriteExtensionStartElement = ActivitiListenerExport.writeListeners(process, false, xtw);
        didWriteExtensionStartElement = BpmnXMLUtil.writeExtensionElements(process, didWriteExtensionStartElement, xtw);
        if (didWriteExtensionStartElement) {
            // closing extensions element
            xtw.writeEndElement();
        }

        LaneExport.writeLanes(process, xtw);
        //流程配置 processConfig
        ProccessConfigExport.writeConfigs(process, xtw);
        //拓展配置
        ProccessExtendedConfigExport.writeConfigs(process, xtw);
        //自定义事件
        ProccessCustomEventListenersConfigExport.writeConfigs(process, xtw);
        //启动配置
        ProccessStartupSettingsConfigExport.writeConfigs(process, xtw);
        //消息
        ProccessMessageSettingConfigExport.writeConfigs(process, xtw);
        //按钮配置
        ProcessButtonsConfigExport.writeButtonsConfig(process, xtw);
    }
}
