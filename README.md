# JEBPM项目

## 项目简介

JECloudBPM项目，用于实现基于JECloud特色的工作流引擎，引擎基于Activiti7.1开发，为用户提供便捷的WEB端可视化的编辑界
面。引擎支持启动、发起、撤销、取回、提交、直送、退回、驳回、转办、委托、调拨、领取任务、改签、传阅、审阅、催办、作废、
加签、减签、回签、分支、聚合、会签、消息提醒、审批告知提醒、跳跃等多种具有中国特色的流程规则。

## 环境依赖

* jdk1.8
* maven
* 请使用官方仓库(http://maven.jepaas.com)，暂不同步jar到中央仓库。

> Maven安装 (http://maven.apache.org/download.cgi)

> Maven学习，请参考[maven-基础](docs/mannual/maven-基础.md)

## 服务模块介绍

- jecloud-bpm-api： API定义模块
- jecloud-bpm-api-impl: API定义实现模块
- jecloud-bpm-common: 通用模块
- jecloud-bpm-core: 核心实现模块
- jecloud-bpm-spring: spring集成模块
- jecloud-bpm-spring-boot-starter: springboot starter
- jecloud-bpm-test: jecloud集成测试模块

## 编译部署

``` shell
mvn clean package -Dmaven.test.skip=true
```

## 开源协议

- [MIT](./LICENSE)
- [平台证书补充协议](./SUPPLEMENTAL_LICENSE.md)

## JECloud主目录
[JECloud 微服务架构低代码平台（点击了解更多）](https://gitee.com/ketr/jecloud.git)