/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.common.operation;

public enum OperatorEnum {

    //----------------------------------------流程----------------------------------------

    /**
     * 获取流程定义操作
     */
    GET_PROCESS_DEFINITION_OPERATOR("getProcessDefinitionOperation", "获取流程定义操作"),
    /**
     * 获取流程定义集合操作
     */
    GET_PROCESS_DEFINITION_LIST_OPERATOR("getProcessDefinitionListOperation", "获取流程定义集合操作"),
    /**
     * 流程定义暂停操作
     */
    PROCESS_DEFINITION_HANG_OPERATOR("processDefinitionHangOperation", "流程定义暂停操作"),
    /**
     * 流程定义激活操作
     */
    PROCESS_DEFINITION_ACTIVE_OPERATOR("processDefinitionActiveOperation", "流程定义激活操作"),
    /**
     * 流程部署删除
     */
    PROCESS_DEPLOYMENT_DELETE_OPERATOR("processDeploymentDeleteOperation", "流程部署删除操作"),
    /**
     * 流程部署集合
     */
    PROCESS_DEPLOYMENT_LIST_OPERATOR("processDeploymentGetListOperation", "流程部署集合操作"),
    /**
     * 流程部署获取
     */
    PROCESS_DEPLOYMENT_GET_OPERATOR("processDeploymentGetOperation", "获取流程部署操作"),
    /**
     * 流程模型获取
     */
    PROCESS_MODEL_LIST_OPERATOR("processModelGetListOperation", "获取流程模型集合操作"),
    /**
     * 流程模型获取
     */
    PROCESS_MODEL_GET_OPERATOR("processModelGetOperation", "获取流程模型操作"),
    /**
     * 流程模型获取
     */
    PROCESS_GET_RUN_FROM_CONFIG_OPERATION("processGetRunFromConfigOperation", "获取流程模型操作"),
    /**
     * 流程模型获取
     */
    PROCESS_LAST_MODEL_GET_OPERATOR("processLastModelGetOperation", "获取流程模型操作"),
    /**
     * 流程模型删除
     */
    PROCESS_MODEL_DELETE_OPERATOR("processModelDeleteOperation", "删除流程模型操作"),
    /**
     * 流程模型部署
     */
    PROCESS_MODEL_DEPLOY_OPERATOR("processModelDeployOperation", "流程模型部署操作"),
    /**
     * 流程模型保存
     */
    PROCESS_MODEL_SAVE_OPERATOR("processModelSaveOperation", "保存流程模型操作"),
    /**
     * 流程空启动
     */
    PROCESS_EMPTY_START_OPERATOR("processEmptyStartOperation", "流程空启动操作"),
    /**
     * 流程按钮
     */
    PROCESS_GET_INITIAL_BUTTON_OPERATOR("processGetInitialButtonOperator", "流程按钮"),
    /**
     * 流程消息启动
     */
    PROCESS_MESSAGE_START_OPERATOR("processMessageStartOperation", "流程消息启动操作"),
    /**
     * 流程空发起
     */
    PROCESS_EMPTY_SPONSOR_OPERATOR("processEmptySponsorOperation", "流程空发起操作"),
    /**
     * 流程消息发起
     */
    PROCESS_MESSAGE_SPONSOR_OPERATOR("processMessageSponsorOperation", "流程消息发起操作"),
    /**
     * 流程挂起
     */
    PROCESS_HANG_OPERATOR("processHangOperation", "流程挂起操作"),
    /**
     * 流程激活
     */
    PROCESS_ACTIVE_OPERATOR("processActivateOperation", "流程激活操作"),
    /**
     * 流程撤销
     */
    PROCESS_CANCEL_OPERATOR("processCancelOperation", "流程撤销操作"),
    /**
     * 流程撤销
     */
    PROCESS_INVALID_OPERATOR("processInvalidOperation", "流程作废操作"),
    /**
     * 获取提交节点信息
     */
    PROCESS_GET_NEXT_ELEMENT_OPERATOR("processGetNextElementOperator", "提交节点信息"),
    /**
     * 获取提交节点信息处理人
     */
    PROCESS_GET_NEXT_ELEMENT_ASSIGNEE_OPERATOR("processGetNextElementAssigneeOperator", "提交节点信息"),
    /**
     * 获取传阅节点信息
     */
    PROCESS_GET_CIRCULATED_INFO_OPERATOR("processGetCirculatedInfoOperator", "获取传阅人员信息"),
    /**
     * 获取驳回节点信息
     */
    PROCESS_GET_DISMISS_ELEMENT_OPERATOR("processGetDismissElementOperator", "驳回节点信息"),
    /**
     * 获取委托节点信息
     */
    PROCESS_GET_DELEGATE_ELEMENT_OPERATOR("processGetDelegateElementOperator", "获取委托节点信息"),
    /**
     * 获取提交节点信息
     */
    PROCESS_GET_GO_BACK_OPERATOR("processGetGobackElementOperator", "退回节点信息"),

    //----------------------------------------任务----------------------------------------
    /**
     * 任务提交
     */
    TASK_SUBMIT_OPERATOR("taskSubmitOperation", "任务提交操作"),
    /**
     * 任务委托
     */
    TASK_DELEGATE_OPERATOR("taskDelegateOperation", "任务委托操作"),
    /**
     * 任务取消委托
     */
    TASK_CANCEL_DELEGATE_OPERATOR("taskCancelDelegateOperation", "任务取消委托操作"),
    /**
     * 加签节点操作
     */
    TASK_ADD_SIGNATURE_NODE_OPERATOR("taskAddSignatureNodeOperator", "加签节点操作"),
    /**
     * 减签节点操作
     */
    TASK_DEL_SIGNATURE_NODE_OPERATOR("taskDelSignatureNodeOperator", "减签节点操作"),
    /**
     * 任务更换负责人
     */
    TASK_CHANGE_ASSIGNEE_OPERATOR("taskChangeAssigneeOperation", "任务更换负责人操作"),
    /**
     * 任务单人节点处理人
     */
    TASK_ADJUSTING_PERSONNEL_OPERATION("taskAdjustingPersonnelOperation", "任务单人节点处理人"),
    /**
     * 任务领取
     */
    TASK_CLAIM_OPERATOR("taskClaimOperation", "任务领取操作"),
    /**
     * 任务延迟处理操作
     */
    TASK_DELAY_OPERATOR("taskDelayOperation", "任务延迟处理操作"),
    /**
     * 任务直送
     */
    TASK_DIRECT_SEND_OPERATOR("taskDirectSendOperation", "任务直送操作"),
    /**
     * 任务驳回
     */
    TASK_DISMISS_OPERATOR("taskDismissOperation", "任务驳回操作"),
    /**
     * 任务退回
     */
    TASK_GOBACK_OPERATOR("taskGobackOperation", "任务退回操作"),
    /**
     * 任务传阅
     */
    TASK_PASSROUND_OPERATOR("taskPassroundOperation", "任务传阅操作"),
    /**
     * 任务传阅已读
     */
    TASK_PASSROUND_READ_OPERATOR("taskPassroundReadOperation", "任务传阅已读操作"),
    /**
     * 任务取回
     */
    TASK_RETREIEVE_OPERATOR("taskRetrieveOperation", "任务取回操作"),
    /**
     * 任务撤回
     */
    TASK_WITHDRAW_OPERATOR("taskWithdrawOperator", "任务撤回操作"),
    /**
     * 任务转办
     */
    TASK_TRANSFER_OPERATOR("taskTransferOperation", "任务转办操作"),
    /**
     * 任务催办
     */
    TASK_URGE_OPERATOR("taskUrgeOperation", "任务催办操作"),
    /**
     * 任务通过
     */
    TASK_PASS_OPERATOR("taskPassOperator", "任务通过操作"),
    /**
     * 任务否决
     */
    TASK_VETO_OPERATOR("taskVetoOperator", "任务否决操作"),
    /**
     * 任务弃权
     */
    TASK_ABSTAIN_OPERATOR("taskAbstainOperator", "任务弃权操作"),
    /**
     * 会签-改签
     */
    TASK_REBOOK_OPERATOR("taskRebookOperator", "任务改签操作"),
    /**
     * 会签-人员调整
     */
    TASK_PERSONNEL_ADJUSTMENTS_OPERATOR("taskPersonnelAdjustmentsOperator", "人员调整"),
    /**
     * 会签-加签
     */
    TASK_COUNTERSIGNED_ADD_SIGNATURE_OPERATOR("taskCountersignedAddSignatureOperator", "会签-加签操作"),
    /**
     * 会签-减签
     */
    TASK_COUNTERSIGNED_VISA_REDUCTION_OPERATOR("taskCountersignedVisaReductionOperator", "会签-减签操作"),
    /**
     * 任务加签
     */
    TASK_COUNTERSIGN_OPERATOR("taskCountersignOperator", "任务加签操作"),
    /**
     * 任务回签
     */
    TASK_SIGN_BACK_OPERATOR("taskSignBackOperator", "任务回签操作"),
    /**
     * 暂存
     */
    TASK_STAGING_OPERATOR("taskStagingOperator", "任务暂存操作");

    public static OperatorEnum getOperatorEnumById(String id) {
        if (id.equals(GET_PROCESS_DEFINITION_OPERATOR.getId())) {
            return GET_PROCESS_DEFINITION_OPERATOR;
        } else if (id.equals(GET_PROCESS_DEFINITION_LIST_OPERATOR.getId())) {
            return GET_PROCESS_DEFINITION_LIST_OPERATOR;
        } else if (id.equals(PROCESS_DEFINITION_HANG_OPERATOR.getId())) {
            return PROCESS_DEFINITION_HANG_OPERATOR;
        } else if (id.equals(PROCESS_DEFINITION_ACTIVE_OPERATOR.getId())) {
            return PROCESS_DEFINITION_ACTIVE_OPERATOR;
        } else if (id.equals(PROCESS_DEPLOYMENT_DELETE_OPERATOR.getId())) {
            return PROCESS_DEPLOYMENT_DELETE_OPERATOR;
        } else if (id.equals(PROCESS_DEPLOYMENT_LIST_OPERATOR.getId())) {
            return PROCESS_DEPLOYMENT_LIST_OPERATOR;
        } else if (id.equals(PROCESS_DEPLOYMENT_GET_OPERATOR.getId())) {
            return PROCESS_DEPLOYMENT_GET_OPERATOR;
        } else if (id.equals(PROCESS_MODEL_LIST_OPERATOR.getId())) {
            return PROCESS_MODEL_LIST_OPERATOR;
        } else if (id.equals(PROCESS_MODEL_GET_OPERATOR.getId())) {
            return PROCESS_MODEL_GET_OPERATOR;
        } else if (id.equals(PROCESS_GET_RUN_FROM_CONFIG_OPERATION.getId())) {
            return PROCESS_GET_RUN_FROM_CONFIG_OPERATION;
        } else if (id.equals(PROCESS_LAST_MODEL_GET_OPERATOR.getId())) {
            return PROCESS_LAST_MODEL_GET_OPERATOR;
        } else if (id.equals(PROCESS_MODEL_DELETE_OPERATOR.getId())) {
            return PROCESS_MODEL_DELETE_OPERATOR;
        } else if (id.equals(PROCESS_MODEL_DEPLOY_OPERATOR.getId())) {
            return PROCESS_MODEL_DEPLOY_OPERATOR;
        } else if (id.equals(PROCESS_MODEL_SAVE_OPERATOR.getId())) {
            return PROCESS_MODEL_SAVE_OPERATOR;
        } else if (id.equals(PROCESS_EMPTY_START_OPERATOR.getId())) {
            return PROCESS_EMPTY_START_OPERATOR;
        } else if (id.equals(PROCESS_GET_INITIAL_BUTTON_OPERATOR.getId())) {
            return PROCESS_GET_INITIAL_BUTTON_OPERATOR;
        } else if (id.equals(PROCESS_MESSAGE_START_OPERATOR.getId())) {
            return PROCESS_MESSAGE_START_OPERATOR;
        } else if (id.equals(PROCESS_EMPTY_SPONSOR_OPERATOR.getId())) {
            return PROCESS_EMPTY_SPONSOR_OPERATOR;
        } else if (id.equals(PROCESS_MESSAGE_SPONSOR_OPERATOR.getId())) {
            return PROCESS_MESSAGE_SPONSOR_OPERATOR;
        } else if (id.equals(PROCESS_HANG_OPERATOR.getId())) {
            return PROCESS_HANG_OPERATOR;
        } else if (id.equals(PROCESS_ACTIVE_OPERATOR.getId())) {
            return PROCESS_ACTIVE_OPERATOR;
        } else if (id.equals(PROCESS_CANCEL_OPERATOR.getId())) {
            return PROCESS_CANCEL_OPERATOR;
        } else if (id.equals(PROCESS_INVALID_OPERATOR.getId())) {
            return PROCESS_INVALID_OPERATOR;
        } else if (id.equals(PROCESS_GET_NEXT_ELEMENT_OPERATOR.getId())) {
            return PROCESS_GET_NEXT_ELEMENT_OPERATOR;
        } else if (id.equals(PROCESS_GET_DISMISS_ELEMENT_OPERATOR.getId())) {
            return PROCESS_GET_DISMISS_ELEMENT_OPERATOR;
        } else if (id.equals(PROCESS_GET_DELEGATE_ELEMENT_OPERATOR.getId())) {
            return PROCESS_GET_DELEGATE_ELEMENT_OPERATOR;
        } else if (id.equals(PROCESS_GET_GO_BACK_OPERATOR.getId())) {
            return PROCESS_GET_GO_BACK_OPERATOR;
        } else if (id.equals(TASK_SUBMIT_OPERATOR.getId())) {
            return TASK_SUBMIT_OPERATOR;
        } else if (id.equals(TASK_DELEGATE_OPERATOR.getId())) {
            return TASK_DELEGATE_OPERATOR;
        } else if (id.equals(TASK_CANCEL_DELEGATE_OPERATOR.getId())) {
            return TASK_CANCEL_DELEGATE_OPERATOR;
        } else if (id.equals(TASK_CHANGE_ASSIGNEE_OPERATOR.getId())) {
            return TASK_CHANGE_ASSIGNEE_OPERATOR;
        } else if (id.equals(TASK_CLAIM_OPERATOR.getId())) {
            return TASK_CLAIM_OPERATOR;
        } else if (id.equals(TASK_DELAY_OPERATOR.getId())) {
            return TASK_DELAY_OPERATOR;
        } else if (id.equals(TASK_DIRECT_SEND_OPERATOR.getId())) {
            return TASK_DIRECT_SEND_OPERATOR;
        } else if (id.equals(TASK_DISMISS_OPERATOR.getId())) {
            return TASK_DISMISS_OPERATOR;
        } else if (id.equals(TASK_GOBACK_OPERATOR.getId())) {
            return TASK_GOBACK_OPERATOR;
        } else if (id.equals(TASK_PASSROUND_OPERATOR.getId())) {
            return TASK_PASSROUND_OPERATOR;
        } else if (id.equals(TASK_PASSROUND_READ_OPERATOR.getId())) {
            return TASK_PASSROUND_READ_OPERATOR;
        } else if (id.equals(TASK_RETREIEVE_OPERATOR.getId())) {
            return TASK_RETREIEVE_OPERATOR;
        } else if (id.equals(TASK_TRANSFER_OPERATOR.getId())) {
            return TASK_TRANSFER_OPERATOR;
        } else if (id.equals(TASK_URGE_OPERATOR.getId())) {
            return TASK_URGE_OPERATOR;
        } else if (id.equals(TASK_PASS_OPERATOR.getId())) {
            return TASK_PASS_OPERATOR;
        } else if (id.equals(TASK_VETO_OPERATOR.getId())) {
            return TASK_VETO_OPERATOR;
        } else if (id.equals(TASK_ABSTAIN_OPERATOR.getId())) {
            return TASK_ABSTAIN_OPERATOR;
        } else if (id.equals(TASK_REBOOK_OPERATOR.getId())) {
            return TASK_REBOOK_OPERATOR;
        } else if (id.equals(TASK_COUNTERSIGNED_ADD_SIGNATURE_OPERATOR.getId())) {
            return TASK_COUNTERSIGNED_ADD_SIGNATURE_OPERATOR;
        } else if (id.equals(TASK_COUNTERSIGNED_VISA_REDUCTION_OPERATOR.getId())) {
            return TASK_COUNTERSIGNED_VISA_REDUCTION_OPERATOR;
        } else if (id.equals(TASK_COUNTERSIGN_OPERATOR.getId())) {
            return TASK_COUNTERSIGN_OPERATOR;
        } else if (id.equals(TASK_SIGN_BACK_OPERATOR.getId())) {
            return TASK_SIGN_BACK_OPERATOR;
        } else if (id.equals(TASK_STAGING_OPERATOR.getId())) {
            return TASK_STAGING_OPERATOR;
        }
        return null;
    }


    private String id;
    private String name;

    OperatorEnum(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
