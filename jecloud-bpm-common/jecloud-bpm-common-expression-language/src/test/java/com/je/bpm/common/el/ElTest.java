/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.common.el;


import de.odysseus.el.ExpressionFactoryImpl;
import de.odysseus.el.util.SimpleContext;
import de.odysseus.el.util.SimpleResolver;
import org.junit.Assert;
import org.junit.Test;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import java.lang.reflect.Method;
import java.util.Date;

public class ElTest {

    @Test
    public void testSimpleEl(){
        //step1
        ExpressionFactory factory = new ExpressionFactoryImpl();
        SimpleContext context = new SimpleContext();
        //step2
        context.setVariable("var1", factory.createValueExpression("Hello", String.class));
        context.setVariable("var2", factory.createValueExpression("World", String.class));
        //step3
        String s = "{\"argIn1\":\"${var1}\",\"argIn2\":\"${var2}\"}";
        ValueExpression e = factory.createValueExpression(context, s, String.class);
        System.out.println(e.getValue(context));// --> {"argIn1":"Hello","argIn2":"World"}
    }

    @Test
    public void testMethodEl() throws NoSuchMethodException {
        // the ExpressionFactory implementation is de.odysseus.el.ExpressionFactoryImpl
        ExpressionFactory factory = new ExpressionFactoryImpl();
        // package de.odysseus.el.util provides a ready-to-use subclass of ELContext
        SimpleContext context = new SimpleContext();
        // map function math:max(int, int) to java.lang.Math.max(int, int)
        context.setFunction("math", "max", Math.class.getMethod("max", int.class, int.class));
        // map variable foo to 0
        context.setVariable("foo", factory.createValueExpression(0, int.class));
        // parse our expression
        ValueExpression e = factory.createValueExpression(context, "${math:max(foo,bar)}", int.class);
        // set value for top-level property "bar" to 1
        factory.createValueExpression(context, "${bar}", int.class).setValue(context, 1);
        // get value for our expression
        Assert.assertEquals(1,e.getValue(context));
    }

    @Test
    public void testSimpleResolverEl(){
        ExpressionFactory factory = new ExpressionFactoryImpl();
        SimpleContext context = new SimpleContext(new SimpleResolver());
        // resolve top-level property
        factory.createValueExpression(context, "#{pi}", double.class).setValue(context, Math.PI);
        ValueExpression expr1 = factory.createValueExpression(context, "${pi/2}", double.class);
        System.out.println("pi/2 = " + expr1.getValue(context));  // pi/2 = 1.5707963267948966
        // resolve bean property
        factory.createValueExpression(context, "#{current}", Date.class).setValue(context, new Date());
        ValueExpression expr2 = factory.createValueExpression(context, "${current.time}", long.class);
        System.out.println("current.time = " + expr2.getValue(context));// --> current.time = 1538048848843
    }

}
