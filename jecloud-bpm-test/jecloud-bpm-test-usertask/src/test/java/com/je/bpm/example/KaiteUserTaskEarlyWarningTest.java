/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.example;

import com.alibaba.fastjson2.JSONObject;
import com.je.bpm.core.converter.converter.BpmnXMLConverter;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.SequenceFlow;
import com.je.bpm.core.model.config.task.HandlePeriodTypeEnum;
import com.je.bpm.core.model.config.task.assignment.RoleAssignmentConfigImpl;
import com.je.bpm.core.model.event.EndEvent;
import com.je.bpm.core.model.event.StartEvent;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.core.model.task.KaiteUserTask;
import com.je.bpm.engine.ProcessEngine;
import com.je.bpm.engine.impl.util.io.ResourceStreamSource;
import com.je.bpm.engine.repository.Deployment;
import com.je.bpm.engine.repository.DeploymentBuilder;
import com.je.bpm.engine.runtime.ProcessInstance;
import com.je.bpm.engine.task.Task;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class KaiteUserTaskEarlyWarningTest {

    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private ProcessEngine processEngine;

    private BpmnModel bpmnModel;

    @Before
    public void setUser(){
        securityUtil.logInAs("system");
        bpmnModel = new BpmnModel();
        Process process = new Process();
        process.setName("测试延迟预警流程");
        process.setId("testDelayAndWarning");

        //开始节点的属性
        StartEvent startEvent=new StartEvent();
        startEvent.setId("start");
        startEvent.setName("start");

        RoleAssignmentConfigImpl manageRoleAssignmentConfig = new RoleAssignmentConfigImpl();

        RoleAssignmentConfigImpl developRoleAssignmentConfig = new RoleAssignmentConfigImpl();

        //节点1
        KaiteUserTask kaiteUserTask1 = new KaiteUserTask();
        kaiteUserTask1.setId("kaiteUserTask1");
        kaiteUserTask1.setName("凯特用户任务1");

        //节点2
        KaiteUserTask kaiteUserTask2 = new KaiteUserTask();
        kaiteUserTask2.setId("kaiteUserTask2");
        kaiteUserTask2.setName("凯特用户任务2");
        //处理时效配置
        kaiteUserTask2.getTaskBasicConfig().setHandlePeriodType(HandlePeriodTypeEnum.HOUR);
        //延迟配置
        //预警配置

        //节点3
        KaiteUserTask kaiteUserTask3 = new KaiteUserTask();
        kaiteUserTask3.setId("kaiteUserTask3");
        kaiteUserTask3.setName("凯特用户任务3");

        //结束节点属性
        EndEvent endEvent=new EndEvent();
        endEvent.setId("endEvent");
        endEvent.setName("endEvent");

        //连线信息
        List<SequenceFlow> sequenceFlows1=new ArrayList<SequenceFlow>();
        SequenceFlow s1=new SequenceFlow();
        s1.setId("start_to_kaiteUserTask1");
        s1.setName("start_to_kaiteUserTask1");
        s1.setSourceRef("start");
        s1.setTargetRef("kaiteUserTask1");
        sequenceFlows1.add(s1);

        List<SequenceFlow> sequenceFlows2=new ArrayList<SequenceFlow>();
        SequenceFlow s2=new SequenceFlow();
        s2.setId("kaiteUserTask1_kaiteUserTask2");
        s2.setName("kaiteUserTask1_kaiteUserTask2");
        s2.setSourceRef("kaiteUserTask1");
        s2.setTargetRef("kaiteUserTask2");
        sequenceFlows2.add(s2);

        List<SequenceFlow> sequenceFlows3=new ArrayList<SequenceFlow>();
        SequenceFlow s3=new SequenceFlow();
        s3.setId("kaiteUserTask2_kaiteUserTask3");
        s3.setName("kaiteUserTask2_kaiteUserTask3");
        s3.setSourceRef("kaiteUserTask2");
        s3.setTargetRef("kaiteUserTask3");
        sequenceFlows3.add(s3);

        List<SequenceFlow> toEnd=new ArrayList<SequenceFlow>();
        SequenceFlow s4=new SequenceFlow();
        s4.setId("kaiteUserTask3_endEvent");
        s4.setName("kaiteUserTask3_endEvent");
        s4.setSourceRef("kaiteUserTask3");
        s4.setTargetRef("endEvent");
        toEnd.add(s4);

        startEvent.setOutgoingFlows(sequenceFlows1);
        kaiteUserTask1.setIncomingFlows(sequenceFlows1);
        kaiteUserTask1.setOutgoingFlows(sequenceFlows2);
        kaiteUserTask2.setIncomingFlows(sequenceFlows2);
        kaiteUserTask2.setOutgoingFlows(sequenceFlows3);
        kaiteUserTask3.setIncomingFlows(sequenceFlows3);
        kaiteUserTask3.setOutgoingFlows(toEnd);
        endEvent.setIncomingFlows(toEnd);

        process.addFlowElement(startEvent);
        process.addFlowElement(kaiteUserTask1);
        process.addFlowElement(kaiteUserTask2);
        process.addFlowElement(kaiteUserTask3);
        process.addFlowElement(endEvent);
        process.addFlowElement(s1);
        process.addFlowElement(s2);
        process.addFlowElement(s3);
        process.addFlowElement(s4);

        bpmnModel.addProcess(process);
    }

    @Test
    public void convertToXml(){
        BpmnXMLConverter bpmnXMLConverter=new BpmnXMLConverter();
        byte[] convertToXML = bpmnXMLConverter.convertToXML(bpmnModel);
        String bytes=new String(convertToXML);
        System.out.println(bytes);
    }

    @Test
    public void convertToBpmnModel() throws IOException {
        BpmnXMLConverter bpmnXMLConverter=new BpmnXMLConverter();
        ResourceStreamSource resourceStreamSource = new ResourceStreamSource("process/testconverter.bpmn20.xml");
        bpmnModel = bpmnXMLConverter.convertToBpmnModel(resourceStreamSource,false,false);
        byte[] convertToXML = bpmnXMLConverter.convertToXML(bpmnModel);
        FileOutputStream fos = new FileOutputStream(new File("/Users/liulijun/Documents/testconverter.bpmn20.xml"));
        fos.write(convertToXML);
        fos.close();
    }

    @Test
    public void deploy(){
        DeploymentBuilder deploymentBuilder = processEngine.getRepositoryService().createDeployment().addClasspathResource("process/delayAndWarning.bpmn20.xml");
        deploymentBuilder.disableBpmnValidation();
        deploymentBuilder.disableSchemaValidation();
        deploymentBuilder.key("testDelayAndWarning");
        deploymentBuilder.name("testDelayAndWarning");
        deploymentBuilder.category("bpmnModel");
        Deployment deployment = deploymentBuilder.deploy();
        System.out.println(deployment.getId());
    }

    @Test
    public void sponsor(){
        ProcessInstance processInstance = processEngine.getRuntimeService().sponsorProcessInstanceByKey("testDelayAndWarning","");
        System.out.println(processInstance.getName());
        System.out.println(processInstance.getProcessInstanceId());
    }

    @Test
    public void checkAndEarlyWarning(){
        processEngine.getTaskService().checkAndEarlyWarning("04e2264e-79d4-11ec-8b1d-72b06cec201d");
    }

    @Test
    public void readEarlyWarning(){
        processEngine.getTaskService().earlyWarningRead("d9ab0aad-7a6a-11ec-ace5-ba29e9520d35");
    }

    @Test
    public void delay(){
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId("04e2264e-79d4-11ec-8b1d-72b06cec201d").list();
        for (Task each:tasks) {
            processEngine.getTaskService().delayTask(each.getId());
        }
    }

    @Test
    public void delayRead(){
        processEngine.getTaskService().delayLogRead("7bf1a2b7-7a6d-11ec-807b-ba29e9520d35");
    }

    @Test
    public void findTask(){
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId("7f872dc4-775a-11ec-9442-d64933d16f90").list();
        for (Task each : tasks) {
            Object variables = processEngine.getTaskService().getVariable(each.getId(),"configVariables");
            JSONObject jsonObject = JSONObject.parseObject(variables.toString());
            for (Map.Entry<String, Object> eachEntry:jsonObject.entrySet()) {
                System.out.println("-------" + eachEntry.getKey() + "-------" + eachEntry.getValue());
            }
        }
    }

    @Test
    public void completeTask(){
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId("8b232c8a-775b-11ec-a9c4-d64933d16f90").list();
        for (Task each : tasks) {
//            processEngine.getTaskService().complete(each.getId(),"test1");
//            processEngine.getTaskService().complete(each.getId());
        }
    }

    @Test
    public void delegateTask(){
        //测试委托
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId("8b232c8a-775b-11ec-a9c4-d64933d16f90").list();
        for (Task each : tasks) {
//            processEngine.getTaskService().delegateTask(each.getId(),"liu1","");
        }
    }

    @Test
    /**
     * 测试解决委托
     */
    public void cancelDelegateTask(){
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId("8b232c8a-775b-11ec-a9c4-d64933d16f90").list();
        for (Task each : tasks) {
//            processEngine.getTaskService().cancelDelegateTask(each.getId());
        }
    }

    @Test
    /**
     * 传阅
     */
    public void passRoundTask(){
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId("921b4cd7-7449-11ec-85d6-7a99d37ed607").list();
        for (Task each : tasks) {
//            processEngine.getTaskService().passRoundTask(each.getId(), Lists.newArrayList("user1","user2"));
        }
    }

    @Test
    /**
     * 已阅
     */
    public void readPassRound(){
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId("921b4cd7-7449-11ec-85d6-7a99d37ed607").list();
        for (Task each : tasks) {
            processEngine.getTaskService().readPassRoundTask(each.getId(),"user2","");
        }
    }

}
