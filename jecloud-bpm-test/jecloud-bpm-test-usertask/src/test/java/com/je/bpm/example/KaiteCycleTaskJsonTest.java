/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.example;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.converter.converter.BpmnXMLConverter;
import com.je.bpm.core.json.converter.json.converter.BpmnJsonConverter;
import com.je.bpm.core.layout.BpmnAutoLayout;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.SequenceFlow;
import com.je.bpm.core.model.config.process.ProcessBasicConfigImpl;
import com.je.bpm.core.model.config.task.*;
import com.je.bpm.core.model.config.task.assignment.RoleAssignmentConfigImpl;
import com.je.bpm.core.model.event.EndEvent;
import com.je.bpm.core.model.event.StartEvent;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.core.model.task.KaiteFixedUserTask;
import com.je.bpm.core.model.task.KaiteLoopUserTask;
import com.je.bpm.core.model.task.KaiteUserTask;
import com.je.bpm.engine.ProcessEngine;
import com.je.bpm.engine.impl.util.io.ResourceStreamSource;
import com.je.bpm.model.process.model.ProcessNextNodeInfo;
import com.je.bpm.model.process.model.ProcessRunForm;
import com.je.bpm.model.process.results.ProcessButtonListResult;
import com.je.bpm.model.process.results.ProcessModelResult;
import com.je.bpm.model.process.results.ProcessNextElementResult;
import com.je.bpm.model.shared.Result;
import com.je.bpm.model.shared.model.Button;
import com.je.bpm.model.task.model.TaskButton;
import com.je.bpm.model.task.result.TaskListResult;
import com.je.bpm.runtime.process.ProcessOperatorService;
import com.je.bpm.runtime.shared.operator.Operator;
import com.je.bpm.runtime.shared.operator.OperatorRegistry;
import com.je.bpm.runtime.shared.operator.validator.PayloadValidErrorException;
import com.je.bpm.runtime.task.TaskOperatorService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 自由
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class KaiteCycleTaskJsonTest {

    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private ProcessEngine processEngine;
    @Autowired
    private ProcessOperatorService processOperatorService;
    @Autowired
    private TaskOperatorService taskOperatorService;
    private BpmnModel bpmnModel;


    @Before
    public void setUser() {
        securityUtil.logInAs("zhangsan");
        bpmnModel = new BpmnModel();
        Process process = new Process();
        process.setName("请假申请");
        process.setId("qingjiashenqing");

        //开始节点的属性
        StartEvent startEvent = new StartEvent();
        startEvent.setId("start");
        startEvent.setName("start");

        RoleAssignmentConfigImpl manageRoleAssignmentConfig = new RoleAssignmentConfigImpl();

        RoleAssignmentConfigImpl developRoleAssignmentConfig = new RoleAssignmentConfigImpl();

        //节点1
        KaiteUserTask kaiteUserTask1 = new KaiteUserTask();
        kaiteUserTask1.setId("kaiteUserTask1");
        kaiteUserTask1.setName("凯特用户任务1");
        // 委托配置
        TaskDelegateConfigImpl taskDelegateConfig = new TaskDelegateConfigImpl();
        taskDelegateConfig.setEnable(true);
        // 驳回配置
        TaskDismissConfigImpl taskDismissConfig = new TaskDismissConfigImpl();
        taskDismissConfig.setEnable(true);
        taskDismissConfig.setDismissTaskId("kaiteUserTask1");
        // 任务退回配置
        TaskGobackConfigImpl taskGobackConfig = new TaskGobackConfigImpl();
        taskGobackConfig.setEnable(true);
        // 流程作废配置
        TaskInvalidConfigImpl taskInvalidConfig = new TaskInvalidConfigImpl();
        taskInvalidConfig.setEnable(true);
        // 任务传阅配置
        TaskPassRoundConfigImpl taskPassRoundConfig = new TaskPassRoundConfigImpl();
        taskPassRoundConfig.setEnable(true);
        // 催办配置
        TaskUrgeConfigImpl taskUrgeConfig = new TaskUrgeConfigImpl();
        taskUrgeConfig.setEnable(true);
        // 转办配置
        TaskTransferConfigImpl taskTransferConfig = new TaskTransferConfigImpl();
        taskTransferConfig.setEnable(true);
        // 取回配置
        TaskRetrieveConfigImpl taskRetrieveConfig = new TaskRetrieveConfigImpl();
        taskRetrieveConfig.setEnable(true);
        kaiteUserTask1.setTaskDismissConfig(taskDismissConfig);
        kaiteUserTask1.setTaskPassRoundConfig(taskPassRoundConfig);

        //节点2
        KaiteLoopUserTask kaiteUserTask2 = new KaiteLoopUserTask();
        kaiteUserTask2.setId("kaiteCounterSignUserTask");
        kaiteUserTask2.setName("凯特自由节点");

        kaiteUserTask2.setTaskDismissConfig(taskDismissConfig);
        kaiteUserTask2.setTaskPassRoundConfig(taskPassRoundConfig);

        //节点3
        KaiteFixedUserTask kaiteUserTask3 = new KaiteFixedUserTask();
        kaiteUserTask3.setId("kaiteUserTask3");
        kaiteUserTask3.setName("凯特固定人任务3");

        //节点4
        KaiteUserTask kaiteUserTask4 = new KaiteUserTask();
        kaiteUserTask4.setId("kaiteUserTask4");
        kaiteUserTask4.setName("凯特用户任务4");

        //结束节点属性
        EndEvent endEvent = new EndEvent();
        endEvent.setId("endEvent");
        endEvent.setName("endEvent");

        //连线信息
        List<SequenceFlow> sequenceFlows1 = new ArrayList();

        SequenceFlow s1 = new SequenceFlow();
        s1.setId("s1");
        s1.setName("s1");
        s1.setSourceRef("start");
        s1.setTargetRef("kaiteUserTask1");
        sequenceFlows1.add(s1);

        List<SequenceFlow> sequenceFlows2 = new ArrayList<>();
        SequenceFlow s2 = new SequenceFlow();
        s2.setId("s2");
        s2.setName("s2");
        s2.setSourceRef("kaiteUserTask1");
        s2.setTargetRef("kaiteCounterSignUserTask");
        sequenceFlows2.add(s2);

        List<SequenceFlow> sequenceFlows3 = new ArrayList<>();
        SequenceFlow s3 = new SequenceFlow();
        s3.setId("s3");
        s3.setName("s3");
        s3.setSourceRef("kaiteCounterSignUserTask");
        s3.setTargetRef("kaiteUserTask3");
        sequenceFlows3.add(s3);

        List<SequenceFlow> sequenceFlows4 = new ArrayList<>();
        sequenceFlows4.add(s3);

        SequenceFlow s4 = new SequenceFlow();
        s4.setId("s4");
        s4.setName("s4");
        s4.setSourceRef("kaiteUserTask3");
        s4.setTargetRef("kaiteUserTask4");
        sequenceFlows3.add(s4);

        List<SequenceFlow> sequenceFlows5 = new ArrayList<>();
        sequenceFlows5.add(s4);

        List<SequenceFlow> toEnd1 = new ArrayList();
        SequenceFlow s5 = new SequenceFlow();
        s5.setId("s5");
        s5.setName("s5");
        s5.setSourceRef("kaiteUserTask3");
        s5.setTargetRef("endEvent");
        toEnd1.add(s5);

        List<SequenceFlow> toEnd2 = new ArrayList();
        SequenceFlow s6 = new SequenceFlow();
        s6.setId("s6");
        s6.setName("s6");
        s6.setSourceRef("kaiteUserTask4");
        s6.setTargetRef("endEvent");
        toEnd2.add(s6);

        List<SequenceFlow> toEnd = new ArrayList();
        toEnd.add(s5);
        toEnd.add(s6);

        startEvent.setOutgoingFlows(sequenceFlows1);
        kaiteUserTask1.setIncomingFlows(sequenceFlows1);
        kaiteUserTask1.setOutgoingFlows(sequenceFlows2);
        kaiteUserTask2.setIncomingFlows(sequenceFlows2);
        kaiteUserTask2.setOutgoingFlows(sequenceFlows3);

        kaiteUserTask3.setIncomingFlows(sequenceFlows4);
        kaiteUserTask3.setOutgoingFlows(toEnd1);
        kaiteUserTask4.setIncomingFlows(sequenceFlows5);
        kaiteUserTask4.setOutgoingFlows(toEnd2);
        endEvent.setIncomingFlows(toEnd);

        process.addFlowElement(startEvent);
        process.addFlowElement(kaiteUserTask1);
        process.addFlowElement(kaiteUserTask2);
        process.addFlowElement(kaiteUserTask3);
        process.addFlowElement(kaiteUserTask4);
        process.addFlowElement(endEvent);
        process.addFlowElement(s1);
        process.addFlowElement(s2);
        process.addFlowElement(s3);
        process.addFlowElement(s4);
        process.addFlowElement(s5);
        process.addFlowElement(s6);
        ProcessBasicConfigImpl processBasicConfig = new ProcessBasicConfigImpl();
        process.setProcessConfig(processBasicConfig);
        bpmnModel.addProcess(process);
    }

    static String json2 = "{\"jebpm\":{\"id\":\"JEBPM\",\"url\":\"http://jepaas.com\"},\"bounds\":{\"lowerRight\":{\"x\":1485.0,\"y\":700.0},\"upperLeft\":{\"x\":0.0,\"y\":0.0}},\"resourceId\":\"canvas\",\"properties\":{\"process_id\":\"qingjiashenqing\",\"name\":\"请假申请\",\"processButtonConfig\":[{\"buttonId\":\"startBtn\",\"buttonCode\":\"startBtn\",\"buttonName\":\"启动\",\"operationId\":\"processEmptyStartOperation\",\"backendListeners\":null,\"appListeners\":null,\"pcListeners\":null,\"displayWhenNotStarted\":true,\"displayExpression\":\"startBtn\"},{\"buttonId\":\"sponsorBtn\",\"buttonCode\":\"sponsorBtn\",\"buttonName\":\"发起\",\"operationId\":\"processEmptySponsorOperation\",\"backendListeners\":null,\"appListeners\":null,\"pcListeners\":null,\"displayWhenNotStarted\":true,\"displayExpression\":\"sponsorBtn\"},{\"buttonId\":\"invalidBtn\",\"buttonCode\":\"invalidBtn\",\"buttonName\":\"作废\",\"operationId\":\"processInvalidOperation\",\"backendListeners\":null,\"appListeners\":null,\"pcListeners\":null,\"displayWhenNotStarted\":false,\"displayExpression\":\"invalidBtn\"},{\"buttonId\":\"hangBtn\",\"buttonCode\":\"hangBtn\",\"buttonName\":\"挂起\",\"operationId\":\"processHangOperation\",\"backendListeners\":null,\"appListeners\":null,\"pcListeners\":null,\"displayWhenNotStarted\":false,\"displayExpression\":\"hangBtn\"},{\"buttonId\":\"cancelBtn\",\"buttonCode\":\"cancelBtn\",\"buttonName\":\"撤销\",\"operationId\":\"processCancelOperation\",\"backendListeners\":null,\"appListeners\":null,\"pcListeners\":null,\"displayWhenNotStarted\":false,\"displayExpression\":\"cancelBtn\"},{\"buttonId\":\"activeBtn\",\"buttonCode\":\"activeBtn\",\"buttonName\":\"激活\",\"operationId\":\"processActivateOperation\",\"backendListeners\":null,\"appListeners\":null,\"pcListeners\":null,\"displayWhenNotStarted\":false,\"displayExpression\":\"activeBtn\"}],\"processBasicConfig\":{\"funcCode\":null,\"funcName\":null,\"mode\":\"PRODUCT\",\"canRetrieve\":true,\"canSponsor\":false,\"canTransfer\":true,\"canDelegate\":true,\"canUrged\":true,\"canInvalid\":true,\"canCancel\":true,\"predefined\":false,\"predefineType\":\"ONECE\",\"canEveryoneStart\":true,\"canReturn\":true,\"remindTypes\":\"PUSH\",\"displayTraceButton\":true},\"messages\":[],\"executionlisteners\":{\"executionListeners\":[]},\"eventlisteners\":{\"eventListeners\":[]},\"signaldefinitions\":[],\"messagedefinitions\":[]},\"childShapes\":[{\"bounds\":{\"lowerRight\":{\"x\":30.0,\"y\":97.0},\"upperLeft\":{\"x\":0.0,\"y\":67.0}},\"resourceId\":\"start\",\"childShapes\":[],\"stencil\":{\"id\":\"StartNoneEvent\"},\"properties\":{\"overrideid\":\"start\",\"name\":\"start\",\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"s1\"}]},{\"bounds\":{\"lowerRight\":{\"x\":180.0,\"y\":112.0},\"upperLeft\":{\"x\":80.0,\"y\":52.0}},\"resourceId\":\"kaiteUserTask1\",\"childShapes\":[],\"stencil\":{\"id\":\"KaiteUserTask\"},\"properties\":{\"overrideid\":\"kaiteUserTask1\",\"name\":\"凯特用户任务1\",\"assignmentConfig\":{\"roleConfig\":[{\"id\":\"manager\",\"code\":\"manager\",\"name\":\"管理员\"},{\"id\":\"develop\",\"code\":\"develop\",\"name\":\"开发人员\"}],\"departmentConfig\":[],\"positionConfig\":[],\"workGroupConfig\":[],\"userConfig\":[],\"businessSqlConfig\":{\"sql\":null},\"customConfig\":{\"serviceName\":null},\"formFieldConfig\":{\"formFieldCode\":null},\"orgTreeConfig\":{\"enable\":false},\"rbacSqlConfig\":{\"sql\":null},\"spectialConfig\":{\"types\":\"\"}},\"buttonsConfig\":[{\"operationId\":\"taskDelegateOperation\",\"buttonId\":\"delegateBtn\",\"buttonCode\":\"delegateBtn\",\"buttonName\":\"委托\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskCancelDelegateOperation\",\"buttonId\":\"cancelDelegateBtn\",\"buttonCode\":\"cancelDelegateBtn\",\"buttonName\":\"取消委托\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskSubmitOperation\",\"buttonId\":\"submitBtn\",\"buttonCode\":\"submitBtn\",\"buttonName\":\"提交\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskDirectSendOperation\",\"buttonId\":\"directSendBtn\",\"buttonCode\":\"directSendBtn\",\"buttonName\":\"直送\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskDismissOperation\",\"buttonId\":\"dismissBtn\",\"buttonCode\":\"dismissBtn\",\"buttonName\":\"驳回\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskRetrieveOperation\",\"buttonId\":\"retrieveBtn\",\"buttonCode\":\"retrieveBtn\",\"buttonName\":\"取回\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskTransferOperation\",\"buttonId\":\"transferBtn\",\"buttonCode\":\"transferBtn\",\"buttonName\":\"转办\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskGobackOperation\",\"buttonId\":\"gobackBtn\",\"buttonCode\":\"gobackBtn\",\"buttonName\":\"退回\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskPassroundOperation\",\"buttonId\":\"passroundBtn\",\"buttonCode\":\"passroundBtn\",\"buttonName\":\"传阅\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskPassroundReadOperation\",\"buttonId\":\"passroundReadBtn\",\"buttonCode\":\"passroundReadBtn\",\"buttonName\":\"已阅\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskChangeAssigneeOperation\",\"buttonId\":\"changeAssigneeBtn\",\"buttonCode\":\"changeAssigneeBtn\",\"buttonName\":\"更换负责人\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskUrgeOperation\",\"buttonId\":\"urgeBtn\",\"buttonCode\":\"urgeBtn\",\"buttonName\":\"催办\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskAbstainOperator\",\"buttonId\":\"abstainBtn\",\"buttonCode\":\"abstainBtn\",\"buttonName\":\"弃权\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskPassOperator\",\"buttonId\":\"passBtn\",\"buttonCode\":\"passBtn\",\"buttonName\":\"通过\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskVetoOperator\",\"buttonId\":\"vetoBtn\",\"buttonCode\":\"vetoBtn\",\"buttonName\":\"否决\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskCountersignedVisaReductionOperator\",\"buttonId\":\"countersignedVisaReductionBtn\",\"buttonCode\":\"countersignedVisaReductionBtn\",\"buttonName\":\"会签-减签\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskRebookOperator\",\"buttonId\":\"countersignedRebookReductionBtn\",\"buttonCode\":\"countersignedRebookReductionBtn\",\"buttonName\":\"会签-改签\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskCountersignedAddSignatureOperator\",\"buttonId\":\"countersignedAddSignatureBtn\",\"buttonCode\":\"countersignedAddSignatureBtn\",\"buttonName\":\"会签-加签\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskPassOperator\",\"buttonId\":\"passBtn\",\"buttonCode\":\"passBtn\",\"buttonName\":\"领取任务\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":\"\"}],\"commentsConfig\":[],\"delayConfig\":{\"enable\":false,\"delayTimeType\":\"DAY\",\"delayPeriod\":0,\"delayTimes\":2},\"delegateConfig\":{\"enable\":true},\"dismissConfig\":{\"enable\":true,\"directSendAfterDismiss\":false,\"disableSubmitAfterDismiss\":false,\"dismissTaskName\":\"凯特用户任务1\",\"dismissTaskId\":\"kaiteUserTask1\"},\"earlyWarningStrategy\":{\"strategyType\":\"HOUR\",\"strategyPeriod\":1,\"strategyServiceName\":null},\"earlyWarningConfig\":{\"enable\":false,\"workday\":true},\"gobackConfig\":{\"enable\":true,\"directSubmitAfterGoback\":false,\"disableSubmitAfterGoback\":false},\"invalidConfig\":{\"enable\":true},\"labelCommentConfig\":[],\"passRoundConfig\":{\"enable\":true,\"auto\":false,\"dept\":false,\"position\":false,\"starter\":false,\"formField\":\"\",\"custom\":false,\"serviceName\":null,\"departmentConfig\":[],\"roleConfig\":[],\"positionConfig\":[],\"userConfig\":[],\"workGroupConfig\":[]},\"predefinedConfig\":{\"enable\":false},\"remindConfig\":{\"enable\":true,\"remindType\":\"\"},\"retrieveConfig\":{\"enable\":true},\"transferConfig\":{\"enable\":true},\"urgeConfig\":{\"enable\":true},\"formBasic\":{\"editable\":false},\"formButtons\":[],\"formChildFuncs\":[],\"formFields\":[],\"fieldValues\":[],\"jumpConfig\":{\"enable\":false,\"jumpTasks\":\"\"},\"basicConfig\":{\"documentation\":null,\"name\":null,\"icon\":null,\"label\":null,\"handlePeriod\":null,\"allUserDefaultSelected\":false,\"asyncTree\":false},\"listenerConfig\":[],\"categorydefinition\":\"kaiteUserTask\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"s2\"}]},{\"bounds\":{\"lowerRight\":{\"x\":330.0,\"y\":115.0},\"upperLeft\":{\"x\":230.0,\"y\":55.0}},\"resourceId\":\"kaiteCounterSignUserTask\",\"childShapes\":[],\"stencil\":{\"id\":\"KaiteLoopUserTask\"},\"properties\":{\"overrideid\":\"kaiteCounterSignUserTask\",\"name\":\"凯特自由节点\",\"assignmentConfig\":{\"roleConfig\":[{\"id\":\"manager\",\"code\":\"manager\",\"name\":\"管理员\"},{\"id\":\"develop\",\"code\":\"develop\",\"name\":\"开发人员\"}],\"departmentConfig\":[],\"positionConfig\":[],\"workGroupConfig\":[],\"userConfig\":[],\"businessSqlConfig\":{\"sql\":null},\"customConfig\":{\"serviceName\":null},\"formFieldConfig\":{\"formFieldCode\":null},\"orgTreeConfig\":{\"enable\":false},\"rbacSqlConfig\":{\"sql\":null},\"spectialConfig\":{\"types\":\"\"}},\"buttonsConfig\":[{\"operationId\":\"taskDelegateOperation\",\"buttonId\":\"delegateBtn\",\"buttonCode\":\"delegateBtn\",\"buttonName\":\"委托\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskCancelDelegateOperation\",\"buttonId\":\"cancelDelegateBtn\",\"buttonCode\":\"cancelDelegateBtn\",\"buttonName\":\"取消委托\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskSubmitOperation\",\"buttonId\":\"submitBtn\",\"buttonCode\":\"submitBtn\",\"buttonName\":\"提交\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskDirectSendOperation\",\"buttonId\":\"directSendBtn\",\"buttonCode\":\"directSendBtn\",\"buttonName\":\"直送\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskDismissOperation\",\"buttonId\":\"dismissBtn\",\"buttonCode\":\"dismissBtn\",\"buttonName\":\"驳回\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskRetrieveOperation\",\"buttonId\":\"retrieveBtn\",\"buttonCode\":\"retrieveBtn\",\"buttonName\":\"取回\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskTransferOperation\",\"buttonId\":\"transferBtn\",\"buttonCode\":\"transferBtn\",\"buttonName\":\"转办\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskGobackOperation\",\"buttonId\":\"gobackBtn\",\"buttonCode\":\"gobackBtn\",\"buttonName\":\"退回\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskPassroundOperation\",\"buttonId\":\"passroundBtn\",\"buttonCode\":\"passroundBtn\",\"buttonName\":\"传阅\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskPassroundReadOperation\",\"buttonId\":\"passroundReadBtn\",\"buttonCode\":\"passroundReadBtn\",\"buttonName\":\"已阅\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskChangeAssigneeOperation\",\"buttonId\":\"changeAssigneeBtn\",\"buttonCode\":\"changeAssigneeBtn\",\"buttonName\":\"更换负责人\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskUrgeOperation\",\"buttonId\":\"urgeBtn\",\"buttonCode\":\"urgeBtn\",\"buttonName\":\"催办\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskAbstainOperator\",\"buttonId\":\"abstainBtn\",\"buttonCode\":\"abstainBtn\",\"buttonName\":\"弃权\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskPassOperator\",\"buttonId\":\"passBtn\",\"buttonCode\":\"passBtn\",\"buttonName\":\"通过\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskVetoOperator\",\"buttonId\":\"vetoBtn\",\"buttonCode\":\"vetoBtn\",\"buttonName\":\"否决\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskCountersignedVisaReductionOperator\",\"buttonId\":\"countersignedVisaReductionBtn\",\"buttonCode\":\"countersignedVisaReductionBtn\",\"buttonName\":\"会签-减签\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskRebookOperator\",\"buttonId\":\"countersignedRebookReductionBtn\",\"buttonCode\":\"countersignedRebookReductionBtn\",\"buttonName\":\"会签-改签\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskCountersignedAddSignatureOperator\",\"buttonId\":\"countersignedAddSignatureBtn\",\"buttonCode\":\"countersignedAddSignatureBtn\",\"buttonName\":\"会签-加签\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskPassOperator\",\"buttonId\":\"passBtn\",\"buttonCode\":\"passBtn\",\"buttonName\":\"领取任务\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":\"\"}],\"commentsConfig\":[],\"delayConfig\":{\"enable\":false,\"delayTimeType\":\"DAY\",\"delayPeriod\":0,\"delayTimes\":2},\"delegateConfig\":{\"enable\":true},\"dismissConfig\":{\"enable\":true,\"directSendAfterDismiss\":false,\"disableSubmitAfterDismiss\":false,\"dismissTaskName\":\"凯特用户任务1\",\"dismissTaskId\":\"kaiteUserTask1\"},\"earlyWarningStrategy\":{\"strategyType\":\"HOUR\",\"strategyPeriod\":1,\"strategyServiceName\":null},\"earlyWarningConfig\":{\"enable\":false,\"workday\":true},\"gobackConfig\":{\"enable\":true,\"directSubmitAfterGoback\":false,\"disableSubmitAfterGoback\":false},\"invalidConfig\":{\"enable\":true},\"labelCommentConfig\":[],\"passRoundConfig\":{\"enable\":true,\"auto\":false,\"dept\":false,\"position\":false,\"starter\":false,\"formField\":\"\",\"custom\":false,\"serviceName\":null,\"departmentConfig\":[],\"roleConfig\":[],\"positionConfig\":[],\"userConfig\":[],\"workGroupConfig\":[]},\"predefinedConfig\":{\"enable\":false},\"remindConfig\":{\"enable\":true,\"remindType\":\"\"},\"retrieveConfig\":{\"enable\":true},\"transferConfig\":{\"enable\":true},\"urgeConfig\":{\"enable\":true},\"formBasic\":{\"editable\":false},\"formButtons\":[],\"formChildFuncs\":[],\"formFields\":[],\"fieldValues\":[],\"jumpConfig\":{\"enable\":false,\"jumpTasks\":\"\"},\"basicConfig\":{\"documentation\":null,\"name\":null,\"icon\":null,\"label\":null,\"handlePeriod\":null,\"allUserDefaultSelected\":false,\"asyncTree\":false},\"listenerConfig\":[],\"categorydefinition\":\"kaiteLoopUserTask\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"s3\"}]},{\"bounds\":{\"lowerRight\":{\"x\":480.0,\"y\":120.0},\"upperLeft\":{\"x\":380.0,\"y\":60.0}},\"resourceId\":\"kaiteUserTask3\",\"childShapes\":[],\"stencil\":{\"id\":\"KaiteFixUserTask\"},\"properties\":{\"overrideid\":\"kaiteUserTask3\",\"name\":\"凯特固定人任务3\",\"fixedUserConfig\":{\"id\":null,\"code\":null,\"name\":null},\"buttonsConfig\":[{\"operationId\":\"taskDelegateOperation\",\"buttonId\":\"delegateBtn\",\"buttonCode\":\"delegateBtn\",\"buttonName\":\"委托\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskCancelDelegateOperation\",\"buttonId\":\"cancelDelegateBtn\",\"buttonCode\":\"cancelDelegateBtn\",\"buttonName\":\"取消委托\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskSubmitOperation\",\"buttonId\":\"submitBtn\",\"buttonCode\":\"submitBtn\",\"buttonName\":\"提交\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskDirectSendOperation\",\"buttonId\":\"directSendBtn\",\"buttonCode\":\"directSendBtn\",\"buttonName\":\"直送\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskDismissOperation\",\"buttonId\":\"dismissBtn\",\"buttonCode\":\"dismissBtn\",\"buttonName\":\"驳回\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskRetrieveOperation\",\"buttonId\":\"retrieveBtn\",\"buttonCode\":\"retrieveBtn\",\"buttonName\":\"取回\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskTransferOperation\",\"buttonId\":\"transferBtn\",\"buttonCode\":\"transferBtn\",\"buttonName\":\"转办\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskGobackOperation\",\"buttonId\":\"gobackBtn\",\"buttonCode\":\"gobackBtn\",\"buttonName\":\"退回\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskPassroundOperation\",\"buttonId\":\"passroundBtn\",\"buttonCode\":\"passroundBtn\",\"buttonName\":\"传阅\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskPassroundReadOperation\",\"buttonId\":\"passroundReadBtn\",\"buttonCode\":\"passroundReadBtn\",\"buttonName\":\"已阅\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskChangeAssigneeOperation\",\"buttonId\":\"changeAssigneeBtn\",\"buttonCode\":\"changeAssigneeBtn\",\"buttonName\":\"更换负责人\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskUrgeOperation\",\"buttonId\":\"urgeBtn\",\"buttonCode\":\"urgeBtn\",\"buttonName\":\"催办\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskAbstainOperator\",\"buttonId\":\"abstainBtn\",\"buttonCode\":\"abstainBtn\",\"buttonName\":\"弃权\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskPassOperator\",\"buttonId\":\"passBtn\",\"buttonCode\":\"passBtn\",\"buttonName\":\"通过\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskVetoOperator\",\"buttonId\":\"vetoBtn\",\"buttonCode\":\"vetoBtn\",\"buttonName\":\"否决\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskCountersignedVisaReductionOperator\",\"buttonId\":\"countersignedVisaReductionBtn\",\"buttonCode\":\"countersignedVisaReductionBtn\",\"buttonName\":\"会签-减签\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskRebookOperator\",\"buttonId\":\"countersignedRebookReductionBtn\",\"buttonCode\":\"countersignedRebookReductionBtn\",\"buttonName\":\"会签-改签\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskCountersignedAddSignatureOperator\",\"buttonId\":\"countersignedAddSignatureBtn\",\"buttonCode\":\"countersignedAddSignatureBtn\",\"buttonName\":\"会签-加签\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskPassOperator\",\"buttonId\":\"passBtn\",\"buttonCode\":\"passBtn\",\"buttonName\":\"领取任务\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":\"\"}],\"commentsConfig\":[],\"delayConfig\":{\"enable\":false,\"delayTimeType\":\"DAY\",\"delayPeriod\":0,\"delayTimes\":2},\"delegateConfig\":{\"enable\":false},\"dismissConfig\":{\"enable\":false,\"directSendAfterDismiss\":false,\"disableSubmitAfterDismiss\":false,\"dismissTaskName\":null,\"dismissTaskId\":null},\"earlyWarningStrategy\":{\"strategyType\":\"HOUR\",\"strategyPeriod\":1,\"strategyServiceName\":null},\"earlyWarningConfig\":{\"enable\":false,\"workday\":true},\"gobackConfig\":{\"enable\":true,\"directSubmitAfterGoback\":false,\"disableSubmitAfterGoback\":false},\"invalidConfig\":{\"enable\":false},\"labelCommentConfig\":[],\"passRoundConfig\":{\"enable\":false,\"auto\":false,\"dept\":false,\"position\":false,\"starter\":false,\"formField\":\"\",\"custom\":false,\"serviceName\":null,\"departmentConfig\":[],\"roleConfig\":[],\"positionConfig\":[],\"userConfig\":[],\"workGroupConfig\":[]},\"predefinedConfig\":{\"enable\":false},\"remindConfig\":{\"enable\":true,\"remindType\":\"\"},\"retrieveConfig\":{\"enable\":true},\"transferConfig\":{\"enable\":false},\"urgeConfig\":{\"enable\":false},\"formBasic\":{\"editable\":false},\"formButtons\":[],\"formChildFuncs\":[],\"formFields\":[],\"fieldValues\":[],\"jumpConfig\":{\"enable\":false,\"jumpTasks\":\"\"},\"basicConfig\":{\"documentation\":null,\"name\":null,\"icon\":null,\"label\":null,\"handlePeriod\":null,\"allUserDefaultSelected\":false,\"asyncTree\":false},\"listenerConfig\":[],\"categorydefinition\":\"kaiteFixedUserTask\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"s5\"}]},{\"bounds\":{\"lowerRight\":{\"x\":630.0,\"y\":60.0},\"upperLeft\":{\"x\":530.0,\"y\":0.0}},\"resourceId\":\"kaiteUserTask4\",\"childShapes\":[],\"stencil\":{\"id\":\"KaiteUserTask\"},\"properties\":{\"overrideid\":\"kaiteUserTask4\",\"name\":\"凯特用户任务4\",\"assignmentConfig\":{\"roleConfig\":[{\"id\":\"manager\",\"code\":\"manager\",\"name\":\"管理员\"},{\"id\":\"develop\",\"code\":\"develop\",\"name\":\"开发人员\"}],\"departmentConfig\":[],\"positionConfig\":[],\"workGroupConfig\":[],\"userConfig\":[],\"businessSqlConfig\":{\"sql\":null},\"customConfig\":{\"serviceName\":null},\"formFieldConfig\":{\"formFieldCode\":null},\"orgTreeConfig\":{\"enable\":false},\"rbacSqlConfig\":{\"sql\":null},\"spectialConfig\":{\"types\":\"\"}},\"buttonsConfig\":[{\"operationId\":\"taskDelegateOperation\",\"buttonId\":\"delegateBtn\",\"buttonCode\":\"delegateBtn\",\"buttonName\":\"委托\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskCancelDelegateOperation\",\"buttonId\":\"cancelDelegateBtn\",\"buttonCode\":\"cancelDelegateBtn\",\"buttonName\":\"取消委托\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskSubmitOperation\",\"buttonId\":\"submitBtn\",\"buttonCode\":\"submitBtn\",\"buttonName\":\"提交\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskDirectSendOperation\",\"buttonId\":\"directSendBtn\",\"buttonCode\":\"directSendBtn\",\"buttonName\":\"直送\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskDismissOperation\",\"buttonId\":\"dismissBtn\",\"buttonCode\":\"dismissBtn\",\"buttonName\":\"驳回\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskRetrieveOperation\",\"buttonId\":\"retrieveBtn\",\"buttonCode\":\"retrieveBtn\",\"buttonName\":\"取回\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskTransferOperation\",\"buttonId\":\"transferBtn\",\"buttonCode\":\"transferBtn\",\"buttonName\":\"转办\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskGobackOperation\",\"buttonId\":\"gobackBtn\",\"buttonCode\":\"gobackBtn\",\"buttonName\":\"退回\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskPassroundOperation\",\"buttonId\":\"passroundBtn\",\"buttonCode\":\"passroundBtn\",\"buttonName\":\"传阅\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskPassroundReadOperation\",\"buttonId\":\"passroundReadBtn\",\"buttonCode\":\"passroundReadBtn\",\"buttonName\":\"已阅\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskChangeAssigneeOperation\",\"buttonId\":\"changeAssigneeBtn\",\"buttonCode\":\"changeAssigneeBtn\",\"buttonName\":\"更换负责人\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskUrgeOperation\",\"buttonId\":\"urgeBtn\",\"buttonCode\":\"urgeBtn\",\"buttonName\":\"催办\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskAbstainOperator\",\"buttonId\":\"abstainBtn\",\"buttonCode\":\"abstainBtn\",\"buttonName\":\"弃权\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskPassOperator\",\"buttonId\":\"passBtn\",\"buttonCode\":\"passBtn\",\"buttonName\":\"通过\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskVetoOperator\",\"buttonId\":\"vetoBtn\",\"buttonCode\":\"vetoBtn\",\"buttonName\":\"否决\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskCountersignedVisaReductionOperator\",\"buttonId\":\"countersignedVisaReductionBtn\",\"buttonCode\":\"countersignedVisaReductionBtn\",\"buttonName\":\"会签-减签\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskRebookOperator\",\"buttonId\":\"countersignedRebookReductionBtn\",\"buttonCode\":\"countersignedRebookReductionBtn\",\"buttonName\":\"会签-改签\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskCountersignedAddSignatureOperator\",\"buttonId\":\"countersignedAddSignatureBtn\",\"buttonCode\":\"countersignedAddSignatureBtn\",\"buttonName\":\"会签-加签\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":null},{\"operationId\":\"taskPassOperator\",\"buttonId\":\"passBtn\",\"buttonCode\":\"passBtn\",\"buttonName\":\"领取任务\",\"appListeners\":null,\"pcListeners\":null,\"backendListeners\":null,\"displayExpression\":\"\"}],\"commentsConfig\":[],\"delayConfig\":{\"enable\":false,\"delayTimeType\":\"DAY\",\"delayPeriod\":0,\"delayTimes\":2},\"delegateConfig\":{\"enable\":false},\"dismissConfig\":{\"enable\":false,\"directSendAfterDismiss\":false,\"disableSubmitAfterDismiss\":false,\"dismissTaskName\":null,\"dismissTaskId\":null},\"earlyWarningStrategy\":{\"strategyType\":\"HOUR\",\"strategyPeriod\":1,\"strategyServiceName\":null},\"earlyWarningConfig\":{\"enable\":false,\"workday\":true},\"gobackConfig\":{\"enable\":true,\"directSubmitAfterGoback\":false,\"disableSubmitAfterGoback\":false},\"invalidConfig\":{\"enable\":false},\"labelCommentConfig\":[],\"passRoundConfig\":{\"enable\":false,\"auto\":false,\"dept\":false,\"position\":false,\"starter\":false,\"formField\":\"\",\"custom\":false,\"serviceName\":null,\"departmentConfig\":[],\"roleConfig\":[],\"positionConfig\":[],\"userConfig\":[],\"workGroupConfig\":[]},\"predefinedConfig\":{\"enable\":false},\"remindConfig\":{\"enable\":true,\"remindType\":\"\"},\"retrieveConfig\":{\"enable\":true},\"transferConfig\":{\"enable\":false},\"urgeConfig\":{\"enable\":false},\"formBasic\":{\"editable\":false},\"formButtons\":[],\"formChildFuncs\":[],\"formFields\":[],\"fieldValues\":[],\"jumpConfig\":{\"enable\":false,\"jumpTasks\":\"\"},\"basicConfig\":{\"documentation\":null,\"name\":null,\"icon\":null,\"label\":null,\"handlePeriod\":null,\"allUserDefaultSelected\":false,\"asyncTree\":false},\"listenerConfig\":[],\"categorydefinition\":\"kaiteUserTask\",\"asynchronousdefinition\":false,\"exclusivedefinition\":true,\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[{\"resourceId\":\"s6\"}]},{\"bounds\":{\"lowerRight\":{\"x\":710.0,\"y\":110.0},\"upperLeft\":{\"x\":680.0,\"y\":80.0}},\"resourceId\":\"endEvent\",\"childShapes\":[],\"stencil\":{\"id\":\"EndNoneEvent\"},\"properties\":{\"overrideid\":\"endEvent\",\"name\":\"endEvent\",\"executionlisteners\":{\"executionListeners\":[]}},\"outgoing\":[]},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"s1\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":15.0,\"y\":15.0},{\"x\":50.0,\"y\":30.0}],\"outgoing\":[{\"resourceId\":\"kaiteUserTask1\"}],\"target\":{\"resourceId\":\"kaiteUserTask1\"},\"properties\":{\"overrideid\":\"s1\",\"name\":\"s1\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"s2\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":50.0,\"y\":30.0},{\"x\":192.0,\"y\":82.0},{\"x\":192.0,\"y\":85.0},{\"x\":50.0,\"y\":30.0}],\"outgoing\":[{\"resourceId\":\"kaiteCounterSignUserTask\"}],\"target\":{\"resourceId\":\"kaiteCounterSignUserTask\"},\"properties\":{\"overrideid\":\"s2\",\"name\":\"s2\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"s3\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":50.0,\"y\":30.0},{\"x\":342.0,\"y\":85.0},{\"x\":342.0,\"y\":90.0},{\"x\":50.0,\"y\":30.0}],\"outgoing\":[{\"resourceId\":\"kaiteUserTask3\"}],\"target\":{\"resourceId\":\"kaiteUserTask3\"},\"properties\":{\"overrideid\":\"s3\",\"name\":\"s3\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"s4\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":50.0,\"y\":30.0},{\"x\":492.0,\"y\":77.5},{\"x\":492.0,\"y\":30.0},{\"x\":50.0,\"y\":30.0}],\"outgoing\":[{\"resourceId\":\"kaiteUserTask4\"}],\"target\":{\"resourceId\":\"kaiteUserTask4\"},\"properties\":{\"overrideid\":\"s4\",\"name\":\"s4\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"s5\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":50.0,\"y\":30.0},{\"x\":492.0,\"y\":102.5},{\"x\":492.0,\"y\":95.0},{\"x\":15.0,\"y\":15.0}],\"outgoing\":[{\"resourceId\":\"endEvent\"}],\"target\":{\"resourceId\":\"endEvent\"},\"properties\":{\"overrideid\":\"s5\",\"name\":\"s5\"}},{\"bounds\":{\"lowerRight\":{\"x\":172.0,\"y\":212.0},\"upperLeft\":{\"x\":128.0,\"y\":212.0}},\"resourceId\":\"s6\",\"childShapes\":[],\"stencil\":{\"id\":\"SequenceFlow\"},\"dockers\":[{\"x\":50.0,\"y\":30.0},{\"x\":642.0,\"y\":30.0},{\"x\":642.0,\"y\":95.0},{\"x\":15.0,\"y\":15.0}],\"outgoing\":[{\"resourceId\":\"endEvent\"}],\"target\":{\"resourceId\":\"endEvent\"},\"properties\":{\"overrideid\":\"s6\",\"name\":\"s6\"}}]}";

    @Test
    public void convertToJson() throws IOException {
        new BpmnAutoLayout(bpmnModel).execute();
        BpmnJsonConverter bpmnJsonConverter = new BpmnJsonConverter();
        ObjectNode objectNode = bpmnJsonConverter.convertToJson(bpmnModel);
        String property = objectNode.toPrettyString();
        File f = new File("C:\\Users\\Administrator\\Desktop\\json.txt");
        f.createNewFile();
        FileOutputStream fileOutputStream = new FileOutputStream(f);
        PrintStream printStream = new PrintStream(fileOutputStream);
        System.setOut(printStream);
        System.out.println(property);
    }

    @Test
    public void convertToBpmnModel() throws IOException {
        BpmnJsonConverter bpmnJsonConverter = new BpmnJsonConverter();
        ResourceStreamSource resourceStreamSource = new ResourceStreamSource("process/formConfig.bpmn20.json");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode modelNode = mapper.readTree(resourceStreamSource.getInputStream());
        bpmnModel = bpmnJsonConverter.convertToBpmnModel(modelNode);
        System.out.println(1111);
    }

    @Test
    public void convertToXml() {
        BpmnXMLConverter bpmnXMLConverter = new BpmnXMLConverter();
        byte[] convertToXML = bpmnXMLConverter.convertToXML(bpmnModel);
        String bytes = new String(convertToXML);
        System.out.println(bytes);
    }

    @Test
    public void convertToBpmnModel2() {
        BpmnXMLConverter bpmnXMLConverter = new BpmnXMLConverter();
        ResourceStreamSource resourceStreamSource = new ResourceStreamSource("process/decide.bpmn20.xml");
        bpmnModel = bpmnXMLConverter.convertToBpmnModel(resourceStreamSource, false, false);
        byte[] convertToXML = bpmnXMLConverter.convertToXML(bpmnModel);
        String xml = new String(convertToXML);
        System.out.println(xml);
    }

    /**
     * 修改model
     */
    @Test
    public void updateModel() throws PayloadValidErrorException {
        Operator operator = OperatorRegistry.get(OperatorEnum.PROCESS_MODEL_SAVE_OPERATOR.getId());
        operator.getParamDesc();
        Map<String, Object> params = new HashMap<>();
        params.put("modelId", "a8bac1c5-b970-11ec-ba93-1c1b0dcdefb3");
        params.put("editorSourceValueId", "a8ba9ab4-b970-11ec-ba93-1c1b0dcdefb3");
        params.put("metaInfo", json2);
        params.put("funcCode", "qjsq");
        params.put("funcName", "请假申请");
        params.put("category", "OA");
        params.put("runModeName", "生产环境模式");
        params.put("runModeCode", "PRODUCT");
        Result result = operator.operate(params);
        Assert.assertNotNull(result);
        if (result instanceof ProcessModelResult) {
            ProcessModelResult processInstanceResult = (ProcessModelResult) result;
            System.out.println("============流程：" + processInstanceResult.getEntity().getName());
        }

        Operator operator2 = OperatorRegistry.get(OperatorEnum.PROCESS_MODEL_DEPLOY_OPERATOR.getId());
        Map<String, Object> params2 = new HashMap<>();
        params2.put("modelId", "a8bac1c5-b970-11ec-ba93-1c1b0dcdefb3");
        Result result2 = operator2.operate(params);
        Assert.assertNotNull(result2);
    }

    /**
     * 启动model
     */
    @Test
    public void start() throws PayloadValidErrorException {
        String prod = "meta";
        String key = "qingjiashenqing";
        String beanId = "61";
        processOperatorService.start(prod, key, beanId);
    }

    /**
     * 提交
     */
    @Test
    public void submit() throws PayloadValidErrorException {
        String piid = "42238fb7-befe-11ec-baa2-1c1b0dcdefb3";
        String beanId = "61";
        String taskId = "23b667c1-befe-11ec-a3bf-1c1b0dcdefb3";
        String assignee = "zhangsan";
        String prod = "meta";
        String target = "s2";
        TaskListResult listResult = taskOperatorService.submit(prod, piid, beanId, taskId, assignee, "提交", target);
    }


    /**
     * 获取提交的节点和名称
     */
    @Test
    public void getSubmitOutGoingNode() throws PayloadValidErrorException {
        String piid = "1c59b531-bee2-11ec-a9f1-1c1b0dcdefb3";
        String taskId = "436b895f-bee2-11ec-a9b4-1c1b0dcdefb3";
        String pdid = "qingjiashenqing:23:eb0057d8-bee1-11ec-9284-1c1b0dcdefb3";
        Map<String, Object> bean = new HashMap<>();
         String prod="";
        ProcessNextElementResult processNextElementResult = processOperatorService.getSubmitOutGoingNode(taskId, pdid,prod, bean,"");
        List<ProcessNextNodeInfo> list = processNextElementResult.getEntity();
        System.out.println("list.size= " + list.size());
        for (ProcessNextNodeInfo processNextNodeInfo : list) {
            System.out.println("flowId:" + processNextNodeInfo.getTarget());
            System.out.println("nodeName:" + processNextNodeInfo.getNodeName());
            System.out.println("----------------------------------------------------");
        }
    }


    /**
     * 提交2
     */
    @Test
    public void submit2() throws PayloadValidErrorException {
        String piid = "23b667c1-befe-11ec-a3bf-1c1b0dcdefb3";
        String beanId = "61";
        String taskId = "816dcd39-befe-11ec-b1ba-1c1b0dcdefb3";
        String assignee = "wangwu";
        String prod = "meta";
        String target = "s3";
        TaskListResult listResult = taskOperatorService.submit(prod, piid, beanId, taskId, assignee, "提交", target,"kaiteCounterSignUserTask");
    }


    /**
     * 根据funcCode，获取流程初始按钮
     */
    @Test
    public void getProcessInitialButton() throws PayloadValidErrorException {
        String funcCode = "qjsq";
        String userId = "system";
        String beanId = "41";
        Map<String, Object> bean = new HashMap<>();
        Result result = processOperatorService.getButton(funcCode, userId, beanId, bean);
        if (result instanceof ProcessButtonListResult) {
            ProcessButtonListResult processButtonListResult = (ProcessButtonListResult) result;
            List<ProcessRunForm> list = processButtonListResult.getEntity();
            for (ProcessRunForm processRunForm : list) {
                List<Button> buttonList = processRunForm.getList();
                for (Button button : buttonList) {
                    if (button instanceof TaskButton) {
                        System.out.println("taskId:" + ((TaskButton) button).getTaskId());
                    }
                    System.out.println("pdid:" + button.getPdid());
                    System.out.println("buttonName:" + button.getName());
                    System.out.println("OperationId:" + button.getOperationId());
                    System.out.println("piid:" + button.getPiid());
                    System.out.println("code:" + button.getCode());
                }
                System.out.println("--------------------------------------------------");
            }
        }
        Assert.assertNotNull(result);
    }


}
