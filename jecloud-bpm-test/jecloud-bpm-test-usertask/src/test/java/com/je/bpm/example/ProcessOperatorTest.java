/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.example;

import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.converter.converter.BpmnXMLConverter;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.engine.ProcessEngine;
import com.je.bpm.engine.repository.Deployment;
import com.je.bpm.engine.repository.DeploymentBuilder;
import com.je.bpm.model.process.results.ProcessInstanceResult;
import com.je.bpm.model.shared.Result;
import com.je.bpm.model.task.result.TaskListResult;
import com.je.bpm.model.task.result.TaskResult;
import com.je.bpm.runtime.shared.OperatorSharedService;
import com.je.bpm.runtime.shared.operator.Operator;
import com.je.bpm.runtime.shared.operator.OperatorRegistry;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.PayloadValidErrorException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProcessOperatorTest {

    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private ProcessEngine processEngine;
    private BpmnModel bpmnModel;
    @Autowired
    private OperatorSharedService operatorSharedService;

    @Before
    public void setUser() {
        securityUtil.logInAs("system");
    }

    @Test
    public void findOperator() {
        Operator operator = OperatorRegistry.get(OperatorEnum.TASK_CANCEL_DELEGATE_OPERATOR.getId());
        Assert.assertNotNull(operator);
        System.out.println("------------------" + operator.getName());
    }

    @Test
    public void findOperatorParams() {
        OperationParamDesc paramDesc = operatorSharedService.getOperatorParams(OperatorEnum.PROCESS_EMPTY_START_OPERATOR.getId());
        Assert.assertNotNull(paramDesc);
        System.out.println(paramDesc);
    }

    @Test
    public void deploy() {
        DeploymentBuilder deploymentBuilder = processEngine.getRepositoryService().createDeployment().addClasspathResource("process/decide.bpmn20.xml");
        deploymentBuilder.disableBpmnValidation();
        deploymentBuilder.disableSchemaValidation();
        deploymentBuilder.key("testDecide");
        deploymentBuilder.name("测试判断节点流程");
        deploymentBuilder.category("decide");
        Deployment deployment = deploymentBuilder.deploy();
        System.out.println(deployment.getId());
    }

    @Test
    public void convertToXml() {
        BpmnXMLConverter bpmnXMLConverter = new BpmnXMLConverter();
        bpmnModel = processEngine.getRepositoryService().getBpmnModel("testDecideUserTask:1:1c5783d2-8ecd-11ec-91b4-b22d63840e5e", "","");
        byte[] convertToXML = bpmnXMLConverter.convertToXML(bpmnModel);
        String xml = new String(convertToXML);
        System.out.println(xml);
    }

    @Test
    public void operatorStart() throws PayloadValidErrorException {
        Operator operator = OperatorRegistry.get(OperatorEnum.PROCESS_EMPTY_START_OPERATOR.getId());
        Map<String, Object> params = new HashMap<>();
        params.put("pdid", "testDecideUserTask:1:1c5783d2-8ecd-11ec-91b4-b22d63840e5e");
        params.put("prod", "test");
        params.put("beanId", "aa");
        Result result = operator.operate(params);
        Assert.assertNotNull(result);
        if (result instanceof ProcessInstanceResult) {
            ProcessInstanceResult processInstanceResult = (ProcessInstanceResult) result;
            System.out.println("============流程：" + processInstanceResult.getEntity().getName());
        } else if (result instanceof TaskResult) {
            TaskResult taskResult = (TaskResult) result;
            System.out.println("============任务：" + taskResult.getEntity().getName());
        }
    }

    @Test
    public void opeatorSubmit() throws PayloadValidErrorException {
        Operator operator = OperatorRegistry.get(OperatorEnum.TASK_SUBMIT_OPERATOR.getId());
        Map<String, Object> params = new HashMap<>();
        params.put("piid", "971c62e7-8ed9-11ec-be5e-b22d63840e5e");
        params.put("prod", "test");
        params.put("taskId", "9724522e-8ed9-11ec-be5e-b22d63840e5e");
        params.put("beanId", "aa");
        params.put("assignee", "operator2");
        Result result = operator.operate(params);
        Assert.assertNotNull(result);
        if (result instanceof ProcessInstanceResult) {
            ProcessInstanceResult processInstanceResult = (ProcessInstanceResult) result;
            System.out.println("============流程：" + processInstanceResult.getEntity().getName());
        } else if (result instanceof TaskResult) {
            TaskResult taskResult = (TaskResult) result;
            System.out.println("============任务：" + taskResult.getEntity().getName());
        } else if (result instanceof TaskListResult) {
            TaskListResult taskResult = (TaskListResult) result;
            for (com.je.bpm.model.task.model.Task eachTask : taskResult.getEntity()) {
                System.out.println("============任务：" + eachTask.getName());
            }
        }
    }

    @Test
    public void opeatorSubmitDecide() throws PayloadValidErrorException {
        Operator operator = OperatorRegistry.get(OperatorEnum.TASK_SUBMIT_OPERATOR.getId());
        Map<String, Object> params = new HashMap<>();
        params.put("piid", "971c62e7-8ed9-11ec-be5e-b22d63840e5e");
        params.put("prod", "test");
        params.put("taskId", "d0327e35-8ed9-11ec-950c-b22d63840e5e");
        params.put("beanId", "aa");
        params.put("assignee", "operator3");
        params.put("target", "kaiteUserTask3");
        Result result = operator.operate(params);
        Assert.assertNotNull(result);
        if (result instanceof ProcessInstanceResult) {
            ProcessInstanceResult processInstanceResult = (ProcessInstanceResult) result;
            System.out.println("============流程：" + processInstanceResult.getEntity().getName());
        } else if (result instanceof TaskResult) {
            TaskResult taskResult = (TaskResult) result;
            System.out.println("============任务：" + taskResult.getEntity().getName());
        } else if (result instanceof TaskListResult) {
            TaskListResult taskResult = (TaskListResult) result;
            for (com.je.bpm.model.task.model.Task eachTask : taskResult.getEntity()) {
                System.out.println("============任务：" + eachTask.getName());
            }
        }
    }

    @Test
    public void opeatorSubmitFinal() throws PayloadValidErrorException {
        Operator operator = OperatorRegistry.get(OperatorEnum.TASK_SUBMIT_OPERATOR.getId());
        Map<String, Object> params = new HashMap<>();
        params.put("piid", "971c62e7-8ed9-11ec-be5e-b22d63840e5e");
        params.put("prod", "test");
        params.put("taskId", "fd4ef3be-8ed9-11ec-a4ff-b22d63840e5e");
        params.put("beanId", "aa");
        Result result = operator.operate(params);
        Assert.assertNotNull(result);
        if (result instanceof ProcessInstanceResult) {
            ProcessInstanceResult processInstanceResult = (ProcessInstanceResult) result;
            System.out.println("============流程：" + processInstanceResult.getEntity().getName());
        } else if (result instanceof TaskResult) {
            TaskResult taskResult = (TaskResult) result;
            System.out.println("============任务：" + taskResult.getEntity().getName());
        } else if (result instanceof TaskListResult) {
            TaskListResult taskResult = (TaskListResult) result;
            for (com.je.bpm.model.task.model.Task eachTask : taskResult.getEntity()) {
                System.out.println("============任务：" + eachTask.getName());
            }
        }
    }

}
