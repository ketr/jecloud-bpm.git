/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.example;

import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.converter.converter.BpmnXMLConverter;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.SequenceFlow;
import com.je.bpm.core.model.config.task.assignment.RoleAssignmentConfigImpl;
import com.je.bpm.core.model.event.EndEvent;
import com.je.bpm.core.model.event.StartEvent;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.core.model.task.KaiteDecideUserTask;
import com.je.bpm.core.model.task.KaiteUserTask;
import com.je.bpm.engine.ProcessEngine;
import com.je.bpm.engine.repository.Deployment;
import com.je.bpm.engine.repository.DeploymentBuilder;
import com.je.bpm.model.process.results.ProcessInstanceResult;
import com.je.bpm.model.shared.Result;
import com.je.bpm.model.task.result.TaskListResult;
import com.je.bpm.model.task.result.TaskResult;
import com.je.bpm.runtime.shared.OperatorSharedService;
import com.je.bpm.runtime.shared.operator.Operator;
import com.je.bpm.runtime.shared.operator.OperatorRegistry;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.PayloadValidErrorException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OperatorTest {

    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private ProcessEngine processEngine;

    private BpmnModel bpmnModel;
    @Autowired
    private OperatorSharedService operatorSharedService;

    @Before
    public void setUser(){
        securityUtil.logInAs("system");
        bpmnModel = new BpmnModel();
        Process process = new Process();
        process.setName("测试凯特用户任务");
        process.setId("testKaiteUserTask");

        //开始节点的属性
        StartEvent startEvent=new StartEvent();
        startEvent.setId("start");
        startEvent.setName("start");

        RoleAssignmentConfigImpl manageRoleAssignmentConfig = new RoleAssignmentConfigImpl();

        RoleAssignmentConfigImpl developRoleAssignmentConfig = new RoleAssignmentConfigImpl();

        //节点1
        KaiteUserTask kaiteUserTask1 = new KaiteUserTask();
        kaiteUserTask1.setId("kaiteUserTask1");
        kaiteUserTask1.setName("凯特用户任务1");

        //节点2
        KaiteDecideUserTask kaiteUserTask2 = new KaiteDecideUserTask();
        kaiteUserTask2.setId("kaiteDecideUserTask2");
        kaiteUserTask2.setName("凯特判断用户任务2");

        //节点3
        KaiteUserTask kaiteUserTask3 = new KaiteUserTask();
        kaiteUserTask3.setId("kaiteUserTask3");
        kaiteUserTask3.setName("凯特用户任务3");

        //节点4
        KaiteUserTask kaiteUserTask4 = new KaiteUserTask();
        kaiteUserTask4.setId("kaiteUserTask4");
        kaiteUserTask4.setName("凯特用户任务4");

        //结束节点属性
        EndEvent endEvent=new EndEvent();
        endEvent.setId("endEvent");
        endEvent.setName("endEvent");

        //连线信息
        List<SequenceFlow> sequenceFlows1=new ArrayList();

        SequenceFlow s1=new SequenceFlow();
        s1.setId("s1");
        s1.setName("s1");
        s1.setSourceRef("start");
        s1.setTargetRef("kaiteUserTask1");
        sequenceFlows1.add(s1);

        List<SequenceFlow> sequenceFlows2=new ArrayList<>();
        SequenceFlow s2=new SequenceFlow();
        s2.setId("s2");
        s2.setName("s2");
        s2.setSourceRef("kaiteUserTask1");
        s2.setTargetRef("kaiteDecideUserTask2");
        sequenceFlows2.add(s2);

        List<SequenceFlow> sequenceFlows3=new ArrayList<>();
        SequenceFlow s3=new SequenceFlow();
        s3.setId("s3");
        s3.setName("s3");
        s3.setSourceRef("kaiteDecideUserTask2");
        s3.setTargetRef("kaiteUserTask3");
        sequenceFlows3.add(s3);

        List<SequenceFlow> sequenceFlows4 = new ArrayList<>();
        sequenceFlows4.add(s3);

        SequenceFlow s4=new SequenceFlow();
        s4.setId("s4");
        s4.setName("s4");
        s4.setSourceRef("kaiteDecideUserTask2");
        s4.setTargetRef("kaiteUserTask4");
        sequenceFlows3.add(s4);

        List<SequenceFlow> sequenceFlows5 = new ArrayList<>();
        sequenceFlows5.add(s4);

        List<SequenceFlow> toEnd1=new ArrayList();
        SequenceFlow s5=new SequenceFlow();
        s5.setId("s5");
        s5.setName("s5");
        s5.setSourceRef("kaiteUserTask3");
        s5.setTargetRef("endEvent");
        toEnd1.add(s5);

        List<SequenceFlow> toEnd2=new ArrayList();
        SequenceFlow s6=new SequenceFlow();
        s6.setId("s6");
        s6.setName("s6");
        s6.setSourceRef("kaiteUserTask4");
        s6.setTargetRef("endEvent");
        toEnd2.add(s6);

        List<SequenceFlow> toEnd=new ArrayList();
        toEnd.add(s5);
        toEnd.add(s6);

        startEvent.setOutgoingFlows(sequenceFlows1);
        kaiteUserTask1.setIncomingFlows(sequenceFlows1);
        kaiteUserTask1.setOutgoingFlows(sequenceFlows2);
        kaiteUserTask2.setIncomingFlows(sequenceFlows2);
        kaiteUserTask2.setOutgoingFlows(sequenceFlows3);

        kaiteUserTask3.setIncomingFlows(sequenceFlows4);
        kaiteUserTask3.setOutgoingFlows(toEnd1);
        kaiteUserTask4.setIncomingFlows(sequenceFlows5);
        kaiteUserTask4.setOutgoingFlows(toEnd2);
        endEvent.setIncomingFlows(toEnd);

        process.addFlowElement(startEvent);
        process.addFlowElement(kaiteUserTask1);
        process.addFlowElement(kaiteUserTask2);
        process.addFlowElement(kaiteUserTask3);
        process.addFlowElement(kaiteUserTask4);
        process.addFlowElement(endEvent);
        process.addFlowElement(s1);
        process.addFlowElement(s2);
        process.addFlowElement(s3);
        process.addFlowElement(s4);
        process.addFlowElement(s5);
        process.addFlowElement(s6);

        bpmnModel.addProcess(process);
    }

    @Test
    public void findOperator(){
        Operator operator = OperatorRegistry.get(OperatorEnum.TASK_CANCEL_DELEGATE_OPERATOR.getId());
        Assert.assertNotNull(operator);
        System.out.println("------------------" + operator.getName());
    }

    @Test
    public void findOperatorParams(){
        OperationParamDesc paramDesc = operatorSharedService.getOperatorParams(OperatorEnum.PROCESS_EMPTY_START_OPERATOR.getId());
        Assert.assertNotNull(paramDesc);
        System.out.println(paramDesc);
    }

    @Test
    public void deploy(){
        DeploymentBuilder deploymentBuilder = processEngine.getRepositoryService().createDeployment().addClasspathResource("process/decide.bpmn20.xml");
        deploymentBuilder.disableBpmnValidation();
        deploymentBuilder.disableSchemaValidation();
        deploymentBuilder.key("testDecide");
        deploymentBuilder.name("测试判断节点流程");
        deploymentBuilder.category("decide");
        Deployment deployment = deploymentBuilder.deploy();
        System.out.println(deployment.getId());
    }

    @Test
    public void convertToXml(){
        BpmnXMLConverter bpmnXMLConverter=new BpmnXMLConverter();
        bpmnModel = processEngine.getRepositoryService().getBpmnModel("testDecideUserTask:1:1c5783d2-8ecd-11ec-91b4-b22d63840e5e","","");
        byte[] convertToXML = bpmnXMLConverter.convertToXML(bpmnModel);
        String xml = new String(convertToXML);
        System.out.println(xml);
    }

    @Test
    public void operatorStart() throws PayloadValidErrorException {
        Operator operator = OperatorRegistry.get(OperatorEnum.PROCESS_EMPTY_START_OPERATOR.getId());
        Map<String,Object> params = new HashMap<>();
        params.put("pdid","testDecideUserTask:1:1c5783d2-8ecd-11ec-91b4-b22d63840e5e");
        params.put("prod","test");
        params.put("beanId","aa");
        Result result = operator.operate(params);
        Assert.assertNotNull(result);
        if(result instanceof ProcessInstanceResult){
            ProcessInstanceResult processInstanceResult = (ProcessInstanceResult) result;
            System.out.println("============流程：" + processInstanceResult.getEntity().getName());
        } else if(result instanceof TaskResult){
            TaskResult taskResult = (TaskResult) result;
            System.out.println("============任务：" + taskResult.getEntity().getName());
        }
    }
    @Test
    public void operatorProcessEmptySponsorOperation() throws PayloadValidErrorException {
        Operator operator = OperatorRegistry.get(OperatorEnum.PROCESS_EMPTY_START_OPERATOR.getId());
        Map<String,Object> params = new HashMap<>();
        params.put("assignee","[{\"nodeId\":\"task8PxZxkpxJBaIOXfjKoZ\",\"nodeName\":\"任务4\",\"assignee\":\"7cf7e4b9a8864354b58c813338ca0e1f\",\"assigneeName\":\"于春辉\"}]");
        params.put("comment","同意");
        params.put("target","linepwmtj6efSDl7LaTD2zY");
        params.put("sequentials","[]");
        params.put("pdid","wzSej7ZJQaim1xsvtpo:1:e39ce00b-834e-11ed-8b89-0242f87b94e8");
        params.put("piid","");
        params.put("prod","test");
        params.put("beanId","2913999a4ccd4cb18d4509a38e66f530");
        params.put("isJump","1");
        Result result = operator.operate(params);
        Assert.assertNotNull(result);
        if(result instanceof ProcessInstanceResult){
            ProcessInstanceResult processInstanceResult = (ProcessInstanceResult) result;
            System.out.println("============流程：" + processInstanceResult.getEntity().getName());
        } else if(result instanceof TaskResult){
            TaskResult taskResult = (TaskResult) result;
            System.out.println("============任务：" + taskResult.getEntity().getName());
        }
    }
    @Test
    public void opeatorSubmit() throws PayloadValidErrorException {
        Operator operator = OperatorRegistry.get(OperatorEnum.TASK_SUBMIT_OPERATOR.getId());
        Map<String,Object> params = new HashMap<>();
        params.put("piid","971c62e7-8ed9-11ec-be5e-b22d63840e5e");
        params.put("prod","test");
        params.put("taskId","9724522e-8ed9-11ec-be5e-b22d63840e5e");
        params.put("beanId","aa");
        params.put("assignee","operator2");
        Result result = operator.operate(params);
        Assert.assertNotNull(result);
        if(result instanceof ProcessInstanceResult){
            ProcessInstanceResult processInstanceResult = (ProcessInstanceResult) result;
            System.out.println("============流程：" + processInstanceResult.getEntity().getName());
        } else if(result instanceof TaskResult){
            TaskResult taskResult = (TaskResult) result;
            System.out.println("============任务：" + taskResult.getEntity().getName());
        }  else if(result instanceof TaskListResult){
            TaskListResult taskResult = (TaskListResult) result;
            for (com.je.bpm.model.task.model.Task eachTask : taskResult.getEntity()) {
                System.out.println("============任务：" + eachTask.getName());
            }
        }
    }

    @Test
    public void opeatorSubmitDecide() throws PayloadValidErrorException {
        Operator operator = OperatorRegistry.get(OperatorEnum.TASK_SUBMIT_OPERATOR.getId());
        Map<String,Object> params = new HashMap<>();
        params.put("piid","971c62e7-8ed9-11ec-be5e-b22d63840e5e");
        params.put("prod","test");
        params.put("taskId","d0327e35-8ed9-11ec-950c-b22d63840e5e");
        params.put("beanId","aa");
        params.put("assignee","operator3");
        params.put("target","kaiteUserTask3");
        Result result = operator.operate(params);
        Assert.assertNotNull(result);
        if(result instanceof ProcessInstanceResult){
            ProcessInstanceResult processInstanceResult = (ProcessInstanceResult) result;
            System.out.println("============流程：" + processInstanceResult.getEntity().getName());
        } else if(result instanceof TaskResult){
            TaskResult taskResult = (TaskResult) result;
            System.out.println("============任务：" + taskResult.getEntity().getName());
        } else if(result instanceof TaskListResult){
            TaskListResult taskResult = (TaskListResult) result;
            for (com.je.bpm.model.task.model.Task eachTask : taskResult.getEntity()) {
                System.out.println("============任务：" + eachTask.getName());
            }
        }
    }

    @Test
    public void opeatorSubmitFinal() throws PayloadValidErrorException {
        Operator operator = OperatorRegistry.get(OperatorEnum.TASK_SUBMIT_OPERATOR.getId());
        Map<String,Object> params = new HashMap<>();
        params.put("piid","971c62e7-8ed9-11ec-be5e-b22d63840e5e");
        params.put("prod","test");
        params.put("taskId","fd4ef3be-8ed9-11ec-a4ff-b22d63840e5e");
        params.put("beanId","aa");
        Result result = operator.operate(params);
        Assert.assertNotNull(result);
        if(result instanceof ProcessInstanceResult){
            ProcessInstanceResult processInstanceResult = (ProcessInstanceResult) result;
            System.out.println("============流程：" + processInstanceResult.getEntity().getName());
        } else if(result instanceof TaskResult){
            TaskResult taskResult = (TaskResult) result;
            System.out.println("============任务：" + taskResult.getEntity().getName());
        } else if(result instanceof TaskListResult){
            TaskListResult taskResult = (TaskListResult) result;
            for (com.je.bpm.model.task.model.Task eachTask : taskResult.getEntity()) {
                System.out.println("============任务：" + eachTask.getName());
            }
        }
    }

}
