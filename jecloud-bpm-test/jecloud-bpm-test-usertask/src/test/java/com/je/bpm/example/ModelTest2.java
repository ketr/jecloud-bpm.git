/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.example;

import com.je.bpm.engine.ProcessEngine;
import com.je.bpm.model.process.model.ProcessRunForm;
import com.je.bpm.model.process.results.ProcessButtonListResult;
import com.je.bpm.model.shared.Result;
import com.je.bpm.model.shared.model.Button;
import com.je.bpm.model.task.model.TaskButton;
import com.je.bpm.model.task.result.TaskListResult;
import com.je.bpm.runtime.process.ProcessOperatorService;
import com.je.bpm.runtime.shared.operator.validator.PayloadValidErrorException;
import com.je.bpm.runtime.task.TaskOperatorService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 控制台test
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ModelTest2 {
    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private ProcessEngine processEngine;
    @Autowired
    private ProcessOperatorService processOperatorService;
    @Autowired
    private TaskOperatorService taskOperatorService;


    @Before
    public void setUser() {
        securityUtil.logInAs("system");
    }

    /**
     * 根据funcCode，获取流程初始按钮
     */
    @Test
    public void getProcessInitialButton() throws PayloadValidErrorException {
        String funcCode = "qjsq";
        String userId = "system";
        String beanId = "10";
        Map<String, Object> bean = new HashMap<>();
        Result result = processOperatorService.getButton(funcCode, userId, beanId, bean);
        if (result instanceof ProcessButtonListResult) {
            ProcessButtonListResult processButtonListResult = (ProcessButtonListResult) result;
            List<ProcessRunForm> list = processButtonListResult.getEntity();
        }
        Assert.assertNotNull(result);
    }

    /**
     * 启动model
     */
    @Test
    public void start() throws PayloadValidErrorException {
        String prod = "meta";
        String key = "qingjiashenqing";
        String beanId = "16";
        processOperatorService.start(prod, key, beanId);
    }

    /**
     * 提交
     */
    @Test
    public void submit() throws PayloadValidErrorException {
        String piid = "253841e6-ba26-11ec-9c02-1c1b0dcdefb3";
        String beanId = "16";
        String taskId = "29608edd-ba26-11ec-9c02-1c1b0dcdefb3";
        String assignee = "zhangsan,lisi";
        String prod = "meta";
        String target = "s3";
        TaskListResult listResult = taskOperatorService.submit(prod, piid, beanId, taskId, assignee, "提交", target);
    }

    /**
     * 提交2
     */
    @Test
    public void submit2() throws PayloadValidErrorException {
        String pdid = "IndBX1XsGgoAHF5etfP:1:a58b9712-ac3b-11ed-b9d4-0242f87b94e8";
        String piid ="79ecbc73-acd6-11ed-9d9d-0242f87b94e8";
        String taskId ="84333298-acd6-11ed-9d9d-0242f87b94e8";
//        operationId:taskSubmitOperation
//        tableCode:JE_LCSP_JBSQ
//        funcCode:JE_LCSP_JBSQ
//        funcId:6i0BW8GNAStczKP5RsW
        String prod = "test";
        String beanId = "f24631202bbc4eb38992a8acc903410f";
        String assignee = "[{\"nodeId\":\"endCD7ZdDi4KzeIOtKIssc\",\"nodeName\":\"结束节点\",\"assignee\":\"a71dff3412e44542bfaecf4ee64272af\",\"assigneeName\":\"王超\"}]";
//        comment:同意
        String target = "lineMhYoITXnzdSb5qpOdfc";
//        sequentials:[]
//        isJump:0
        TaskListResult listResult = taskOperatorService.submit(prod, piid, beanId, taskId, assignee, "提交", target);
    }

}
