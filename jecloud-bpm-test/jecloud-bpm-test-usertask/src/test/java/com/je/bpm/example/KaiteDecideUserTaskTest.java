/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.example;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.je.bpm.core.converter.converter.BpmnXMLConverter;
import com.je.bpm.core.json.converter.json.converter.BpmnJsonConverter;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.SequenceFlow;
import com.je.bpm.core.model.config.process.ProcessBasicConfigImpl;
import com.je.bpm.core.model.config.task.*;
import com.je.bpm.core.model.config.task.assignment.RoleAssignmentConfigImpl;
import com.je.bpm.core.model.event.EndEvent;
import com.je.bpm.core.model.event.StartEvent;
import com.je.bpm.core.model.process.Process;
import com.je.bpm.core.model.task.KaiteMultiUserTask;
import com.je.bpm.core.model.task.KaiteUserTask;
import com.je.bpm.engine.ProcessEngine;
import com.je.bpm.engine.impl.util.io.ResourceStreamSource;
import com.je.bpm.engine.repository.Deployment;
import com.je.bpm.engine.repository.DeploymentBuilder;
import com.je.bpm.engine.runtime.ProcessInstance;
import com.je.bpm.engine.task.Task;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class KaiteDecideUserTaskTest {

    @Autowired
    private SecurityUtil securityUtil;
    @Autowired
    private ProcessEngine processEngine;

    private BpmnModel bpmnModel;

    @Before
    public void setUser() {
        securityUtil.logInAs("system");
        bpmnModel = new BpmnModel();
        Process process = new Process();
        process.setName("请假申请");
        process.setId("qingjiashenqing");

        //开始节点的属性
        StartEvent startEvent = new StartEvent();
        startEvent.setId("start");
        startEvent.setName("start");

        RoleAssignmentConfigImpl manageRoleAssignmentConfig = new RoleAssignmentConfigImpl();

        RoleAssignmentConfigImpl developRoleAssignmentConfig = new RoleAssignmentConfigImpl();

        //节点1
        KaiteUserTask kaiteUserTask1 = new KaiteUserTask();
        kaiteUserTask1.setId("kaiteUserTask1");
        kaiteUserTask1.setName("凯特用户任务1");
        // 委托配置
        TaskDelegateConfigImpl taskDelegateConfig = new TaskDelegateConfigImpl();
        taskDelegateConfig.setEnable(true);
        // 驳回配置
        TaskDismissConfigImpl taskDismissConfig = new TaskDismissConfigImpl();
        taskDismissConfig.setEnable(true);
        taskDismissConfig.setDismissTaskId("kaiteUserTask1");
        // 任务退回配置
        TaskGobackConfigImpl taskGobackConfig = new TaskGobackConfigImpl();
        taskGobackConfig.setEnable(true);
        // 流程作废配置
        TaskInvalidConfigImpl taskInvalidConfig = new TaskInvalidConfigImpl();
        taskInvalidConfig.setEnable(true);
        // 任务传阅配置
        TaskPassRoundConfigImpl taskPassRoundConfig = new TaskPassRoundConfigImpl();
        taskPassRoundConfig.setEnable(true);
        // 催办配置
        TaskUrgeConfigImpl taskUrgeConfig = new TaskUrgeConfigImpl();
        taskUrgeConfig.setEnable(true);
        // 转办配置
        TaskTransferConfigImpl taskTransferConfig = new TaskTransferConfigImpl();
        taskTransferConfig.setEnable(true);
        // 取回配置
        TaskRetrieveConfigImpl taskRetrieveConfig = new TaskRetrieveConfigImpl();
        taskRetrieveConfig.setEnable(true);
        kaiteUserTask1.setTaskDismissConfig(taskDismissConfig);
        kaiteUserTask1.setTaskPassRoundConfig(taskPassRoundConfig);

        //节点2
        KaiteMultiUserTask kaiteUserTask2 = new KaiteMultiUserTask(true);
        kaiteUserTask2.setId("kaiteDecideUserTask2");
        kaiteUserTask2.setName("凯特多人用户任务");

        kaiteUserTask2.setTaskDismissConfig(taskDismissConfig);
        kaiteUserTask2.setTaskPassRoundConfig(taskPassRoundConfig);

        //节点3
        KaiteUserTask kaiteUserTask3 = new KaiteUserTask();
        kaiteUserTask3.setId("kaiteUserTask3");
        kaiteUserTask3.setName("凯特用户任务3");

        //节点4
        KaiteUserTask kaiteUserTask4 = new KaiteUserTask();
        kaiteUserTask4.setId("kaiteUserTask4");
        kaiteUserTask4.setName("凯特用户任务4");

        //结束节点属性
        EndEvent endEvent = new EndEvent();
        endEvent.setId("endEvent");
        endEvent.setName("endEvent");

        //连线信息
        List<SequenceFlow> sequenceFlows1 = new ArrayList();

        SequenceFlow s1 = new SequenceFlow();
        s1.setId("s1");
        s1.setName("s1");
        s1.setSourceRef("start");
        s1.setTargetRef("kaiteUserTask1");
        sequenceFlows1.add(s1);

        List<SequenceFlow> sequenceFlows2 = new ArrayList<>();
        SequenceFlow s2 = new SequenceFlow();
        s2.setId("s2");
        s2.setName("s2");
        s2.setSourceRef("kaiteUserTask1");
        s2.setTargetRef("kaiteDecideUserTask2");
        sequenceFlows2.add(s2);

        List<SequenceFlow> sequenceFlows3 = new ArrayList<>();
        SequenceFlow s3 = new SequenceFlow();
        s3.setId("s3");
        s3.setName("s3");
        s3.setSourceRef("kaiteDecideUserTask2");
        s3.setTargetRef("kaiteUserTask3");
        sequenceFlows3.add(s3);

        List<SequenceFlow> sequenceFlows4 = new ArrayList<>();
        sequenceFlows4.add(s3);

        SequenceFlow s4 = new SequenceFlow();
        s4.setId("s4");
        s4.setName("s4");
        s4.setSourceRef("kaiteDecideUserTask2");
        s4.setTargetRef("kaiteUserTask4");
        sequenceFlows3.add(s4);

        List<SequenceFlow> sequenceFlows5 = new ArrayList<>();
        sequenceFlows5.add(s4);

        List<SequenceFlow> toEnd1 = new ArrayList();
        SequenceFlow s5 = new SequenceFlow();
        s5.setId("s5");
        s5.setName("s5");
        s5.setSourceRef("kaiteUserTask3");
        s5.setTargetRef("endEvent");
        toEnd1.add(s5);

        List<SequenceFlow> toEnd2 = new ArrayList();
        SequenceFlow s6 = new SequenceFlow();
        s6.setId("s6");
        s6.setName("s6");
        s6.setSourceRef("kaiteUserTask4");
        s6.setTargetRef("endEvent");
        toEnd2.add(s6);

        List<SequenceFlow> toEnd = new ArrayList();
        toEnd.add(s5);
        toEnd.add(s6);

        startEvent.setOutgoingFlows(sequenceFlows1);
        kaiteUserTask1.setIncomingFlows(sequenceFlows1);
        kaiteUserTask1.setOutgoingFlows(sequenceFlows2);
        kaiteUserTask2.setIncomingFlows(sequenceFlows2);
        kaiteUserTask2.setOutgoingFlows(sequenceFlows3);

        kaiteUserTask3.setIncomingFlows(sequenceFlows4);
        kaiteUserTask3.setOutgoingFlows(toEnd1);
        kaiteUserTask4.setIncomingFlows(sequenceFlows5);
        kaiteUserTask4.setOutgoingFlows(toEnd2);
        endEvent.setIncomingFlows(toEnd);

        process.addFlowElement(startEvent);
        process.addFlowElement(kaiteUserTask1);
        process.addFlowElement(kaiteUserTask2);
        process.addFlowElement(kaiteUserTask3);
        process.addFlowElement(kaiteUserTask4);
        process.addFlowElement(endEvent);
        process.addFlowElement(s1);
        process.addFlowElement(s2);
        process.addFlowElement(s3);
        process.addFlowElement(s4);
        process.addFlowElement(s5);
        process.addFlowElement(s6);
        ProcessBasicConfigImpl processBasicConfig = new ProcessBasicConfigImpl();
        process.setProcessConfig(processBasicConfig);
        bpmnModel.addProcess(process);
    }

    @Test
    public void convertToXml() throws IOException {
        BpmnJsonConverter bpmnJsonConverter = new BpmnJsonConverter();
        ResourceStreamSource resourceStreamSource = new ResourceStreamSource("process/decide.bpmn20.json");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode modelNode = mapper.readTree(resourceStreamSource.getInputStream());
        bpmnModel = bpmnJsonConverter.convertToBpmnModel(modelNode);
        BpmnXMLConverter bpmnXMLConverter = new BpmnXMLConverter();
        byte[] convertToXML = bpmnXMLConverter.convertToXML(bpmnModel);
        String bytes = new String(convertToXML);
        System.out.println(bytes);
    }

    @Test
    public void convertToBpmnModel() {
        BpmnXMLConverter bpmnXMLConverter = new BpmnXMLConverter();
        ResourceStreamSource resourceStreamSource = new ResourceStreamSource("process/decide.bpmn20.xml");
        bpmnModel = bpmnXMLConverter.convertToBpmnModel(resourceStreamSource, false, false);
        byte[] convertToXML = bpmnXMLConverter.convertToXML(bpmnModel);
        String xml = new String(convertToXML);
        System.out.println(xml);
    }

    @Test
    public void deploy() {
        DeploymentBuilder deploymentBuilder = processEngine.getRepositoryService().createDeployment().addClasspathResource("process/decide.bpmn20.xml");
        deploymentBuilder.disableBpmnValidation();
        deploymentBuilder.disableSchemaValidation();
        deploymentBuilder.key("testDecide");
        deploymentBuilder.name("测试判断节点流程");
        deploymentBuilder.category("decide");
        Deployment deployment = deploymentBuilder.deploy();
        System.out.println(deployment.getId());
    }

    @Test
    public void sponsor() {
        ProcessInstance processInstance = processEngine.getRuntimeService().sponsorProcessInstanceByKey("testDecideFlow", "");
        System.out.println(processInstance.getName());
        System.out.println(processInstance.getProcessInstanceId());
    }

    @Test
    public void completeDecideTaskWithOutgoing() {
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId("49321261-87d3-11ec-bbcd-ba0eb2fe114d").list();
        for (Task each : tasks) {
            //下一个节点的taskId
//            processEngine.getTaskService().complete(each.getId(),"test3");
//            processEngine.getTaskService().complete(each.getId(),"test1");
//            processEngine.getTaskService().complete(each.getId());
        }
    }

    /**
     * 退回测试
     */
    @Test
    public void goback() {
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId("49321261-87d3-11ec-bbcd-ba0eb2fe114d").list();
        for (Task each : tasks) {
            processEngine.getTaskService().gobackTask(each.getId());
        }
    }

    @Test
    public void completeTask() {
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().processInstanceId("49321261-87d3-11ec-bbcd-ba0eb2fe114d").list();
        for (Task each : tasks) {
            processEngine.getTaskService().complete(each.getId());
        }
    }


}
