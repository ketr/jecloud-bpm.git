/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.shared.conf;

import com.je.bpm.api.runtime.shared.OperatorSharedServiceImpl;
import com.je.bpm.engine.RuntimeService;
import com.je.bpm.engine.delegate.event.ActivitiEventType;
import com.je.bpm.api.runtime.shared.event.impl.ToVariableCreatedConverter;
import com.je.bpm.api.runtime.shared.event.impl.ToVariableUpdatedConverter;
import com.je.bpm.api.runtime.shared.event.internal.VariableCreatedListenerDelegate;
import com.je.bpm.api.runtime.shared.event.internal.VariableEventFilter;
import com.je.bpm.api.runtime.shared.event.internal.VariableUpdatedListenerDelegate;
import com.je.bpm.api.runtime.shared.impl.VariableNameValidator;
import com.je.bpm.api.runtime.shared.model.impl.APIVariableInstanceConverter;
import com.je.bpm.model.shared.event.VariableCreatedEvent;
import com.je.bpm.model.shared.event.VariableUpdatedEvent;
import com.je.bpm.runtime.shared.OperatorSharedService;
import com.je.bpm.runtime.shared.event.listener.VariableEventListener;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

import static java.util.Collections.emptyList;

@Configuration
public class CommonRuntimeAutoConfiguration {

    @Bean
    public OperatorSharedService operatorSharedService(){
        return new OperatorSharedServiceImpl();
    }

    @Bean
    public APIVariableInstanceConverter apiVariableInstanceConverter() {
        return new APIVariableInstanceConverter();
    }

    @Bean
    public VariableEventFilter variableEventFilter() {
        return new VariableEventFilter();
    }

    @Bean
    public InitializingBean registerVariableCreatedListenerDelegate(RuntimeService runtimeService,
                                                                    @Autowired(required = false) List<VariableEventListener<VariableCreatedEvent>> listeners,
                                                                    VariableEventFilter variableEventFilter) {
        return () -> runtimeService.addEventListener(
                new VariableCreatedListenerDelegate(getInitializedListeners(listeners),
                        new ToVariableCreatedConverter(),
                        variableEventFilter), ActivitiEventType.VARIABLE_CREATED);
    }

    private <T> List<T> getInitializedListeners(List<T> eventListeners) {
        return eventListeners != null ? eventListeners : emptyList();
    }

    @Bean
    public InitializingBean registerVariableUpdatedListenerDelegate(RuntimeService runtimeService,
                                                                    @Autowired(required = false) List<VariableEventListener<VariableUpdatedEvent>> listeners,
                                                                    VariableEventFilter variableEventFilter) {
        return () -> runtimeService.addEventListener(
                new VariableUpdatedListenerDelegate(getInitializedListeners(listeners),
                        new ToVariableUpdatedConverter(),
                        variableEventFilter), ActivitiEventType.VARIABLE_UPDATED);
    }

    @Bean
    public VariableNameValidator variableNameValidator() {
        return new VariableNameValidator();
    }

}
