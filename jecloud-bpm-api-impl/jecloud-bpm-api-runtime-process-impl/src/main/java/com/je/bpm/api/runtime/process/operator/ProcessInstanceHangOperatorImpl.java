/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator;

import com.je.bpm.api.runtime.process.impl.APIProcessInstanceConverter;
import com.je.bpm.api.runtime.process.operator.desc.ProcessHangParamDesc;
import com.je.bpm.api.runtime.process.operator.validator.ProcessHangOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.engine.RuntimeService;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.runtime.ProcessInstance;
import com.je.bpm.model.process.payloads.ProcessInstanceHangPayload;
import com.je.bpm.model.process.results.ProcessInstanceResult;
import com.je.bpm.runtime.process.operator.ProcessInstanceHangOperator;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;
import java.util.Map;

/**
 * 流程挂起操作
 */
public class ProcessInstanceHangOperatorImpl extends AbstractOperator<ProcessInstanceHangPayload, ProcessInstanceResult> implements ProcessInstanceHangOperator {

    private APIProcessInstanceConverter processInstanceConverter;
    private RuntimeService runtimeService;
    private OperatorPayloadParamsValidator<ProcessInstanceHangPayload> validator;

    public ProcessInstanceHangOperatorImpl(APIProcessInstanceConverter processInstanceConverter, RuntimeService runtimeService) {
        this.validator = new ProcessHangOperatorValidator();
        this.processInstanceConverter = processInstanceConverter;
        this.runtimeService = runtimeService;
    }

    @Override
    public String getId() {
        return OperatorEnum.PROCESS_HANG_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.PROCESS_HANG_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new ProcessHangParamDesc();
    }

    @Override
    public ProcessInstanceResult execute(ProcessInstanceHangPayload processHangPayload) {
        runtimeService.suspendProcessInstanceById(processHangPayload.getPiid());
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processHangPayload.getPiid()).singleResult();
        ProcessInstanceResult result = new ProcessInstanceResult(processHangPayload,processInstanceConverter.from(processInstance));
        return result;
    }

    @Override
    public ProcessInstanceHangPayload paramsParse(Map<String, Object> params) {
        ProcessInstanceHangPayload processHangPayload = new ProcessInstanceHangPayload(
                formatString(params.get(AbstractOperator.PROD)),
                formatString(params.get("piid")),
                formatString(params.get("beanId")));
        return processHangPayload;
    }

    @Override
    public OperatorPayloadParamsValidator<ProcessInstanceHangPayload> getValidator() {
        return validator;
    }
}
