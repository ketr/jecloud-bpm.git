/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.conf;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.je.bpm.engine.impl.bpmn.parser.factory.DefaultActivityBehaviorFactory;
import com.je.bpm.engine.impl.delegate.invocation.DefaultDelegateInterceptor;
import com.je.bpm.engine.impl.el.ExpressionManager;
import com.je.bpm.api.runtime.process.connector.DefaultServiceTaskBehavior;
import com.je.bpm.api.runtime.process.connector.IntegrationContextBuilder;
import com.je.bpm.api.runtime.process.impl.ExpressionResolver;
import com.je.bpm.api.runtime.process.impl.ExtensionsVariablesMappingProvider;
import com.je.bpm.spring.process.ProcessExtensionService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConnectorsAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public ExpressionManager expressionManager() {
        return new ExpressionManager();
    }

    @Bean
    @ConditionalOnMissingBean
    public ExpressionResolver expressionResolver(ExpressionManager expressionManager, ObjectMapper objectMapper) {
        return new ExpressionResolver(expressionManager, objectMapper, new DefaultDelegateInterceptor());
    }

    @Bean
    @ConditionalOnMissingBean
    public IntegrationContextBuilder integrationContextBuilder(ExtensionsVariablesMappingProvider variablesMappingProvider) {
        return new IntegrationContextBuilder(variablesMappingProvider);
    }

    @Bean(name = DefaultActivityBehaviorFactory.DEFAULT_SERVICE_TASK_BEAN_NAME)
    @ConditionalOnMissingBean(name = DefaultActivityBehaviorFactory.DEFAULT_SERVICE_TASK_BEAN_NAME)
    public DefaultServiceTaskBehavior defaultServiceTaskBehavior(ApplicationContext applicationContext, IntegrationContextBuilder integrationContextBuilder, ExtensionsVariablesMappingProvider outboundVariablesProvider) {
        return new DefaultServiceTaskBehavior(applicationContext, integrationContextBuilder, outboundVariablesProvider);
    }

    @Bean
    @ConditionalOnMissingBean
    public ExtensionsVariablesMappingProvider variablesMappingProvider(ProcessExtensionService processExtensionService, ExpressionResolver expressionResolver) {
        return new ExtensionsVariablesMappingProvider(processExtensionService, expressionResolver);
    }
}
