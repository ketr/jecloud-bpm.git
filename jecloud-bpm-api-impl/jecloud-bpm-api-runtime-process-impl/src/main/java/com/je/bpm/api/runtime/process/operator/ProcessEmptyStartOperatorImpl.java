/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator;

import com.je.bpm.api.runtime.process.impl.APIProcessInstanceConverter;
import com.je.bpm.api.runtime.process.operator.desc.ProcessEmptyStartParamDesc;
import com.je.bpm.api.runtime.process.operator.validator.ProcessEmptyStartOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.RuntimeService;
import com.je.bpm.engine.impl.interceptor.CommandContext;
import com.je.bpm.engine.impl.util.ProcessInstanceHelper;
import com.je.bpm.engine.runtime.ProcessInstance;
import com.je.bpm.model.process.payloads.ProcessEmptyStartPayload;
import com.je.bpm.model.process.results.ProcessInstanceResult;
import com.je.bpm.runtime.process.operator.ProcessEmptyStartOperator;
import com.je.bpm.runtime.shared.RemoteCallServeManager;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;

import java.util.Map;

/**
 * 流程启动操作
 */
public class ProcessEmptyStartOperatorImpl extends AbstractOperator<ProcessEmptyStartPayload, ProcessInstanceResult> implements ProcessEmptyStartOperator {

    private APIProcessInstanceConverter processInstanceConverter;
    private RuntimeService runtimeService;
    private OperatorPayloadParamsValidator<ProcessEmptyStartPayload> validator;
    private RemoteCallServeManager remoteCallServeManager;
    private RepositoryService repositoryService;
    private ProcessInstanceHelper processInstanceHelper;

    public ProcessEmptyStartOperatorImpl(APIProcessInstanceConverter processInstanceConverter, RuntimeService runtimeService
            , RemoteCallServeManager remoteCallServeManager, RepositoryService repositoryService, ProcessInstanceHelper processInstanceHelper) {
        this.validator = new ProcessEmptyStartOperatorValidator();
        this.processInstanceConverter = processInstanceConverter;
        this.runtimeService = runtimeService;
        this.remoteCallServeManager = remoteCallServeManager;
        this.repositoryService = repositoryService;
        this.processInstanceHelper = processInstanceHelper;
    }

    @Override
    public String getId() {
        return OperatorEnum.PROCESS_EMPTY_START_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.PROCESS_EMPTY_START_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new ProcessEmptyStartParamDesc();
    }

    @Override
    public ProcessInstanceResult execute(ProcessEmptyStartPayload processStartPayload) {
        if (runtimeService.createExecutionQuery().processInstanceBusinessKey(processStartPayload.getBeanId()).list().size() > 0) {
            throw new ActivitiException("业务数据已经启动流程！");
        }
        ProcessInstance internalProcessInstance = runtimeService.startProcessInstanceByIdAndBean
                (processStartPayload.getProcessDefinitionId(), processStartPayload.getBeanId(), processStartPayload.getBean(),
                        processStartPayload.getProd());
        return new ProcessInstanceResult(processStartPayload, processInstanceConverter.from(internalProcessInstance));
    }

    @Override
    public ProcessEmptyStartPayload paramsParse(Map<String, Object> params) {
        ProcessEmptyStartPayload processStartPayload = new ProcessEmptyStartPayload(
                formatString(params.get(AbstractOperator.PROD)),
                formatString(params.get("pdid")),
                formatString(params.get("beanId")),
                formatString(params.get("tableCode"))
        );
        processStartPayload.setBean(params.get(AbstractOperator.BEAN) == null ? null : (Map<String, Object>) params.get(AbstractOperator.BEAN));
        return processStartPayload;
    }

    @Override
    public OperatorPayloadParamsValidator<ProcessEmptyStartPayload> getValidator() {
        return validator;
    }

}
