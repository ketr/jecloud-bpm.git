/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator.desc;

import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.desc.ParamDesc;
import com.je.bpm.runtime.shared.operator.desc.ParamType;

/**
 * 流程启动操作参数描述
 */
public class ProcessDefinitionGetListParamDesc extends OperationParamDesc {

    private ParamDesc processDefinitionKeyParam;
    private ParamDesc startParam;
    private ParamDesc limitParam;

    public ProcessDefinitionGetListParamDesc() {
        processDefinitionKeyParam = new ParamDesc();
        processDefinitionKeyParam.setName("processDefinitionKey");
        processDefinitionKeyParam.setDescription("流程定义key。");
        processDefinitionKeyParam.setRequired(true);
        processDefinitionKeyParam.setType(ParamType.String);

        startParam = new ParamDesc();
        startParam.setName("start");
        startParam.setDescription("起始数据。");
        startParam.setRequired(false);
        startParam.setType(ParamType.Integer);

        limitParam = new ParamDesc();
        limitParam.setName("limit");
        limitParam.setDescription("数据条数。");
        limitParam.setRequired(false);
        limitParam.setType(ParamType.Integer);

        paramKeys.add(processDefinitionKeyParam.getName());
        paramKeys.add(startParam.getName());
        paramKeys.add(limitParam.getName());
    }

    public ParamDesc getProcessDefinitionKeyParam() {
        return processDefinitionKeyParam;
    }

    public ParamDesc getStartParam() {
        return startParam;
    }

    public ParamDesc getLimitParam() {
        return limitParam;
    }

    @Override
    public String toString() {
        return "ProcessDefinitionGetListParamDesc{" +
                "processDefinitionKeyParam=" + processDefinitionKeyParam +
                ", startParam=" + startParam +
                ", limitParam=" + limitParam +
                ", paramKeys=" + paramKeys +
                '}';
    }
}
