/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator.desc;

import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.desc.ParamDesc;
import com.je.bpm.runtime.shared.operator.desc.ParamType;

/**
 * 获取流程启动按钮操作
 */
public class ProcessGetGobackElementParamDesc extends OperationParamDesc {

    private ParamDesc taskId;
    private ParamDesc piid;
    private ParamDesc bean;

    public ProcessGetGobackElementParamDesc() {
        taskId = new ParamDesc();
        taskId.setName("funcCode");
        taskId.setDescription("funcCode功能编码。");
        taskId.setRequired(true);
        taskId.setType(ParamType.String);

        piid = new ParamDesc();
        piid.setName("userId");
        piid.setDescription("业务数据创建人ID。");
        piid.setRequired(false);
        piid.setType(ParamType.String);

        bean = new ParamDesc();
        bean.setName("beanId");
        bean.setDescription("业务主键ID。");
        bean.setRequired(false);
        bean.setType(ParamType.String);

        paramKeys.add(taskId.getName());
        paramKeys.add(piid.getName());
        paramKeys.add(bean.getName());
    }


    @Override
    public String toString() {
        return "ProcessGetNextElementParamDesc{" +
                ", paramKeys=" + paramKeys +
                '}';
    }
}
