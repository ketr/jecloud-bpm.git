/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.impl;

import com.google.common.base.Strings;
import com.je.bpm.api.runtime.shared.model.impl.ListConverter;
import com.je.bpm.api.runtime.shared.model.impl.ModelConverter;
import com.je.bpm.core.model.button.*;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.model.process.model.ProcessButton;
import com.je.bpm.model.process.model.impl.*;

import java.util.Map;

public class ApiProcessButtonConverter extends ListConverter<com.je.bpm.core.model.button.ProcessButton, Map<String, Object>, ProcessButton> implements ModelConverter<com.je.bpm.core.model.button.ProcessButton, Map<String, Object>, ProcessButton> {

    @Override
    public ProcessButton from(com.je.bpm.core.model.button.ProcessButton source, Map<String, Object> params) {
        ProcessButton processButton = null;
        String modelName = params.get("modelName").toString();
        if (source instanceof ProcessStartButton) {
            String processDefinitionKey = (String) params.get("processDefinitionKey");
            processButton = new ProcessStartButtonImpl(source.getId(), String.format("%s->%s", source.getName(), modelName),
                    source.getCode(), processDefinitionKey, source.getDisplayExpressionWhenStarted(), source.getDisplayExpressionWhenStartedFn());
            setState(processButton, params);
        } else if (source instanceof ProcessSponsorButton) {
            String processDefinitionKey = (String) params.get("processDefinitionKey");
            processButton = new ProcessSponsorButtonImpl(source.getId(), String.format("%s->%s", source.getName(), modelName),
                    source.getCode(), processDefinitionKey, source.getDisplayExpressionWhenStarted(), source.getDisplayExpressionWhenStartedFn());
            setState(processButton, params);
        } else if (source instanceof ProcessHangButton) {
            processButton = new ProcessHangButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof ProcessActivateButton) {
            processButton = new ProcessActiveButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof ProcessCancelButton) {
            processButton = new ProcessCancelButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof ProcessInvalidButton) {
            processButton = new ProcessInvalidButtionImpl(source.getId(), source.getName(), source.getCode());
        } else {
            throw new ActivitiException("Converter can't find the converted defined button from the source");
        }

        if (processButton instanceof ProcessStartButtonImpl) {
            ProcessStartButtonImpl processStartButton = (ProcessStartButtonImpl) processButton;
            processStartButton.setModelName(modelName);
        }

        if (processButton instanceof ProcessSponsorButtonImpl) {
            ProcessSponsorButtonImpl processSponsorButton = (ProcessSponsorButtonImpl) processButton;
            processSponsorButton.setModelName(modelName);
        }

        processButton.setDisplayExpressionWhenStarted(source.getDisplayExpressionWhenStarted());
        processButton.setAppListeners(source.getAppListeners());
        processButton.setBackendListeners(source.getBackendListeners());
        processButton.setPcListeners(source.getPcListeners());
        processButton.setOperationId(source.getOperationId());

        if (!Strings.isNullOrEmpty(source.getCustomizeComments())) {
            processButton.setCustomizeComments(source.getCustomizeComments());
        }

        if (!Strings.isNullOrEmpty(source.getCustomizeName())) {
            processButton.setName(source.getCustomizeName());
        }

        Object piid = params.get("piid");
        if (piid == null) {
            processButton.setPiid("");
        } else {
            processButton.setPiid(String.valueOf(piid));
        }
        Object pdid = params.get("pdid");
        if (pdid != null) {
            processButton.setPdid(String.valueOf(pdid));
        }
        return processButton;
    }

    private void setState(ProcessButton processButton, Map<String, Object> params) {
        //紧急状态
        if (params.get("exigency") != null && (boolean) params.get("exigency")) {
            processButton.setExigency(1);
        } else {
            processButton.setExigency(0);
        }

        //简易审批
        if (params.get("simpleApproval") != null && (boolean) params.get("simpleApproval")) {
            processButton.setSimpleApproval(1);
        } else {
            processButton.setSimpleApproval(0);
        }

    }

}
