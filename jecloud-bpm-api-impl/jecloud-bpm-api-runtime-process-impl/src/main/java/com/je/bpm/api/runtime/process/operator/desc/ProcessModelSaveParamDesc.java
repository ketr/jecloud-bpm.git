/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator.desc;

import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.desc.ParamDesc;
import com.je.bpm.runtime.shared.operator.desc.ParamType;

/**
 * 获取流程模型操作参数描述
 */
public class ProcessModelSaveParamDesc extends OperationParamDesc {

    private ParamDesc modelIdParam;
    private ParamDesc modelNameParam;
    private ParamDesc modelKeyParam;
    private ParamDesc runModeCodeParam;
    private ParamDesc runModeNameParam;
    private ParamDesc categoryParam;
    private ParamDesc funcCodeParam;
    private ParamDesc funcNameParam;
    private ParamDesc metaInfoParam;
    private ParamDesc editorSourceValueIdParam;
    private ParamDesc deployStatus;
    private ParamDesc disabled;

    public ProcessModelSaveParamDesc() {
        modelIdParam = new ParamDesc();
        modelIdParam.setName("modelId");
        modelIdParam.setDescription("流程模型主键,不传输此参数则执行更新。");
        modelIdParam.setRequired(false);
        modelIdParam.setType(ParamType.String);

        modelNameParam = new ParamDesc();
        modelNameParam.setName("modelName");
        modelNameParam.setDescription("流程模型名称。");
        modelNameParam.setRequired(true);
        modelNameParam.setType(ParamType.String);

        modelKeyParam = new ParamDesc();
        modelKeyParam.setName("modelKey");
        modelKeyParam.setDescription("流程模型键，新建不必填，修改必须加上key。");
        modelKeyParam.setRequired(false);
        modelKeyParam.setType(ParamType.String);

        runModeCodeParam = new ParamDesc();
        runModeCodeParam.setName("runModeCode");
        runModeCodeParam.setDescription("流程运行模式编码，默认为生产模式。");
        runModeCodeParam.setRequired(true);
        runModeCodeParam.setType(ParamType.String);

        runModeNameParam = new ParamDesc();
        runModeNameParam.setName("runModeName");
        runModeNameParam.setDescription("流程运行模式名称，默认为生产模式。");
        runModeNameParam.setRequired(true);
        runModeNameParam.setType(ParamType.String);

        categoryParam = new ParamDesc();
        categoryParam.setName("category");
        categoryParam.setDescription("流程模型类型。");
        categoryParam.setRequired(true);
        categoryParam.setType(ParamType.String);

        funcCodeParam = new ParamDesc();
        funcCodeParam.setName("funcCode");
        funcCodeParam.setDescription("流程所属功能编码。");
        funcCodeParam.setRequired(true);
        funcCodeParam.setType(ParamType.String);

        funcNameParam = new ParamDesc();
        funcNameParam.setName("funcName");
        funcNameParam.setDescription("流程所属功能名称。");
        funcNameParam.setRequired(true);
        funcNameParam.setType(ParamType.String);

        metaInfoParam = new ParamDesc();
        metaInfoParam.setName("metaInfo");
        metaInfoParam.setDescription("流程模型Json信息。");
        metaInfoParam.setRequired(true);
        metaInfoParam.setType(ParamType.String);

        editorSourceValueIdParam = new ParamDesc();
        editorSourceValueIdParam.setName("editorSourceValueId");
        editorSourceValueIdParam.setDescription("模型json信息，修改时不能为空。");
        editorSourceValueIdParam.setRequired(false);
        editorSourceValueIdParam.setType(ParamType.String);

        deployStatus = new ParamDesc();
        deployStatus.setName("deployStatus");
        deployStatus.setDescription("是否部署。");
        deployStatus.setRequired(false);
        deployStatus.setType(ParamType.Integer);

        disabled = new ParamDesc();
        disabled.setName("disabled");
        disabled.setDescription("是否禁用。");
        disabled.setRequired(false);
        disabled.setType(ParamType.Integer);

        paramKeys.add(modelIdParam.getName());
        paramKeys.add(modelNameParam.getName());
        paramKeys.add(modelKeyParam.getName());
        paramKeys.add(runModeCodeParam.getName());
        paramKeys.add(runModeNameParam.getName());
        paramKeys.add(categoryParam.getName());
        paramKeys.add(funcCodeParam.getName());
        paramKeys.add(funcNameParam.getName());
        paramKeys.add(metaInfoParam.getName());
        paramKeys.add(editorSourceValueIdParam.getName());
        paramKeys.add(deployStatus.getName());
        paramKeys.add(disabled.getName());
    }

    public ParamDesc getModelIdParam() {
        return modelIdParam;
    }

    public ParamDesc getModelNameParam() {
        return modelNameParam;
    }

    public ParamDesc getModelKeyParam() {
        return modelKeyParam;
    }

    public ParamDesc getRunModeCodeParam() {
        return runModeCodeParam;
    }

    public ParamDesc getRunModeNameParam() {
        return runModeNameParam;
    }

    public ParamDesc getCategoryParam() {
        return categoryParam;
    }

    public ParamDesc getFuncCodeParam() {
        return funcCodeParam;
    }

    public ParamDesc getFuncNameParam() {
        return funcNameParam;
    }

    public ParamDesc getMetaInfoParam() {
        return metaInfoParam;
    }

}
