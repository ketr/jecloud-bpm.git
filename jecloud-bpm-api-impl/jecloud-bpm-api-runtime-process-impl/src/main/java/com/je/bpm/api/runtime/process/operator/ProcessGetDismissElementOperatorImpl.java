/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator;

import com.je.bpm.api.runtime.process.impl.APIProcessDismissElementConverter;
import com.je.bpm.api.runtime.process.operator.desc.ProcessGetNextElementParamDesc;
import com.je.bpm.api.runtime.process.operator.validator.ProcessGetDismissElementPayloadValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.model.DismissNodeInfo;
import com.je.bpm.engine.RuntimeService;
import com.je.bpm.model.process.results.ProcessDismissElementResult;
import com.je.bpm.runtime.process.operator.ProcessGetDismissElementOperator;
import com.je.bpm.runtime.process.payloads.ProcessGetDismissElementPayload;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;

import java.util.List;
import java.util.Map;

/**
 * 获取驳回节点信息
 */
public class ProcessGetDismissElementOperatorImpl extends AbstractOperator<ProcessGetDismissElementPayload, ProcessDismissElementResult>
        implements ProcessGetDismissElementOperator {

    private OperatorPayloadParamsValidator<ProcessGetDismissElementPayload> validator = new ProcessGetDismissElementPayloadValidator();
    private RuntimeService runtimeService;
    private APIProcessDismissElementConverter apiProcessDismissElementConverter;

    public ProcessGetDismissElementOperatorImpl(APIProcessDismissElementConverter apiProcessDismissElementConverter, RuntimeService runtimeService) {
        this.runtimeService = runtimeService;
        this.apiProcessDismissElementConverter = apiProcessDismissElementConverter;
    }

    @Override
    public String getId() {
        return OperatorEnum.PROCESS_GET_DISMISS_ELEMENT_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.PROCESS_GET_DISMISS_ELEMENT_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new ProcessGetNextElementParamDesc();
    }

    @Override
    public OperatorPayloadParamsValidator<ProcessGetDismissElementPayload> getValidator() {
        return validator;
    }

    @Override
    public ProcessGetDismissElementPayload paramsParse(Map<String, Object> params) {
        String executionId = (String) params.get("piid");
        String taskId = (String) params.get("taskId");
        Map<String, Object> bean = formatMap(params.get(AbstractOperator.BEAN));
        ProcessGetDismissElementPayload processGetNextElementPayload = new ProcessGetDismissElementPayload();
        processGetNextElementPayload.setBean(bean);
        processGetNextElementPayload.setPiid(executionId);
        processGetNextElementPayload.setTaskId(taskId);
        return processGetNextElementPayload;
    }

    @Override
    public ProcessDismissElementResult execute(ProcessGetDismissElementPayload processGetNextElementPayload) {
        String taskId = processGetNextElementPayload.getTaskId();
        //获取可以提交的节点信息
        List<DismissNodeInfo> dismissNodeInfos = runtimeService.getDismissElement(processGetNextElementPayload.getPiid(),
                taskId, processGetNextElementPayload.getBean());
        return new ProcessDismissElementResult(processGetNextElementPayload, apiProcessDismissElementConverter.from(dismissNodeInfos));
    }


}

