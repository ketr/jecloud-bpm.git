/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.je.bpm.api.runtime.process.operator.desc.ProcessModelDeployParamDesc;
import com.je.bpm.api.runtime.process.operator.validator.ProcessModelDeployOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.json.converter.json.converter.BpmnJsonConverter;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.impl.persistence.entity.ByteArrayEntity;
import com.je.bpm.engine.impl.persistence.entity.ByteArrayEntityImpl;
import com.je.bpm.engine.impl.persistence.entity.CsByteArrayEntity;
import com.je.bpm.engine.repository.DeploymentBuilder;
import com.je.bpm.engine.repository.Model;
import com.je.bpm.model.process.payloads.ProcessModelDeployPayload;
import com.je.bpm.model.shared.EmptyResult;
import com.je.bpm.runtime.process.operator.ProcessModelDeployOperator;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Map;

/**
 * 根据modelID部署流程
 */
public class ProcessModelDeployOperatorImpl extends AbstractOperator<ProcessModelDeployPayload, EmptyResult> implements ProcessModelDeployOperator {

    private OperatorPayloadParamsValidator<ProcessModelDeployPayload> validator = new ProcessModelDeployOperatorValidator();
    private RepositoryService repositoryService;

    public ProcessModelDeployOperatorImpl(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }

    @Override
    public OperatorPayloadParamsValidator<ProcessModelDeployPayload> getValidator() {
        return validator;
    }

    @Override
    public String getId() {
        return OperatorEnum.PROCESS_MODEL_DEPLOY_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.PROCESS_MODEL_DEPLOY_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new ProcessModelDeployParamDesc();
    }

    @Override
    public ProcessModelDeployPayload paramsParse(Map<String, Object> params) {
        ProcessModelDeployPayload payload = new ProcessModelDeployPayload(
                formatString(params.get("modelId"))
        );
        return payload;
    }

    @Override
    public EmptyResult execute(ProcessModelDeployPayload payload) {
        Model model = repositoryService.getModel(payload.getModelId());
        ByteArrayEntity byteArray = null;
        if (!Strings.isNullOrEmpty(model.getCategory()) && model.getCategory().equals("cs")) {
            CsByteArrayEntity csByteArray = (CsByteArrayEntity) repositoryService.getCsByteArrayById(model.getEditorCsSourceValueId());
            byteArray = ByteArrayEntityImpl.buildCsByteToByte(csByteArray);
        } else {
            byteArray = (ByteArrayEntity) repositoryService.getByteArrayById(model.getEditorSourceValueId());
        }
        BpmnModel bpmnModel = null;
        JsonNode modelNode = null;
        try {
            modelNode = new ObjectMapper().readTree(new ByteArrayInputStream(byteArray.getBytes()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        bpmnModel = new BpmnJsonConverter().convertToBpmnModel(modelNode);
        DeploymentBuilder deploymentBuilder = repositoryService.createDeployment().addBpmnModel(byteArray.getName(), bpmnModel);
        deploymentBuilder.disableBpmnValidation();
        deploymentBuilder.disableSchemaValidation();
        deploymentBuilder.key(model.getKey());
        deploymentBuilder.name(model.getName());
        deploymentBuilder.category(model.getCategory());
        deploymentBuilder.deploy();
        model.setDeployStatus(1);
        repositoryService.saveModel(model);
        EmptyResult emptyResult = new EmptyResult(payload);
        return emptyResult;
    }


}
