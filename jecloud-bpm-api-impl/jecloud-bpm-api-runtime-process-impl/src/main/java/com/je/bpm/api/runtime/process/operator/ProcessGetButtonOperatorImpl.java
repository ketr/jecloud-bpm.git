/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.process.operator;

import com.google.common.base.Strings;
import com.je.bpm.api.runtime.process.impl.ApiFormConfigConverter;
import com.je.bpm.api.runtime.process.impl.ApiWorkFlowConfigConverter;
import com.je.bpm.api.runtime.process.operator.desc.ProcessGetNextElementParamDesc;
import com.je.bpm.api.runtime.process.operator.validator.ProcessGetButtonOperatorValidator;
import com.je.bpm.api.runtime.shared.impl.ApiButtonConverter;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.button.Button;
import com.je.bpm.core.model.button.*;
import com.je.bpm.core.model.button.factory.ButtonEnum;
import com.je.bpm.core.model.event.EndEvent;
import com.je.bpm.core.model.task.KaiteBaseUserTask;
import com.je.bpm.engine.HistoryService;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.RuntimeService;
import com.je.bpm.engine.TaskService;
import com.je.bpm.engine.button.validator.ButtonValidateParam;
import com.je.bpm.engine.button.validator.factory.ButtonValidatorFactory;
import com.je.bpm.engine.history.HistoricProcessInstance;
import com.je.bpm.engine.impl.bpmn.behavior.KaiteBaseUserTaskActivityBehavior;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.engine.impl.persistence.entity.PassRoundEntity;
import com.je.bpm.engine.impl.persistence.entity.TaskEntityImpl;
import com.je.bpm.engine.impl.persistence.entity.VariableInstanceEntity;
import com.je.bpm.engine.repository.ProcessDefinition;
import com.je.bpm.engine.task.Task;
import com.je.bpm.model.process.model.ProcessRunForm;
import com.je.bpm.model.process.model.impl.ProcessRunFormImpl;
import com.je.bpm.model.process.results.ProcessButtonListResult;
import com.je.bpm.model.shared.model.*;
import com.je.bpm.model.shared.model.impl.WorkFlowConfigImpl;
import com.je.bpm.runtime.process.operator.ProcessGetButtonOperator;
import com.je.bpm.runtime.process.payloads.ProcessGetButtonPayload;
import com.je.bpm.runtime.shared.identity.UserRoleManager;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 获取流程初始按钮
 */
public class ProcessGetButtonOperatorImpl extends AbstractOperator<ProcessGetButtonPayload, ProcessButtonListResult> implements ProcessGetButtonOperator {

    private static final String TASK_GLOBAL_VAR = "taskConfigs";
    private static final String SIMPLE_APPROVAL = "simpleApproval";
    private ApiButtonConverter apiButtonConverter;
    private ApiFormConfigConverter apiFormConfigConverter;
    private OperatorPayloadParamsValidator<ProcessGetButtonPayload> validator;
    private RepositoryService repositoryService;
    private RuntimeService runtimeService;
    private HistoryService historyService;
    private TaskService taskService;
    private ApiWorkFlowConfigConverter apiWorkFlowConfigConverter;
    private UserRoleManager userRoleManager;

    public ProcessGetButtonOperatorImpl(ApiButtonConverter apiButtonConverter, RepositoryService repositoryService,
                                        RuntimeService runtimeService, ApiFormConfigConverter apiFormConfigConverter,
                                        ApiWorkFlowConfigConverter apiWorkFlowConfigConverter, UserRoleManager userRoleManager,
                                        HistoryService historyService, TaskService taskService) {
        this.validator = new ProcessGetButtonOperatorValidator();
        this.repositoryService = repositoryService;
        this.runtimeService = runtimeService;
        this.apiButtonConverter = apiButtonConverter;
        this.apiFormConfigConverter = apiFormConfigConverter;
        this.apiWorkFlowConfigConverter = apiWorkFlowConfigConverter;
        this.userRoleManager = userRoleManager;
        this.historyService = historyService;
        this.taskService = taskService;
    }

    @Override
    public String getId() {
        return OperatorEnum.PROCESS_GET_INITIAL_BUTTON_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.PROCESS_GET_INITIAL_BUTTON_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new ProcessGetNextElementParamDesc();
    }

    @Override
    public OperatorPayloadParamsValidator<ProcessGetButtonPayload> getValidator() {
        return validator;
    }

    @Override
    public ProcessGetButtonPayload paramsParse(Map<String, Object> params) {
        String funcCode = formatString(params.get("funcCode"));
        String userId = formatString(params.get("userId"));
        String beanId = formatString(params.get("beanId"));
        Map<String, Object> bean = formatMap(params.get(AbstractOperator.BEAN));
        ProcessGetButtonPayload processStartPayload = new ProcessGetButtonPayload(funcCode, userId, beanId, bean);
        return processStartPayload;
    }

    @Override
    public ProcessButtonListResult execute(ProcessGetButtonPayload processGetButtonPayload) {
        String funcCode = processGetButtonPayload.getFuncCode();
        String userId = processGetButtonPayload.getUserId();
        Map<String, Object> bean = processGetButtonPayload.getBean();
        String beanId = processGetButtonPayload.getBeanId();
        String prod = processGetButtonPayload.getProd();
        return new ProcessButtonListResult(processGetButtonPayload, getButtons(funcCode, userId, beanId, bean, prod));
    }

    private List<ProcessRunForm> getButtons(String funcCode, String userId, String beanId, Map<String, Object> bean, String prod) {
        List<ProcessRunForm> list = new ArrayList<>();
        ButtonValidateParam buttonValidateParam = runtimeService.getTaskButtonVariable(ButtonValidateParam.builder().logUserId(Authentication.getAuthenticatedUser().getDeptId()).funcCode(funcCode).beanUserId(userId).bean(bean).beanId(beanId));
        //如果流程结束了
        if (buttonValidateParam.getEnding()) {
            getEndTaskState(list, beanId);
        }
        //获取流程已启动时需要展示的按钮
        if (buttonValidateParam.getStarting()) {
            getRunTaskButton(buttonValidateParam, list, funcCode, prod);
            if (list.isEmpty()) {
                String piid = "";
                BpmnModel bpmnModel = null;
                FormConfig formConfig = null;
                List<Task> tasks = taskService.createTaskQuery().processInstanceBusinessKey(beanId).listPage(0, 1);
                if (!tasks.isEmpty()) {
                    Task task = tasks.get(0);
                    piid = task.getProcessInstanceId();
                    String defKey = task.getTaskDefinitionKey();
                    bpmnModel = repositoryService.getBpmnModel(task.getProcessDefinitionId());
                    FlowElement flowElement = bpmnModel.getFlowElement(defKey);
                    if (flowElement instanceof KaiteBaseUserTask) {
                        KaiteBaseUserTask kaiteBaseUserTask = (KaiteBaseUserTask) flowElement;
                        formConfig = apiFormConfigConverter.from(kaiteBaseUserTask);
                        formConfig.setFormButtonList(new ArrayList<>());
                        formConfig.setFormEditable(false);
                        //子功能 ，将编辑设置成false
                        List<FormChildFunc> formChildFuncs = formConfig.getFormChildFuncs();
                        for (FormChildFunc formChildFunc : formChildFuncs) {
                            formChildFunc.setEditable(false);
                        }
                        for (FormButton formButton : formConfig.getFormButtonList()) {
                            formButton.setEnable(false);
                        }
                        for (FormField formField : formConfig.getFormFieldList()) {
                            formField.setEditable(false);
                        }
                    }
                }
                getRunTaskStateButton(list, formConfig, piid, bpmnModel);
            }
            if (list.size() == 1 && list.get(0).getWorkFlowConfig() == null) {
                WorkFlowConfigImpl workFlowConfig = new WorkFlowConfigImpl();
                workFlowConfig.setAudFlag(WorkFlowConfigImpl.WorkFlowAudFlag.WAIT);
                list.get(0).setWorkFlowConfig(workFlowConfig);
            }
        }
        //获取流程启动按钮
        if (buttonValidateParam.getStarting() == false && buttonValidateParam.getEnding() == false) {
            getNoStartingButton(buttonValidateParam, list);
        }
        //解析
        return list;
    }

    private void getRunTaskStateButton(List<ProcessRunForm> list, FormConfig formConfig, String piid, BpmnModel bpmnModel) {
        //任务级别按钮
        ProcessRunForm processRunForm = new ProcessRunFormImpl();
        WorkFlowConfigImpl workFlowConfig = new WorkFlowConfigImpl();
        workFlowConfig.setAudFlag(WorkFlowConfigImpl.WorkFlowAudFlag.WAIT);
        processRunForm.setWorkFlowConfig(workFlowConfig);
        processRunForm.setFromConfig(formConfig);
        List<PassRoundEntity> passList = historyService.getPassRoundByProcessInstanceIdAndTo(piid, Authentication.getAuthenticatedUser().getDeptId());
        if (passList.size() > 0) {
            buildPassReadButton(processRunForm, passList.get(0), bpmnModel);
            workFlowConfig.setAudFlag(WorkFlowConfigImpl.WorkFlowAudFlag.WAIT);
        }
        list.add(processRunForm);
    }

    /**
     * 获取流程结束按钮
     *
     * @param list
     * @param beanId
     */
    private void getEndTaskState(List<ProcessRunForm> list, String beanId) {
        //任务级别按钮
        ProcessRunForm processRunForm = new ProcessRunFormImpl();
        WorkFlowConfigImpl workFlowConfig = new WorkFlowConfigImpl();
        workFlowConfig.setAudFlag(WorkFlowConfigImpl.WorkFlowAudFlag.ENDED);
        processRunForm.setWorkFlowConfig(workFlowConfig);
        List<HistoricProcessInstance> historicProcessInstanceList = historyService.createHistoricProcessInstanceQuery().processInstanceBusinessKey(beanId).list();
        if (historicProcessInstanceList.size() > 0) {
            String piid = historicProcessInstanceList.get(0).getId();
            String endActivityId = historicProcessInstanceList.get(0).getEndActivityId();
            BpmnModel bpmnModel = repositoryService.getBpmnModel(historicProcessInstanceList.get(0).getProcessDefinitionId(), historicProcessInstanceList.get(0).getSuperProcessInstanceId(), historicProcessInstanceList.get(0).getBusinessKey());
            if (bpmnModel.getMainProcess().getFlowElement(endActivityId) instanceof EndEvent) {
                EndEvent endEvent = (EndEvent) bpmnModel.getMainProcess().getFlowElement(endActivityId);
                processRunForm.setFromConfig(apiFormConfigConverter.from(endEvent));
            }
            //已经结束，判断是否还有未审阅的待办
            List<PassRoundEntity> passList = historyService.getPassRoundByProcessInstanceIdAndTo(piid, Authentication.getAuthenticatedUser().getDeptId());
            if (passList.size() > 0) {
                buildPassReadButton(processRunForm, passList.get(0), bpmnModel);
                workFlowConfig.setAudFlag(WorkFlowConfigImpl.WorkFlowAudFlag.WAIT);
            }
        }
        list.add(processRunForm);
    }

    /**
     * 构建审阅按钮
     *
     * @param processRunForm
     * @param passRoundEntity
     * @param bpmnModel
     */
    private void buildPassReadButton(ProcessRunForm processRunForm, PassRoundEntity passRoundEntity, BpmnModel bpmnModel) {
        Map<String, Object> params = new HashMap<>();
        List<Button> resultList = new ArrayList<>();
        params.put("taskId", passRoundEntity.getTaskId());
        params.put("piid", passRoundEntity.getProcessInstanceId());
        params.put("pdid", passRoundEntity.getProcessDefinitionId());
        params.put("modelName", bpmnModel.getMainProcess().getName());
        params.put("exigency", bpmnModel.getMainProcess().getExtendedConfiguration().isExigency());
        Button button = new TaskPassroundReadButton();
        resultList.add(button);
        List<com.je.bpm.model.shared.model.Button> buttonList = apiButtonConverter.from(resultList, params);
        processRunForm.addAllButton(buttonList);
    }


    /**
     * 获取流程运行按钮，可能有多份，一个人可能同时处理两个任务，前端可以通过按钮切换，要提交的任务
     *
     * @return
     */
    private void getRunTaskButton(ButtonValidateParam buttonValidateParam, List<ProcessRunForm> list, String funcCode, String prod) {
        //任务级别按钮
        if (buttonValidateParam.getBpmnModels().size() == 0) {
            return;
        }
        BpmnModel bpmnModel = buttonValidateParam.getBpmnModels().get(0);

        //设置流程启动人
        if (buttonValidateParam.getTaskList().size() > 0) {
            buttonValidateParam.starter(runtimeService.getStartIdentityLinksForProcessInstance(buttonValidateParam.getTaskList().get(0).getProcessInstanceId()));
        }
        //设置流程发起人操作操作按钮（发起人可撤回、发起人可作废、发起人可催办）
        if (buttonValidateParam.getStartUserButtons() != null) {
            Map<String, Object> params = new HashMap<>();
            Map<String, Object> startUserButtons = buttonValidateParam.getStartUserButtons();
            ProcessRunForm processRunForm = new ProcessRunFormImpl();
            List<Button> resultList = (List<Button>) startUserButtons.get("buttons");
            params.put("taskId", startUserButtons.get("taskId"));
            params.put("piid", startUserButtons.get("piid"));
            params.put("pdid", startUserButtons.get("pdid"));
            params.put("modelName", bpmnModel.getMainProcess().getName());
            params.put("exigency", bpmnModel.getMainProcess().getExtendedConfiguration().isExigency());
            List<com.je.bpm.model.shared.model.Button> buttonList = apiButtonConverter.from(resultList, params);
            processRunForm.addAllButton(buttonList);
            list.add(processRunForm);
        }
        // 设置流程审批按钮
        for (VariableInstanceEntity taskConfigVariableEntity : buttonValidateParam.getButtonVariableList()) {
            ProcessRunForm processRunForm = new ProcessRunFormImpl();
            List<Button> resultList = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();
            //如果不是流程按钮配置直接跳过
            if (!taskConfigVariableEntity.getName().equals(TASK_GLOBAL_VAR)) {
                continue;
            }
            String taskId = taskConfigVariableEntity.getTaskId();

            TaskEntityImpl taskEntity = getTaskEntityByTaskId(taskId, buttonValidateParam.getTaskList());
            Map<String, Object> taskEntityVariables = runtimeService.getTaskEntityVariables(taskId);
            String taskKey = taskEntity.getTaskDefinitionKey();
            //是不是节点处理人，前置节点处理人等
            boolean isHandler = repositoryService.isTaskButtonHandler(taskEntity, false, false, false, taskEntity.getAssignee(), taskEntity.getOwner(), new ArrayList<>());
            if (funcCode.equals(bpmnModel.getMainProcess().getProcessConfig().getFuncCode()) && taskEntity != null) {
                if (bpmnModel.getFlowElement(taskKey) instanceof KaiteBaseUserTask) {
                    KaiteBaseUserTask kaiteUserTask = (KaiteBaseUserTask) bpmnModel.getFlowElement(taskKey);
                    //表单项配置
                    FormConfig formConfig = apiFormConfigConverter.from(kaiteUserTask);
                    //如果不是当前处理人，只显示字段控制和子功能显示隐藏
                    if (!isHandler) {
                        formConfig.setFormButtonList(new ArrayList<>());
                        formConfig.setFormEditable(false);
                        //子功能 ，将编辑设置成false
                        List<FormChildFunc> formChildFuncs = formConfig.getFormChildFuncs();
                        for (FormChildFunc formChildFunc : formChildFuncs) {
                            formChildFunc.setEditable(false);
                        }
                        for (FormButton formButton : formConfig.getFormButtonList()) {
                            formButton.setEnable(false);
                        }
                        for (FormField formField : formConfig.getFormFieldList()) {
                            formField.setEditable(false);
                        }
                    }
                    processRunForm.setFromConfig(formConfig);
                }
            }

            if (bpmnModel.getFlowElement(taskKey) instanceof KaiteBaseUserTask) {
                KaiteBaseUserTask kaiteUserTask = (KaiteBaseUserTask) bpmnModel.getFlowElement(taskKey);
                Map<String, Object> workFlowParams = new HashMap<>();
                if (taskEntityVariables.get(KaiteBaseUserTaskActivityBehavior.DIRECT_TASK_TARGET) == null) {
                    workFlowParams.put("currentTarget", "");
                } else {
                    workFlowParams.put("currentTarget", taskEntityVariables.get(KaiteBaseUserTaskActivityBehavior.DIRECT_TASK_TARGET).toString());
                }
                //流程配置 启动 节点 目标节点信息
                workFlowParams.put("exigency", bpmnModel.getMainProcess().getExtendedConfiguration().isExigency());
                processRunForm.setWorkFlowConfig(apiWorkFlowConfigConverter.from(kaiteUserTask, workFlowParams));
                //流程开启简易审批
                boolean simpleApproval = bpmnModel.getMainProcess().getExtendedConfiguration().isSimpleApproval();
                //节点关闭简易审批
                boolean isClosesimpleApproval = kaiteUserTask.getTaskBasicConfig().getCloseSimpleApproval();
                params.put(SIMPLE_APPROVAL, simpleApproval && !isClosesimpleApproval);
            }

            if (funcCode.equals(bpmnModel.getMainProcess().getProcessConfig().getFuncCode())) {
                List<Button> runTaskButtonList = repositoryService.getRunTaskButtons(taskConfigVariableEntity, taskEntity, buttonValidateParam, prod);
                for (Button runTaskButton : runTaskButtonList) {
                    buttonValidateParam.setRootExecutionVars(runtimeService.getMultiInstanceTaskRootExecutionVariables(taskEntity));
                    Button button = ButtonValidatorFactory.getButtonValidator(runTaskButton.getCode()).exec(buttonValidateParam, runTaskButton, bpmnModel, taskEntity);
                    if (button != null) {
                        resultList.add(button);
                    }
                }
            }
            params.put("taskId", taskId);
            params.put("piid", taskConfigVariableEntity.getProcessInstanceId());
            params.put("pdid", taskEntity.getProcessDefinitionId());
            params.put("modelName", bpmnModel.getMainProcess().getName());
            params.put("exigency", bpmnModel.getMainProcess().getExtendedConfiguration().isExigency());
            List<com.je.bpm.model.shared.model.Button> buttonList = apiButtonConverter.from(resultList, params);
            if (buttonList.size() == 0) {
                continue;
            }
            processRunForm.addAllButton(buttonList);

            WorkFlowConfig workFlowConfig = processRunForm.getWorkFlowConfig();
            if (bpmnModel.getMainProcess().getExtendedConfiguration().isExigency()) {
                workFlowConfig.setExigency("1");
            } else {
                workFlowConfig.setExigency("0");
            }
            if (bpmnModel.getMainProcess().getExtendedConfiguration().isSimpleApproval()) {
                workFlowConfig.setSimpleApproval("1");
            } else {
                workFlowConfig.setSimpleApproval("0");
            }

            if (bpmnModel.getMainProcess().getExtendedConfiguration().isEnableSimpleComments()) {
                workFlowConfig.setEnableSimpleComments("1");
            } else {
                workFlowConfig.setEnableSimpleComments("0");
            }

            list.add(processRunForm);
        }
    }

    /**
     * 流程级别按钮只会存在一份，只有两种情况，一只会展示启动或发起，二只会展示流程处理级别的按钮比如作废
     *
     * @param buttonValidateParam
     * @return
     */
    private void getNoStartingButton(ButtonValidateParam buttonValidateParam, List<ProcessRunForm> processRunForms) {
        List<BpmnModel> bpmnModels = buttonValidateParam.getBpmnModels();
        ProcessRunForm processRunFormResult = null;
        if (processRunForms.size() > 0) {
            processRunFormResult = processRunForms.get(0);
        }
        //获取可发起的model
        for (BpmnModel bpmnModel : bpmnModels) {
            ProcessRunForm processRunForm = new ProcessRunFormImpl();
            Map<String, Object> params = new HashMap<>();
            List<Button> processButtons = new ArrayList<>();
            List<ProcessButton> processButtonList = bpmnModel.getMainProcess().getButtons().getButtons();
            if (!Strings.isNullOrEmpty(bpmnModel.getMainProcess().getStartupSettings().getCanEveryoneRolesId())) {
                buttonValidateParam.setLogUserRoleInStartRoleIds(userRoleManager.logUserRoleIsInRoleIds(Authentication.getAuthenticatedUser().getDeptId(), bpmnModel.getMainProcess().getStartupSettings().getCanEveryoneRolesId()));
            }
            for (Button processButton : processButtonList) {
                Button button = ButtonValidatorFactory.getButtonValidator(processButton.getCode()).exec(buttonValidateParam, processButton, bpmnModel, null);
                if (button != null) {
                    processButtons.add(button);
                }
            }
            //加签节点按钮
            Button button = ButtonValidatorFactory.getButtonValidator(ButtonEnum.ADD_SIGNATURE_NODE.getCode()).exec(buttonValidateParam,
                    new TaskAddSignatureNodeButton(), bpmnModel, null);
            if (button != null) {
                processButtons.add(button);
                Button delButton = ButtonValidatorFactory.getButtonValidator(ButtonEnum.DEL_SIGNATURE_NODE.getCode()).exec(buttonValidateParam,
                        new TaskDelSignatureNodeButton(), bpmnModel, null);
                processButtons.add(delButton);
            }

            if (processButtons.size() == 0) {
                continue;
            }
            WorkFlowConfigImpl workFlowConfig = new WorkFlowConfigImpl();
            workFlowConfig.setAudFlag(WorkFlowConfigImpl.WorkFlowAudFlag.NOSTATUS);
            processRunForm.setWorkFlowConfig(workFlowConfig);
            List<ProcessDefinition> list = repositoryService.createProcessDefinitionQuery().processDefinitionKey(bpmnModel.getMainProcess().getId()).latestVersion().list();
            params.put("pdid", list.get(0).getId());
            params.put("processDefinitionKey", bpmnModel.getMainProcess().getId());
            params.put("modelName", bpmnModel.getMainProcess().getName());
            params.put("exigency", bpmnModel.getMainProcess().getExtendedConfiguration().isExigency());
            params.put(SIMPLE_APPROVAL, bpmnModel.getMainProcess().getExtendedConfiguration().isSimpleApproval());
            processRunForm.addAllButton(apiButtonConverter.from(processButtons, params));
            if (processRunFormResult == null) {
                processRunForms.add(processRunForm);
                processRunFormResult = processRunForm;
            } else {
                processRunFormResult.getList().addAll(processRunForm.getList());
            }
        }

        if (buttonValidateParam.getNotStartAllBpmModels().size() == 0) {
            return;
        }

        if (processRunFormResult == null) {
            processRunFormResult = new ProcessRunFormImpl();
            processRunForms.add(processRunFormResult);
        }

        //获取可查看流程图信息
        List<Map<String, String>> allModelsInfo = new ArrayList<>();
        for (BpmnModel bpmnModel : buttonValidateParam.getNotStartAllBpmModels()) {
            Map<String, String> modelInfo = new HashMap<>();
            String modelName = bpmnModel.getMainProcess().getName();
            List<ProcessDefinition> list = repositoryService.createProcessDefinitionQuery().processDefinitionKey(bpmnModel.getMainProcess().getId()).latestVersion().list();
            modelInfo.put("pdid", list.get(0).getId());
            modelInfo.put("modelName", bpmnModel.getMainProcess().getName());
            allModelsInfo.add(modelInfo);
        }

        processRunFormResult.setAllModelsInfo(allModelsInfo);

    }

    private TaskEntityImpl getTaskEntityByTaskId(String taskId, List<Task> list) {
        TaskEntityImpl taskEntity = new TaskEntityImpl();
        for (Task task : list) {
            if (task.getId().equals(taskId)) {
                taskEntity = (TaskEntityImpl) task;
                break;
            }
        }
        return taskEntity;
    }

}

