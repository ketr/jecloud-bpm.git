/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.task.conf;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.module.SimpleAbstractTypeResolver;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.je.bpm.model.task.model.Task;
import com.je.bpm.model.task.model.TaskCandidateGroup;
import com.je.bpm.model.task.model.TaskCandidateUser;
import com.je.bpm.model.task.model.impl.TaskCandidateGroupImpl;
import com.je.bpm.model.task.model.impl.TaskCandidateUserImpl;
import com.je.bpm.model.task.model.impl.TaskImpl;
import com.je.bpm.model.task.payloads.*;
import com.je.bpm.model.task.result.TaskResult;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@AutoConfigureBefore({JacksonAutoConfiguration.class})
@Configuration
public class TaskModelAutoConfiguration {

    //this bean will be automatically injected inside boot's ObjectMapper
    @Bean
    public Module customizeTaskModelObjectMapper() {
        SimpleModule module = new SimpleModule("mapTaskRuntimeInterfaces", Version.unknownVersion());
        SimpleAbstractTypeResolver resolver = new SimpleAbstractTypeResolver() {
            //this is a workaround for https://github.com/FasterXML/jackson-databind/issues/2019
            //once version 2.9.6 is related we can remove this @override method
            @Override
            public JavaType resolveAbstractType(DeserializationConfig config,
                                                BeanDescription typeDesc) {
                return findTypeMapping(config, typeDesc.getType());
            }
        };

        resolver.addMapping(Task.class, TaskImpl.class);
        resolver.addMapping(TaskCandidateUser.class, TaskCandidateUserImpl.class);
        resolver.addMapping(TaskCandidateGroup.class, TaskCandidateGroupImpl.class);

        module.registerSubtypes(new NamedType(TaskResult.class, TaskResult.class.getSimpleName()));
        module.registerSubtypes(new NamedType(TaskClaimPayload.class, TaskClaimPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(TaskCancelDelegatePayload.class, TaskCancelDelegatePayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(TaskDelayPayload.class, TaskDelayPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(TaskChangeAssigneePayload.class, TaskChangeAssigneePayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(TaskClaimPayload.class, TaskClaimPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(GetTasksPayload.class, GetTasksPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(GetTaskVariablesPayload.class, GetTaskVariablesPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(TaskDirectSendPayload.class, TaskDirectSendPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(TaskDismissPayload.class, TaskDismissPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(TaskGobackPayload.class, TaskGobackPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(TaskPassRoundPayload.class, TaskPassRoundPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(TaskPassRoundReadePayload.class, TaskPassRoundReadePayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(TaskRetrievePayload.class, TaskRetrievePayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(TaskSubmitPayload.class, TaskSubmitPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(TaskTransferPayload.class, TaskTransferPayload.class.getSimpleName()));
        module.registerSubtypes(new NamedType(TaskUrgePayload.class, TaskUrgePayload.class.getSimpleName()));
        module.setAbstractTypes(resolver);
        return module;
    }

}
