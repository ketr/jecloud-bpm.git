/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.je.bpm.model.process.model.impl.ObjectValue;
import com.je.bpm.model.process.model.impl.ProcessVariableValue;
import com.je.bpm.model.process.model.impl.ProcessVariablesMap;
import org.springframework.core.convert.ConversionService;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.je.bpm.model.process.model.impl.ProcessVariablesMapTypeRegistry.*;

public class ProcessVariablesMapSerializer extends StdSerializer<ProcessVariablesMap<String, Object>> {

    private static final long serialVersionUID = 1L;
    private final ConversionService conversionService;

    public ProcessVariablesMapSerializer(ConversionService conversionService) {
        super(ProcessVariablesMap.class, true);
        this.conversionService = conversionService;
    }

    @Override
    public void serialize(ProcessVariablesMap<String, Object> processVariablesMap, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        HashMap<String, ProcessVariableValue> map = new HashMap<>();
        for (Map.Entry<String, Object> entry : processVariablesMap.entrySet()) {
            String name = entry.getKey();
            Object value = entry.getValue();
            map.put(name, buildProcessVariableValue(value));
        }
        gen.writeObject(map);
    }

    private ProcessVariableValue buildProcessVariableValue(Object value) {
        ProcessVariableValue variableValue = null;
        if (value != null) {
            Class<?> entryValueClass = value.getClass();
            String entryType = resolveEntryType(entryValueClass, value);
            if (OBJECT_TYPE_KEY.equals(entryType)) {
                value = new ObjectValue(value);
            }
            String entryValue = conversionService.convert(value, String.class);
            variableValue = new ProcessVariableValue(entryType, entryValue);
        }
        return variableValue;
    }

    private String resolveEntryType(Class<?> clazz, Object value) {
        Class<?> entryType;
        if (isScalarType(clazz)) {
            entryType = clazz;
        } else {
            entryType = getContainerType(clazz, value).orElse(ObjectValue.class);
        }
        return forClass(entryType);
    }
}
