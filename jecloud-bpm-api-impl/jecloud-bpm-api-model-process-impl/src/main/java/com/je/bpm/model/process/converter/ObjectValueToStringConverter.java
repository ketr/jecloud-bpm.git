/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.je.bpm.model.process.model.impl.ObjectValue;
import org.springframework.core.convert.converter.Converter;
import java.util.Map;

import static com.je.bpm.model.process.model.impl.ProcessVariablesMapTypeRegistry.OBJECT_TYPE_KEY;

@ProcessVariableTypeConverter
public class ObjectValueToStringConverter implements Converter<ObjectValue, String> {

    private static final String CLASS = "@class";
    private final ObjectMapper objectMapper;

    public ObjectValueToStringConverter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String convert(ObjectValue source) {
        try {
            Map<String, Object> value = objectMapper.convertValue(source, Map.class);
            if (Map.class.isInstance(value.get(OBJECT_TYPE_KEY))) {
                Map<String, Object> object = objectMapper.convertValue(source.getObject(), Map.class);
                if (object.containsKey(CLASS)) {
                    Map.class.cast(value.get(OBJECT_TYPE_KEY)).put(CLASS, object.get(CLASS));
                }
            }
            return objectMapper.writeValueAsString(value);
        } catch (Exception cause) {
            throw new RuntimeException(cause);
        }
    }
}
