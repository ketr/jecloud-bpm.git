/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.model.impl;

import com.je.bpm.model.process.model.BPMNSequenceFlow;

import java.util.Objects;

public class BPMNSequenceFlowImpl extends BPMNElementImpl implements BPMNSequenceFlow {

    private String sourceActivityElementId;
    private String sourceActivityName;
    private String sourceActivityType;
    private String targetActivityElementId;
    private String targetActivityName;
    private String targetActivityType;

    public BPMNSequenceFlowImpl() {
    }

    public BPMNSequenceFlowImpl(String elementId, String sourceActivityElementId, String targetActivityElementId) {
        this.setElementId(elementId);
        this.sourceActivityElementId = sourceActivityElementId;
        this.targetActivityElementId = targetActivityElementId;
    }

    @Override
    public String getSourceActivityElementId() {
        return sourceActivityElementId;
    }

    @Override
    public String getSourceActivityName() {
        return sourceActivityName;
    }

    public void setSourceActivityName(String sourceActivityName) {
        this.sourceActivityName = sourceActivityName;
    }

    @Override
    public String getSourceActivityType() {
        return sourceActivityType;
    }

    public void setSourceActivityType(String sourceActivityType) {
        this.sourceActivityType = sourceActivityType;
    }

    @Override
    public String getTargetActivityElementId() {
        return targetActivityElementId;
    }

    @Override
    public String getTargetActivityName() {
        return targetActivityName;
    }

    public void setTargetActivityName(String targetActivityName) {
        this.targetActivityName = targetActivityName;
    }

    @Override
    public String getTargetActivityType() {
        return targetActivityType;
    }

    public void setTargetActivityType(String targetActivityType) {
        this.targetActivityType = targetActivityType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BPMNSequenceFlowImpl that = (BPMNSequenceFlowImpl) o;
        return Objects.equals(getElementId(),
                that.getElementId()) &&
                Objects.equals(sourceActivityElementId,
                        that.getSourceActivityElementId()) &&
                Objects.equals(sourceActivityType,
                        that.getSourceActivityType()) &&
                Objects.equals(sourceActivityName,
                        that.getSourceActivityName()) &&
                Objects.equals(targetActivityElementId,
                        that.getTargetActivityElementId()) &&
                Objects.equals(targetActivityType,
                        that.getTargetActivityType()) &&
                Objects.equals(targetActivityName,
                        that.getTargetActivityName());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getElementId(),
                sourceActivityElementId,
                targetActivityElementId);
    }

    @Override
    public String toString() {
        return "SequenceFlowImpl{" +
                "sourceActivityElementId='" + sourceActivityElementId + '\'' +
                ", sourceActivityName='" + sourceActivityName + '\'' +
                ", sourceActivityType='" + sourceActivityType + '\'' +
                ", targetActivityElementId='" + targetActivityElementId + '\'' +
                ", targetActivityName='" + targetActivityName + '\'' +
                ", targetActivityType='" + targetActivityType + '\'' +
                '}';
    }
}
