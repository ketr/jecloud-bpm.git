/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.model.impl;

import com.je.bpm.model.process.model.BPMNError;

import java.util.Objects;

public class BPMNErrorImpl extends BPMNActivityImpl implements BPMNError {

    private String errorCode;
    private String errorId;

    public BPMNErrorImpl() {
    }

    public BPMNErrorImpl(String elementId) {
        this.setElementId(elementId);
    }

    public BPMNErrorImpl(String elementId, String activityName, String activityType) {
        this.setElementId(elementId);
        this.setActivityName(activityName);
        this.setActivityType(activityType);
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorId() {
        return errorId;
    }

    public void setErrorId(String errorId) {
        this.errorId = errorId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getElementId(),
                getActivityName(),
                getActivityType(),
                getErrorId(),
                getErrorCode());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BPMNErrorImpl that = (BPMNErrorImpl) o;

        return Objects.equals(getElementId(),
                that.getElementId()) &&
                Objects.equals(getActivityName(),
                        that.getActivityName()) &&
                Objects.equals(getActivityType(),
                        that.getActivityType()) &&
                Objects.equals(getErrorCode(),
                        that.getErrorCode()) &&
                Objects.equals(getErrorId(),
                        that.getErrorId());
    }

    @Override
    public String toString() {
        return "BPMNActivityImpl{" +
                "activityName='" + getActivityName() + '\'' +
                ", activityType='" + getActivityType() + '\'' +
                ", elementId='" + getElementId() + '\'' +
                ", errorId='" + getErrorId() + '\'' +
                ", errorCode='" + getErrorCode() + '\'' +
                '}';
    }

}
