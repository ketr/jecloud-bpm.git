/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.model.impl;

import com.je.bpm.model.process.model.ProcessButton;
import com.je.bpm.model.shared.model.impl.AbstractButtonImpl;

public abstract class AbstractProcessButtonImpl extends AbstractButtonImpl implements ProcessButton {

    /**
     * 如果是启动后展示按钮，展示条件
     */
    public String displayExpressionWhenStarted;

    public String displayExpressionWhenStartedFn;

    /**
     * 简易审批
     */
    private int simpleApproval;

    private int enableSimpleComments;

    /**
     * 紧急程度
     */
    private int exigency;

    public int getSimpleApproval() {
        return simpleApproval;
    }

    public void setSimpleApproval(int simpleApproval) {
        this.simpleApproval = simpleApproval;
    }

    public int getExigency() {
        return exigency;
    }

    public void setExigency(int exigency) {
        this.exigency = exigency;
    }

    public AbstractProcessButtonImpl(String name, String code) {
        super(name, code);
    }

    public AbstractProcessButtonImpl(String id, String name, String code, String icon) {
        super(id, name, code, icon);
    }

    public int getEnableSimpleComments() {
        return enableSimpleComments;
    }

    public void setEnableSimpleComments(int enableSimpleComments) {
        this.enableSimpleComments = enableSimpleComments;
    }

    @Override
    public String getDisplayExpressionWhenStarted() {
        return displayExpressionWhenStarted;
    }

    @Override
    public void setDisplayExpressionWhenStarted(String displayExpressionWhenStarted) {
        this.displayExpressionWhenStarted = displayExpressionWhenStarted;
    }

    public String getDisplayExpressionWhenStartedFn() {
        return displayExpressionWhenStartedFn;
    }

    public void setDisplayExpressionWhenStartedFn(String displayExpressionWhenStartedFn) {
        this.displayExpressionWhenStartedFn = displayExpressionWhenStartedFn;
    }
}
