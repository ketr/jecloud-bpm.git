/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.process.model.impl;

import com.je.bpm.model.process.model.MessageSubscription;

import java.util.Date;
import java.util.Objects;

public class MessageSubscriptionImpl implements MessageSubscription {

    private String id;
    private String eventName;
    private String executionId;
    private String processInstanceId;
    private String processDefinitionId;
    private String businessKey;
    private String configuration;
    private String activityId;
    private Date created;

    private MessageSubscriptionImpl(Builder builder) {
        this.id = builder.id;
        this.eventName = builder.eventName;
        this.executionId = builder.executionId;
        this.processInstanceId = builder.processInstanceId;
        this.processDefinitionId = builder.processDefinitionId;
        this.businessKey = builder.businessKey;
        this.configuration = builder.configuration;
        this.activityId = builder.activityId;
        this.created = builder.created;
    }

    MessageSubscriptionImpl() {
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getEventName() {
        return eventName;
    }

    @Override
    public String getExecutionId() {
        return executionId;
    }

    @Override
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    @Override
    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    @Override
    public String getConfiguration() {
        return configuration;
    }

    @Override
    public String getActivityId() {
        return activityId;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public int hashCode() {
        return Objects.hash(activityId,
                configuration,
                created,
                eventName,
                executionId,
                id,
                processDefinitionId,
                processInstanceId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MessageSubscriptionImpl other = (MessageSubscriptionImpl) obj;
        return Objects.equals(activityId, other.activityId) &&
                Objects.equals(configuration, other.configuration) &&
                Objects.equals(created, other.created) &&
                Objects.equals(eventName, other.eventName) &&
                Objects.equals(executionId, other.executionId) &&
                Objects.equals(id, other.id) &&
                Objects.equals(processDefinitionId, other.processDefinitionId) &&
                Objects.equals(processInstanceId, other.processInstanceId);
    }

    @Override
    public String toString() {
        StringBuilder builder2 = new StringBuilder();
        builder2.append("MessageEventSubscriptionImpl [id=")
                .append(id)
                .append(", eventName=")
                .append(eventName)
                .append(", executionId=")
                .append(executionId)
                .append(", processInstanceId=")
                .append(processInstanceId)
                .append(", processDefinitionId=")
                .append(processDefinitionId)
                .append(", configuration=")
                .append(configuration)
                .append(", activityId=")
                .append(activityId)
                .append(", created=")
                .append(created)
                .append("]");
        return builder2.toString();
    }

    @Override
    public String getBusinessKey() {
        return businessKey;
    }


    /**
     * Creates a builder to build {@link MessageSubscriptionImpl}.
     *
     * @return created builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Creates a builder to build {@link MessageSubscriptionImpl} and initialize it with the given object.
     *
     * @param messageEventSubscriptionImpl to initialize the builder with
     * @return created builder
     */
    public static Builder builderFrom(MessageSubscriptionImpl messageEventSubscriptionImpl) {
        return new Builder(messageEventSubscriptionImpl);
    }


    /**
     * Builder to build {@link MessageSubscriptionImpl}.
     */
    public static final class Builder {

        private String id;
        private String eventName;
        private String executionId;
        private String processInstanceId;
        private String processDefinitionId;
        private String businessKey;
        private String configuration;
        private String activityId;
        private Date created;

        public Builder() {
        }

        private Builder(MessageSubscriptionImpl messageEventSubscriptionImpl) {
            this.id = messageEventSubscriptionImpl.id;
            this.eventName = messageEventSubscriptionImpl.eventName;
            this.executionId = messageEventSubscriptionImpl.executionId;
            this.processInstanceId = messageEventSubscriptionImpl.processInstanceId;
            this.processDefinitionId = messageEventSubscriptionImpl.processDefinitionId;
            this.businessKey = messageEventSubscriptionImpl.businessKey;
            this.configuration = messageEventSubscriptionImpl.configuration;
            this.activityId = messageEventSubscriptionImpl.activityId;
            this.created = messageEventSubscriptionImpl.created;
        }

        /**
         * Builder method for id parameter.
         *
         * @param id field to set
         * @return builder
         */
        public Builder withId(String id) {
            this.id = id;
            return this;
        }

        /**
         * Builder method for eventName parameter.
         *
         * @param eventName field to set
         * @return builder
         */
        public Builder withEventName(String eventName) {
            this.eventName = eventName;
            return this;
        }

        /**
         * Builder method for executionId parameter.
         *
         * @param executionId field to set
         * @return builder
         */
        public Builder withExecutionId(String executionId) {
            this.executionId = executionId;
            return this;
        }

        /**
         * Builder method for processInstanceId parameter.
         *
         * @param processInstanceId field to set
         * @return builder
         */
        public Builder withProcessInstanceId(String processInstanceId) {
            this.processInstanceId = processInstanceId;
            return this;
        }

        /**
         * Builder method for processDefinitionId parameter.
         *
         * @param processDefinitionId field to set
         * @return builder
         */
        public Builder withProcessDefinitionId(String processDefinitionId) {
            this.processDefinitionId = processDefinitionId;
            return this;
        }

        /**
         * Builder method for businessKey parameter.
         *
         * @param businessKey field to set
         * @return builder
         */
        public Builder withBusinessKey(String businessKey) {
            this.businessKey = businessKey;
            return this;
        }

        /**
         * Builder method for configuration parameter.
         *
         * @param configuration field to set
         * @return builder
         */
        public Builder withConfiguration(String configuration) {
            this.configuration = configuration;
            return this;
        }

        /**
         * Builder method for activityId parameter.
         *
         * @param activityId field to set
         * @return builder
         */
        public Builder withActivityId(String activityId) {
            this.activityId = activityId;
            return this;
        }

        /**
         * Builder method for created parameter.
         *
         * @param created field to set
         * @return builder
         */
        public Builder withCreated(Date created) {
            this.created = created;
            return this;
        }

        /**
         * Builder method of the builder.
         *
         * @return built class
         */
        public MessageSubscriptionImpl build() {
            return new MessageSubscriptionImpl(this);
        }
    }
}
