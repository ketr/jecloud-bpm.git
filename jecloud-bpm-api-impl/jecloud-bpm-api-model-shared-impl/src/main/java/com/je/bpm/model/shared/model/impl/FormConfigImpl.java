/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.shared.model.impl;

import com.je.bpm.model.shared.model.FormButton;
import com.je.bpm.model.shared.model.FormChildFunc;
import com.je.bpm.model.shared.model.FormConfig;
import com.je.bpm.model.shared.model.FormField;

import java.util.List;

public class FormConfigImpl implements FormConfig {

    List<FormButton> formButtonList;

    List<FormField> formFieldList;

    List<FormChildFunc> formChildFuncs;
    /**
     * 列表同步
     */
    private Boolean listSynchronization = false;
    /**
     * 表单可编辑
     */
    private Boolean formEditable = false;

    @Override
    public String getAppVersion() {
        return null;
    }

    public List<FormButton> getFormButtonList() {
        return formButtonList;
    }

    public void setFormButtonList(List<FormButton> formButtonList) {
        this.formButtonList = formButtonList;
    }

    public List<FormField> getFormFieldList() {
        return formFieldList;
    }

    public void setFormFieldList(List<FormField> formFieldList) {
        this.formFieldList = formFieldList;
    }

    public List<FormChildFunc> getFormChildFuncs() {
        return formChildFuncs;
    }

    public void setFormChildFuncs(List<FormChildFunc> formChildFuncs) {
        this.formChildFuncs = formChildFuncs;
    }

    public Boolean getListSynchronization() {
        return listSynchronization;
    }

    public void setListSynchronization(Boolean listSynchronization) {
        this.listSynchronization = listSynchronization;
    }

    public Boolean getFormEditable() {
        return formEditable;
    }

    public void setFormEditable(Boolean formEditable) {
        this.formEditable = formEditable;
    }

}
