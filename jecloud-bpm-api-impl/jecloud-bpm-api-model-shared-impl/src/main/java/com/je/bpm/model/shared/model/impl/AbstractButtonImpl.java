/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.shared.model.impl;

import com.je.bpm.model.shared.model.Button;
import java.util.UUID;

/**
 * 按钮父类
 */
public abstract class AbstractButtonImpl extends ApplicationElementImpl implements Button {

    /**
     * 按钮id
     */
    private String id;
    /**
     * 按钮名称
     */
    private String name;
    /**
     * 按钮编码
     */
    private String code;
    /**
     * pc端事件
     */
    private String pcListeners;
    /**
     * 移动端事件
     */
    private String appListeners;
    /**
     * 后端事件
     */
    private String backendListeners;
    /**
     * 操作标识
     */
    private String operationId;
    /**
     * 流程实例id
     */
    private String piid;
    /**
     * 流程部署实例id
     */
    private String pdid;
    /**
     * 图标
     */
    private String icon;
    /**
     * 自定义审批意见
     */
    private String customizeComments = "";

    public AbstractButtonImpl(String name, String code) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.code = code;
    }

    public AbstractButtonImpl(String id, String name, String code,String icon) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.icon = icon;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getPcListeners() {
        return pcListeners;
    }

    @Override
    public void setPcListeners(String pcListeners) {
        this.pcListeners = pcListeners;
    }

    @Override
    public String getAppListeners() {
        return appListeners;
    }

    @Override
    public void setAppListeners(String appListeners) {
        this.appListeners = appListeners;
    }

    @Override
    public String getBackendListeners() {
        return backendListeners;
    }

    @Override
    public void setBackendListeners(String backendListeners) {
        this.backendListeners = backendListeners;
    }

    @Override
    public String getOperationId() {
        return operationId;
    }

    @Override
    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    @Override
    public String getPiid() {
        return piid;
    }

    public void setPiid(String piid) {
        this.piid = piid;
    }

    @Override
    public String getPdid() {
        return pdid;
    }

    @Override
    public void setPdid(String pdid) {
        this.pdid = pdid;
    }

    @Override
    public String getIcon() {
        return icon;
    }

    @Override
    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String getCustomizeComments() {
        return customizeComments;
    }

    @Override
    public void setCustomizeComments(String customizeComments) {
        this.customizeComments = customizeComments;
    }

    @Override
    public int setSortOrder() {
        return 9999;
    }
}
