/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.model.shared.event.impl;

import com.je.bpm.model.shared.event.RuntimeEvent;

import java.util.Objects;
import java.util.UUID;

/**
 * 运行时事件抽象实现
 *
 * @param <ENTITY_TYPE> 实体类型
 * @param <EVENT_TYPE>  事件类型
 * @author opensource
 * @date 2021-10-21
 */
public abstract class AbstractRuntimeEventImpl<ENTITY_TYPE, EVENT_TYPE extends Enum<?>> implements RuntimeEvent<ENTITY_TYPE, EVENT_TYPE> {

    /**
     * 事件ID
     */
    private String id;

    /**
     * 发生时间
     */
    private Long timestamp;

    /**
     * 流程实例ID
     */
    private String processInstanceId;

    /**
     * 流程定义ID
     */
    private String processDefinitionId;

    /**
     * 流程定义KEY
     */
    private String processDefinitionKey;

    /**
     * 流程定义版本
     */
    private Integer processDefinitionVersion;

    /**
     * 流程业务KEY
     */
    private String businessKey;

    /**
     * 父流程实例ID
     */
    private String parentProcessInstanceId;

    /**
     * 发生此事件的实体
     */
    private ENTITY_TYPE entity;

    public AbstractRuntimeEventImpl() {
        id = UUID.randomUUID().toString();
        timestamp = System.currentTimeMillis();
    }

    public AbstractRuntimeEventImpl(ENTITY_TYPE entity) {
        this();
        this.entity = entity;
    }

    public AbstractRuntimeEventImpl(String id, Long timestamp, ENTITY_TYPE entity) {
        this.id = id;
        this.timestamp = timestamp;
        this.entity = entity;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public ENTITY_TYPE getEntity() {
        return entity;
    }

    @Override
    public Long getTimestamp() {
        return timestamp;
    }

    @Override
    public String getProcessInstanceId() {
        return processInstanceId;
    }

    @Override
    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    @Override
    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }

    @Override
    public Integer getProcessDefinitionVersion() {
        return processDefinitionVersion;
    }

    @Override
    public String getBusinessKey() {
        return businessKey;
    }

    @Override
    public String getParentProcessInstanceId() {
        return parentProcessInstanceId;
    }

    public void setParentProcessInstanceId(String parentProcessInstanceId) {
        this.parentProcessInstanceId = parentProcessInstanceId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public void setProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
    }

    public void setProcessDefinitionVersion(Integer processDefinitionVersion) {
        this.processDefinitionVersion = processDefinitionVersion;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    protected void setEntity(ENTITY_TYPE entity) {
        this.entity = entity;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("RuntimeEventImpl [id=")
                .append(id)
                .append(", timestamp=")
                .append(timestamp)
                .append(", processInstanceId=")
                .append(processInstanceId)
                .append(", processDefinitionId=")
                .append(processDefinitionId)
                .append(", processDefinitionKey=")
                .append(processDefinitionKey)
                .append(", processDefinitionVersion=")
                .append(processDefinitionVersion)
                .append(", businessKey=")
                .append(businessKey)
                .append(", parentProcessInstanceId=")
                .append(parentProcessInstanceId)
                .append(", entity=")
                .append(entity)
                .append("]");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(businessKey,
                entity,
                id,
                parentProcessInstanceId,
                processDefinitionId,
                processDefinitionKey,
                processDefinitionVersion,
                processInstanceId,
                timestamp,
                getEventType());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AbstractRuntimeEventImpl other = (AbstractRuntimeEventImpl) obj;
        return Objects.equals(businessKey, other.businessKey)
                && Objects.equals(entity, other.entity)
                && Objects.equals(id, other.id)
                && Objects.equals(parentProcessInstanceId, other.parentProcessInstanceId)
                && Objects.equals(processDefinitionId, other.processDefinitionId)
                && Objects.equals(processDefinitionKey, other.processDefinitionKey)
                && Objects.equals(processDefinitionVersion, other.processDefinitionVersion)
                && Objects.equals(processInstanceId, other.processInstanceId)
                && Objects.equals(timestamp, other.timestamp);
    }

}
