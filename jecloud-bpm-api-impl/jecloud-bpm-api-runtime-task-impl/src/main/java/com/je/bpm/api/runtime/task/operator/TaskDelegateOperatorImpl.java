/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.task.operator;

import com.je.bpm.api.runtime.task.impl.APITaskConverter;
import com.je.bpm.api.runtime.task.operator.desc.TaskCancelDelegateParamDesc;
import com.je.bpm.api.runtime.task.operator.validator.TaskDelegateOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.task.KaiteRandomUserTask;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.TaskService;
import com.je.bpm.engine.delegate.DelegateHelper;
import com.je.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import com.je.bpm.engine.impl.cmd.RandomTaskDelegateTransferGetUserInfoCmd;
import com.je.bpm.engine.impl.cmd.SubmitTypeEnum;
import com.je.bpm.engine.task.Task;
import com.je.bpm.engine.task.TaskQuery;
import com.je.bpm.engine.upcoming.ActivitiUpcomingRun;
import com.je.bpm.engine.upcoming.UpcomingCommentInfoDTO;
import com.je.bpm.model.task.payloads.TaskDelegatePayload;
import com.je.bpm.model.task.result.TaskResult;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;
import com.je.bpm.runtime.task.operator.TaskDelegateOperator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 委托操作
 */
public class TaskDelegateOperatorImpl extends AbstractOperator<TaskDelegatePayload, TaskResult> implements TaskDelegateOperator {

    private APITaskConverter apiTaskConverter;
    private TaskService taskService;
    private OperatorPayloadParamsValidator<TaskDelegatePayload> validator;
    private ActivitiUpcomingRun activitiUpcomingRun;
    private RepositoryService repositoryService;
    private ProcessEngineConfigurationImpl processEngineConfiguration;

    public TaskDelegateOperatorImpl(APITaskConverter apiTaskConverter, TaskService taskService, ActivitiUpcomingRun activitiUpcomingRun, RepositoryService repositoryService, ProcessEngineConfigurationImpl processEngineConfiguration) {
        this.validator = new TaskDelegateOperatorValidator();
        this.apiTaskConverter = apiTaskConverter;
        this.taskService = taskService;
        this.activitiUpcomingRun = activitiUpcomingRun;
        this.repositoryService = repositoryService;
        this.processEngineConfiguration = processEngineConfiguration;
    }

    @Override
    public String getId() {
        return OperatorEnum.TASK_DELEGATE_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.TASK_DELEGATE_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new TaskCancelDelegateParamDesc();
    }

    @Override
    public TaskResult execute(TaskDelegatePayload taskDelegatePayload) {
        TaskQuery taskQuery = taskService.createTaskQuery().taskId(taskDelegatePayload.getTaskId());
        List<Task> list = taskQuery.list();
        if (list.size() == 0) {
            throw new ActivitiException(String.format("Can't find task definiton from the process"));
        }
        Task task = list.get(0);
        BpmnModel bpmnModel = repositoryService.getBpmnModel(task.getProcessDefinitionId(), task.getProcessInstanceId(), task.getBusinessKey());
        FlowElement taskFlowElement = bpmnModel.getFlowElement(task.getTaskDefinitionKey());
        if (taskFlowElement == null) {
            throw new ActivitiException(String.format("Can't find task %s definiton from the process.", task.getName()));
        }
        //如果是随机节点委托操作
        if (taskFlowElement instanceof KaiteRandomUserTask) {

            String userIds = (String) processEngineConfiguration.getCommandExecutor().execute(new RandomTaskDelegateTransferGetUserInfoCmd(taskDelegatePayload.getTaskId(), taskDelegatePayload.getPiid(), taskDelegatePayload.getAssignee()));

            taskService.delegateTask(taskDelegatePayload.getTaskId(), taskDelegatePayload.getProd(), taskDelegatePayload.getBean(),
                    userIds, taskDelegatePayload.getComment(), taskDelegatePayload.getAssignee(), taskDelegatePayload.getFiles());

            Map<String, String> params = new HashMap<>();
            params.put("transferUserId", userIds);
            UpcomingCommentInfoDTO upcomingInfo = DelegateHelper.buildUpcomingInfo(null, taskDelegatePayload.getComment(),
                    SubmitTypeEnum.DELEGATE, taskDelegatePayload.getBeanId(), taskDelegatePayload.getTaskId(), params);
            activitiUpcomingRun.completeUpcoming(upcomingInfo);

            TaskResult result = new TaskResult(taskDelegatePayload, null);
            return result;
        }

        taskService.delegateTask(taskDelegatePayload.getTaskId(), taskDelegatePayload.getProd(), taskDelegatePayload.getBean(),
                getAssigneeUser(taskDelegatePayload.getAssignee()), taskDelegatePayload.getComment(), taskDelegatePayload.getAssignee(), taskDelegatePayload.getFiles());

        Map<String, String> params = new HashMap<>();
        String userId = getAssigneeUser(taskDelegatePayload.getAssignee());
        params.put("transferUserId", userId);
        UpcomingCommentInfoDTO upcomingInfo = DelegateHelper.buildUpcomingInfo(null, taskDelegatePayload.getComment(),
                SubmitTypeEnum.DELEGATE, taskDelegatePayload.getBeanId(), taskDelegatePayload.getTaskId(), params);
        activitiUpcomingRun.completeUpcoming(upcomingInfo);

        TaskResult result = new TaskResult(taskDelegatePayload, null);
        return result;
    }

    @Override
    public TaskDelegatePayload paramsParse(Map<String, Object> params) {
        TaskDelegatePayload taskDelegatePayload = new TaskDelegatePayload(
                formatString(params.get("piid")),
                formatString(params.get(AbstractOperator.PROD)),
                formatString(params.get("taskId")),
                formatString(params.get("beanId")),
                formatString(params.get("assignee")),
                formatString(params.get("comment"))
        );
        taskDelegatePayload.setFiles(formatString(params.get("files")));
        taskDelegatePayload.setBean(params.get(AbstractOperator.BEAN) == null ? null : (Map<String, Object>) params.get(AbstractOperator.BEAN));
        return taskDelegatePayload;
    }

    @Override
    public OperatorPayloadParamsValidator<TaskDelegatePayload> getValidator() {
        return validator;
    }


}
