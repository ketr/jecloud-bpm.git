/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.task.operator;

import com.google.common.base.Strings;
import com.je.bpm.api.runtime.task.impl.APITaskConverter;
import com.je.bpm.api.runtime.task.operator.desc.TaskSubmitParamDesc;
import com.je.bpm.api.runtime.task.operator.validator.TaskPersonnelAdjustmentsOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.engine.TaskService;
import com.je.bpm.engine.impl.persistence.entity.TaskEntity;
import com.je.bpm.model.task.payloads.TaskPersonnelAdjustmentsPayload;
import com.je.bpm.model.task.result.TaskResult;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;
import com.je.bpm.runtime.task.operator.TaskPersonnelAdjustmentsOperator;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 会签人员调整
 */
public class TaskPersonnelAdjustmentsOperatorImpl extends AbstractOperator<TaskPersonnelAdjustmentsPayload, TaskResult> implements TaskPersonnelAdjustmentsOperator {

    private APITaskConverter apiTaskConverter;
    private TaskService taskService;
    private OperatorPayloadParamsValidator<TaskPersonnelAdjustmentsPayload> validator;

    public TaskPersonnelAdjustmentsOperatorImpl(APITaskConverter apiTaskConverter, TaskService taskService) {
        this.validator = new TaskPersonnelAdjustmentsOperatorValidator();
        this.apiTaskConverter = apiTaskConverter;
        this.taskService = taskService;
    }

    @Override
    public String getId() {
        return OperatorEnum.TASK_PERSONNEL_ADJUSTMENTS_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.TASK_PERSONNEL_ADJUSTMENTS_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new TaskSubmitParamDesc();
    }

    @Override
    public TaskResult execute(TaskPersonnelAdjustmentsPayload payload) {
        String assignees = payload.getAssignees();

        List<String> newUserIds = Arrays.asList(getAssigneeUser(assignees).split(","));
        List<String> users = taskService.getCounterSignStaff(payload.getTaskId());

        //加签人员
        for (String userId : newUserIds) {
            if (!users.contains(userId)) {
                taskService.countersignedAddSignature(userId, payload.getTaskId());
            }
        }

        //减签人员
        String taskId = payload.getTaskId();
        String rootExecutionId = "";
        for (String userId : users) {
            if (!newUserIds.contains(userId)) {
                String nowRootExecutionId = taskService.getRootExecutionIdByTaskId(taskId);
                if (!Strings.isNullOrEmpty(rootExecutionId) && !rootExecutionId.equals(nowRootExecutionId)) {
                    return null;
                }
                if (Strings.isNullOrEmpty(rootExecutionId)) {
                    rootExecutionId = nowRootExecutionId;
                }
                taskService.countersignedVisaReduction(userId, taskId);
                List<TaskEntity> taskEntityList = taskService.getTaskIdByRootExecutionId(rootExecutionId);
                if (taskEntityList == null || taskEntityList.size() == 0) {
                    return null;
                }
                taskId = taskEntityList.get(0).getId();
            }
        }
        return null;
    }

    @Override
    public TaskPersonnelAdjustmentsPayload paramsParse(Map<String, Object> params) {
        TaskPersonnelAdjustmentsPayload taskPersonnelAdjustmentsPayload = new TaskPersonnelAdjustmentsPayload(
                formatString(params.get(AbstractOperator.PROD)),
                formatString(params.get("piid")),
                formatString(params.get("taskId")),
                formatString(params.get("beanId")),
                formatString(params.get("assignee"))
        );
        taskPersonnelAdjustmentsPayload.setFiles(formatString(params.get("files")));
        return taskPersonnelAdjustmentsPayload;
    }

    @Override
    public OperatorPayloadParamsValidator<TaskPersonnelAdjustmentsPayload> getValidator() {
        return validator;
    }

}
