/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.task.operator;

import com.google.common.base.Strings;
import com.je.bpm.api.runtime.task.impl.APITaskConverter;
import com.je.bpm.api.runtime.task.operator.desc.TaskSubmitParamDesc;
import com.je.bpm.api.runtime.task.operator.validator.TaskPassOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.engine.TaskService;
import com.je.bpm.engine.delegate.DelegateHelper;
import com.je.bpm.engine.impl.cmd.SubmitTypeEnum;
import com.je.bpm.engine.upcoming.ActivitiUpcomingRun;
import com.je.bpm.engine.upcoming.UpcomingCommentInfoDTO;
import com.je.bpm.model.task.payloads.TaskPassPayload;
import com.je.bpm.model.task.result.TaskResult;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;
import com.je.bpm.runtime.task.operator.TaskPassOperator;

import java.util.Map;

/**
 * 任务通过操作
 */
public class TaskPassOperatorImpl extends AbstractOperator<TaskPassPayload, TaskResult> implements TaskPassOperator {

    private APITaskConverter apiTaskConverter;
    private TaskService taskService;
    private OperatorPayloadParamsValidator<TaskPassPayload> validator;
    private ActivitiUpcomingRun activitiUpcomingRun;

    public TaskPassOperatorImpl(APITaskConverter apiTaskConverter, TaskService taskService, ActivitiUpcomingRun activitiUpcomingRun) {
        this.validator = new TaskPassOperatorValidator();
        this.apiTaskConverter = apiTaskConverter;
        this.taskService = taskService;
        this.activitiUpcomingRun = activitiUpcomingRun;
    }

    @Override
    public String getId() {
        return OperatorEnum.TASK_PASS_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.TASK_PASS_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new TaskSubmitParamDesc();
    }

    @Override
    public TaskResult execute(TaskPassPayload taskPassPayload) {
        UpcomingCommentInfoDTO upcomingInfo = DelegateHelper.buildUpcomingInfo(taskPassPayload.getBean(), taskPassPayload.getComment(),
                SubmitTypeEnum.PASS, taskPassPayload.getBeanId(), taskPassPayload.getTaskId(), null);
        activitiUpcomingRun.completeUpcoming(upcomingInfo);
        taskService.pass(taskPassPayload.getTaskId(), taskPassPayload.getProd(), taskPassPayload.getBean(), taskPassPayload.getComment(), taskPassPayload.getFiles());
        return null;
    }

    @Override
    public TaskPassPayload paramsParse(Map<String, Object> params) {
        TaskPassPayload taskPassPayload = new TaskPassPayload(
                formatString(params.get(AbstractOperator.PROD)),
                formatString(params.get("piid")),
                formatString(params.get("taskId")),
                formatString(params.get("beanId"))
        );
        taskPassPayload.setBean(params.get(AbstractOperator.BEAN) == null ? null : (Map<String, Object>) params.get(AbstractOperator.BEAN));
        taskPassPayload.setComment(formatString(params.get("comment")));
        if (Strings.isNullOrEmpty(taskPassPayload.getComment())) {
            taskPassPayload.setComment("通过");
        }
        taskPassPayload.setFiles(formatString(params.get("files")));
        return taskPassPayload;
    }

    @Override
    public OperatorPayloadParamsValidator<TaskPassPayload> getValidator() {
        return validator;
    }

}
