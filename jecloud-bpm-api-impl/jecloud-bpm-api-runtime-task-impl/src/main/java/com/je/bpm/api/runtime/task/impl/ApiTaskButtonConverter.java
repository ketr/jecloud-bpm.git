/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.task.impl;

import com.google.common.base.Strings;
import com.je.bpm.api.runtime.shared.model.impl.ListConverter;
import com.je.bpm.api.runtime.shared.model.impl.ModelConverter;
import com.je.bpm.core.model.button.*;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.model.task.model.impl.*;

import java.util.Map;

public class ApiTaskButtonConverter extends ListConverter<com.je.bpm.core.model.button.TaskButton, Map<String, Object>, com.je.bpm.model.task.model.TaskButton>
        implements ModelConverter<com.je.bpm.core.model.button.TaskButton, Map<String, Object>, com.je.bpm.model.task.model.TaskButton> {

    @Override
    public com.je.bpm.model.task.model.TaskButton from(TaskButton source, Map<String, Object> params) {
        com.je.bpm.model.task.model.TaskButton taskButton = null;
        if (source instanceof TaskDismissButton) {
            taskButton = new TaskDismissButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskTransferButton) {//转办按钮
            taskButton = new TaskTransferButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskPassroundButton) {
            taskButton = new TaskPassRoundButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskChangeAssigneeButton) {
            taskButton = new TaskChangeAssigneeButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskRetrieveButton) {
            taskButton = new TaskRetrieveButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskCountersignButton) {
            taskButton = new TaskCountersignButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskSignBackButton) {
            taskButton = new TaskSignBackButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskStagingButton) {
            taskButton = new TaskStagingButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskWithdrawButton) {
            taskButton = new TaskWithdrawButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskCancelDelegateButton) {
            taskButton = new TaskCancelDelegateButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskAddSignatureNodeButton) {
            taskButton = new TaskAddSignatureNodeButtonImpl(source.getId(), source.getName(), source.getCode(), source.getDisplayExpressionWhenStarted(), source.getDisplayExpressionWhenStartedFn());
        } else if (source instanceof TaskDelSignatureNodeButton) {
            taskButton = new TaskDelSignatureNodeButtonImpl(source.getId(), source.getName(), source.getCode(), source.getDisplayExpressionWhenStarted(), source.getDisplayExpressionWhenStartedFn());
        } else if (source instanceof TaskClaimButton) {
            taskButton = new TaskClaimButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskDelegateButton) {//委托按钮
            taskButton = new TaskDelegateButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskDirectSendButton) {
            taskButton = new TaskDirectSendButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskGobackButton) {
            taskButton = new TaskGoBackButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskPassroundReadButton) {
            taskButton = new TaskPassRoundReadButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskSubmitButton) {
            taskButton = new TaskSubmitButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskUrgeButton) {
            taskButton = new TaskUrgeButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskAbstainButton) {//弃权
            taskButton = new TaskCountersignedAbstainButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskCountersignedAddSignatureButton) {//加签
            taskButton = new TaskCountersignedAddSignatureButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskCountersignedRebookReductionButton) {//改签
            taskButton = new TaskCountersignedRebookButtonImpl(source.getId(), "改签", source.getCode());
        } else if (source instanceof TaskCountersignedVisaReductionButton) {//减签
            taskButton = new TaskCountersignedVisaReductionButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskPassButton) {//通过
            taskButton = new TaskCountersignedPassButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskVetoButton) {//否决
            taskButton = new TaskCountersignedVetoButtonImpl(source.getId(), source.getName(), source.getCode());
        } else if (source instanceof TaskCustomSubmitButton) {//自定义提交
            TaskCustomSubmitButton taskCustomSubmitButton = (TaskCustomSubmitButton) source;
            taskButton = new TaskCustomSubmitButtonImpl(source.getId(), source.getName(), source.getCode(), taskCustomSubmitButton.getNode());
        } else {
            throw new ActivitiException("Converter can't find the converted defined button from the source");
        }
        taskButton.setAppListeners(source.getAppListeners());
        taskButton.setBackendListeners(source.getBackendListeners());
        taskButton.setPcListeners(source.getPcListeners());
        taskButton.setOperationId(source.getOperationId());
        if (!Strings.isNullOrEmpty(source.getCustomizeComments())) {
            taskButton.setCustomizeComments(source.getCustomizeComments());
        }

        if (!Strings.isNullOrEmpty(source.getCustomizeName())) {
            taskButton.setName(source.getCustomizeName());
        }
        taskButton.setTaskId((String) params.get("taskId"));
        Object piid = params.get("piid");
        if (piid == null) {
            taskButton.setPiid("");
        } else {
            taskButton.setPiid(String.valueOf(piid));
        }

        Object pdid = params.get("pdid");
        if (pdid != null) {
            taskButton.setPdid(String.valueOf(pdid));
        }

        //简易审批
        if (params.get("simpleApproval") != null && (boolean) params.get("simpleApproval")) {
            taskButton.setSimpleApproval(1);
        } else {
            taskButton.setSimpleApproval(0);
        }

        return taskButton;
    }
}
