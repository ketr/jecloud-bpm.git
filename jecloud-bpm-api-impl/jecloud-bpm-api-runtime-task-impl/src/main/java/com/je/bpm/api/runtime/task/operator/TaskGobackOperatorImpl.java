/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.task.operator;

import com.je.bpm.api.runtime.task.impl.APITaskConverter;
import com.je.bpm.api.runtime.task.operator.desc.TaskGobackParamDesc;
import com.je.bpm.api.runtime.task.operator.validator.TaskGobackOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.engine.TaskService;
import com.je.bpm.engine.delegate.DelegateHelper;
import com.je.bpm.engine.impl.cmd.SubmitTypeEnum;
import com.je.bpm.engine.upcoming.ActivitiUpcomingRun;
import com.je.bpm.engine.upcoming.UpcomingCommentInfoDTO;
import com.je.bpm.model.task.payloads.TaskGobackPayload;
import com.je.bpm.model.task.result.TaskResult;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;
import com.je.bpm.runtime.task.operator.TaskGobackOperator;

import java.util.Map;

/**
 * 退回操作
 */
public class TaskGobackOperatorImpl extends AbstractOperator<TaskGobackPayload, TaskResult> implements TaskGobackOperator {

    private APITaskConverter apiTaskConverter;
    private TaskService taskService;
    private OperatorPayloadParamsValidator<TaskGobackPayload> validator;
    private ActivitiUpcomingRun activitiUpcomingRun;

    public TaskGobackOperatorImpl(APITaskConverter apiTaskConverter, TaskService taskService, ActivitiUpcomingRun activitiUpcomingRun) {
        this.validator = new TaskGobackOperatorValidator();
        this.apiTaskConverter = apiTaskConverter;
        this.taskService = taskService;
        this.activitiUpcomingRun = activitiUpcomingRun;
    }

    @Override
    public String getId() {
        return OperatorEnum.TASK_GOBACK_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.TASK_GOBACK_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new TaskGobackParamDesc();
    }

    @Override
    public TaskResult execute(TaskGobackPayload taskGobackPayload) {
        UpcomingCommentInfoDTO upcomingInfo = DelegateHelper.buildUpcomingInfo(taskGobackPayload.getBean(), taskGobackPayload.getComment(),
                SubmitTypeEnum.GOBACK, taskGobackPayload.getBeanId(), taskGobackPayload.getTaskId(), null);
        activitiUpcomingRun.completeUpcoming(upcomingInfo);

        taskService.gobackTask(taskGobackPayload.getTaskId(), taskGobackPayload.getProd(), taskGobackPayload.getBean(),
                taskGobackPayload.getComment(), taskGobackPayload.getFiles());
        TaskResult result = new TaskResult(taskGobackPayload, null);
        return result;
    }

    @Override
    public TaskGobackPayload paramsParse(Map<String, Object> params) {
        TaskGobackPayload taskGobackPayload = new TaskGobackPayload(
                formatString(params.get("piid")),
                formatString(params.get(AbstractOperator.PROD)),
                formatString(params.get("taskId")),
                formatString(params.get("beanId"))
        );
        taskGobackPayload.setComment(formatString(params.get("comment")));
        taskGobackPayload.setFiles(formatString(params.get("files")));
        taskGobackPayload.setBean(params.get(AbstractOperator.BEAN) == null ? null : (Map<String, Object>) params.get(AbstractOperator.BEAN));
        return taskGobackPayload;
    }

    @Override
    public OperatorPayloadParamsValidator<TaskGobackPayload> getValidator() {
        return validator;
    }
}
