/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.task.operator;

import com.google.common.base.Strings;
import com.je.bpm.api.runtime.task.impl.APITaskConverter;
import com.je.bpm.api.runtime.task.operator.desc.TaskSubmitParamDesc;
import com.je.bpm.api.runtime.task.operator.validator.TaskRebookOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.engine.TaskService;
import com.je.bpm.engine.delegate.DelegateHelper;
import com.je.bpm.engine.delegate.event.ActivitiCountersignedOpinionType;
import com.je.bpm.engine.impl.cmd.SubmitTypeEnum;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.engine.upcoming.ActivitiUpcomingRun;
import com.je.bpm.engine.upcoming.UpcomingCommentInfoDTO;
import com.je.bpm.model.task.payloads.TaskRebookPayload;
import com.je.bpm.model.task.result.TaskResult;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;
import com.je.bpm.runtime.task.operator.TaskRebookOperator;

import java.util.Map;

/**
 * 改签
 */
public class TaskRebookOperatorImpl extends AbstractOperator<TaskRebookPayload, TaskResult> implements TaskRebookOperator {

    private APITaskConverter apiTaskConverter;
    private TaskService taskService;
    private OperatorPayloadParamsValidator<TaskRebookPayload> validator;
    private ActivitiUpcomingRun activitiUpcomingRun;

    public TaskRebookOperatorImpl(APITaskConverter apiTaskConverter, TaskService taskService, ActivitiUpcomingRun activitiUpcomingRun) {
        this.validator = new TaskRebookOperatorValidator();
        this.apiTaskConverter = apiTaskConverter;
        this.taskService = taskService;
        this.activitiUpcomingRun = activitiUpcomingRun;
    }

    @Override
    public String getId() {
        return OperatorEnum.TASK_REBOOK_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.TASK_REBOOK_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new TaskSubmitParamDesc();
    }

    @Override
    public TaskResult execute(TaskRebookPayload payload) {
        String userId = Authentication.getAuthenticatedUser().getDeptId();
        String taskId = payload.getTaskId();
        ActivitiCountersignedOpinionType type = ActivitiCountersignedOpinionType.PASS;
        if (payload.getOpinionType().toString().equals(ActivitiCountersignedOpinionType.PASS.toString())) {
            type = ActivitiCountersignedOpinionType.PASS;
        }

        if (payload.getOpinionType().toString().equals(ActivitiCountersignedOpinionType.VETO.toString())) {
            type = ActivitiCountersignedOpinionType.VETO;
        }

        if (payload.getOpinionType().toString().equals(ActivitiCountersignedOpinionType.ABSTAIN.toString())) {
            type = ActivitiCountersignedOpinionType.ABSTAIN;
        }

        UpcomingCommentInfoDTO upcomingInfo = DelegateHelper.buildUpcomingInfo(payload.getBean(),
                payload.getComment(), SubmitTypeEnum.REBOOK,
                payload.getBeanId(), payload.getTaskId(), null);
        activitiUpcomingRun.completeUpcoming(upcomingInfo);

        taskService.countersignedRebook(userId, taskId, payload.getComment(), payload.getBean(), payload.getProd(), type, payload.getFiles());
        return null;
    }

    @Override
    public TaskRebookPayload paramsParse(Map<String, Object> params) {
        String rebookType = formatString(params.get("rebookType"));
        TaskRebookPayload.RebookTypeEnum rebookTypeEnum = TaskRebookPayload.RebookTypeEnum.PASS;
        if (rebookType.equals(TaskRebookPayload.RebookTypeEnum.PASS.toString())) {
            rebookTypeEnum = TaskRebookPayload.RebookTypeEnum.PASS;
        }
        if (rebookType.equals(TaskRebookPayload.RebookTypeEnum.VETO.toString())) {
            rebookTypeEnum = TaskRebookPayload.RebookTypeEnum.VETO;
        }
        if (rebookType.equals(TaskRebookPayload.RebookTypeEnum.ABSTAIN.toString())) {
            rebookTypeEnum = TaskRebookPayload.RebookTypeEnum.ABSTAIN;
        }
        TaskRebookPayload taskRebookPayload = new TaskRebookPayload(
                formatString(params.get(AbstractOperator.PROD)),
                formatString(params.get("piid")),
                formatString(params.get("taskId")),
                formatString(params.get("beanId")),
                formatString(params.get("comment")),
                rebookTypeEnum, null);
        taskRebookPayload.setBean(params.get(AbstractOperator.BEAN) == null ? null : (Map<String, Object>) params.get(AbstractOperator.BEAN));
        taskRebookPayload.setComment(formatString(params.get("comment")));
        if (Strings.isNullOrEmpty(taskRebookPayload.getComment())) {
            taskRebookPayload.setComment("改签");
        }
        taskRebookPayload.setFiles(formatString(params.get("files")));
        return taskRebookPayload;
    }

    @Override
    public OperatorPayloadParamsValidator<TaskRebookPayload> getValidator() {
        return validator;
    }

}
