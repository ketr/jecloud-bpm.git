/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.task.operator;

import com.je.bpm.api.runtime.task.impl.APITaskConverter;
import com.je.bpm.api.runtime.task.operator.desc.TaskSubmitParamDesc;
import com.je.bpm.api.runtime.task.operator.validator.SignBackOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.core.model.BpmnModel;
import com.je.bpm.core.model.FlowElement;
import com.je.bpm.core.model.task.KaiteMultiUserTask;
import com.je.bpm.engine.ActivitiException;
import com.je.bpm.engine.RepositoryService;
import com.je.bpm.engine.TaskService;
import com.je.bpm.engine.impl.bpmn.behavior.KaiteBaseUserTaskActivityBehavior;
import com.je.bpm.engine.impl.persistence.entity.VariableInstance;
import com.je.bpm.engine.task.Task;
import com.je.bpm.engine.task.TaskQuery;
import com.je.bpm.engine.upcoming.ActivitiUpcomingRun;
import com.je.bpm.model.task.payloads.TaskSignBackPayload;
import com.je.bpm.model.task.result.TaskResult;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;
import com.je.bpm.runtime.task.operator.TaskSignBackOperator;

import java.util.List;
import java.util.Map;

/**
 * 任务回签操作
 */
public class TaskSignBackOperatorImpl extends AbstractOperator<TaskSignBackPayload, TaskResult> implements TaskSignBackOperator {

    private APITaskConverter apiTaskConverter;
    private TaskService taskService;
    private OperatorPayloadParamsValidator<TaskSignBackPayload> validator;
    private ActivitiUpcomingRun activitiUpcomingRun;
    private RepositoryService repositoryService;

    public TaskSignBackOperatorImpl(APITaskConverter apiTaskConverter, TaskService taskService,
                                    ActivitiUpcomingRun activitiUpcomingRun, RepositoryService repositoryService) {
        this.validator = new SignBackOperatorValidator();
        this.apiTaskConverter = apiTaskConverter;
        this.taskService = taskService;
        this.activitiUpcomingRun = activitiUpcomingRun;
        this.repositoryService = repositoryService;
    }

    @Override
    public String getId() {
        return OperatorEnum.TASK_SIGN_BACK_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.TASK_SIGN_BACK_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new TaskSubmitParamDesc();
    }

    @Override
    public TaskSignBackPayload paramsParse(Map<String, Object> params) {
        TaskSignBackPayload payload = TaskSignBackPayload.build()
                .setBean(params.get(AbstractOperator.BEAN) == null ? null : (Map<String, Object>) params.get(AbstractOperator.BEAN))
                .setBeanId(formatString(params.get("beanId")))
                .setComment(formatString(params.get("comment")))
                .setProd(formatString(params.get(AbstractOperator.PROD)))
                .setTaskId(formatString(params.get("taskId")))
                .setSignBackId(formatString(params.get("signBackId")));
        payload.setFiles(formatString(params.get("files")));
        return payload;
    }

    @Override
    public TaskResult execute(TaskSignBackPayload payload) {
//        taskService.countersign(payload.getTaskId(),
//                payload.getProd(),
//                payload.getBean(),
//                payload.getComment(),
//                payload.getBeanId(),
//                payload.getAssignee(), payload.getSignBackId());
//        return null;
        //检查流程是否已经提交
        TaskQuery taskQuery = taskService.createTaskQuery().taskId(payload.getTaskId());
        List<Task> list = taskQuery.list();
        if (list.size() == 0) {
            throw new ActivitiException(String.format("Can't find task definiton from the process"));
        }
        Task task = list.get(0);
        BpmnModel bpmnModel = repositoryService.getBpmnModel(task.getProcessDefinitionId(), task.getProcessInstanceId(), task.getBusinessKey());
        FlowElement taskFlowElement = bpmnModel.getFlowElement(task.getTaskDefinitionKey());
        if (taskFlowElement == null) {
            throw new ActivitiException(String.format("Can't find task %s definiton from the process.", task.getName()));
        }
        if (taskFlowElement instanceof KaiteMultiUserTask) {

        } else {
            //查询加签人员信息
            Map<String, VariableInstance> vars = taskService.getVariableInstances(task.getId());
            VariableInstance prevAssignee = vars.get("countersignPrevAssignee");
            String prevAssigneeStr = prevAssignee.getTextValue();

            VariableInstance directTarget = vars.get("countersignDirectTarget");
            String directTargetStr = directTarget.getTextValue();

            VariableInstance directTaskId = vars.get("countersignDirectTaskId");
            String directTaskIdStr = directTaskId.getTextValue();

            VariableInstance directTaskName = vars.get("countersignDirectTaskName");
            String directTaskNameStr = directTaskName.getTextValue();

            String assignee = String.format("[{\"nodeId\":\"%s\",\"nodeName\":\"%s\",\"assignee\":\"%s\",\"assigneeName\":\"\"}]"
                    , directTaskIdStr, directTaskNameStr, prevAssigneeStr);
            VariableInstance histVar = vars.get(KaiteBaseUserTaskActivityBehavior.HIST_COUNTERSIGN);
            //节点提交完成操作
            taskService.complete(payload.getTaskId(), assignee, directTargetStr,
                    payload.getProd(), payload.getBean(), payload.getComment(), "[]", "0", false, true, histVar.getValue(), payload.getFiles());
        }
        return null;
    }

    @Override
    public OperatorPayloadParamsValidator<TaskSignBackPayload> getValidator() {
        return validator;
    }
}
