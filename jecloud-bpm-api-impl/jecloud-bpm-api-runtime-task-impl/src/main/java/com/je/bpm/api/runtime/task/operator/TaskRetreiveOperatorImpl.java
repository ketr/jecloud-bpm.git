/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.api.runtime.task.operator;

import com.je.bpm.api.runtime.task.impl.APITaskConverter;
import com.je.bpm.api.runtime.task.operator.desc.TaskRetrieveParamDesc;
import com.je.bpm.api.runtime.task.operator.validator.TaskRetreiveOperatorValidator;
import com.je.bpm.common.operation.OperatorEnum;
import com.je.bpm.engine.TaskService;
import com.je.bpm.engine.delegate.DelegateHelper;
import com.je.bpm.engine.impl.cmd.SubmitTypeEnum;
import com.je.bpm.engine.upcoming.ActivitiUpcomingRun;
import com.je.bpm.engine.upcoming.UpcomingCommentInfoDTO;
import com.je.bpm.model.task.payloads.TaskRetrievePayload;
import com.je.bpm.model.task.result.TaskResult;
import com.je.bpm.runtime.shared.operator.AbstractOperator;
import com.je.bpm.runtime.shared.operator.desc.OperationParamDesc;
import com.je.bpm.runtime.shared.operator.validator.OperatorPayloadParamsValidator;
import com.je.bpm.runtime.task.operator.TaskRetreiveOperator;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * 取回操作
 */
public class TaskRetreiveOperatorImpl extends AbstractOperator<TaskRetrievePayload, TaskResult> implements TaskRetreiveOperator {

    private APITaskConverter apiTaskConverter;
    private TaskService taskService;
    private OperatorPayloadParamsValidator<TaskRetrievePayload> validator;
    private ActivitiUpcomingRun activitiUpcomingRun;

    public TaskRetreiveOperatorImpl(APITaskConverter apiTaskConverter, TaskService taskService, ActivitiUpcomingRun activitiUpcomingRun) {
        this.validator = new TaskRetreiveOperatorValidator();
        this.apiTaskConverter = apiTaskConverter;
        this.taskService = taskService;
        this.activitiUpcomingRun = activitiUpcomingRun;
    }

    @Override
    public String getId() {
        return OperatorEnum.TASK_RETREIEVE_OPERATOR.getId();
    }

    @Override
    public String getName() {
        return OperatorEnum.TASK_RETREIEVE_OPERATOR.getName();
    }

    @Override
    public OperationParamDesc getParamDesc() {
        return new TaskRetrieveParamDesc();
    }

    @Override
    public TaskResult execute(TaskRetrievePayload taskRetrievePayload) {
        UpcomingCommentInfoDTO upcomingInfo = DelegateHelper.buildUpcomingInfo(null, "", SubmitTypeEnum.RETRIEVE,
                taskRetrievePayload.getBeanId(), taskRetrievePayload.getTaskId(), null);
        activitiUpcomingRun.completeUpcoming(upcomingInfo);
        if (StringUtils.isEmpty(taskRetrievePayload.getComment())) {
            taskService.retrieveTask(taskRetrievePayload.getTaskId(), taskRetrievePayload.getProd(), taskRetrievePayload.getBean()
                    , taskRetrievePayload.getBeanId());
        } else {
            taskService.retrieveTask(taskRetrievePayload.getTaskId(), taskRetrievePayload.getProd(), taskRetrievePayload.getBean()
                    , taskRetrievePayload.getComment(), taskRetrievePayload.getBeanId(), taskRetrievePayload.getFiles());
        }
        TaskResult result = new TaskResult(taskRetrievePayload, null);
        return result;
    }

    @Override
    public TaskRetrievePayload paramsParse(Map<String, Object> params) {
        TaskRetrievePayload taskRetrievePayload = new TaskRetrievePayload(
                formatString(params.get("piid")),
                formatString(params.get(AbstractOperator.PROD)),
                formatString(params.get("taskId")),
                formatString(params.get("beanId"))
        );
        taskRetrievePayload.setBean(params.get(AbstractOperator.BEAN) == null ? null : (Map<String, Object>) params.get(AbstractOperator.BEAN));
        taskRetrievePayload.setFiles(formatString(params.get("files")));
        return taskRetrievePayload;
    }

    @Override
    public OperatorPayloadParamsValidator<TaskRetrievePayload> getValidator() {
        return validator;
    }

}

