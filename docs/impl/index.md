# 核心实现模块

## 目录

```shell
./jecloud-bpm-api-impl
├── ./jecloud-bpm-api-impl/jecloud-bpm-api-model-operation-impl
├── ./jecloud-bpm-api-impl/jecloud-bpm-api-model-process-impl
├── ./jecloud-bpm-api-impl/jecloud-bpm-api-model-shared-impl
├── ./jecloud-bpm-api-impl/jecloud-bpm-api-model-task-impl
├── ./jecloud-bpm-api-impl/jecloud-bpm-api-runtime-operation-impl
├── ./jecloud-bpm-api-impl/jecloud-bpm-api-runtime-process-impl
├── ./jecloud-bpm-api-impl/jecloud-bpm-api-runtime-shared-impl
└── ./jecloud-bpm-api-impl/jecloud-bpm-api-runtime-task-impl

```