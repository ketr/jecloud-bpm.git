# API模块



## 目录

```shell

./jecloud-bpm-api
├── ./jecloud-bpm-api/jecloud-bpm-api-model-operation 操作实体定义
├── ./jecloud-bpm-api/jecloud-bpm-api-model-process 流程实体定义
├── ./jecloud-bpm-api/jecloud-bpm-api-model-shared 基础实体定义
├── ./jecloud-bpm-api/jecloud-bpm-api-model-task 任务实体定义
├── ./jecloud-bpm-api/jecloud-bpm-api-runtime-operation 操作运行时定义
├── ./jecloud-bpm-api/jecloud-bpm-api-runtime-process 流程运行时定义
├── ./jecloud-bpm-api/jecloud-bpm-api-runtime-shared 基础运行时定义
└── ./jecloud-bpm-api/jecloud-bpm-api-runtime-task 任务运行时定义
```


