# 核心引擎模块

```shell
./jecloud-bpm-core
├── ./jecloud-bpm-core/jecloud-bpm-core-converter
├── ./jecloud-bpm-core/jecloud-bpm-core-engine
├── ./jecloud-bpm-core/jecloud-bpm-core-image-generator
├── ./jecloud-bpm-core/jecloud-bpm-core-json-converter
├── ./jecloud-bpm-core/jecloud-bpm-core-layout
├── ./jecloud-bpm-core/jecloud-bpm-core-model
└── ./jecloud-bpm-core/jecloud-bpm-core-process-validation
```

## 数据库设计

[请参考流程引擎数据库设计](./database.md)

## 流程引擎实现

## 流程BpmnModel转换

## 流程json转换

## 流程布局

## 流程校验
