# 数据库设计

在流程的产生、执行及结束等周期，都会产生各种与流程相关的数据，Activiti提供了一整套 数据表来保存这些数据。Activiti流程引擎的数据表分为5大类，每一类数据表均有不同的职责。
例如运行时数据表，专门用来记录流程运行时产生的数据；身份数据表专门保存身份数据，包括用户 、用户组等。

Activiti为这些数据表的命名制定了规范，不同职责的数据表，均可以通过命名来体现。例如运行时数据表 ，会以ACT_RU作为开头；历史数据表以ACT_HI作为开头。

## 通用数据表

通用数据表用于存放一些通用数据，这些表本身不关心特定的流程或者业务，只用于存放这些业务或流程使用的通用资源。 他们可以独立存在于流程引擎或者应用系统中，其他的数据表有可能会使用这些表中的数据。通用数据表有两个，他们都以
"ACT_GE"开头，GE是单词general的缩写。

> ACT_GE_BYTEARRAY，用于保存与流程引擎相关的资源，只要调用了Activiti存储服务的API，涉及的资源均会被转换 为byte数组保存到这个表中。在资源表中设计了一个BYTES字段，用来保存资源的内容，因此理论上其可以保存任何类型的资源 （文件或其他来源的输入流）。一般情况下，Activiti使用这个表来保存字符串、流程文件的内容、流程图片内容。包含的子弹如下：

1. REV_:数据版本（乐观锁），Activiti为一些有可能会被频繁修改的数据表，加入改字段，用来标识数据被操作修改的次数。
2. NAME_:资源名称，类型为varchar，长度为255字节。
3. DEPLOYMENT_ID_:一次部署可以添加多个资源，改字段与部署表ACT_RE_DEPLOYMENT的主键关联。
4. BYTES_:资源内容，数据类型为longblob，最大可存储4GB数据。
5. GENERATED_:是否由Activiti自动产生的资源，0表示false，1为true。

> ACT_GE_PROPERTY，Activiti将全部属性抽象为Key-value对，每个属性都有名称和值，该表有以下三个字段：

1. NAME_:属性名称，varchar类型。
2. VALUE:属性值，varchar类型。
3. REV_:数据的版本号。

## 流程存储数据表

流程引擎使用仓储来保存流程定义和部署信息这类数据，存储表形成以"ACT_RE"开头，"RE"是repository单词的缩写。

> ACT_GE_BYTEARRAY,部署数据表，在Activiti中，一次部署可以添加多个资源，资源会被保存在"ACT_GE_BYTEARRAY"，而对于部署，则部署信息会被保存到部署表""ACT_RE_DEPLOYMENT"
> 该表主要包含以下字段：

1. REV_:乐观锁,数据版本
2. NAME_:名称 ，部署的文件名称如leave.bpmn.png或leave.bpmn20.xml
3. DEPLOYMENT_ID_:部署ID
4. BYTES_:字节，部署文件
5. GENERATED_:是否引擎生成，0位用户生成，1位activiti生成

> ACT_RE_PROCDEF，流程定义表，Activiti在部署添加资源时，如果发布部署的文件是流程文件（.bpmn或.BPMN20.xml），则除了会解析这些流程定义文件，将内容保存到资源表中外，还会解析
> 流程文件的内容，形成特定的流程定义数据，写入流程定义表ACT_RE_PROCDEF中。其主要包含如下字段：

1. REV_: 乐观锁，数据版本
2. CATEGORY_: 流程定义的分类，读取流程XML文件的targetNamespace值
3. NAME_: 流程定义名称，读取流程文件中Process元素name属性
4. KEY_: 流程定义KEY，读取流程文件中process元素id属性
5. VERSION_:版本
6. DEPLOYMENT_ID_: 流程定义对应部署数据id
7. RESOURCE_NAME_: 流程定义对应的资源名称，一般为流程文件的相对路径。
8. DGRAM_RESOURCE_NAME_: 流程定义对应的流程图资源名称。
9. DESCRIPTION_: 描述
10. HAS_START_FORM_KEY_: 是否存在开始节点的FormKey
11. HAS_GRAPHICAL_NOTATION_:
12. SUSPENSION_STATE_: 表示流程定义的状态是激活还是中止，激活状态时该字段值为1，中止时字段值为2，如果流程定义被设置为中止状态，那么将不能启动流程。
13. TENANT_ID_: 租户ID
14. ENGINE_VERSION_: 引擎版本
15. APP_VERSION_: 应用版本

> ACT_RE_MODEL，流程设计模型的部署表，流程设计器设计流程后，保存数据到该表。该表字段如下所示：

1. REV_: 乐观锁，数据版本
2. NAME_: 名称
3. KEY_: 流程定义键
4. CATEGORY: 分类
5. CREATE_TIME_: 创建时间
6. LAST_UPDATE_TIME_: 最后更新时间
7. VERSION_: 版本
8. META_INFO_: 以Json格式保存流程定义的信息
9. DEPLOYMENT_ID_: 部署ID
10. EDITOR_SOURCE_VALUE_ID_:
11. EDITOR_SOURCE_EXTRA_VALUE_ID_:
12. TENANT_ID_: 租户ID

> ACT_RE_BUTTON，流程按钮表，此表用于流程部署后，个节点类型所包含的按钮。该表字段如下：

1. NAME_: 按钮名称
2. CODE_: 按钮编码
3. REV_: 按钮版本
4. GROUP_: 所属分类，此分类为节点类型分类
5. DISPLAY_EXPRESSION_: 展示表达式，根据当时流程变量或任务变量计算出相应结果
6. ENABLE_: 是否启用，默认启用1，禁用0
7. DESCRIPTION_: 描述
8. ICON_: 按钮图标
9. TENANT_ID_: 租户ID
10. PARAMS_: 按钮参数

## 运行时数据表

- ACT_RU_EXECUTION，流程实例（执行流）表，流程启动后，会产生一个流程实例，同事会产生相应的执行流，流程实例和执行流数据都会被保存在此表中，如果一个流程实例
- 只有一条执行流，那额该表只产生一条数据，该数据既表示执行表，也表示流程实例。此表中包含如下字段：

1. REV_: 乐观锁，数据版本。
2. PROC_INST_ID_: 流程实例ID，一个流程实例有可能会产生多个执行流，该字段标识执行流所属的流程实例。
3. BUSINESS_KEY_: 启动流程时制定的业务主键。
4. PARENT_ID_: 父执行流的ID，一个流程实例会产生执行流，该字段保存父执行流ID。
5. PROC_DEF_ID_: 流程定义数据的ID。
6. SUPER_EXEC_:
7. ROOT_PROC_INST_ID_:
8. ACT_ID_: 当前执行流行为的ID，ID在流程文件中定义。
9. IS_ACTIVE_: 执行流是否活跃的标识。
10. IS_CONCURRENT_: 执行流是否正在并行。
11. IS_SCOPE_:
12. IS_EVENT_SCOPE:
13. IS_MI_ROOT_:
14. SUSPENSION_STATE_: 标识流程的中断状态。
15. CACHED_END_STATE_:
16. TENANT_ID_:
17. NAME_:
18. START_TIME_:
19. START_USER_ID_:
20. LOCK_TIME_:
21. IS_COUNT_ENABLED_:
22. EVT_SUBSCR_COUNT_:
23. TASK_COUNT_:
24. JOB_COUNT_:
25. TIMER_JOB_COUNT_:
26. SUSP_JOB_COUNT_:
27. DEADLETTER_JOB_COUNT_:
28. VAR_COUNT_:
29. ID_LINK_COUNT_:
30. APP_VERSION_:

> ACT_RU_TASK，流程任务表，流程在运行过程中所产生的任务数据保存在此表中，该表主要有如下字段：

1. REV_: 乐观锁，数据版本。
2. EXECUTION_ID_: 任务所在的执行流ID。
3. PROC_INST_ID_: 任务对应的流程实例ID。
4. PROC_DEF_ID_: 对应流程定义数据的ID。
5. NAME_: 任务名称，在流程文件中定义。
6. BUSINESS_KEY_:  业务键
7. PARENT_TASK_ID_: 父节点实例ID
8. DESCRIPTION_: 任务描述，在流程文件中配置。
9. TASK_DEF_KEY_：任务定义的ID值，在流程文件中定义。
10. OWNER_: 任务拥有人，没有做外键关联。
11. ASSIGNEE_: 被指派执行该任务的人，没有做外键关联。
12. DELEGATION_: 委托类型
13. PRIORITY_: 任务优先级。
14. CREATE_TIME_: 创建时间
15. DUE_DATE_： 任务预定日期，类型为datetime。
16. CATEGORY_: 类别
17. SUSPENSION_STATE_：中止状态
18. TENANT_ID_: 租户ID
19. FORM_KEY_: 表单键
20. CLAIM_TIME_: （候选）认领事件
21. APP_VERSION_: 应用版本

> ACT_RU_JOB，工作表，在流程执行的过程中，会有一些工作需要定时或重复执行，这类数据被保存到工作表中。

1. REV_: 版本，乐观锁
2. TYPE_: 类型
3. LOCK_EXP_TIME_: 锁定释放时间
4. LOCK_OWNER_: 挂起者
5. EXCLUSIVE_:
6. EXECUTION_ID_: 执行实例ID
7. PROCESS_INSTANCE_ID_: 流程实例ID
8. PROC_DEF_ID_: 流程定义ID
9. RETRIES_: 重试次数
10. EXCEPTION_STACK_ID_: 异常信息ID
11. EXCEPTION_MSG_: 异常信息
12. DUEDATE_: 到期时间
13. REPEAT_: 重复
14. HANDLER_TYPE_: 处理类型
15. HANDLER_CFG_: 处理配置
16. TENANT_ID_: 租户ID

> ACT_RU_TIMER_JOB，定时器工作表，用于存放定时器工作。

1. REV_:
2. TYPE_:
3. LOCK_EXP_TIME_:
4. LOCK_OWNER_:
5. EXCLUSIVE_:
6. EXECUTION_ID_
7. PROCESS_INSTANCE_ID_:
8. PROC_DEF_ID_:
9. RETRIES_:
10. EXCEPTION_STACK_ID_:
11. EXCEPTION_MSG_:
12. DUEDATE_:
13. REPEAT_:
14. HANDLER_TYPE_:
15. HANDLER_CFG_:
16. TENANT_ID_:

> ACT_RU_DEADLETTER_JOB，无法执行工作表，用于存放无法执行的工作。

1. REV_:
2. TYPE_:
5. EXCLUSIVE_:
6. EXECUTION_ID_
7. PROCESS_INSTANCE_ID_:
8. PROC_DEF_ID_:
9. RETRIES_:
10. EXCEPTION_STACK_ID_:
11. EXCEPTION_MSG_:
12. DUEDATE_:
13. REPEAT_:
14. HANDLER_TYPE_:
15. HANDLER_CFG_:
16. TENANT_ID_:

> ACT_RU_SUSPENDED_JOB，中断工作表，中断工作产生后，会将工作保存到该表中。

1. REV_:
2. TYPE_:
5. EXCLUSIVE_:
6. EXECUTION_ID_
7. PROCESS_INSTANCE_ID_:
8. PROC_DEF_ID_:
9. RETRIES_:
10. EXCEPTION_STACK_ID_:
11. EXCEPTION_MSG_:
12. DUEDATE_:
13. REPEAT_:
14. HANDLER_TYPE_:
15. HANDLER_CFG_:
16. TENANT_ID_:

> ACT_RU_EVENT_SUBSCR，事件描述表。如果流程到达某类事件节点，Activiti会往此表中加入事件描述数据，这些事件描述数据将会决定
> 流程事件的触发。此表包含的字段如下所示：

1. REV_: 版本，乐观锁
2. EVENT_TYPE_，事件类型，不同的事件会产生不同类型的事件描述，并不是所有的事件都会产生事件描述。
3. EVENT_NAME_，事件名称，在流程文件中定义。
4. EXECUTION_ID_，事件所在的执行流ID
5. PROC_INST_ID_，事件所在的流程实例ID
6. ACTIVITY_ID_，具体事件的ID，在流程文件中定义
7. CONFIGURATION_
8. CREATED_，创建时间
9. PROC_DEF_ID_，流程定义ID
10. TENANT_ID_，租户ID

> ACT_RU_IDENTITYLINK_，运行时流程人员表，任务参与者数据表。主要存储当前节点参与者的信息，该表主要包含如下字段：

1. REV_:乐观锁
2. GROUP_ID_: 该关系数据中的用户组ID。
3. TYPE_: 该关系数据类型，当前提供了3个值：assginee、candidate和owner，表示流程数据的指派人（组）、候选人（组）和拥有人。
4. USER_ID_: 关系数据中的用户ID
5. TASK_ID_: 关系数据中的任务ID
6. PROC_INST_ID_: 关系数据中的流程实例ID
7. PROC_DEF_ID_: 关系数据中的流程定义ID

> ACT_RU_INTEGRATION


> ACT_RU_VARIABLE，流程实例参数表，存放流程中的参数，这类参数包括流程实例参数，执行流参数和任务参数，参会有可能会有多种类型，因此该表使用多个字段
> 存放参数值。此表主要包括如下字段：

1. REV_: 乐观锁，数据版本。
2. TYPE_:
   参数类型，该字段值可以为boolean,bytes,serializable,date,double,integer,jpa-entity,long,null,short,string，这些字段值均为activiti提供，还可以通过扩展来自定义参数类型。
3. NAME_: 参数名称。
4. EXCUTION_ID_: 该参数对应的执行ID，可以为Null
5. PROC_INST_ID_: 该参数对应的流程实例ID，可以为Null
6. TASK_ID_ 如果该参数是任务参数，就需要设置任务ID
7. BYTEARRAY_ID_: 如果参数值是序列化兑现个，那么可以将该对象作为资源保存到资源表中，该字段保存资源表中数据的ID。
8. DOUBLE_ID_: 参数类型为double的话，则会保存到该字段中
9. LONG_: 参数类型为long的话，则值会保存到该字段中。
10. TEXT_: 用于保存文本类型的参数值，该字段为varchar类型，长度为4000字节。
11. TEXT2_: 与TEXT_字段一样，用于保存文本类型的参数值。

> ACT_RU_BUTTON,流程按钮表，存放流程流转中节点所属的按钮，此表主要字段如下：

1. NAME_: 按钮名称
2. CODE_: 按钮编码
3. REV_: 按钮版本
4. GROUP_: 按钮分组，参考ACT_RE_BUTTON
5. DISPLAY_EXPRESSION_: 展示表达式，参考ACT_RE_BUTTON
6. ENABLE_: 是否启用，参考ACT_RE_BUTTON
7. DESCRIPTION_: 按钮描述
8. ICON_: 按钮图标
9. TASK_ID_: 任务ID
10. PROC_INSTANCE_ID_: 流程实例ID
11. PROC_DEF_ID_: 流程定义ID
12. OPERATION_ID_: 操作ID
13. CREATE_TIME_: 创建时间
14. LAST_UPDATE_TIME_: 最后更新时间
15. BUTTON_DEF_ID_: 按钮定义ID
16. TENANT_ID_: 租户ID

## 历史数据表

历史数据表就好像流程引擎的日志表，操作过的流程元素将会被记录到历史表中，历史数据表以ACT_HI开头，HI是history得缩写。

> ACT_HI_ACTINST，历史行为表，会记录一个流程活动的实例，一个流程活动将会被记录为一条数据，根据该表可以追踪到最完整的流程信息。

1. PROC_DEF_ID_: 流程定义ID
2. PROC_INST_ID_: 流程实例ID
3. EXECUTION_ID_: 执行实例ID
4. ACT_ID_: 节点ID
5. TASK_ID_: 任务实例ID
6. CALL_PROC_INST_ID_: 调用外部的流程实例ID
7. ACT_NAME_: 节点名称
8. ACT_TYPE_: 节点类型
9. ASSIGNEE_: 处理人
10. START_TIME_: 开始时间
11. END_TIME_: 中止时间
12. DURATION_: 耗时
13. DELETE_REASON_: 删除原因
14. TENANT_ID_: 租户ID

> ACT_HI_ATTACHMENT，附件表，使用任务服务的API，可以添加附件，这些附件会保存在此表中，此表包含如下字段：

1. REV_: 版本，乐观锁。
2. USER_ID_: 附件对用的用户ID，可以为Null
3. NAME_: 附件名称
4. DESCRIPTION_：附件描述
5. TYPE_：附件类型
6. TASK_ID_：该附件对应的任务ID
7. PROC_INST_ID_： 对应的流程实例ID
8. URL_：链接到该附件的URL
9. CONTENT_ID_：该附件内容ID，附件的内容将会被保存到资源表中，该字段记录资源数据ID
10. TIME_：创建时间

> ACT_HI_COMMENT，评论表，使用任务服务的API，可以添加评论，这些评论的数据将会保存在此表中，此表包含如下字段：

1. TYPE_：评论的类型，可以设置为event或者comment，表示事件记录数据或评论数据。
2. TIME_： 数据产生的时间。
3. USER_ID_：产生评论数据的用户ID。
4. TASK_ID_：该评论数据的任务ID。
5. PROC_INST_ID_： 数据对应的流程实例ID。
6. ACTION_：该评论数据的操作标识。
7. MESSAGE_：该评论数据的信息。
8. FULL_MSG_： 该字段同样记录评论数据的信息。

> ACT_HI_DETAIL，流程明细表。记录流程执行过程中的参数或者表单数据，由于在流程执行过程中，会产生大量这类数据，因此默认
> 情况下，Activiti不会保存流程明细数据，除非流程引擎的历史数据（history）配置为full。此表主要包含字段如下：

1. TYPE_: 类型
2. PROC_INST_ID_: 流程实例ID
3. EXECUTION_ID_: 执行实例ID
4. TASK_ID_: 任务实例ID
5. ACT_INST_ID: 节点实例ID
6. NAME_: 名称
7. VAR_TYPE_: 参数类型
8. REV_: 乐观锁，数据版本
9. TIME_：创建时间
10. BYTEARRAY_ID_：字节表ID
11. DOUBLE_：存储变量类型为Double
12. LONG_：存储变量类型为Long
13. TEXT_：存储变量类型为String
14. TEXT2_：存储变量类型为JPA持久化对象时，才会有此值。此值为对象ID（当前修正已去除JPA依赖）

> ACT_HI_IDENTITYLINK，历史流程人员表，任务参与者数据表。主要存储历史节点参与者的信息。主要字段如下：

1. GROUP_ID_: 组ID
2. TYPE_: 类型
3. USER_ID_: 用户ID
4. TASK_ID_: 节点实例ID
5. PROC_INST_ID_: 流程实例ID

> ACT_HI_PROCINST，流程历史实例表。流程实例的历史数据会被保存到此表中，只要流程被启动，就会将流程实例的数据写入此表中。
> 除了基本的流程字段外，与运行时数据不同的是，历史流程实例表还会记录流程的开始活动ID，结束活动ID等信息。此表主要包括的
> 字段如下所示：

1. PROC_INST_ID_：流程实例ID
2. BUSINESS_KEY_：流程业务键
3. PROC_DEF_ID_：流程定义ID
4. START_TIME_：开始时间
5. END_TIME_：结束时间
6. DURATION_：持续时间
7. START_USER_ID_：启动人ID
8. START_ACT_ID_：开始活动的ID，一般是流程开始事件的ID，在流程文件中定义。
9. END_ACT_ID_：流程最后一个活动的ID，一般是流程结束事件的ID，在流程文件中定义。
10. SUPER_PROCESS_INSTANCE_ID_
11. DELETE_REASON_：该流程实例被删除的原因
12. TENANT_ID_: 租户ID
13. NAME_: 名称

> ACT_HI_TASKINST，历史任务表，当流程到达某个任务节点时，就会向此表中写入历史人物数据，该表与运行时的任务表类似，该表
> 字段如下所示：

1. PROC_DEF_ID_: 流程定义ID
2. TASK_DEF_KEY_: 流程定义KEY
3. PROC_INST_ID_: 流程实例ID
4. EXECUTION_ID_: 执行实例ID
5. NAME_: 名称
6. PARENT_TASK_ID_: 父节点实例ID
7. DESCRIPTION_: 描述
8. OWNER_: 任务实际拥有人
9. ASSIGNEE_: 任务实际处理人
10. START_TIME_: 开始时间
11. CLAIM_TIME_: 认领时间
12. END_TIME_: 中止时间
13. DURATION_: 耗时
14. DELETE_REASON_: 删除原因
15. PRIORITY_: 优先级
16. DUE_DATE_: 过期时间
17. FORM_KEY_: 节点定义的formKey
18. CATEGORY_: 类别
19. TENANT_ID_: 租户ID

> ACT_HI_VARINST，历史变量表

1. PROC_INST_ID_: 流程实例ID
2. EXECUTION_ID_: 执行实例ID
3. TASK_ID_: 任务实例ID
4. NAME_: 名称
5. VAR_TYPE_: 参数类型
6. REV_: 版本，乐观锁
7. BYTEARRAY_ID_: 字节表ID
8. DOUBLE_: 存储Double类型的值
9. LONG_: 存储Long类型的值
10. TEXT_: 存储文本类型的
11. TEXT2_: JPA对象的ID（此版本已去除JPA依赖）
12. CREATE_TIME_: 创建时间
13. LAST_UPDATED_TIME_: 最后更新时间

> ACT_HI_BUTTON，流程历史按钮表，用于存储任务节点按钮历史

1. NAME_: 按钮名称
2. CODE_: 按钮编码
3. REV_: 按钮版本
4. GROUP_: 按钮分组，参考ACT_RE_BUTTON
5. DISPLAY_EXPRESSION_: 展示表达式，参考ACT_RE_BUTTON
6. ENABLE_: 使用启用，参考ACT_RE_BUTTON
7. DESCRIPTION_: 按钮描述
8. ICON_: 按钮图标
9. TASK_ID_: 任务ID
10. PROC_INSTANCE_ID_: 流程实例ID
11. PROC_DEF_ID_: 流程定义ID
12. OPERATION_ID_: 操作ID
13. CREATE_TIME_: 创建时间
14. LAST_UPDATE_TIME_: 最后更新时间
15. BUTTON_DEF_ID_: 按钮定义ID
16. TENANT_ID_: 租户ID
17. BUTTON_REAL_PARAMS_: 按钮执行真实参数json