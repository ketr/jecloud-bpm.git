/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring;

import com.je.bpm.engine.RepositoryService;
import com.je.bpm.api.runtime.process.impl.APIDeploymentConverter;
import com.je.bpm.model.process.event.impl.ApplicationDeployedEventImpl;
import com.je.bpm.model.process.event.impl.ApplicationDeployedEvents;
import com.je.bpm.model.process.events.ApplicationDeployedEvent;
import com.je.bpm.model.process.model.Deployment;
import com.je.bpm.runtime.process.event.listener.ProcessRuntimeEventListener;
import org.springframework.context.ApplicationEventPublisher;

import java.util.ArrayList;
import java.util.List;

public class ApplicationDeployedEventProducer extends AbstractActivitiSmartLifeCycle {

    private RepositoryService repositoryService;
    private APIDeploymentConverter deploymentConverter;
    private List<ProcessRuntimeEventListener<ApplicationDeployedEvent>> listeners;
    private ApplicationEventPublisher eventPublisher;
    private static final String APPLICATION_DEPLOYMENT_NAME = "SpringAutoDeployment";

    public ApplicationDeployedEventProducer(RepositoryService repositoryService,
                                            APIDeploymentConverter deploymentConverter,
                                            List<ProcessRuntimeEventListener<ApplicationDeployedEvent>> listeners,
                                            ApplicationEventPublisher eventPublisher) {
        this.repositoryService = repositoryService;
        this.deploymentConverter = deploymentConverter;
        this.listeners = listeners;
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void doStart() {
        List<ApplicationDeployedEvent> applicationDeployedEvents = getApplicationDeployedEvents();
        for (ProcessRuntimeEventListener<ApplicationDeployedEvent> listener : listeners) {
            applicationDeployedEvents.forEach(listener::onEvent);
        }
        if (!applicationDeployedEvents.isEmpty()) {
            eventPublisher.publishEvent(new ApplicationDeployedEvents(applicationDeployedEvents));
        }
    }

    private List<ApplicationDeployedEvent> getApplicationDeployedEvents() {
        List<Deployment> deployments = deploymentConverter.from(repositoryService
                .createDeploymentQuery()
                .deploymentName(APPLICATION_DEPLOYMENT_NAME)
                .list());

        List<ApplicationDeployedEvent> applicationDeployedEvents = new ArrayList<>();
        for (Deployment deployment : deployments) {
            ApplicationDeployedEventImpl applicationDeployedEvent = new ApplicationDeployedEventImpl(deployment);
            applicationDeployedEvents.add(applicationDeployedEvent);
        }
        return applicationDeployedEvents;
    }

    @Override
    public void doStop() {
        //nothing
    }
}
