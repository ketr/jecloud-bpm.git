/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.SmartLifecycle;

public abstract class AbstractActivitiSmartLifeCycle implements SmartLifecycle, DisposableBean {

    private static Logger logger = LoggerFactory.getLogger(AbstractActivitiSmartLifeCycle.class);

    private Object lifeCycleMonitor = new Object();
    private boolean autoStartup = true;
    private int phase = DEFAULT_PHASE;
    private volatile boolean running = false;

    public AbstractActivitiSmartLifeCycle() {
    }

    public abstract void doStart();

    public abstract void doStop();

    /**
     * Set whether to auto-start the activation after this component
     * has been initialized and the context has been refreshed.
     * <p>Default is "true". Turn this flag off to defer the endpoint
     * activation until an explicit {@link #start()} call.
     */
    public void setAutoStartup(boolean autoStartup) {
        this.autoStartup = autoStartup;
    }

    /**
     * Return the value for the 'autoStartup' property. If "true", this
     * component will start upon a ContextRefreshedEvent.
     */
    @Override
    public boolean isAutoStartup() {
        return this.autoStartup;
    }

    /**
     * Specify the phase in which this component should be started
     * and stopped. The startup order proceeds from lowest to highest, and
     * the shutdown order is the reverse of that. By default this value is
     * Integer.MAX_VALUE meaning that this component starts as late
     * as possible and stops as soon as possible.
     */
    public void setPhase(int phase) {
        this.phase = phase;
    }

    /**
     * Return the phase in which this component will be started and stopped.
     */
    @Override
    public int getPhase() {
        return this.phase;
    }

    @Override
    public void start() {
        synchronized (this.lifeCycleMonitor) {
            if (!this.running) {
                logger.info("Starting...");

                doStart();

                this.running = true;
                logger.info("Started.");
            }
        }
    }

    @Override
    public void stop() {
        synchronized (this.lifeCycleMonitor) {
            if (this.running) {
                logger.info("Stopping...");
                doStop();

                this.running = false;
                logger.info("Stopped.");
            }
        }
    }

    @Override
    public void stop(Runnable callback) {
        synchronized (this.lifeCycleMonitor) {
            stop();
            callback.run();
        }
    }

    @Override
    public boolean isRunning() {
        return this.running;
    }

    @Override
    public void destroy() {
        stop();
    }

}
