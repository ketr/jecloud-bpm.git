/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/*
 * This copy of Woodstox XML processor is licensed under the
 * Apache (Software) License, version 2.0 ("the License").
 * See the License for details about distribution rights, and the
 * specific rights regarding derivate works.
 *
 * You may obtain a copy of the License at:
 *
 * http://www.apache.org/licenses/
 *
 * A copy is also included in the downloadable source code package
 * containing Woodstox, in file "ASL2.0", under the same directory
 * as this file.
 */
package com.je.bpm.spring.boot.process.validation;

import com.je.bpm.core.model.*;
import com.je.bpm.core.model.event.Event;
import com.je.bpm.core.process.validation.ValidationError;
import com.je.bpm.core.process.validation.validator.Problems;
import com.je.bpm.core.process.validation.validator.ProcessLevelValidator;
import com.je.bpm.core.model.process.Process;
import java.util.List;

public class AsyncPropertyValidator extends ProcessLevelValidator {

    @Override
    protected void executeValidation(BpmnModel bpmnModel, Process process, List<ValidationError> errors) {
        validateFlowElementsInContainer(process, errors, process);
    }

    protected void validateFlowElementsInContainer(FlowElementsContainer container, List<ValidationError> errors, Process process) {
        for (FlowElement flowElement : container.getFlowElements()) {
            if (flowElement instanceof FlowElementsContainer) {
                FlowElementsContainer subProcess = (FlowElementsContainer) flowElement;
                validateFlowElementsInContainer(subProcess, errors, process);
            }

            if ((flowElement instanceof FlowNode) && ((FlowNode) flowElement).isAsynchronous()) {
                addWarning(errors, Problems.FLOW_ELEMENT_ASYNC_NOT_AVAILABLE, process , flowElement, "Async property is not available when asyncExecutor is disabled.");
            }

            if ((flowElement instanceof Event)) {
                ((Event) flowElement).getEventDefinitions().stream().forEach(event -> {
                    if (event instanceof TimerEventDefinition) {
                        addWarning(errors, Problems.EVENT_TIMER_ASYNC_NOT_AVAILABLE, process, flowElement, "Timer event is not available when asyncExecutor is disabled.");
                    } else if ((event instanceof SignalEventDefinition) && ((SignalEventDefinition) event).isAsync() ) {
                        addWarning(errors, Problems.SIGNAL_ASYNC_NOT_AVAILABLE, process, flowElement, "Async property is not available when asyncExecutor is disabled.");
                    }
                });
            }
        }
    }
}
